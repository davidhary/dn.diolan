# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2.0.8126] - 2020-04-01
* Tests pass in project and package reference modes.

## [2.0.8112] - 2020-03-18
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.
* Package.

## [2.0.8070] - 2020-02-04
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Remove unused references. 
* Update build version.
* Display version file when updating build version.

## [2.0.8018] - 2020-12-14
* Breaks compatibility
  * Change Modalities to Subsystems
  * Use Win Controls class as a base class for the rest of the controls.
  * Split off GPIO and GPIO Controls projects from Diolan and Diolan Win Forms.
  * Split off Handler and Handler Controls projects from Diolan and Diolan Win Forms.
  * Rename GPIO Handler Driver to GPIO Handler and Driver + Emulator to Material Handler.

## [1.4.8015] - 2020-12-11
* Supports Net 4.72, 4.8 and 5.0.
* Breaks compatibility by changing the controls namespace to WinControls.

## [1.3.7632] - 2020-11-23
* Converted to C#

## [1.3.7612] - 2020-11-03
* Using C# framework

## [1.3.6667] - 2018-04-03
* 2018 release.

## [1.2.6333] - 2021705-04
* Uses cancel details event arguments instead of referenced details.

## [1.1.6097] - 2016-09-10
* Uses DLN Library 3.4.x. Adds support for No Hau handler.

## [1.1.5941] - 2016-04-07
* Uses DLN Library 3.1.x. Fixes disconnection. Handles the lost connection event. Uses device ID instead of serial numbers as keys.

## [1.0.5654] - 2015-06-25
* Resets emulator before and after the GPIO handler.

## [1.0.5649] - 2015-06-20
* Separates configuration from initialization to prevent GPIO Handler from registering bogus emulator events. Validates configuration.

## [1.0.5647] - 2015-06-18
* Adds a base class for modality connector. Uses the base class for the GPIO Handler Emulator.

## [1.0.5646] - 2015-06-17
* Adds the Material Handler, which integrated a GPIO handler and its emulator class with the console.

## [1.0.5644] - 2015-06-15
* Auto list panels. Change to using active low nomenclature. Improves handling of open server with detached instruments. Publishes known state.

## [1.0.5624] - 2015-05-26
* Created.

\(C\) 2015 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*Library*
*Forms*
*etc.*
```
[2.0.8126]: https://www.bitbucket.org/davidhary/dn.diolan.core

