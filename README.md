# DIOLAN Libraries

Supporting the [DIOLAN](https://www/diolan.com) devices.

* [Runtime Pre-Requisites](#Runtime-Pre-Requisites)
* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Runtime-Pre-Requisites"></a>
## Runtime Pre-Requisites

### DLN-Series Setup for Windows
[DLN Setup] 3.4.0 and above is required for accessing devices.

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [Enums Repo] - .NET Enums library.
* [DLN Repo] - DLN library.  
* [DIOLAN Repo] - DIOLAN library
* [IDE Repo] - IDE support files. 
```
git clone git@bitbucket.org:davidhary/dn.enums.git
git clone git@bitbucket.org:davidhary/dn.dln.git
git clone git@bitbucket.org:davidhary/dn.diolan.git
git clone git@bitbucket.org:davidhary/vs.ide.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%dnlib%\core\enums
%dnlib%\io\diolan
%dnlib%\core\dln
%vslib%\core\ide
```
where %dnlib% and %vslib% are  the root folders of the .NET libraries, e.g., %my%\lib\vs 
and %my%\libraries\vs, respectively, and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

#### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Funduc Search and Replace]
* [DIOLAN](https://www/diolan.com) - DIOLAN

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix] -- we are but a spec on the shoulders of giants  
* [John Simmons] - outlaw programmer  
* [Stack overflow] - Joel Spolsky  
* [.Net Foundation] - The .NET Foundation<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at thefollowing sites:  [Enums Repo]  DLN Repo]  [DIOLAN Repo]  <a name="Closed-software"></a>### Closed software Closed software used by this software are described and licensed onthe following sites:  [Enums Repo]  [DLN Repo]  ### Links[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg[DIOLAN]: https://www/diolan.com  [DLNWARE]: http://www.dlnware.com[DLN Setup]: http://www.dlnware.com/dln.net[Enums Repo]: https://bitbucket.org/davidhary/dn.enums[DLN Repo]: https://bitbucket.org/davidhary/dn.dln [DIOLAN Repo]: https://bitbucket.org/davidhary/dn.diolan IDE Repo]: https://bitbucket.org/davidhary/vs.ide
[Units Amounts]: https://bitbucket.org/davidhary/Arebis.UnitsAmounts
[VI Libraries]: https://www.bitbucket.org/davidhary/dn.vi
[Lua Global Support Libraries]: https://bitbucket.org/davidhary/tsp.core

[IVI Foundation]: https://www.ivifoundation.org
[IVI Foundation]: http://www.ivifoundation.org
[Keysight I/O Suite]: https://www.keysight.com/en/pd-1985909/io-libraries-suite
[NI VISA]: https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html#346210
[Test Script Builder]: https://www.tek.com/keithley-test-script-builder
[Microsoft /.NET Framework]: https://dotnet.microsoft.com/download

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[WiX Repository]: https://www.bitbucket.org/davidhary/vs.wix

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
[.Net Foundation]: https://source.dot.net




