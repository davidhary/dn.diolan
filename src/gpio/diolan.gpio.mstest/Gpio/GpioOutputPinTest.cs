using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using Dln.Gpio;

using isr.Diolan.Gpio;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Diolan.Gpio.MSTest
{

    /// <summary>
    /// This is a test class for GpioOutputPinTest and is intended to contain all GpioOutputPinTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    [TestClass()]
    public class GpioOutputPinTest
    {

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        private Pin _Input_Pin;


        /// <summary>   Gets or sets the input pin. </summary>
        /// <value> The input pin. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private Pin Input_Pin
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._Input_Pin;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._Input_Pin != null )
                {
                    this._Input_Pin.ConditionMetThreadSafe -= this.InputPin_ConditionMetThreadSafe;
                }

                this._Input_Pin = value;
                if ( this._Input_Pin != null )
                {
                    this._Input_Pin.ConditionMetThreadSafe += this.InputPin_ConditionMetThreadSafe;
                }
            }
        }

        /// <summary> Input pin condition met thread safe. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Condition met event information. </param>
        private void InputPin_ConditionMetThreadSafe( object sender, ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                Debug.WriteLine( $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}" );
            }
        }

        /// <summary> A test for BitValue. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        [TestMethod()]
        public void BitValueTest()
        {
            var serverConnector = LocalhostConnector.SingleInstance();
            serverConnector.Connect();
            if ( serverConnector.IsConnected )
            {
                using var deviceConnector = new DeviceConnector( serverConnector );
                var (success, details) = deviceConnector.TryOpenDevice( DeviceInfo.DefaultDeviceId, SubsystemTypes.None );
                // TO_DO: Add selection of device id.
                if ( success )
                {
                    var pins = deviceConnector.Device.Gpio.Pins;
                    int pinNumber = 0;
                    var target = new GpioOutputPin( pins, pinNumber, ActiveLogic.ActiveLow );
                    byte expected = 0;
                    byte actual;
                    target.BitValue = expected;
                    actual = target.BitValue;
                    Assert.AreEqual( expected, actual );
                    deviceConnector.CloseDevice( SubsystemTypes.Gpio );
                }
                else
                {
                    Assert.Fail( details );
                }
            }
        }

        /// <summary> Tests start. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="outputPinNumber"> The output pin number. </param>
        /// <param name="inputPinNumber">  The input pin number. </param>
        /// <param name="activeLogic">     The active logic. </param>
        /// <param name="logicalValue">    The logical value. </param>
        public static void StartTestTest( int outputPinNumber, int inputPinNumber, ActiveLogic activeLogic, byte logicalValue )
        {
            var serverConnector = LocalhostConnector.SingleInstance();
            serverConnector.Connect();
            if ( serverConnector.IsConnected )
            {
                using var deviceConnector = new DeviceConnector( serverConnector );
                var (success, details) = deviceConnector.TryOpenDevice( DeviceInfo.DefaultDeviceId, SubsystemTypes.None );
                if ( success )
                {
                    var pins = deviceConnector.Device.Gpio.Pins;
                    var outputPin = new GpioOutputPin( pins, outputPinNumber, activeLogic );
                    var inputPin = new GpioInputPin( pins, inputPinNumber, activeLogic );
                    byte expected = ( byte ) (1 & logicalValue);
                    byte actual;
                    outputPin.LogicalValue = logicalValue;
                    actual = inputPin.LogicalValue;
                    Assert.AreEqual( expected, actual );
                    deviceConnector.CloseDevice( SubsystemTypes.Gpio );
                }
                else
                {
                    Assert.Fail( details );
                }
            }
        }

        /// <summary> A test for start test off. Output from pin 4 to pin 0. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        [TestMethod()]
        public void StartTestOff()
        {
            StartTestTest( 4,0, ActiveLogic.ActiveLow, 0 );
        }

        /// <summary> A test for start test on. Output from pin 4 to pin 0. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        [TestMethod()]
        public void StartTestOn()
        {
            StartTestTest( 4, 0, ActiveLogic.ActiveLow, 1 );
        }
    }
}
