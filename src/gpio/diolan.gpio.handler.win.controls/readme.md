# About

isr.Diolan.Gpio.Hander.WinControls is a .Net library supporting Windows Controls for the DIOLAN GPIO handler interface.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Diolan.Gpio.Hander.WinControls is released as open source under the MIT license.
Bug reports and contributions are welcome at the [DIOLAN Repository].

[DIOLAN Repository]: https://bitbucket.org/davidhary/dn.diolan

