using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using FastEnums;
using isr.Diolan.WinControls.ErrorProviderExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Gpio.Handler.WinControls
{

    /// <summary> User interface for managing a GPIO Handler interface. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    public partial class GpioHandlerView
    {

        #region " CONSTRUCTION "

        /// <summary> Executes the custom dispose action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to disposing. </param>
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.OnSubsystemTypeClosed();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " SUBSYSTEM TYPE "

        /// <summary> the subsystem type. </summary>
        /// <value> the subsystem type. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SubsystemTypes SubsystemType { get; set; } = SubsystemTypes.Gpio;

        /// <summary> Executes the subsystem type closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeClosed()
        {
            if ( this.GpioHandlerEmulatorInternal is object )
            {
                this.GpioHandlerEmulatorInternal.Dispose();
                this.GpioHandlerEmulatorInternal = null;
            }

            if ( this.GpioHandlerInternal is object )
            {
                this.GpioHandlerInternal.Dispose();
                this.GpioHandlerInternal = null;
            }

            this.Device = null;
            this.OnSubsystemTypeConnectionChanged();
        }

        /// <summary> Executes the subsystem type closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnSubsystemTypeClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsSubsystemTypeOpen() )
                {
                    // un-register event handlers for all pins
                    foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                        pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                }
            }
        }

        /// <summary> Executes the subsystem type opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeOpening()
        {
            this.GpioHandlerInternal = null;
            this.Device = this.DeviceConnectorInternal.Device;
            this._ErrorProvider.Clear();

            // Get port count
            if ( this.Device.Gpio.Pins.Count == 0 )
            {
                // this is already done when opening the device.
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, "Adapter '{0}' doesn't support GPIO interface.", this.Device.Caption() );
                this.Device = null;
                this._DeviceInfoTextBox.Text = "not supported";
            }
            else
            {
                this._DeviceInfoTextBox.Text = this.Device.Caption();

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handler for all pins
                foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                    pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                this.GpioHandlerInternal = new GpioHandler();
            }
        }

        /// <summary> Queries if a Subsystem Type is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
        private bool IsSubsystemTypeOpen()
        {
            return this.GpioHandlerInternal is object;
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Gpio.ConditionMetEventArgs e )
        {
            this.AppendEvent( e );
        }

        /// <summary> Appends an event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Condition met event information. </param>
        private void AppendEvent( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                string data = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}";
                // This event is handled in main thread,
                // so it is not needed to invoke when modifying form's controls.
                this._EventLogTextBox.AppendText( data );
            }
        }

        #endregion

        #region " GPIO HANDLER "

        private GpioHandler _GpioHandlerInternal;

        /// <summary>   Gets or sets the gpio handler internal. </summary>
        /// <value> The gpio handler internal. </value>
        private GpioHandler GpioHandlerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._GpioHandlerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._GpioHandlerInternal != null )
                {
                    this._GpioHandlerInternal.PropertyChanged -= this.HandleGpioHandlerPropertyChanged;
                }

                this._GpioHandlerInternal = value;
                if ( this._GpioHandlerInternal != null )
                {
                    this._GpioHandlerInternal.PropertyChanged += this.HandleGpioHandlerPropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the Gpio Handler. </summary>
        /// <value> The Gpio Handler. </value>
        [CLSCompliant( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandler GpioHandler
        {
            get => this.GpioHandlerInternal;

            set {
                this.GpioHandlerInternal = value;
                if ( this.GpioHandlerInternal is object )
                {
                    this._StartTestPinNumberNumeric.Value = this.GpioHandlerInternal.StartTestPinNumber;
                    this._EndTestPinNumberNumeric.Value = this.GpioHandlerInternal.EndTestPinNumber;
                    this._ActiveLowLogicRadioButton.Checked = this.GpioHandlerInternal.ActiveLogic == ActiveLogic.ActiveLow;
                    this.BinPortMask = this.GpioHandlerInternal.BinMask;
                }
            }
        }

        /// <summary> Gets or sets the handler mask. </summary>
        /// <value> The entered mask. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private long BinPortMask
        {
            get => Convert.ToInt32( this._BinPortMaskTextBox.Text, 2 );

            set => this._BinPortMaskTextBox.Text = Convert.ToString( value, 2 );
        }

        /// <summary> Bin port mask text box validating. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void BinPortMaskTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.BinPortMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                    e.Cancel = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
                e.Cancel = true;
            }
        }

        /// <summary> Handles the Configure Emulator button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureHandlerInterfaceButton_Click( object sender, EventArgs e )
        {
            var ( success, details ) = ( true, string.Empty );
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    if ( !this.IsDeviceSubsystemTypeOpen )
                    {
                        (success, details) = (false, "Device not open");
                    }
                    else if ( this.Device.Gpio is null )
                    {
                        (success, details) = (false, $"Gpio not supported on {this.Device.Caption()}" );
                    }
                    else if ( this.Device.Gpio.Pins is null )
                    {
                        (success, details) = (false, $"Gpio not supported on {this.Device.Caption()}" );
                    }
                    else
                    {
                        if ( this.GpioHandlerInternal is null )
                        {
                            this.GpioHandlerInternal = new GpioHandler();
                        }

                        this.GpioHandlerInternal.StartTestPinNumber = ( int ) this._StartTestPinNumberNumeric.Value;
                        this.GpioHandlerInternal.EndTestPinNumber = ( int ) this._EndTestPinNumberNumeric.Value;
                        this.GpioHandlerInternal.ActiveLogic = this._ActiveLowLogicRadioButton.Checked ? ActiveLogic.ActiveLow : ActiveLogic.ActiveHigh;
                        this.GpioHandlerInternal.BinEndTestOnsetDelay = TimeSpan.FromMilliseconds( ( double ) this._EndTestDelayNumeric.Value );
                        this.GpioHandlerInternal.BinMask = this.BinPortMask;
                        (success, details) = this.GpioHandlerInternal.TryValidate( this.Device.Gpio.Pins );
                        if ( success )
                            this.GpioHandlerInternal.Configure( this.Device.Gpio.Pins );
                    }
                }
                else
                {
                    (success, details) = (false, "Device not open");
                }
            }
            catch ( Exception ex )
            {
                (success, details) = (false, ex.ToString() );
            }
            finally
            {
                if ( !success )
                    _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Updates the sot indicator described by sender. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void UpdateSotIndicator( GpioHandlerBase sender )
        {
            if ( sender is object )
            {
                this._SotLabel.Image = sender.StartTestLogicalValue ? isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_available : isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_invisible;
            }
        }

        /// <summary> EOT label click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EotLabel_Click( object sender, EventArgs e )
        {
            this.UpdateEotIndicator( this.GpioHandlerInternal );
        }

        /// <summary> Updates the EOT indicator described by sender. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void UpdateEotIndicator( GpioHandlerBase sender )
        {
            if ( sender is object )
            {
                this._EotLabel.Image = sender.EndTestLogicalValue ? isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_available : isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_invisible;
            }
        }

        /// <summary> Handles the GPIO Handler property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandler sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( GpioHandlerBase.State ):
                    {
                        this._HandlerStateLabel.Text = sender.State.Description();
                        switch ( sender.State )
                        {
                            case HandlerState.StartTestReceived:
                                {
                                    break;
                                }

                            default:
                                {
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( GpioHandlerBase.BinValue ):
                    {
                        this._HandlerInterfaceBinValueLabel.Text = sender.BinValueCaption( "..." );
                        break;
                    }

                case nameof( GpioHandlerBase.StartTestLogicalValue ):
                    {
                        this.UpdateSotIndicator( sender );
                        break;
                    }

                case nameof( GpioHandlerBase.EndTestLogicalValue ):
                    {
                        this.UpdateEotIndicator( sender );
                        break;
                    }

                case nameof( GpioHandlerBase.BinMask ):
                    {
                        this.BinPortMask = sender.BinMask;
                        break;
                    }

                case nameof( GpioHandlerBase.BinEndTestOnsetDelay ):
                    {
                        this._EndTestDelayNumeric.Value = ( decimal ) this.GpioHandlerInternal.BinEndTestOnsetDelay.TotalMilliseconds;
                        break;
                    }

                case nameof( GpioHandlerBase.StartTestPinNumber ):
                    {
                        this._StartTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerBase.EndTestPinNumber ):
                    {
                        this._EndTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerBase.ActiveLogic ):
                    {
                        this._ActiveLowLogicRadioButton.Checked = sender.ActiveLogic == ActiveLogic.ActiveLow;
                        break;
                    }
            }
        }

        /// <summary> Handles the GPIO Handler property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandleGpioHandlerPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleGpioHandlerPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as GpioHandler, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                details = $"Exception handling {nameof(GpioHandlerView)}.{nameof(GpioHandlerView.GpioHandler)}.{e?.PropertyName} property change;.  {ex}";
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, details );
            }
        }


        #endregion

        #region " GPIO HANDLER EMULATOR "

        private GpioHandlerEmulator _GpioHandlerEmulator;

        /// <summary>   Gets or sets the gpio handler emulator internal. </summary>
        /// <value> The gpio handler emulator internal. </value>
        private GpioHandlerEmulator GpioHandlerEmulatorInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._GpioHandlerEmulator;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._GpioHandlerEmulator != null )
                {
                    this._GpioHandlerEmulator.PropertyChanged -= this.HandleGpioHandlerEmulatorPropertyChanged;
                }

                this._GpioHandlerEmulator = value;
                if ( this._GpioHandlerEmulator != null )
                {
                    this._GpioHandlerEmulator.PropertyChanged += this.HandleGpioHandlerEmulatorPropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the GPIO Handler Emulator. </summary>
        /// <value> The GPIO Handler Emulator. </value>
        [CLSCompliant( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandlerEmulator GpioHandlerEmulator
        {
            get => this.GpioHandlerEmulatorInternal;

            set {
                this.GpioHandlerEmulatorInternal = value;
                if ( this.GpioHandlerEmulator is object )
                {
                    this._EmulatorStartTestPinNumberNumeric.Value = this.GpioHandlerEmulator.StartTestPinNumber;
                    this._EmulatorEndTestPinNumberNumeric.Value = this.GpioHandlerEmulator.EndTestPinNumber;
                    this.EmulatorBinMask = this.GpioHandlerInternal.BinMask;
                }
            }
        }

        /// <summary> Gets or sets the entered bin mask. </summary>
        /// <value> The entered mask. </value>
        private long EmulatorBinMask
        {
            get => Convert.ToInt32( this._EmulatorBinPortMaskTextBox.Text, 2 );

            set => this._EmulatorBinPortMaskTextBox.Text = Convert.ToString( value, 2 );
        }

        /// <summary> Emulator Bin port mask text box validating. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void EmulatorBinPortMaskTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.EmulatorBinMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                    e.Cancel = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
                e.Cancel = true;
            }
        }

        /// <summary> Applies the button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureEmulatorButton_Click( object sender, EventArgs e )
        {
            var (success, details) = (true, string.Empty);
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    (success, details) = (false, "Sender is empty");
                }
                else if ( this.IsDeviceSubsystemTypeOpen )
                {
                    if ( !this.IsDeviceSubsystemTypeOpen )
                    {
                        (success, details) = (false, "Device not open");
                    }
                    else if ( this.Device.Gpio is null )
                    {
                        (success, details) = (false, $"Gpio not supported on {this.Device.Caption()}" );
                    }
                    else if ( this.Device.Gpio.Pins is null )
                    {
                        (success, details) = (false, $"Gpio not supported on {this.Device.Caption()}" );
                    }
                    else
                    {
                        if ( this.GpioHandlerEmulatorInternal is null )
                        {
                            this.GpioHandlerEmulatorInternal = new GpioHandlerEmulator();
                        }

                        this.GpioHandlerEmulatorInternal.StartTestPinNumber = ( int ) this._EmulatorStartTestPinNumberNumeric.Value;
                        this.GpioHandlerEmulatorInternal.EndTestPinNumber = ( int ) this._EmulatorEndTestPinNumberNumeric.Value;
                        this.GpioHandlerEmulatorInternal.ActiveLogic = this._ActiveLowLogicRadioButton.Checked ? ActiveLogic.ActiveLow : ActiveLogic.ActiveHigh;
                        this.GpioHandlerEmulatorInternal.BinMask = this.EmulatorBinMask;
                        (success, details) = this.GpioHandlerEmulatorInternal.TryValidate( this.Device.Gpio.Pins );
                        if ( success )
                            this.GpioHandlerEmulatorInternal.Configure( this.Device.Gpio.Pins );
                    }
                }
                else
                {
                    (success, details) = (false, "Device not open");
                }
            }
            catch ( Exception ex )
            {
                (success, details) = (false, ex.ToString() );
            }
            finally
            {
                if ( !success )
                    _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Handles the emulator property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandlerEmulator sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( GpioHandlerBase.State ):
                    {
                        this._HandlerStateLabel.Text = sender.State.Description();
                        switch ( sender.State )
                        {
                            case HandlerState.StartTestReceived:
                                {
                                    this._EmulatorStateLabel.Text = "e: Start test sent";
                                    break;
                                }

                            case HandlerState.EndTestSent:
                                {
                                    this._EmulatorStateLabel.Text = "e: End test received";
                                    break;
                                }

                            default:
                                {
                                    this._EmulatorStateLabel.Text = sender.State.Description();
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( GpioHandlerBase.BinValue ):
                    {
                        this._EmulatorBinValueLabel.Text = sender.BinValueCaption( "..." );
                        break;
                    }

                case nameof( GpioHandlerBase.StartTestLogicalValue ):
                    {
                        break;
                    }

                case nameof( GpioHandlerBase.EndTestLogicalValue ):
                    {
                        break;
                    }

                case nameof( GpioHandlerBase.BinMask ):
                    {
                        this.EmulatorBinMask = sender.BinMask;
                        break;
                    }

                case nameof( GpioHandlerBase.BinEndTestOnsetDelay ):
                    {
                        break;
                    }

                case nameof( GpioHandlerBase.StartTestPinNumber ):
                    {
                        this._EmulatorStartTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerBase.EndTestPinNumber ):
                    {
                        this._EmulatorEndTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerBase.ActiveLogic ):
                    {
                        break;
                    }
                    // TO_DO: Must be the same as the GPIO Handler.
            }
        }

        /// <summary> handles the GPIO Handler Emulator property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandleGpioHandlerEmulatorPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleGpioHandlerEmulatorPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as GpioHandlerEmulator, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                details = $"Exception handling {nameof( GpioHandlerView)}.{nameof( GpioHandlerView.GpioHandlerEmulator)}.{e?.PropertyName} change;. {ex}";
            }
            finally
            {
                if ( !string.IsNullOrEmpty( details ) )
                    _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, details );
            }
        }


        #endregion

        #region " PLAY "

        /// <summary> Clears the start button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ClearStartButton_Click( object sender, EventArgs e )
        {
            string details = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( this.GpioHandlerInternal is null )
                {
                    details = "Handler interface not set";
                }
                else if ( this.GpioHandlerInternal.State == HandlerState.Idle || this.GpioHandlerInternal.State == HandlerState.EndTestAcknowledged )
                {
                    this.GpioHandlerInternal.EnableStartTest();
                }
                else
                {
                    details = $"Invalid handler state @'{this.GpioHandlerInternal.State}'; state should be @'{HandlerState.Idle}'";
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Starts test button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void StartTestButton_Click( object sender, EventArgs e )
        {
            string details = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( this.GpioHandlerInternal is null )
                {
                    details = "Handler interface not set";
                }
                else if ( this.GpioHandlerEmulatorInternal is null )
                {
                    details = "Handler emulator not set";
                }
                else if ( this.GpioHandlerInternal.State != HandlerState.StartTestEnabled )
                {
                    details = $"Invalid handler state @'{this.GpioHandlerInternal.State}'; state should be @'{HandlerState.StartTestEnabled}'";
                }
                else
                {
                    this.GpioHandlerEmulatorInternal.OutputStartTest();
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Resets the button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ResetButton_Click( object sender, EventArgs e )
        {
            string details = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( this.GpioHandlerInternal is null )
                {
                    details = "Handler interface not set";
                }
                else
                {
                    this.GpioHandlerInternal.InitializeKnownState();
                    if ( this.GpioHandlerEmulatorInternal is object )
                    {
                        this.GpioHandlerEmulatorInternal.Dispose();
                        this.GpioHandlerEmulatorInternal = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Outputs the end test signals. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">   The sender. </param>
        /// <param name="binValue"> The bin value. </param>
        private void OutputEndTest( object sender, int binValue )
        {
            string details = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( this.GpioHandlerInternal is null )
                {
                    details = "Handler interface not set";
                }
                else if ( this.GpioHandlerInternal.State == HandlerState.StartTestReceived )
                {
                    this.GpioHandlerInternal.OutputEndTest( binValue );
                }
                else
                {
                    details = $"Invalid handler state @'{this.GpioHandlerInternal.State}'; state should be @'{HandlerState.Idle}'";
                    this.GpioHandlerInternal.InitializeKnownState();
                    if ( this.GpioHandlerEmulatorInternal is object )
                    {
                        this.GpioHandlerEmulatorInternal.InitializeKnownState();
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Output pass bin button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OutputPassBinButton_Click( object sender, EventArgs e )
        {
            this.OutputEndTest( sender, ( int ) this._PassBinValueNumeric.Value );
        }

        /// <summary> Output fail bin button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OutputFailBinButton_Click( object sender, EventArgs e )
        {
            this.OutputEndTest( sender, ( int ) this._FailBinValueNumeric.Value );
        }

        #endregion

        #region " HANDLER SELECTION "

        /// <summary> Handler combo box click event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void HandlerComboBox_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            try
            {
                this._ErrorProvider.Clear();
                SupportedHandler handler = ( SupportedHandler ) ( int )( this._HandlerComboBox.ComboBox.SelectedValue );
                switch ( handler )
                {
                    case SupportedHandler.Aetrium:
                        {
                            this.GpioHandler.ApplyHandlerInfo( HandlerInfo.AetriumHandlerInfo );
                            this.GpioHandlerEmulator.ApplyHandlerInfo( HandlerInfo.AetriumEmulatorInfo );
                            break;
                        }

                    case SupportedHandler.NoHau:
                        {
                            this.GpioHandler.ApplyHandlerInfo( HandlerInfo.NoHauHandlerInfo );
                            this.GpioHandlerEmulator.ApplyHandlerInfo( HandlerInfo.NoHauHandlerInfo );
                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
            }
        }

        #endregion

    }
}
