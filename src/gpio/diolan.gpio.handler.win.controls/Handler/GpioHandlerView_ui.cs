using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using FastEnums;

using isr.Diolan.WinControls.ErrorProviderExtensions;
using isr.Diolan.WinControls.SubsystemExtensions;

namespace isr.Diolan.Gpio.Handler.WinControls
{
    public partial class GpioHandlerView : System.Windows.Forms.UserControl, INotifyPropertyChanged
    {

        private bool InitializingComponents { get; set; }

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioHandlerView()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._TabComboBox.ComboBox.DataSource = this._Tabs.TabPages;
            this._TabComboBox.ComboBox.DisplayMember = "Text";
            this._HandlerComboBox.ComboBox.DataSource = null;
            this._HandlerComboBox.ComboBox.Items.Clear();
            this._HandlerComboBox.ComboBox.DataSource = typeof( Gpio.Handler.SupportedHandler ).ValueDescriptionPairs();
            this._HandlerComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._HandlerComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._HandlerComboBox.ComboBox.Enabled = true;
            this.InitializingComponents = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                if ( !this.IsDisposed && disposing )
                {
                    this.components?.Dispose();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        private void OnDispose( bool disposing )
        {
            this.OnCustomDispose( disposing );
            if ( !this.IsDisposed && disposing )
            {
                if ( this.Device is object )
                    this.Device = null;
                if ( this.DeviceConnectorInternal is object )
                {
                    this.DeviceConnectorInternal.Dispose();
                    this.DeviceConnectorInternal = null;
                }

                if ( this.ServerConnectorThis is object )
                    this.ServerConnectorThis = null;
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " KNOWN STATE "

        /// <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public void InitializeKnownState()
        {
            this.OnServerConnectionChanged();
        }

        #endregion

        #region " SERVER "

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnPropertyChanged( LocalhostConnector sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( e is object )
                {
                    switch ( e.PropertyName ?? "" )
                    {
                        case nameof( LocalhostConnector.IsConnected ):
                            {
                                this.OnServerConnectionChanged();
                                break;
                            }

                        case nameof( LocalhostConnector.AttachedDevicesCount ):
                            {
                                this.OnServerAttachmentChanged();
                                break;
                            }
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, details );
            }
        }

        /// <summary> Server connector property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void ServerConnector_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ServerConnector_PropertyChanged ), new object[] { sender, e } );
            }

            this.OnPropertyChanged( sender as LocalhostConnector, e );
        }

        private LocalhostConnector _ServerConnectorThis;

        /// <summary>   Gets or sets the private server connector. </summary>
        /// <value> The private server connector. </value>
        private LocalhostConnector ServerConnectorThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ServerConnectorThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ServerConnectorThis != null )
                {
                    this._ServerConnectorThis.PropertyChanged -= this.ServerConnector_PropertyChanged;
                }

                this._ServerConnectorThis = value;
                if ( this._ServerConnectorThis != null )
                {
                    this._ServerConnectorThis.PropertyChanged += this.ServerConnector_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LocalhostConnector ServerConnector
        {
            get => this.ServerConnectorThis;

            set => this.ServerConnectorThis = value;
        }

        /// <summary> Connects a server button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConnectServerButton_Click( object sender, EventArgs e )
        {
            if ( this.ServerConnector.IsConnected )
            {
                if ( !this.ServerConnector.HasAttachedDevices() )
                {
                    this.ServerConnector.Disconnect();
                }
            }
            else
            {
                this.ServerConnector.Connect();
            }
        }

        /// <summary> Executes the server connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnServerConnectionChanged()
        {
            if ( this.ServerConnector.IsConnected )
            {
                _ = this.ServerConnector.Connection.ListDevicesById( this._DevicesComboBox );
                this._DevicesComboBox.SelectedIndex = 0;
            }

            this._OpenDeviceSubsystemTypeButton.Enabled = this.ServerConnector.IsConnected;
            this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            this.OnServerAttachmentChanged();
        }

        /// <summary> Executes the server attachment changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OnServerAttachmentChanged()
        {
            this._ConnectServerButton.Image = isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.WIFI_open_22;
            if ( this.ServerConnector.IsConnected )
            {
                this._ConnectServerButton.Text = this.ServerConnector.AttachedDevicesCount().ToString();
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
                this._ConnectServerButton.Text = "X";
            }
        }

        #endregion

        #region " DEVICE CONNECTOR "

        private DeviceConnector _DeviceConnectorInternal;

        /// <summary>   Gets or sets the device connector internal. </summary>
        /// <value> The device connector internal. </value>
        private DeviceConnector DeviceConnectorInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceConnectorInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceConnectorInternal != null )
                {

                    this._DeviceConnectorInternal.DeviceClosed -= this.DeviceConnector_DeviceClosed;

                    this._DeviceConnectorInternal.DeviceClosing -= this.DeviceConnector_DeviceClosing;

                    this._DeviceConnectorInternal.DeviceOpened -= this.DeviceConnector_DeviceOpened;
                }

                this._DeviceConnectorInternal = value;
                if ( this._DeviceConnectorInternal != null )
                {
                    this._DeviceConnectorInternal.DeviceClosed += this.DeviceConnector_DeviceClosed;
                    this._DeviceConnectorInternal.DeviceClosing += this.DeviceConnector_DeviceClosing;
                    this._DeviceConnectorInternal.DeviceOpened += this.DeviceConnector_DeviceOpened;
                }
            }
        }

        /// <summary> Gets or sets the device connector. </summary>
        /// <value> The device connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DeviceConnector DeviceConnector
        {
            get => this.DeviceConnectorInternal;

            set => this.DeviceConnectorInternal = value;
        }

        /// <summary> Device connector device closed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceClosed( object sender, EventArgs e )
        {
            this.OnServerAttachmentChanged();
            this._DeviceInfoTextBox.Text = "closed";
            this.OnSubsystemTypeClosed();
        }

        /// <summary> Device connector device closing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void DeviceConnector_DeviceClosing( object sender, CancelEventArgs e )
        {
            this.OnSubsystemTypeClosing( e );
        }

        /// <summary> Device connector device opened. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceOpened( object sender, EventArgs e )
        {
            this.OnServerAttachmentChanged();
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The device. </value>
        private Dln.Device Device { get; set; }

        /// <summary> Selected device information. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A DeviceInfo. </returns>
        private DeviceInfo SelectedDeviceInfo()
        {
            return string.IsNullOrWhiteSpace( this._DevicesComboBox.Text )
                ? throw new InvalidOperationException( "No devices selected" )
                : new DeviceInfo( this._DevicesComboBox.Text );
        }

        /// <summary> Opens the device for the subsystem type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <param name="sender">   The sender. </param>
        private void OpenDeviceSubsystemType( long deviceId, Control sender )
        {
            if ( this.ServerConnector.IsConnected )
            {
                this._OpenDeviceSubsystemTypeButton.Enabled = this.ServerConnector.IsConnected;
                this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            }
            else
            {
                this.ServerConnector.Connect();
            }

            this.DeviceConnectorInternal = new DeviceConnector( this.ServerConnectorThis );

            // Open device
            var (success, details) = this.DeviceConnectorInternal.TryOpenDevice( deviceId, this.SubsystemType );
            if ( success )
            {
                this.OnSubsystemTypeOpening();
            }
            else
            {
                if ( sender is object )
                    _ = this._ErrorProvider.Annunciate( sender, details );
                this._DeviceInfoTextBox.Text = "No devices";
            }
        }

        /// <summary> Closes device Subsystem Type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void CloseDeviceSubsystemType()
        {
            this.DeviceConnectorInternal.CloseDevice( this.SubsystemType );
            var e = new CancelEventArgs();
            this.OnSubsystemTypeClosing( e );
            if ( !e.Cancel )
            {
                this.OnSubsystemTypeClosed();
            }
        }

        /// <summary> Opens device button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OpenDeviceSubsystemTypeButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    this.CloseDeviceSubsystemType();
                }
                else
                {
                    long deviceId = DeviceInfo.DefaultDeviceId;
                    if ( !string.IsNullOrWhiteSpace( this._DevicesComboBox.Text ) )
                    {
                        deviceId = this.SelectedDeviceInfo().Id;
                    }

                    this.OpenDeviceSubsystemType( deviceId, sender as Control );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
                this.OnSubsystemTypeConnectionChanged();
            }
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        private bool IsDeviceSubsystemTypeOpen => this.Device is object && this.IsSubsystemTypeOpen();

        /// <summary> Executes the subsystem type connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeConnectionChanged()
        {
            if ( this.IsDeviceSubsystemTypeOpen )
            {
                this._OpenDeviceSubsystemTypeButton.Image = isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_online_2;
                this._OpenDeviceSubsystemTypeButton.Text = "Close";
            }
            else
            {
                this._OpenDeviceSubsystemTypeButton.Image = isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_offline_2;
                this._OpenDeviceSubsystemTypeButton.Text = "Open";
            }
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Event log text box double click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EventLogTextBox_DoubleClick( object sender, EventArgs e )
        {
            this._EventLogTextBox.Clear();
        }

        #endregion

        #region " TABS "

        /// <summary> Selects the tab. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void TabComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._TabComboBox.SelectedIndex >= 0 && this._TabComboBox.SelectedIndex < this._Tabs.TabCount )
            {
                this._Tabs.SelectTab( this._Tabs.TabPages[this._TabComboBox.SelectedIndex] );
            }
        }

        #endregion

    }
}
