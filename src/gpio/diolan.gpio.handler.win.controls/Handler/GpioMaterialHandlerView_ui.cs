using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.Diolan.WinControls.ErrorProviderExtensions;
using isr.Diolan.WinControls.SubsystemExtensions;

namespace isr.Diolan.Gpio.Handler.WinControls
{
    public partial class GpioMaterialHandlerView : System.Windows.Forms.UserControl, INotifyPropertyChanged
    {

        #region " CONSTRUCTION "
        private bool InitializingComponents { get; set; }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioMaterialHandlerView()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._TabComboBox.ComboBox.DataSource = this._Tabs.TabPages;
            this._TabComboBox.ComboBox.DisplayMember = "Text";
            this.InitializeSubsystemType();
            this.InitializingComponents = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                if ( !this.IsDisposed && disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Executes the dispose action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to disposing. </param>
        private void OnDispose( bool disposing )
        {
            this.OnCustomDispose( disposing );
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " KNOWN STATE "

        /// <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public void InitializeKnownState()
        {
            this.OnServerConnectionChanged();
        }

        #endregion

        #region " SERVER "

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( LocalhostConnector sender, string propertyName )
        {
            string details = string.Empty;
            try
            {
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( !string.IsNullOrWhiteSpace( propertyName ) )
                {
                    switch ( propertyName ?? "" )
                    {
                        case nameof( LocalhostConnector.IsConnected ):
                            {
                                this.OnServerConnectionChanged();
                                break;
                            }

                        case nameof( LocalhostConnector.AttachedDevicesCount ):
                            {
                                this.OnServerAttachmentChanged();
                                break;
                            }
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, details );
            }
        }

        /// <summary> Connects a server button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConnectServerButton_Click( object sender, EventArgs e )
        {
            if ( this.GpioMaterialHandler.ServerConnector.IsConnected )
            {
                if ( !this.GpioMaterialHandler.ServerConnector.HasAttachedDevices() )
                {
                    this.GpioMaterialHandler.ServerConnector.Disconnect();
                }
            }
            else
            {
                this.GpioMaterialHandler.ServerConnector.Connect();
            }
        }

        /// <summary> Executes the server connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnServerConnectionChanged()
        {
            if ( this.GpioMaterialHandler.ServerConnector.IsConnected )
            {
                _ = this.GpioMaterialHandler.ServerConnector.Connection.ListDevicesById( this._DevicesComboBox );
                this._DevicesComboBox.SelectedIndex = 0;
            }

            this._OpenDeviceSubsystemTypeButton.Enabled = this.GpioMaterialHandler.ServerConnector.IsConnected;
            this._SelectDeviceSplitButton.Enabled = this.GpioMaterialHandler.ServerConnector.IsConnected;
            this.OnServerAttachmentChanged();
        }

        /// <summary> Executes the server attachment changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OnServerAttachmentChanged()
        {
            this._ConnectServerButton.Image = isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.WIFI_open_22;
            if ( this.GpioMaterialHandler.ServerConnector.IsConnected )
            {
                this._ConnectServerButton.Text = this.GpioMaterialHandler.ServerConnector.AttachedDevicesCount().ToString();
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
                this._ConnectServerButton.Text = "X";
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Selected device information. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A DeviceInfo. </returns>
        private DeviceInfo SelectedDeviceInfo()
        {
            return string.IsNullOrWhiteSpace( this._DevicesComboBox.Text )
                ? throw new InvalidOperationException( "No devices selected" )
                : new DeviceInfo( this._DevicesComboBox.Text );
        }

        /// <summary> Opens device button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OpenDeviceSubsystemTypeButton_Click( object sender, EventArgs e )
        {
            var ( success, details ) = ( true, string.Empty );
            try
            {
                this._ErrorProvider.Clear();
                if ( this.GpioMaterialHandler.IsDeviceSubsystemTypeOpen )
                {
                    this.GpioMaterialHandler.CloseDeviceSubsystemType();
                }
                else
                {
                    ( success, details ) = this.GpioMaterialHandler.TryOpenDeviceSubsystemType( this.SelectedDeviceInfo().Id );
                }
            }
            catch ( Exception ex )
            {
                (success, details) = (false, ex.ToString() );
            }
            finally
            {
                if ( !success )
                    _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Event log text box double click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EventLogTextBox_DoubleClick( object sender, EventArgs e )
        {
            this._EventLogTextBox.Clear();
        }

        #endregion

        #region " TABS "

        /// <summary> Selects the tab. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void TabComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._TabComboBox.SelectedIndex >= 0 && this._TabComboBox.SelectedIndex < this._Tabs.TabCount )
            {
                this._Tabs.SelectTab( this._Tabs.TabPages[this._TabComboBox.SelectedIndex] );
            }
        }

        #endregion


    }
}
