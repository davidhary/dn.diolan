using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using FastEnums;
using isr.Diolan.WinControls.ErrorProviderExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Gpio.Handler.WinControls
{

    /// <summary> User interface for managing a GPIO Handler with an and emulator. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    public partial class GpioMaterialHandlerView
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes the subsystem type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void InitializeSubsystemType()
        {
            this.GpioMaterialHandlerInternal = GpioMaterialHandler.Get();

            // enabled once the material handler is assigned.
            this._HandlerComboBox.ComboBox.Enabled = false;
            this._HandlerComboBox.ComboBox.DataSource = null;
            this._HandlerComboBox.ComboBox.Items.Clear();
            this._HandlerComboBox.ComboBox.DataSource = typeof( SupportedHandler ).ValueDescriptionPairs().ToList();
            this._HandlerComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._HandlerComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._EndTestModeComboBox.DataSource = null;
            this._EndTestModeComboBox.Items.Clear();
            this._EndTestModeComboBox.DataSource = typeof( EndTestMode ).ValueDescriptionPairs().ToList();
            this._EndTestModeComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._EndTestModeComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._EndTestModeComboBox.Enabled = true;
            this._HandlerComboBox.Enabled = this.GpioMaterialHandlerInternal?.GpioHandler is object;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.GpioMaterialHandlerInternal is object )
                    {
                        this.GpioMaterialHandlerInternal.Dispose();
                        this.GpioMaterialHandlerInternal = null;
                    }

                    if ( GpioMaterialHandler.Instantiated )
                        GpioMaterialHandler.DisposeInstance();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " GPIO MATERIAL HANDLER: GPIO HANDLER AND EMULATOR "

        private GpioMaterialHandler _GpioMaterialHandlerInternal;

        /// <summary>   Gets or sets the gpio material handler internal. </summary>
        /// <value> The gpio material handler internal. </value>
        private GpioMaterialHandler GpioMaterialHandlerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._GpioMaterialHandlerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._GpioMaterialHandlerInternal != null )
                {
                    this._GpioMaterialHandlerInternal.PropertyChanged -= this.HandleMaterialHandlerPropertyChanged;
                }

                this._GpioMaterialHandlerInternal = value;
                if ( this._GpioMaterialHandlerInternal != null )
                {
                    this._GpioMaterialHandlerInternal.PropertyChanged += this.HandleMaterialHandlerPropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the Gpio handler with an emulator. </summary>
        /// <value> The Gpio test handler. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioMaterialHandler GpioMaterialHandler
        {
            get => this.GpioMaterialHandlerInternal;

            set {
                this.GpioMaterialHandlerInternal = value;
                if ( this.GpioMaterialHandlerInternal is object )
                {
                    this.UpdateGui( this.GpioMaterialHandlerInternal.GpioHandler );
                    this.UpdateGui( this.GpioMaterialHandlerInternal.GpioHandlerEmulator );
                }

                this._HandlerComboBox.Enabled = this.GpioMaterialHandlerInternal?.GpioHandler is object;
            }
        }

        /// <summary> Handles the material handler property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioMaterialHandler sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            if ( propertyName.StartsWith( sender.ServerConnectorSource, StringComparison.OrdinalIgnoreCase ) )
            {
                this.OnPropertyChanged( sender.ServerConnector, propertyName.Remove( 0, sender.ServerConnectorSource.Length ) );
            }
            else if ( propertyName.StartsWith( sender.GpioHandlerSource, StringComparison.OrdinalIgnoreCase ) )
            {
                this.OnPropertyChanged( sender.GpioHandler, propertyName.Remove( 0, sender.GpioHandlerSource.Length ) );
            }
            else if ( propertyName.StartsWith( sender.GpioHandlerEmulatorSource, StringComparison.OrdinalIgnoreCase ) )
            {
                this.OnPropertyChanged( sender.GpioHandlerEmulator, propertyName.Remove( 0, sender.GpioHandlerEmulatorSource.Length ) );
            }
            else
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( Gpio.Handler.GpioMaterialHandler.DeviceCaption ):
                        {
                            this._DeviceInfoTextBox.Text = sender.DeviceCaption;
                            break;
                        }

                    case nameof( Gpio.Handler.GpioMaterialHandler.IsDeviceSubsystemTypeOpen ):
                        {
                            if ( sender.IsDeviceSubsystemTypeOpen )
                            {
                                this._OpenDeviceSubsystemTypeButton.Image = isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_online_2;
                                this._OpenDeviceSubsystemTypeButton.Text = "Close";
                            }
                            else
                            {
                                this._OpenDeviceSubsystemTypeButton.Image = isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_offline_2;
                                this._OpenDeviceSubsystemTypeButton.Text = "Open";
                            }

                            this._HandlerComboBox.Enabled = sender.GpioHandler is object;
                            if ( sender.GpioHandler is object )
                                this.OnHandlerSelected( this._HandlerComboBox );
                            sender.GpioHandler?.PublishAll();
                            sender.GpioHandlerEmulator?.PublishAll();
                            break;
                        }

                    case nameof( Gpio.Handler.GpioMaterialHandler.EventCaption ):
                        {
                            this._EventLogTextBox.AppendText( sender.EventCaption );
                            break;
                        }
                }
            }
        }

        /// <summary> Material handler property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandleMaterialHandlerPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleMaterialHandlerPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as GpioMaterialHandler, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                details = $"Exception handling {nameof(GpioMaterialHandlerView)}.{nameof( GpioMaterialHandlerView.GpioMaterialHandler )}.{e?.PropertyName} property change;.  {ex}";
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, details );
            }
        }

        #endregion

        #region " GPIO HANDLER "

        /// <summary> Updates the graphical user interface described by GPIO Handler. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="gpioHandler"> The gpio handler. </param>
        private void UpdateGui( GpioHandler gpioHandler )
        {
            if ( gpioHandler is object )
            {
                this._StartTestPinNumberNumeric.Value = gpioHandler.StartTestPinNumber;
                this._EndTestPinNumberNumeric.Value = gpioHandler.EndTestPinNumber;
                this._ActiveLowLogicRadioButton.Checked = gpioHandler.ActiveLogic == ActiveLogic.ActiveLow;
                this.BinPortMask = gpioHandler.BinMask;
                this.EndTestMode = gpioHandler.EndTestMode;
            }
        }

        /// <summary> Gets or sets the handler mask. </summary>
        /// <value> The entered mask. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private long BinPortMask
        {
            get => Convert.ToInt32( this._BinPortMaskTextBox.Text, 2 );

            set => this._BinPortMaskTextBox.Text = Convert.ToString( value, 2 );
        }

        /// <summary> Gets or sets the end test mode. </summary>
        /// <value> The end test mode. </value>
        private EndTestMode EndTestMode
        {
            get => ( EndTestMode ) ( int )( this._EndTestModeComboBox.SelectedValue );

            set => this._EndTestModeComboBox.SelectedValue = value;
        }

        /// <summary> Bin port mask text box validating. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void BinPortMaskTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.BinPortMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                    e.Cancel = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
                e.Cancel = true;
            }
        }

        /// <summary> Ends test mode combo box selected index changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EndTestModeComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.BinPortMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Handles the Configure Emulator button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureGpioHandlerButton_Click( object sender, EventArgs e )
        {
            (bool success, string details) = (true, string.Empty);
            try
            {
                this._ErrorProvider.Clear();
                if ( !this.GpioMaterialHandler.IsDeviceSubsystemTypeOpen )
                {
                    ( success, details ) = ( false, "Device not open" );
                }
                else if ( this.GpioMaterialHandler.DeviceConnector.Device.Gpio is null )
                {
                    (success, details) = (false, $"Gpio not supported on {this.GpioMaterialHandler.DeviceConnector.Device.Caption()}" );
                }
                else if ( this.GpioMaterialHandler.DeviceConnector.Device.Gpio.Pins is null )
                {
                    (success, details) = (false, $"Gpio not supported on {this.GpioMaterialHandler.DeviceConnector.Device.Caption()}" );
                }
                else
                {
                    this.GpioMaterialHandler.GpioHandler.StartTestPinNumber = ( int ) this._StartTestPinNumberNumeric.Value;
                    this.GpioMaterialHandler.GpioHandler.EndTestPinNumber = ( int ) this._EndTestPinNumberNumeric.Value;
                    this.GpioMaterialHandler.GpioHandler.ActiveLogic = this._ActiveLowLogicRadioButton.Checked ? ActiveLogic.ActiveLow : ActiveLogic.ActiveHigh;
                    this.GpioMaterialHandler.GpioHandler.BinEndTestOnsetDelay = TimeSpan.FromMilliseconds( ( double ) this._EndTestDelayNumeric.Value );
                    this.GpioMaterialHandler.GpioHandler.BinMask = this.BinPortMask;
                    this.GpioMaterialHandler.GpioHandler.EndTestMode = this.EndTestMode;
                    (success, details) = this.GpioMaterialHandler.TryConfigureGpioHandler();
                }
            }
            catch ( Exception ex )
            {
                (success, details) = (false, ex.ToString() );
            }
            finally
            {
                if ( !success )
                    _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Sot label click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SotLabel_Click( object sender, EventArgs e )
        {
            this.UpdateSotIndicator( this.GpioMaterialHandler.GpioHandler );
        }

        /// <summary> Updates the sot indicator described by sender. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void UpdateSotIndicator( GpioHandlerBase sender )
        {
            if ( sender is object )
            {
                this._SotLabel.Image = sender.StartTestLogicalValue
                    ? isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_available
                    : isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_invisible;
            }
        }

        /// <summary> Eot label click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EotLabel_Click( object sender, EventArgs e )
        {
            this.UpdateEotIndicator( this.GpioMaterialHandler.GpioHandler );
        }

        /// <summary> Updates the eot indicator described by sender. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void UpdateEotIndicator( GpioHandlerBase sender )
        {
            if ( sender is object )
            {
                this._EotLabel.Image = sender.EndTestLogicalValue
                    ? isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_available
                    : isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_invisible;
            }
        }

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandler sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( GpioHandler.State ):
                    {
                        this._GpioHandlerStateLabel.Text = sender.State.Description();
                        switch ( sender.State )
                        {
                            case HandlerState.StartTestReceived:
                                {
                                    break;
                                }

                            default:
                                {
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( GpioHandler.BinValue ):
                    {
                        this._GpioHandlerBinValueLabel.Text = sender.BinValueCaption( "..." );
                        break;
                    }

                case nameof( GpioHandler.StartTestLogicalValue ):
                    {
                        this.UpdateSotIndicator( sender );
                        break;
                    }

                case nameof( GpioHandler.EndTestLogicalValue ):
                    {
                        this.UpdateEotIndicator( sender );
                        break;
                    }

                case nameof( GpioHandler.BinMask ):
                    {
                        this.BinPortMask = sender.BinMask;
                        break;
                    }

                case nameof( GpioHandler.BinEndTestOnsetDelay ):
                    {
                        this._EndTestDelayNumeric.Value = ( decimal ) sender.BinEndTestOnsetDelay.TotalMilliseconds;
                        break;
                    }

                case nameof( GpioHandler.StartTestPinNumber ):
                    {
                        this._StartTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandler.EndTestPinNumber ):
                    {
                        this._EndTestPinNumberNumeric.Value = sender.EndTestPinNumber;
                        break;
                    }

                case nameof( GpioHandler.EndTestMode ):
                    {
                        this.EndTestMode = sender.EndTestMode;
                        break;
                    }

                case nameof( GpioHandler.ActiveLogic ):
                    {
                        this._ActiveLowLogicRadioButton.Checked = sender.ActiveLogic == ActiveLogic.ActiveLow;
                        this._ActiveHighLogicRadioButton.Checked = sender.ActiveLogic == ActiveLogic.ActiveHigh;
                        break;
                    }

                case nameof( GpioHandler.IsConfigured ):
                    {
                        this._ConfigureGpioHandlerButton.Image = sender.IsConfigured ? isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.dialog_ok_2 : isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.configure_2;
                        break;
                    }
            }
        }

        #endregion

        #region " EMULATOR "

        /// <summary>   Updates the graphical user interface described by gpio handler emulator. </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <param name="gpioHandlerEmulator">  The gpio handler emulator. </param>
        private void UpdateGui( GpioHandlerEmulator gpioHandlerEmulator )
        {
            if ( gpioHandlerEmulator is object )
            {
                this._EmulatorStartTestPinNumberNumeric.Value = gpioHandlerEmulator.StartTestPinNumber;
                this._EmulatorEndTestPinNumberNumeric.Value = gpioHandlerEmulator.EndTestPinNumber;
                this.EmulatorBinMask = this.GpioMaterialHandler.GpioHandler.BinMask;
            }
        }

        /// <summary> Gets or sets the entered bin mask. </summary>
        /// <value> The entered mask. </value>
        private long EmulatorBinMask
        {
            get => Convert.ToInt32( this._EmulatorBinPortMaskTextBox.Text, 2 );

            set => this._EmulatorBinPortMaskTextBox.Text = Convert.ToString( value, 2 );
        }

        /// <summary> Emulator Bin port mask text box validating. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void EmulatorBinPortMaskTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.EmulatorBinMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                    e.Cancel = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
                e.Cancel = true;
            }
        }

        /// <summary> Applies the button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureEmulatorButton_Click( object sender, EventArgs e )
        {
            (bool success, string details) = (true, string.Empty);
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    (success, details) = (false, "Sender is empty");
                }
                else if ( this.GpioMaterialHandler.IsDeviceSubsystemTypeOpen )
                {
                    if ( !this.GpioMaterialHandler.IsDeviceSubsystemTypeOpen )
                    {
                        (success, details) = (false, "Device not open");
                    }
                    else if ( this.GpioMaterialHandler.DeviceConnector.Device.Gpio is null )
                    {
                        (success, details) = (false, $"Gpio not supported on {this.GpioMaterialHandler.DeviceConnector.Device.Caption()}" );
                    }
                    else if ( this.GpioMaterialHandler.DeviceConnector.Device.Gpio.Pins is null )
                    {
                        (success, details) = (false, $"Gpio not supported on {this.GpioMaterialHandler.DeviceConnector.Device.Caption()}" );
                    }
                    else
                    {
                        this.GpioMaterialHandler.GpioHandlerEmulator.StartTestPinNumber = ( int ) this._EmulatorStartTestPinNumberNumeric.Value;
                        this.GpioMaterialHandler.GpioHandlerEmulator.EndTestPinNumber = ( int ) this._EmulatorEndTestPinNumberNumeric.Value;
                        this.GpioMaterialHandler.GpioHandlerEmulator.ActiveLogic = this._ActiveLowLogicRadioButton.Checked ? ActiveLogic.ActiveLow : ActiveLogic.ActiveHigh;
                        this.GpioMaterialHandler.GpioHandlerEmulator.BinMask = this.EmulatorBinMask;
                        this.GpioMaterialHandler.GpioHandlerEmulator.EndTestMode = this.EndTestMode;
                        ( success, details ) = this.GpioMaterialHandler.TryConfigureGpioHandlerEmulator();
                    }
                }
                else
                {
                    (success, details) = (false, "Device not open");
                }
            }
            catch ( Exception ex )
            {
                (success, details) = (false, ex.ToString() );
            }
            finally
            {
                if ( !success )
                    _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Handles the emulator property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandlerEmulator sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( GpioHandlerBase.State ):
                    {
                        switch ( sender.State )
                        {
                            case HandlerState.StartTestReceived:
                                {
                                    this._GpioHandlerEmulatorStateLabel.Text = "e: Start test sent";
                                    break;
                                }

                            case HandlerState.EndTestSent:
                                {
                                    this._GpioHandlerEmulatorStateLabel.Text = "e: End test received";
                                    break;
                                }

                            default:
                                {
                                    this._GpioHandlerEmulatorStateLabel.Text = sender.State.Description();
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( GpioHandlerBase.BinValue ):
                    {
                        this._GpioHandlerEmulatorBinValueLabel.Text = sender.BinValueCaption( "..." );
                        break;
                    }

                case nameof( GpioHandlerBase.StartTestLogicalValue ):
                    {
                        break;
                    }

                case nameof( GpioHandlerBase.EndTestLogicalValue ):
                    {
                        break;
                    }

                case nameof( GpioHandlerBase.BinMask ):
                    {
                        this.EmulatorBinMask = sender.BinMask;
                        break;
                    }

                case nameof( GpioHandlerBase.BinEndTestOnsetDelay ):
                    {
                        break;
                    }

                case nameof( GpioHandlerBase.StartTestPinNumber ):
                    {
                        this._EmulatorStartTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerBase.EndTestPinNumber ):
                    {
                        this._EmulatorEndTestPinNumberNumeric.Value = sender.EndTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerBase.ActiveLogic ):
                    {
                        break;
                    }
                // TO_DO: Must be the same as the gpio handler emulator.
                case nameof( GpioHandlerBase.IsConfigured ):
                    {
                        this._ConfigureGpioHandlerEmulatorButton.Image = sender.IsConfigured ? isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.dialog_ok_2 : isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.configure_2;
                        break;
                    }
            }
        }

        #endregion

        #region " PLAY "

        /// <summary> Clears the start button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ClearStartButton_Click( object sender, EventArgs e )
        {
            this._ErrorProvider.Clear();
            var ( success, details ) = this.GpioMaterialHandler.TryClearStart();
            if ( !success )
                _ = this._ErrorProvider.Annunciate( sender, details );
        }

        /// <summary> Starts test button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void StartTestButton_Click( object sender, EventArgs e )
        {
            this._ErrorProvider.Clear();
            var ( success, details ) = this.GpioMaterialHandler.TryStartTest();
            if ( !success )
                _ = this._ErrorProvider.Annunciate( sender, details );
        }

        /// <summary> Resets the button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ResetButton_Click( object sender, EventArgs e )
        {
            this._ErrorProvider.Clear();
            var ( success, details ) = this.GpioMaterialHandler.TryInitializeKnownState();
            if ( !success )
                _ = this._ErrorProvider.Annunciate( sender, details );
        }

        /// <summary> Outputs the end test signals. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">   The sender. </param>
        /// <param name="binValue"> The bin value. </param>
        private void OutputEndTest( object sender, int binValue )
        {
            this._ErrorProvider.Clear();
            var ( success, details ) = this.GpioMaterialHandler.TryOutputEndTest( binValue );
            if ( !success )
                _ = this._ErrorProvider.Annunciate( sender, details );
        }

        /// <summary> Output pass bin button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OutputPassBinButton_Click( object sender, EventArgs e )
        {
            this.OutputEndTest( sender, ( int ) this._PassBinValueNumeric.Value );
        }

        /// <summary> Output fail bin button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OutputFailBinButton_Click( object sender, EventArgs e )
        {
            this.OutputEndTest( sender, ( int ) this._FailBinValueNumeric.Value );
        }

        #endregion

        #region " HANDLER SELECTION "

        /// <summary> Executes the 'handler selected' action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void OnHandlerSelected( ToolStripComboBox sender )
        {
            if ( this.InitializingComponents || sender is null )
                return;
            SupportedHandler handler = ( SupportedHandler ) ( int )( sender.ComboBox.SelectedValue );
            switch ( handler )
            {
                case SupportedHandler.Aetrium:
                    {
                        this.GpioMaterialHandler.GpioHandler.ApplyHandlerInfo( HandlerInfo.AetriumHandlerInfo );
                        this.GpioMaterialHandler.GpioHandlerEmulator.ApplyHandlerInfo( HandlerInfo.AetriumEmulatorInfo );
                        break;
                    }

                case SupportedHandler.NoHau:
                    {
                        this.GpioMaterialHandler.GpioHandler.ApplyHandlerInfo( HandlerInfo.NoHauHandlerInfo );
                        this.GpioMaterialHandler.GpioHandlerEmulator.ApplyHandlerInfo( HandlerInfo.NoHauEmulatorInfo );
                        break;
                    }
            }
        }

        /// <summary> Handler combo box click event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void HandlerComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            try
            {
                this._ErrorProvider.Clear();
                this.OnHandlerSelected( ( ToolStripComboBox ) sender );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
            }
        }

        #endregion

    }
}
