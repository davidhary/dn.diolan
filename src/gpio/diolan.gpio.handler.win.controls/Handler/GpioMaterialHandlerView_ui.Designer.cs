using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Diolan.Gpio.Handler.WinControls
{

    public partial class GpioMaterialHandlerView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._BottomToolStrip = new System.Windows.Forms.ToolStrip();
            this._TabComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._HandlerComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._SelectServerButton = new System.Windows.Forms.ToolStripSplitButton();
            this._ServerNameTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._DefaultServerMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._ConnectServerButton = new System.Windows.Forms.ToolStripButton();
            this._OpenDeviceSubsystemTypeButton = new System.Windows.Forms.ToolStripButton();
            this._SelectDeviceSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this._DevicesComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._DeviceInfoTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._ResetButton = new System.Windows.Forms.ToolStripButton();
            this._Tabs = new isr.Diolan.WinControls.ExtendedTabControl();
            this._GpioHandlerTabPage = new System.Windows.Forms.TabPage();
            this._ConfigureGpioHandlerTabLayout = new System.Windows.Forms.TableLayoutPanel();
            this._ConfigureGroupBox = new System.Windows.Forms.GroupBox();
            this._EndTestModeComboBox = new System.Windows.Forms.ComboBox();
            this._EndTestModeComboBoxLabel = new System.Windows.Forms.Label();
            this._EndTestDelayNumericLabel = new System.Windows.Forms.Label();
            this._EndTestDelayNumeric = new System.Windows.Forms.NumericUpDown();
            this._ConfigureGpioHandlerButton = new System.Windows.Forms.Button();
            this._DigitalLogicLabel = new System.Windows.Forms.Label();
            this._ActiveHighLogicRadioButton = new System.Windows.Forms.RadioButton();
            this._ActiveLowLogicRadioButton = new System.Windows.Forms.RadioButton();
            this._BinPortMaskTextBoxLabel = new System.Windows.Forms.Label();
            this._StartTestPinNumberNumericLabel = new System.Windows.Forms.Label();
            this._BinPortMaskTextBox = new System.Windows.Forms.TextBox();
            this._EndTestPinNumberNumericLabel = new System.Windows.Forms.Label();
            this._EndTestPinNumberNumeric = new System.Windows.Forms.NumericUpDown();
            this._StartTestPinNumberNumeric = new System.Windows.Forms.NumericUpDown();
            this._GpioHandlerEmulatorTabPage = new System.Windows.Forms.TabPage();
            this._GpioHandlerEmulatorLayout = new System.Windows.Forms.TableLayoutPanel();
            this._ConfigureGpioHandlerEmulatorGroupBox = new System.Windows.Forms.GroupBox();
            this._ConfigureGpioHandlerEmulatorButton = new System.Windows.Forms.Button();
            this._EmulatorBinPortMaskTextBoxLabel = new System.Windows.Forms.Label();
            this._EmulatorStartTestPinNumberNumericLabel = new System.Windows.Forms.Label();
            this._EmulatorBinPortMaskTextBox = new System.Windows.Forms.TextBox();
            this._EmulatorEndTestPinNumberNumericLabel = new System.Windows.Forms.Label();
            this._EmulatorEndTestPinNumberNumeric = new System.Windows.Forms.NumericUpDown();
            this._EmulatorStartTestPinNumberNumeric = new System.Windows.Forms.NumericUpDown();
            this._PlayTabPage = new System.Windows.Forms.TabPage();
            this._PlayTabPageLayout = new System.Windows.Forms.TableLayoutPanel();
            this._GpioHandlerPlayGroupBox = new System.Windows.Forms.GroupBox();
            this._FailBinValueNumeric = new System.Windows.Forms.NumericUpDown();
            this._OutputFailBinButton = new System.Windows.Forms.Button();
            this._PassBinValueNumeric = new System.Windows.Forms.NumericUpDown();
            this._OutputPassBinButton = new System.Windows.Forms.Button();
            this._ClearStartButton = new System.Windows.Forms.Button();
            this._HandlerEmulatorPlayGroupBox = new System.Windows.Forms.GroupBox();
            this._StartTestButton = new System.Windows.Forms.Button();
            this._GroupBox = new System.Windows.Forms.GroupBox();
            this._InitializeKnownStateButton = new System.Windows.Forms.Button();
            this._EventLogTabPage = new System.Windows.Forms.TabPage();
            this._EventLogTextBox = new System.Windows.Forms.TextBox();
            this._ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this._TopToolStrip = new System.Windows.Forms.ToolStrip();
            this._GpioHandlerStateLabel = new System.Windows.Forms.ToolStripLabel();
            this._GpioHandlerEmulatorStateLabel = new System.Windows.Forms.ToolStripLabel();
            this._GpioHandlerBinValueLabel = new System.Windows.Forms.ToolStripLabel();
            this._GpioHandlerEmulatorBinValueLabel = new System.Windows.Forms.ToolStripLabel();
            this._SotLabel = new System.Windows.Forms.ToolStripLabel();
            this._EotLabel = new System.Windows.Forms.ToolStripLabel();
            this._BottomToolStrip.SuspendLayout();
            this._Tabs.SuspendLayout();
            this._GpioHandlerTabPage.SuspendLayout();
            this._ConfigureGpioHandlerTabLayout.SuspendLayout();
            this._ConfigureGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._EndTestDelayNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._EndTestPinNumberNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._StartTestPinNumberNumeric)).BeginInit();
            this._GpioHandlerEmulatorTabPage.SuspendLayout();
            this._GpioHandlerEmulatorLayout.SuspendLayout();
            this._ConfigureGpioHandlerEmulatorGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._EmulatorEndTestPinNumberNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._EmulatorStartTestPinNumberNumeric)).BeginInit();
            this._PlayTabPage.SuspendLayout();
            this._PlayTabPageLayout.SuspendLayout();
            this._GpioHandlerPlayGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._FailBinValueNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._PassBinValueNumeric)).BeginInit();
            this._HandlerEmulatorPlayGroupBox.SuspendLayout();
            this._GroupBox.SuspendLayout();
            this._EventLogTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).BeginInit();
            this._ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            this._ToolStripContainer.ContentPanel.SuspendLayout();
            this._ToolStripContainer.TopToolStripPanel.SuspendLayout();
            this._ToolStripContainer.SuspendLayout();
            this._TopToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _BottomToolStrip
            // 
            this._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._BottomToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._BottomToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._TabComboBox,
            this._HandlerComboBox,
            this._SelectServerButton,
            this._ConnectServerButton,
            this._OpenDeviceSubsystemTypeButton,
            this._SelectDeviceSplitButton,
            this._DeviceInfoTextBox,
            this._ResetButton});
            this._BottomToolStrip.Location = new System.Drawing.Point(0, 0);
            this._BottomToolStrip.Name = "_BottomToolStrip";
            this._BottomToolStrip.Size = new System.Drawing.Size(417, 29);
            this._BottomToolStrip.Stretch = true;
            this._BottomToolStrip.TabIndex = 0;
            // 
            // _TabComboBox
            // 
            this._TabComboBox.Name = "_TabComboBox";
            this._TabComboBox.Size = new System.Drawing.Size(81, 29);
            this._TabComboBox.ToolTipText = "Select panel";
            this._TabComboBox.SelectedIndexChanged += new System.EventHandler(this.TabComboBox_SelectedIndexChanged);
            // 
            // _HandlerComboBox
            // 
            this._HandlerComboBox.Name = "_HandlerComboBox";
            this._HandlerComboBox.Size = new System.Drawing.Size(75, 29);
            this._HandlerComboBox.ToolTipText = "Select Handler";
            this._HandlerComboBox.SelectedIndexChanged += new System.EventHandler(this.HandlerComboBox_SelectedIndexChanged);
            // 
            // _SelectServerButton
            // 
            this._SelectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectServerButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ServerNameTextBox,
            this._DefaultServerMenuItem});
            this._SelectServerButton.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.network_server;
            this._SelectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectServerButton.Name = "_SelectServerButton";
            this._SelectServerButton.Size = new System.Drawing.Size(38, 26);
            this._SelectServerButton.Text = "Select Server";
            // 
            // _ServerNameTextBox
            // 
            this._ServerNameTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._ServerNameTextBox.Name = "_ServerNameTextBox";
            this._ServerNameTextBox.Size = new System.Drawing.Size(100, 23);
            this._ServerNameTextBox.Text = "localhost:9656";
            // 
            // _DefaultServerMenuItem
            // 
            this._DefaultServerMenuItem.Checked = true;
            this._DefaultServerMenuItem.CheckOnClick = true;
            this._DefaultServerMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._DefaultServerMenuItem.Name = "_DefaultServerMenuItem";
            this._DefaultServerMenuItem.Size = new System.Drawing.Size(198, 22);
            this._DefaultServerMenuItem.Text = "User Default Server:Port";
            // 
            // _ConnectServerButton
            // 
            this._ConnectServerButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
            this._ConnectServerButton.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.WIFI_open_22;
            this._ConnectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ConnectServerButton.Name = "_ConnectServerButton";
            this._ConnectServerButton.Size = new System.Drawing.Size(41, 26);
            this._ConnectServerButton.Text = "X";
            this._ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this._ConnectServerButton.ToolTipText = "Connect or disconnect the server and show count of attached devices.";
            this._ConnectServerButton.Click += new System.EventHandler(this.ConnectServerButton_Click);
            // 
            // _OpenDeviceSubsystemTypeButton
            // 
            this._OpenDeviceSubsystemTypeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._OpenDeviceSubsystemTypeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._OpenDeviceSubsystemTypeButton.Enabled = false;
            this._OpenDeviceSubsystemTypeButton.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_offline_2;
            this._OpenDeviceSubsystemTypeButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._OpenDeviceSubsystemTypeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._OpenDeviceSubsystemTypeButton.Name = "_OpenDeviceSubsystemTypeButton";
            this._OpenDeviceSubsystemTypeButton.Size = new System.Drawing.Size(26, 26);
            this._OpenDeviceSubsystemTypeButton.Text = "Open";
            this._OpenDeviceSubsystemTypeButton.ToolTipText = "Open or close the device.";
            this._OpenDeviceSubsystemTypeButton.Click += new System.EventHandler(this.OpenDeviceSubsystemTypeButton_Click);
            // 
            // _SelectDeviceSplitButton
            // 
            this._SelectDeviceSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._SelectDeviceSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectDeviceSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._DevicesComboBox});
            this._SelectDeviceSplitButton.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.network_server_database;
            this._SelectDeviceSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton";
            this._SelectDeviceSplitButton.Size = new System.Drawing.Size(38, 26);
            this._SelectDeviceSplitButton.Text = "Device";
            this._SelectDeviceSplitButton.ToolTipText = "Select Device";
            // 
            // _DevicesComboBox
            // 
            this._DevicesComboBox.Name = "_DevicesComboBox";
            this._DevicesComboBox.Size = new System.Drawing.Size(121, 23);
            this._DevicesComboBox.Text = "DLN-4M.1.1";
            // 
            // _DeviceInfoTextBox
            // 
            this._DeviceInfoTextBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._DeviceInfoTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._DeviceInfoTextBox.Name = "_DeviceInfoTextBox";
            this._DeviceInfoTextBox.ReadOnly = true;
            this._DeviceInfoTextBox.Size = new System.Drawing.Size(50, 29);
            this._DeviceInfoTextBox.Text = "closed";
            this._DeviceInfoTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _ResetButton
            // 
            this._ResetButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._ResetButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._ResetButton.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.system_quick_restart;
            this._ResetButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._ResetButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ResetButton.Name = "_ResetButton";
            this._ResetButton.Size = new System.Drawing.Size(26, 26);
            this._ResetButton.Text = "Reset Known State";
            this._ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // _Tabs
            // 
            this._Tabs.Controls.Add(this._GpioHandlerTabPage);
            this._Tabs.Controls.Add(this._GpioHandlerEmulatorTabPage);
            this._Tabs.Controls.Add(this._PlayTabPage);
            this._Tabs.Controls.Add(this._EventLogTabPage);
            this._Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Tabs.HideTabHeaders = true;
            this._Tabs.Location = new System.Drawing.Point(0, 0);
            this._Tabs.Name = "_Tabs";
            this._Tabs.SelectedIndex = 0;
            this._Tabs.Size = new System.Drawing.Size(417, 452);
            this._Tabs.TabIndex = 0;
            // 
            // _GpioHandlerTabPage
            // 
            this._GpioHandlerTabPage.Controls.Add(this._ConfigureGpioHandlerTabLayout);
            this._GpioHandlerTabPage.Location = new System.Drawing.Point(4, 24);
            this._GpioHandlerTabPage.Name = "_GpioHandlerTabPage";
            this._GpioHandlerTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._GpioHandlerTabPage.Size = new System.Drawing.Size(409, 424);
            this._GpioHandlerTabPage.TabIndex = 0;
            this._GpioHandlerTabPage.Text = "GPIO Handler";
            this._GpioHandlerTabPage.UseVisualStyleBackColor = true;
            // 
            // _ConfigureGpioHandlerTabLayout
            // 
            this._ConfigureGpioHandlerTabLayout.ColumnCount = 3;
            this._ConfigureGpioHandlerTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._ConfigureGpioHandlerTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._ConfigureGpioHandlerTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._ConfigureGpioHandlerTabLayout.Controls.Add(this._ConfigureGroupBox, 1, 1);
            this._ConfigureGpioHandlerTabLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ConfigureGpioHandlerTabLayout.Location = new System.Drawing.Point(3, 3);
            this._ConfigureGpioHandlerTabLayout.Name = "_ConfigureGpioHandlerTabLayout";
            this._ConfigureGpioHandlerTabLayout.RowCount = 3;
            this._ConfigureGpioHandlerTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._ConfigureGpioHandlerTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._ConfigureGpioHandlerTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._ConfigureGpioHandlerTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._ConfigureGpioHandlerTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._ConfigureGpioHandlerTabLayout.Size = new System.Drawing.Size(403, 418);
            this._ConfigureGpioHandlerTabLayout.TabIndex = 8;
            // 
            // _ConfigureGroupBox
            // 
            this._ConfigureGroupBox.Controls.Add(this._EndTestModeComboBox);
            this._ConfigureGroupBox.Controls.Add(this._EndTestModeComboBoxLabel);
            this._ConfigureGroupBox.Controls.Add(this._EndTestDelayNumericLabel);
            this._ConfigureGroupBox.Controls.Add(this._EndTestDelayNumeric);
            this._ConfigureGroupBox.Controls.Add(this._ConfigureGpioHandlerButton);
            this._ConfigureGroupBox.Controls.Add(this._DigitalLogicLabel);
            this._ConfigureGroupBox.Controls.Add(this._ActiveHighLogicRadioButton);
            this._ConfigureGroupBox.Controls.Add(this._ActiveLowLogicRadioButton);
            this._ConfigureGroupBox.Controls.Add(this._BinPortMaskTextBoxLabel);
            this._ConfigureGroupBox.Controls.Add(this._StartTestPinNumberNumericLabel);
            this._ConfigureGroupBox.Controls.Add(this._BinPortMaskTextBox);
            this._ConfigureGroupBox.Controls.Add(this._EndTestPinNumberNumericLabel);
            this._ConfigureGroupBox.Controls.Add(this._EndTestPinNumberNumeric);
            this._ConfigureGroupBox.Controls.Add(this._StartTestPinNumberNumeric);
            this._ConfigureGroupBox.Location = new System.Drawing.Point(57, 69);
            this._ConfigureGroupBox.Name = "_ConfigureGroupBox";
            this._ConfigureGroupBox.Size = new System.Drawing.Size(289, 280);
            this._ConfigureGroupBox.TabIndex = 0;
            this._ConfigureGroupBox.TabStop = false;
            this._ConfigureGroupBox.Text = "Configure GPIO Handler";
            // 
            // _EndTestModeComboBox
            // 
            this._EndTestModeComboBox.FormattingEnabled = true;
            this._EndTestModeComboBox.Location = new System.Drawing.Point(147, 191);
            this._EndTestModeComboBox.Name = "_EndTestModeComboBox";
            this._EndTestModeComboBox.Size = new System.Drawing.Size(121, 23);
            this._EndTestModeComboBox.TabIndex = 12;
            this._EndTestModeComboBox.SelectedIndexChanged += new System.EventHandler(this.EndTestModeComboBox_SelectedIndexChanged);
            // 
            // _EndTestModeComboBoxLabel
            // 
            this._EndTestModeComboBoxLabel.AutoSize = true;
            this._EndTestModeComboBoxLabel.Location = new System.Drawing.Point(58, 195);
            this._EndTestModeComboBoxLabel.Name = "_EndTestModeComboBoxLabel";
            this._EndTestModeComboBoxLabel.Size = new System.Drawing.Size(86, 15);
            this._EndTestModeComboBoxLabel.TabIndex = 11;
            this._EndTestModeComboBoxLabel.Text = "End test mode:";
            // 
            // _EndTestDelayNumericLabel
            // 
            this._EndTestDelayNumericLabel.AutoSize = true;
            this._EndTestDelayNumericLabel.Location = new System.Drawing.Point(39, 160);
            this._EndTestDelayNumericLabel.Name = "_EndTestDelayNumericLabel";
            this._EndTestDelayNumericLabel.Size = new System.Drawing.Size(105, 15);
            this._EndTestDelayNumericLabel.TabIndex = 9;
            this._EndTestDelayNumericLabel.Text = "End test delay, ms:";
            this._ToolTip.SetToolTip(this._EndTestDelayNumericLabel, "Time from setting the bin to turning on the end of test signal.");
            // 
            // _EndTestDelayNumeric
            // 
            this._EndTestDelayNumeric.Location = new System.Drawing.Point(147, 156);
            this._EndTestDelayNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this._EndTestDelayNumeric.Name = "_EndTestDelayNumeric";
            this._EndTestDelayNumeric.Size = new System.Drawing.Size(77, 23);
            this._EndTestDelayNumeric.TabIndex = 10;
            this._EndTestDelayNumeric.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // _ConfigureGpioHandlerButton
            // 
            this._ConfigureGpioHandlerButton.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.configure_2;
            this._ConfigureGpioHandlerButton.Location = new System.Drawing.Point(147, 226);
            this._ConfigureGpioHandlerButton.Name = "_ConfigureGpioHandlerButton";
            this._ConfigureGpioHandlerButton.Size = new System.Drawing.Size(120, 37);
            this._ConfigureGpioHandlerButton.TabIndex = 13;
            this._ConfigureGpioHandlerButton.Text = "Apply";
            this._ConfigureGpioHandlerButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this._ToolTip.SetToolTip(this._ConfigureGpioHandlerButton, "Applies the settings to configure the Gpio Handler");
            this._ConfigureGpioHandlerButton.UseVisualStyleBackColor = true;
            this._ConfigureGpioHandlerButton.Click += new System.EventHandler(this.ConfigureGpioHandlerButton_Click);
            // 
            // _DigitalLogicLabel
            // 
            this._DigitalLogicLabel.AutoSize = true;
            this._DigitalLogicLabel.Location = new System.Drawing.Point(32, 128);
            this._DigitalLogicLabel.Name = "_DigitalLogicLabel";
            this._DigitalLogicLabel.Size = new System.Drawing.Size(112, 15);
            this._DigitalLogicLabel.TabIndex = 6;
            this._DigitalLogicLabel.Text = "Digital Logic: Active";
            this._DigitalLogicLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ActiveHighLogicRadioButton
            // 
            this._ActiveHighLogicRadioButton.AutoSize = true;
            this._ActiveHighLogicRadioButton.Location = new System.Drawing.Point(204, 126);
            this._ActiveHighLogicRadioButton.Name = "_ActiveHighLogicRadioButton";
            this._ActiveHighLogicRadioButton.Size = new System.Drawing.Size(51, 19);
            this._ActiveHighLogicRadioButton.TabIndex = 8;
            this._ActiveHighLogicRadioButton.TabStop = true;
            this._ActiveHighLogicRadioButton.Text = "High";
            this._ToolTip.SetToolTip(this._ActiveHighLogicRadioButton, "Active High Logic: Logical 1 = \'High\'");
            this._ActiveHighLogicRadioButton.UseVisualStyleBackColor = true;
            // 
            // _ActiveLowLogicRadioButton
            // 
            this._ActiveLowLogicRadioButton.AutoSize = true;
            this._ActiveLowLogicRadioButton.Checked = true;
            this._ActiveLowLogicRadioButton.Location = new System.Drawing.Point(147, 126);
            this._ActiveLowLogicRadioButton.Name = "_ActiveLowLogicRadioButton";
            this._ActiveLowLogicRadioButton.Size = new System.Drawing.Size(47, 19);
            this._ActiveLowLogicRadioButton.TabIndex = 7;
            this._ActiveLowLogicRadioButton.TabStop = true;
            this._ActiveLowLogicRadioButton.Text = "Low";
            this._ToolTip.SetToolTip(this._ActiveLowLogicRadioButton, "Active low logic: Logic 1 = \'Low\'");
            this._ActiveLowLogicRadioButton.UseVisualStyleBackColor = true;
            // 
            // _BinPortMaskTextBoxLabel
            // 
            this._BinPortMaskTextBoxLabel.AutoSize = true;
            this._BinPortMaskTextBoxLabel.Location = new System.Drawing.Point(25, 97);
            this._BinPortMaskTextBoxLabel.Name = "_BinPortMaskTextBoxLabel";
            this._BinPortMaskTextBoxLabel.Size = new System.Drawing.Size(119, 15);
            this._BinPortMaskTextBoxLabel.TabIndex = 4;
            this._BinPortMaskTextBoxLabel.Text = "Bin Port Binary Mask:";
            this._BinPortMaskTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _StartTestPinNumberNumericLabel
            // 
            this._StartTestPinNumberNumericLabel.AutoSize = true;
            this._StartTestPinNumberNumericLabel.Location = new System.Drawing.Point(20, 31);
            this._StartTestPinNumberNumericLabel.Name = "_StartTestPinNumberNumericLabel";
            this._StartTestPinNumberNumericLabel.Size = new System.Drawing.Size(124, 15);
            this._StartTestPinNumberNumericLabel.TabIndex = 0;
            this._StartTestPinNumberNumericLabel.Text = "Start Test Pin Number:";
            this._StartTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _BinPortMaskTextBox
            // 
            this._BinPortMaskTextBox.Location = new System.Drawing.Point(147, 93);
            this._BinPortMaskTextBox.Name = "_BinPortMaskTextBox";
            this._BinPortMaskTextBox.Size = new System.Drawing.Size(115, 23);
            this._BinPortMaskTextBox.TabIndex = 5;
            this._BinPortMaskTextBox.Text = "1100";
            this._ToolTip.SetToolTip(this._BinPortMaskTextBox, "Bin port binary mask");
            this._BinPortMaskTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.BinPortMaskTextBox_Validating);
            // 
            // _EndTestPinNumberNumericLabel
            // 
            this._EndTestPinNumberNumericLabel.AutoSize = true;
            this._EndTestPinNumberNumericLabel.Location = new System.Drawing.Point(24, 64);
            this._EndTestPinNumberNumericLabel.Name = "_EndTestPinNumberNumericLabel";
            this._EndTestPinNumberNumericLabel.Size = new System.Drawing.Size(120, 15);
            this._EndTestPinNumberNumericLabel.TabIndex = 2;
            this._EndTestPinNumberNumericLabel.Text = "End Test Pin Number:";
            this._EndTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EndTestPinNumberNumeric
            // 
            this._EndTestPinNumberNumeric.Location = new System.Drawing.Point(147, 60);
            this._EndTestPinNumberNumeric.Maximum = new decimal(new int[] {
            47,
            0,
            0,
            0});
            this._EndTestPinNumberNumeric.Name = "_EndTestPinNumberNumeric";
            this._EndTestPinNumberNumeric.Size = new System.Drawing.Size(41, 23);
            this._EndTestPinNumberNumeric.TabIndex = 3;
            this._ToolTip.SetToolTip(this._EndTestPinNumberNumeric, "End test pin number");
            this._EndTestPinNumberNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // _StartTestPinNumberNumeric
            // 
            this._StartTestPinNumberNumeric.Location = new System.Drawing.Point(147, 27);
            this._StartTestPinNumberNumeric.Maximum = new decimal(new int[] {
            47,
            0,
            0,
            0});
            this._StartTestPinNumberNumeric.Name = "_StartTestPinNumberNumeric";
            this._StartTestPinNumberNumeric.Size = new System.Drawing.Size(41, 23);
            this._StartTestPinNumberNumeric.TabIndex = 1;
            this._ToolTip.SetToolTip(this._StartTestPinNumberNumeric, "Start test pin number");
            // 
            // _GpioHandlerEmulatorTabPage
            // 
            this._GpioHandlerEmulatorTabPage.Controls.Add(this._GpioHandlerEmulatorLayout);
            this._GpioHandlerEmulatorTabPage.Location = new System.Drawing.Point(4, 24);
            this._GpioHandlerEmulatorTabPage.Name = "_GpioHandlerEmulatorTabPage";
            this._GpioHandlerEmulatorTabPage.Size = new System.Drawing.Size(409, 424);
            this._GpioHandlerEmulatorTabPage.TabIndex = 3;
            this._GpioHandlerEmulatorTabPage.Text = "Emulator";
            this._GpioHandlerEmulatorTabPage.UseVisualStyleBackColor = true;
            // 
            // _GpioHandlerEmulatorLayout
            // 
            this._GpioHandlerEmulatorLayout.ColumnCount = 3;
            this._GpioHandlerEmulatorLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._GpioHandlerEmulatorLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._GpioHandlerEmulatorLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._GpioHandlerEmulatorLayout.Controls.Add(this._ConfigureGpioHandlerEmulatorGroupBox, 1, 1);
            this._GpioHandlerEmulatorLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._GpioHandlerEmulatorLayout.Location = new System.Drawing.Point(0, 0);
            this._GpioHandlerEmulatorLayout.Name = "_GpioHandlerEmulatorLayout";
            this._GpioHandlerEmulatorLayout.RowCount = 3;
            this._GpioHandlerEmulatorLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._GpioHandlerEmulatorLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._GpioHandlerEmulatorLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._GpioHandlerEmulatorLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._GpioHandlerEmulatorLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._GpioHandlerEmulatorLayout.Size = new System.Drawing.Size(409, 424);
            this._GpioHandlerEmulatorLayout.TabIndex = 9;
            // 
            // _ConfigureGpioHandlerEmulatorGroupBox
            // 
            this._ConfigureGpioHandlerEmulatorGroupBox.Controls.Add(this._ConfigureGpioHandlerEmulatorButton);
            this._ConfigureGpioHandlerEmulatorGroupBox.Controls.Add(this._EmulatorBinPortMaskTextBoxLabel);
            this._ConfigureGpioHandlerEmulatorGroupBox.Controls.Add(this._EmulatorStartTestPinNumberNumericLabel);
            this._ConfigureGpioHandlerEmulatorGroupBox.Controls.Add(this._EmulatorBinPortMaskTextBox);
            this._ConfigureGpioHandlerEmulatorGroupBox.Controls.Add(this._EmulatorEndTestPinNumberNumericLabel);
            this._ConfigureGpioHandlerEmulatorGroupBox.Controls.Add(this._EmulatorEndTestPinNumberNumeric);
            this._ConfigureGpioHandlerEmulatorGroupBox.Controls.Add(this._EmulatorStartTestPinNumberNumeric);
            this._ConfigureGpioHandlerEmulatorGroupBox.Location = new System.Drawing.Point(67, 124);
            this._ConfigureGpioHandlerEmulatorGroupBox.Name = "_ConfigureGpioHandlerEmulatorGroupBox";
            this._ConfigureGpioHandlerEmulatorGroupBox.Size = new System.Drawing.Size(275, 175);
            this._ConfigureGpioHandlerEmulatorGroupBox.TabIndex = 2;
            this._ConfigureGpioHandlerEmulatorGroupBox.TabStop = false;
            this._ConfigureGpioHandlerEmulatorGroupBox.Text = "Configure Emulator";
            // 
            // _ConfigureGpioHandlerEmulatorButton
            // 
            this._ConfigureGpioHandlerEmulatorButton.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.configure_2;
            this._ConfigureGpioHandlerEmulatorButton.Location = new System.Drawing.Point(141, 126);
            this._ConfigureGpioHandlerEmulatorButton.Name = "_ConfigureGpioHandlerEmulatorButton";
            this._ConfigureGpioHandlerEmulatorButton.Size = new System.Drawing.Size(115, 37);
            this._ConfigureGpioHandlerEmulatorButton.TabIndex = 6;
            this._ConfigureGpioHandlerEmulatorButton.Text = "Apply";
            this._ConfigureGpioHandlerEmulatorButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this._ToolTip.SetToolTip(this._ConfigureGpioHandlerEmulatorButton, "Applies the settings to configure the handler emulator");
            this._ConfigureGpioHandlerEmulatorButton.UseVisualStyleBackColor = true;
            this._ConfigureGpioHandlerEmulatorButton.Click += new System.EventHandler(this.ConfigureEmulatorButton_Click);
            // 
            // _EmulatorBinPortMaskTextBoxLabel
            // 
            this._EmulatorBinPortMaskTextBoxLabel.AutoSize = true;
            this._EmulatorBinPortMaskTextBoxLabel.Location = new System.Drawing.Point(19, 96);
            this._EmulatorBinPortMaskTextBoxLabel.Name = "_EmulatorBinPortMaskTextBoxLabel";
            this._EmulatorBinPortMaskTextBoxLabel.Size = new System.Drawing.Size(119, 15);
            this._EmulatorBinPortMaskTextBoxLabel.TabIndex = 4;
            this._EmulatorBinPortMaskTextBoxLabel.Text = "Bin Port Binary Mask:";
            this._EmulatorBinPortMaskTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EmulatorStartTestPinNumberNumericLabel
            // 
            this._EmulatorStartTestPinNumberNumericLabel.AutoSize = true;
            this._EmulatorStartTestPinNumberNumericLabel.Location = new System.Drawing.Point(14, 31);
            this._EmulatorStartTestPinNumberNumericLabel.Name = "_EmulatorStartTestPinNumberNumericLabel";
            this._EmulatorStartTestPinNumberNumericLabel.Size = new System.Drawing.Size(124, 15);
            this._EmulatorStartTestPinNumberNumericLabel.TabIndex = 0;
            this._EmulatorStartTestPinNumberNumericLabel.Text = "Start Test Pin Number:";
            this._EmulatorStartTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EmulatorBinPortMaskTextBox
            // 
            this._EmulatorBinPortMaskTextBox.Location = new System.Drawing.Point(141, 92);
            this._EmulatorBinPortMaskTextBox.Name = "_EmulatorBinPortMaskTextBox";
            this._EmulatorBinPortMaskTextBox.Size = new System.Drawing.Size(115, 23);
            this._EmulatorBinPortMaskTextBox.TabIndex = 5;
            this._EmulatorBinPortMaskTextBox.Text = "11000000";
            this._ToolTip.SetToolTip(this._EmulatorBinPortMaskTextBox, "Emulator bin port binary mask");
            this._EmulatorBinPortMaskTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.EmulatorBinPortMaskTextBox_Validating);
            // 
            // _EmulatorEndTestPinNumberNumericLabel
            // 
            this._EmulatorEndTestPinNumberNumericLabel.AutoSize = true;
            this._EmulatorEndTestPinNumberNumericLabel.Location = new System.Drawing.Point(18, 64);
            this._EmulatorEndTestPinNumberNumericLabel.Name = "_EmulatorEndTestPinNumberNumericLabel";
            this._EmulatorEndTestPinNumberNumericLabel.Size = new System.Drawing.Size(120, 15);
            this._EmulatorEndTestPinNumberNumericLabel.TabIndex = 2;
            this._EmulatorEndTestPinNumberNumericLabel.Text = "End Test Pin Number:";
            this._EmulatorEndTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EmulatorEndTestPinNumberNumeric
            // 
            this._EmulatorEndTestPinNumberNumeric.Location = new System.Drawing.Point(141, 60);
            this._EmulatorEndTestPinNumberNumeric.Maximum = new decimal(new int[] {
            47,
            0,
            0,
            0});
            this._EmulatorEndTestPinNumberNumeric.Name = "_EmulatorEndTestPinNumberNumeric";
            this._EmulatorEndTestPinNumberNumeric.Size = new System.Drawing.Size(41, 23);
            this._EmulatorEndTestPinNumberNumeric.TabIndex = 3;
            this._ToolTip.SetToolTip(this._EmulatorEndTestPinNumberNumeric, "Emulator end test pin number");
            this._EmulatorEndTestPinNumberNumeric.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // _EmulatorStartTestPinNumberNumeric
            // 
            this._EmulatorStartTestPinNumberNumeric.Location = new System.Drawing.Point(141, 27);
            this._EmulatorStartTestPinNumberNumeric.Maximum = new decimal(new int[] {
            47,
            0,
            0,
            0});
            this._EmulatorStartTestPinNumberNumeric.Name = "_EmulatorStartTestPinNumberNumeric";
            this._EmulatorStartTestPinNumberNumeric.Size = new System.Drawing.Size(41, 23);
            this._EmulatorStartTestPinNumberNumeric.TabIndex = 1;
            this._ToolTip.SetToolTip(this._EmulatorStartTestPinNumberNumeric, "Emulator start test pin number");
            this._EmulatorStartTestPinNumberNumeric.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // _PlayTabPage
            // 
            this._PlayTabPage.Controls.Add(this._PlayTabPageLayout);
            this._PlayTabPage.Location = new System.Drawing.Point(4, 24);
            this._PlayTabPage.Name = "_PlayTabPage";
            this._PlayTabPage.Size = new System.Drawing.Size(409, 424);
            this._PlayTabPage.TabIndex = 1;
            this._PlayTabPage.Text = "Play";
            this._PlayTabPage.UseVisualStyleBackColor = true;
            // 
            // _PlayTabPageLayout
            // 
            this._PlayTabPageLayout.ColumnCount = 3;
            this._PlayTabPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PlayTabPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._PlayTabPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PlayTabPageLayout.Controls.Add(this._GpioHandlerPlayGroupBox, 1, 2);
            this._PlayTabPageLayout.Controls.Add(this._HandlerEmulatorPlayGroupBox, 1, 3);
            this._PlayTabPageLayout.Controls.Add(this._GroupBox, 1, 1);
            this._PlayTabPageLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PlayTabPageLayout.Location = new System.Drawing.Point(0, 0);
            this._PlayTabPageLayout.Name = "_PlayTabPageLayout";
            this._PlayTabPageLayout.RowCount = 5;
            this._PlayTabPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._PlayTabPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.02703F));
            this._PlayTabPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._PlayTabPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._PlayTabPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._PlayTabPageLayout.Size = new System.Drawing.Size(409, 424);
            this._PlayTabPageLayout.TabIndex = 0;
            // 
            // _GpioHandlerPlayGroupBox
            // 
            this._GpioHandlerPlayGroupBox.Controls.Add(this._FailBinValueNumeric);
            this._GpioHandlerPlayGroupBox.Controls.Add(this._OutputFailBinButton);
            this._GpioHandlerPlayGroupBox.Controls.Add(this._PassBinValueNumeric);
            this._GpioHandlerPlayGroupBox.Controls.Add(this._OutputPassBinButton);
            this._GpioHandlerPlayGroupBox.Controls.Add(this._ClearStartButton);
            this._GpioHandlerPlayGroupBox.Location = new System.Drawing.Point(108, 152);
            this._GpioHandlerPlayGroupBox.Name = "_GpioHandlerPlayGroupBox";
            this._GpioHandlerPlayGroupBox.Size = new System.Drawing.Size(192, 147);
            this._GpioHandlerPlayGroupBox.TabIndex = 0;
            this._GpioHandlerPlayGroupBox.TabStop = false;
            this._GpioHandlerPlayGroupBox.Text = "Gpio Handler";
            // 
            // _FailBinValueNumeric
            // 
            this._FailBinValueNumeric.Location = new System.Drawing.Point(135, 111);
            this._FailBinValueNumeric.Name = "_FailBinValueNumeric";
            this._FailBinValueNumeric.Size = new System.Drawing.Size(40, 23);
            this._FailBinValueNumeric.TabIndex = 4;
            this._ToolTip.SetToolTip(this._FailBinValueNumeric, "Failed bin value");
            this._FailBinValueNumeric.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // _OutputFailBinButton
            // 
            this._OutputFailBinButton.Location = new System.Drawing.Point(18, 106);
            this._OutputFailBinButton.Name = "_OutputFailBinButton";
            this._OutputFailBinButton.Size = new System.Drawing.Size(111, 33);
            this._OutputFailBinButton.TabIndex = 3;
            this._OutputFailBinButton.Text = "Output Fail Bin";
            this._ToolTip.SetToolTip(this._OutputFailBinButton, "Sends the failed end of test sequence");
            this._OutputFailBinButton.UseVisualStyleBackColor = true;
            this._OutputFailBinButton.Click += new System.EventHandler(this.OutputFailBinButton_Click);
            // 
            // _PassBinValueNumeric
            // 
            this._PassBinValueNumeric.Location = new System.Drawing.Point(135, 71);
            this._PassBinValueNumeric.Name = "_PassBinValueNumeric";
            this._PassBinValueNumeric.Size = new System.Drawing.Size(40, 23);
            this._PassBinValueNumeric.TabIndex = 2;
            this._ToolTip.SetToolTip(this._PassBinValueNumeric, "Pass bin value");
            this._PassBinValueNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // _OutputPassBinButton
            // 
            this._OutputPassBinButton.Location = new System.Drawing.Point(18, 66);
            this._OutputPassBinButton.Name = "_OutputPassBinButton";
            this._OutputPassBinButton.Size = new System.Drawing.Size(111, 33);
            this._OutputPassBinButton.TabIndex = 1;
            this._OutputPassBinButton.Text = "Output Pass Bin";
            this._ToolTip.SetToolTip(this._OutputPassBinButton, "Sends the passed end of test sequence");
            this._OutputPassBinButton.UseVisualStyleBackColor = true;
            this._OutputPassBinButton.Click += new System.EventHandler(this.OutputPassBinButton_Click);
            // 
            // _ClearStartButton
            // 
            this._ClearStartButton.Location = new System.Drawing.Point(17, 25);
            this._ClearStartButton.Name = "_ClearStartButton";
            this._ClearStartButton.Size = new System.Drawing.Size(156, 34);
            this._ClearStartButton.TabIndex = 0;
            this._ClearStartButton.Text = "Clear to Start";
            this._ToolTip.SetToolTip(this._ClearStartButton, "Allows handler to send start test.");
            this._ClearStartButton.UseVisualStyleBackColor = true;
            this._ClearStartButton.Click += new System.EventHandler(this.ClearStartButton_Click);
            // 
            // _HandlerEmulatorPlayGroupBox
            // 
            this._HandlerEmulatorPlayGroupBox.Controls.Add(this._StartTestButton);
            this._HandlerEmulatorPlayGroupBox.Location = new System.Drawing.Point(108, 305);
            this._HandlerEmulatorPlayGroupBox.Name = "_HandlerEmulatorPlayGroupBox";
            this._HandlerEmulatorPlayGroupBox.Size = new System.Drawing.Size(192, 78);
            this._HandlerEmulatorPlayGroupBox.TabIndex = 1;
            this._HandlerEmulatorPlayGroupBox.TabStop = false;
            this._HandlerEmulatorPlayGroupBox.Text = "Handler Emulator";
            // 
            // _StartTestButton
            // 
            this._StartTestButton.Location = new System.Drawing.Point(18, 29);
            this._StartTestButton.Name = "_StartTestButton";
            this._StartTestButton.Size = new System.Drawing.Size(156, 34);
            this._StartTestButton.TabIndex = 0;
            this._StartTestButton.Text = "Start Test";
            this._ToolTip.SetToolTip(this._StartTestButton, "Sends start test from emulator to Gpio Handler");
            this._StartTestButton.UseVisualStyleBackColor = true;
            this._StartTestButton.Click += new System.EventHandler(this.StartTestButton_Click);
            // 
            // _GroupBox
            // 
            this._GroupBox.Controls.Add(this._InitializeKnownStateButton);
            this._GroupBox.Location = new System.Drawing.Point(108, 65);
            this._GroupBox.Name = "_GroupBox";
            this._GroupBox.Size = new System.Drawing.Size(192, 78);
            this._GroupBox.TabIndex = 2;
            this._GroupBox.TabStop = false;
            this._GroupBox.Text = "Gpio Handler + Emulator";
            // 
            // _InitializeKnownStateButton
            // 
            this._InitializeKnownStateButton.Location = new System.Drawing.Point(18, 29);
            this._InitializeKnownStateButton.Name = "_InitializeKnownStateButton";
            this._InitializeKnownStateButton.Size = new System.Drawing.Size(156, 34);
            this._InitializeKnownStateButton.TabIndex = 0;
            this._InitializeKnownStateButton.Text = "Reset";
            this._InitializeKnownStateButton.UseVisualStyleBackColor = true;
            // 
            // _EventLogTabPage
            // 
            this._EventLogTabPage.Controls.Add(this._EventLogTextBox);
            this._EventLogTabPage.Location = new System.Drawing.Point(4, 22);
            this._EventLogTabPage.Name = "_EventLogTabPage";
            this._EventLogTabPage.Size = new System.Drawing.Size(409, 426);
            this._EventLogTabPage.TabIndex = 2;
            this._EventLogTabPage.Text = "Events";
            this._EventLogTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTextBox
            // 
            this._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._EventLogTextBox.Location = new System.Drawing.Point(0, 0);
            this._EventLogTextBox.Multiline = true;
            this._EventLogTextBox.Name = "_EventLogTextBox";
            this._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._EventLogTextBox.Size = new System.Drawing.Size(409, 426);
            this._EventLogTextBox.TabIndex = 1;
            this._EventLogTextBox.DoubleClick += new System.EventHandler(this.EventLogTextBox_DoubleClick);
            // 
            // _ErrorProvider
            // 
            this._ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            this._ToolStripContainer.BottomToolStripPanel.Controls.Add(this._BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            this._ToolStripContainer.ContentPanel.Controls.Add(this._Tabs);
            this._ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(417, 452);
            this._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            this._ToolStripContainer.Name = "_ToolStripContainer";
            this._ToolStripContainer.Size = new System.Drawing.Size(417, 506);
            this._ToolStripContainer.TabIndex = 0;
            this._ToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _ToolStripContainer.TopToolStripPanel
            // 
            this._ToolStripContainer.TopToolStripPanel.Controls.Add(this._TopToolStrip);
            // 
            // _TopToolStrip
            // 
            this._TopToolStrip.AutoSize = false;
            this._TopToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._TopToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._TopToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._GpioHandlerStateLabel,
            this._GpioHandlerEmulatorStateLabel,
            this._GpioHandlerBinValueLabel,
            this._GpioHandlerEmulatorBinValueLabel,
            this._SotLabel,
            this._EotLabel});
            this._TopToolStrip.Location = new System.Drawing.Point(0, 0);
            this._TopToolStrip.Name = "_TopToolStrip";
            this._TopToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this._TopToolStrip.Size = new System.Drawing.Size(417, 25);
            this._TopToolStrip.Stretch = true;
            this._TopToolStrip.TabIndex = 0;
            // 
            // _GpioHandlerStateLabel
            // 
            this._GpioHandlerStateLabel.BackColor = System.Drawing.Color.Black;
            this._GpioHandlerStateLabel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._GpioHandlerStateLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._GpioHandlerStateLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioHandlerStateLabel.ForeColor = System.Drawing.Color.Aqua;
            this._GpioHandlerStateLabel.Name = "_GpioHandlerStateLabel";
            this._GpioHandlerStateLabel.Size = new System.Drawing.Size(111, 22);
            this._GpioHandlerStateLabel.Text = "Gpio Handler state";
            this._GpioHandlerStateLabel.ToolTipText = "Gpio Handler state";
            // 
            // _GpioHandlerEmulatorStateLabel
            // 
            this._GpioHandlerEmulatorStateLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._GpioHandlerEmulatorStateLabel.BackColor = System.Drawing.Color.Black;
            this._GpioHandlerEmulatorStateLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioHandlerEmulatorStateLabel.ForeColor = System.Drawing.Color.Aqua;
            this._GpioHandlerEmulatorStateLabel.Name = "_GpioHandlerEmulatorStateLabel";
            this._GpioHandlerEmulatorStateLabel.Size = new System.Drawing.Size(88, 22);
            this._GpioHandlerEmulatorStateLabel.Text = "Emulator state";
            this._GpioHandlerEmulatorStateLabel.ToolTipText = "Handler Emulator State";
            // 
            // _GpioHandlerBinValueLabel
            // 
            this._GpioHandlerBinValueLabel.BackColor = System.Drawing.Color.Black;
            this._GpioHandlerBinValueLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioHandlerBinValueLabel.ForeColor = System.Drawing.Color.SpringGreen;
            this._GpioHandlerBinValueLabel.Margin = new System.Windows.Forms.Padding(1, 1, 0, 2);
            this._GpioHandlerBinValueLabel.Name = "_GpioHandlerBinValueLabel";
            this._GpioHandlerBinValueLabel.Size = new System.Drawing.Size(24, 22);
            this._GpioHandlerBinValueLabel.Text = "bin";
            this._GpioHandlerBinValueLabel.ToolTipText = "Gpio Handler Bin value";
            // 
            // _GpioHandlerEmulatorBinValueLabel
            // 
            this._GpioHandlerEmulatorBinValueLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._GpioHandlerEmulatorBinValueLabel.BackColor = System.Drawing.Color.Black;
            this._GpioHandlerEmulatorBinValueLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioHandlerEmulatorBinValueLabel.ForeColor = System.Drawing.Color.SpringGreen;
            this._GpioHandlerEmulatorBinValueLabel.Margin = new System.Windows.Forms.Padding(0, 1, 1, 2);
            this._GpioHandlerEmulatorBinValueLabel.Name = "_GpioHandlerEmulatorBinValueLabel";
            this._GpioHandlerEmulatorBinValueLabel.Size = new System.Drawing.Size(24, 22);
            this._GpioHandlerEmulatorBinValueLabel.Text = "bin";
            this._GpioHandlerEmulatorBinValueLabel.ToolTipText = "Emulated bin value";
            // 
            // _SotLabel
            // 
            this._SotLabel.BackColor = System.Drawing.Color.Transparent;
            this._SotLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this._SotLabel.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_invisible;
            this._SotLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SotLabel.Name = "_SotLabel";
            this._SotLabel.Size = new System.Drawing.Size(27, 22);
            this._SotLabel.Text = "SOT";
            this._SotLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this._SotLabel.ToolTipText = "Start test pin logical status";
            this._SotLabel.Click += new System.EventHandler(this.SotLabel_Click);
            // 
            // _EotLabel
            // 
            this._EotLabel.BackColor = System.Drawing.Color.Transparent;
            this._EotLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this._EotLabel.Image = global::isr.Diolan.Gpio.Handler.WinControls.Properties.Resources.user_invisible;
            this._EotLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._EotLabel.Name = "_EotLabel";
            this._EotLabel.Size = new System.Drawing.Size(27, 22);
            this._EotLabel.Text = "EOT";
            this._EotLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this._EotLabel.Click += new System.EventHandler(this.EotLabel_Click);
            // 
            // GpioMaterialHandlerView
            // 
            this.Controls.Add(this._ToolStripContainer);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GpioMaterialHandlerView";
            this.Size = new System.Drawing.Size(417, 506);
            this._BottomToolStrip.ResumeLayout(false);
            this._BottomToolStrip.PerformLayout();
            this._Tabs.ResumeLayout(false);
            this._GpioHandlerTabPage.ResumeLayout(false);
            this._ConfigureGpioHandlerTabLayout.ResumeLayout(false);
            this._ConfigureGroupBox.ResumeLayout(false);
            this._ConfigureGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._EndTestDelayNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._EndTestPinNumberNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._StartTestPinNumberNumeric)).EndInit();
            this._GpioHandlerEmulatorTabPage.ResumeLayout(false);
            this._GpioHandlerEmulatorLayout.ResumeLayout(false);
            this._ConfigureGpioHandlerEmulatorGroupBox.ResumeLayout(false);
            this._ConfigureGpioHandlerEmulatorGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._EmulatorEndTestPinNumberNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._EmulatorStartTestPinNumberNumeric)).EndInit();
            this._PlayTabPage.ResumeLayout(false);
            this._PlayTabPageLayout.ResumeLayout(false);
            this._GpioHandlerPlayGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._FailBinValueNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._PassBinValueNumeric)).EndInit();
            this._HandlerEmulatorPlayGroupBox.ResumeLayout(false);
            this._GroupBox.ResumeLayout(false);
            this._EventLogTabPage.ResumeLayout(false);
            this._EventLogTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).EndInit();
            this._ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            this._ToolStripContainer.BottomToolStripPanel.PerformLayout();
            this._ToolStripContainer.ContentPanel.ResumeLayout(false);
            this._ToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this._ToolStripContainer.ResumeLayout(false);
            this._ToolStripContainer.PerformLayout();
            this._TopToolStrip.ResumeLayout(false);
            this._TopToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        private ToolStrip _BottomToolStrip;
        private ToolStripTextBox _DeviceInfoTextBox;
        private ErrorProvider _ErrorProvider;
        private ToolTip _ToolTip;
        private ToolStripButton _OpenDeviceSubsystemTypeButton;
        private TabPage _GpioHandlerTabPage;
        private ToolStripContainer _ToolStripContainer;
        private isr.Diolan.WinControls.ExtendedTabControl _Tabs;
        private ToolStripComboBox _DevicesComboBox;
        private ToolStripSplitButton _SelectServerButton;
        private ToolStripTextBox _ServerNameTextBox;
        private ToolStripMenuItem _DefaultServerMenuItem;
        private ToolStripButton _ConnectServerButton;
        private ToolStripSplitButton _SelectDeviceSplitButton;
        private TableLayoutPanel _ConfigureGpioHandlerTabLayout;
        private GroupBox _ConfigureGroupBox;
        private Label _DigitalLogicLabel;
        private RadioButton _ActiveHighLogicRadioButton;
        private RadioButton _ActiveLowLogicRadioButton;
        private Label _BinPortMaskTextBoxLabel;
        private Label _StartTestPinNumberNumericLabel;
        private TextBox _BinPortMaskTextBox;
        private Label _EndTestPinNumberNumericLabel;
        private NumericUpDown _EndTestPinNumberNumeric;
        private NumericUpDown _StartTestPinNumberNumeric;
        private TabPage _PlayTabPage;
        private TableLayoutPanel _PlayTabPageLayout;
        private TabPage _EventLogTabPage;
        private TextBox _EventLogTextBox;
        private ToolStripComboBox _TabComboBox;
        private Button _ConfigureGpioHandlerButton;
        private TabPage _GpioHandlerEmulatorTabPage;
        private ToolStrip _TopToolStrip;
        private TableLayoutPanel _GpioHandlerEmulatorLayout;
        private GroupBox _ConfigureGpioHandlerEmulatorGroupBox;
        private Button _ConfigureGpioHandlerEmulatorButton;
        private Label _EmulatorBinPortMaskTextBoxLabel;
        private Label _EmulatorStartTestPinNumberNumericLabel;
        private TextBox _EmulatorBinPortMaskTextBox;
        private Label _EmulatorEndTestPinNumberNumericLabel;
        private NumericUpDown _EmulatorEndTestPinNumberNumeric;
        private NumericUpDown _EmulatorStartTestPinNumberNumeric;
        private GroupBox _GpioHandlerPlayGroupBox;
        private GroupBox _HandlerEmulatorPlayGroupBox;
        private Button _ClearStartButton;
        private Button _StartTestButton;
        private ToolStripButton _ResetButton;
        private NumericUpDown _FailBinValueNumeric;
        private Button _OutputFailBinButton;
        private NumericUpDown _PassBinValueNumeric;
        private Button _OutputPassBinButton;
        private Label _EndTestDelayNumericLabel;
        private NumericUpDown _EndTestDelayNumeric;
        private ToolStripLabel _SotLabel;
        private ToolStripLabel _EotLabel;
        private ToolStripLabel _GpioHandlerStateLabel;
        private ToolStripLabel _GpioHandlerEmulatorBinValueLabel;
        private ToolStripLabel _GpioHandlerEmulatorStateLabel;
        private ToolStripLabel _GpioHandlerBinValueLabel;
        private GroupBox _GroupBox;
        private Button _InitializeKnownStateButton;
        private ToolStripComboBox _HandlerComboBox;
        private ComboBox _EndTestModeComboBox;
        private Label _EndTestModeComboBoxLabel;
    }
}
