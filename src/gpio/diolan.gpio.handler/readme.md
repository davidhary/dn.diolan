# About

isr.Diolan.Gpio.Handler is a .Net library supporting the DIOLAN GPIO Handler interface.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Diolan.Gpio.Handler is released as open source under the MIT license.
Bug reports and contributions are welcome at the [DIOLAN Repository].

[DIOLAN Repository]: https://bitbucket.org/davidhary/dn.diolan

