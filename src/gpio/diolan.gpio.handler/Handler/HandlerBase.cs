using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Gpio.Handler
{

    /// <summary> Handler base class. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    public abstract class HandlerBase : INotifyPropertyChanged, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected HandlerBase() : base()
        {
        }

        #region " DISPOSABLE SUPPORT "

        /// <summary> Gets the disposed sentinel. </summary>
        /// <value> The disposed sentinel. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to
        /// release only
        /// unmanaged resources
        /// when called from
        /// the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    // dispose managed state (managed objects).
                    this.RemovePropertyChangedEventHandlers();
                    this.RemoveStateChangedEventHandlers();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " STATE "

        /// <summary> State of the previous. </summary>

        /// <summary> Gets the state of the previous. </summary>
        /// <value> The previous state. </value>
        public HandlerState PreviousState { get; private set; }

        /// <summary> Gets the state. </summary>
        /// <value> The state. </value>
        public HandlerState State { get; private set; }

        #region " STATE CHANGED "

        /// <summary> Event queue for all listeners interested in <see cref="StateChanged"/> events. </summary>
        public event EventHandler<EventArgs> StateChanged;

        /// <summary>   Raises the state changed event. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        public virtual void OnStateChanged( EventArgs e )
        {
            this.NotifyPropertyChanged( nameof( this.State ) );
            this.NotifyStateChanged( e );
        }

        /// <summary>   Raises the state changed event. </summary>
        private void OnStateChanged()
        {
            if ( this.State != this.PreviousState )
                this.OnStateChanged( EventArgs.Empty );
        }

        /// <summary>   Raises the state changed event. </summary>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnStateChanged( object sender, EventArgs e )
        {
            this.StateChanged?.Invoke( sender, e );
        }

        /// <summary>   Notifies a state changed. </summary>
        /// <remarks>   David, 2021-09-02. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyStateChanged( EventArgs e )
        {
            this.OnStateChanged( this, e );
        }

        /// <summary>   Removes the state changed event handlers. </summary>
        /// <remarks>   David, 2021-09-02. </remarks>
        protected void RemoveStateChangedEventHandlers()
        {
            var handler = this.StateChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( EventHandler<EventArgs> ) item;
                }
            }
        }

        #endregion

        #endregion

        #region " STATE ACTIONS "

        /// <summary> Change state. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="newState"> State of the new. </param>
        protected void ChangeState( HandlerState newState )
        {
            this.PreviousState = this.State;
            this.State = newState;
            this.OnStateChanged();
        }

        /// <summary> Executes the start test enabled action. </summary>
        /// <remarks> This is the same as cleared to start. </remarks>
        protected virtual void OnStartTestEnabled()
        {
            this.ChangeState( HandlerState.StartTestEnabled );
        }

        /// <summary> Executes the start test received action. </summary>
        /// <remarks> The start test signal came in from the handler. </remarks>
        protected virtual void OnStartTestReceived()
        {
            this.ChangeState( HandlerState.StartTestReceived );
        }

        /// <summary> Executes the end test sent action. </summary>
        /// <remarks> Program sent the end of test signal. </remarks>
        protected virtual void OnEndTestSent()
        {
            this.ChangeState( HandlerState.EndTestSent );
        }

        /// <summary> Executes the end test acknowledged action. </summary>
        /// <remarks> Handler received the end test and responded. </remarks>
        protected virtual void OnEndTestAcknowledged()
        {
            this.ChangeState( HandlerState.EndTestAcknowledged );
        }

        #endregion

        #region " CONFIGURATION "

        /// <summary> The name. </summary>
        private string _Name;

        /// <summary> Gets or sets the bin mask. </summary>
        /// <value> The bin mask. </value>
        public string Name
        {
            get => this._Name;

            set {
                this._Name = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The bin mask. </summary>
        private long _BinMask;

        /// <summary> Gets or sets the bin mask. </summary>
        /// <value> The bin mask. </value>
        public long BinMask
        {
            get => this._BinMask;

            set {
                this._BinMask = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The bin end test onset delay. </summary>
        private TimeSpan _BinEndTestOnsetDelay;

        /// <summary> Gets or sets the end of test delay after bin onset. </summary>
        /// <value> The bin to end of test onset delay. </value>
        public TimeSpan BinEndTestOnsetDelay
        {
            get => this._BinEndTestOnsetDelay;

            set {
                this._BinEndTestOnsetDelay = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The end test pin number. </summary>
        private int _EndTestPinNumber;

        /// <summary> Gets or sets the end test pin number. </summary>
        /// <value> The end test pin number. </value>
        public int EndTestPinNumber
        {
            get => this._EndTestPinNumber;

            set {
                this._EndTestPinNumber = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The start test pin number. </summary>
        private int _StartTestPinNumber;

        /// <summary> Gets or sets the Start test pin number. </summary>
        /// <value> The Start test pin number. </value>
        public int StartTestPinNumber
        {
            get => this._StartTestPinNumber;

            set {
                this._StartTestPinNumber = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The active logic. </summary>
        private ActiveLogic _ActiveLogic;

        /// <summary> Gets or sets the active logic. </summary>
        /// <value> The active logic. </value>
        public ActiveLogic ActiveLogic
        {
            get => this._ActiveLogic;

            set {
                this._ActiveLogic = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The end test mode. </summary>
        private EndTestMode _EndTestMode;

        /// <summary> Gets or sets the End Test Mode. </summary>
        /// <value> The End Test Mode. </value>
        public EndTestMode EndTestMode
        {
            get => this._EndTestMode;

            set {
                this._EndTestMode = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> True to enable, false to disable the open drain. </summary>
        private bool _OpenDrainEnabled;

        /// <summary> Gets or sets the Open Drain Enabled. </summary>
        /// <value> The Open Drain Enabled. </value>
        public bool OpenDrainEnabled
        {
            get => this._OpenDrainEnabled;

            set {
                this._OpenDrainEnabled = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> True to enable, false to disable the pullup. </summary>
        private bool _PullupEnabled;

        /// <summary> Gets or sets the Pull-up Enabled. </summary>
        /// <value> The Pull-up Enabled. </value>
        public bool PullupEnabled
        {
            get => this._PullupEnabled;

            set {
                this._PullupEnabled = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Executes the initialize known state action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public virtual void InitializeKnownState()
        {
            this.BinValue = 0L;
            this.ChangeState( HandlerState.Idle );
            this.PublishStateInfo();
        }

        /// <summary> Releases the pins. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected virtual void ReleasePins()
        {
            this.BinCount = 0;
        }

        /// <summary> Gets a value indicating whether this object is configured. </summary>
        /// <value> <c>true</c> if this object is configured; otherwise <c>false</c> </value>
        public virtual bool IsConfigured => this.BinCount > 0;

        /// <summary> Configures this object. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected void Configure()
        {
            this.BinCount = (( ulong ) this.BinMask).BitCount();
        }

        /// <summary> Applies the handler information described by value. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="value"> The value. </param>
        public void ApplyHandlerInfo( HandlerInfo value )
        {
            if ( value is object )
            {
                // turn off previous configuration
                this.ReleasePins();
                this.Name = value.Name;
                this.BinMask = value.BinMask;
                this.BinEndTestOnsetDelay = value.BinEndTestOnsetDelay;
                this.EndTestPinNumber = value.EndTestPinNumber;
                this.StartTestPinNumber = value.StartTestPinNumber;
                this.ActiveLogic = value.ActiveLogic;
                this.OpenDrainEnabled = value.OpenDrainEnabled;
                this.PullupEnabled = value.PullupEnabled;
                this.EndTestMode = value.EndTestMode;
            }
        }

        /// <summary> Publish all. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public abstract void PublishAll();

        #endregion

        #region " STATE AND STATUS "

        /// <summary>
        /// Publishes this object. Required to initialize known state of the user interface.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public virtual void PublishStateInfo()
        {
            this.NotifyPropertyChanged( nameof( this.State ) );
            this.NotifyPropertyChanged( nameof( this.BinValue ) );
            this.NotifyPropertyChanged( nameof( this.EndTestLogicalValue ) );
            this.NotifyPropertyChanged( nameof( this.StartTestLogicalValue ) );
        }

        /// <summary> Number of bins. </summary>
        private int _BinCount;

        /// <summary> Gets or sets the number of bins. </summary>
        /// <value> The number of bins. </value>
        public int BinCount
        {
            get => this._BinCount;

            protected set {
                this._BinCount = value;
                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged( nameof( GpioHandlerBase.IsConfigured ) );
            }
        }

        /// <summary> Bin value caption. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="zeroValueCaption"> The zero value caption. </param>
        /// <returns> A String. </returns>
        public string BinValueCaption( string zeroValueCaption )
        {
            return this.BinValue == 0L ? zeroValueCaption : Convert.ToString( this.BinValue, 2 ).PadLeft( this.BinCount, '0' );
        }

        /// <summary> The bin value. </summary>
        private long _BinValue;

        /// <summary> Gets or sets the bin value. </summary>
        /// <value> The bin value. </value>
        public long BinValue
        {
            get => this._BinValue;

            set {
                this._BinValue = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> True to start test logical value. </summary>
        private bool _StartTestLogicalValue;

        /// <summary> Gets or sets the start test logical value. </summary>
        /// <value> The start test logical value. </value>
        public bool StartTestLogicalValue
        {
            get => this._StartTestLogicalValue;

            set {
                if ( value != this.StartTestLogicalValue )
                {
                    this._StartTestLogicalValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> True to end test logical value. </summary>
        private bool _EndTestLogicalValue;

        /// <summary> Gets or sets the End test logical value. </summary>
        /// <value> The End test logical value. </value>
        public bool EndTestLogicalValue
        {
            get => this._EndTestLogicalValue;

            set {
                if ( value != this.EndTestLogicalValue )
                {
                    this._EndTestLogicalValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }

    /// <summary> Values that represent handler states. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum HandlerState
    {

        /// <summary> Handler was not initialized. </summary>
        [Description( "None -- Not specified" )]
        None,

        /// <summary> Handler is idle. </summary>
        [Description( "Idle -- Not started" )]
        Idle,

        /// <summary> Start of test command was applied. </summary>
        [Description( "Start Test Enabled" )]
        StartTestEnabled,

        /// <summary> The handler received the start of test signal from the handler. </summary>
        [Description( "Start Test Received" )]
        StartTestReceived,

        /// <summary> The end of test and bin signals were sent. </summary>
        [Description( "End Test Sent" )]
        EndTestSent,

        /// <summary> The handler acknowledged receipt of the end of test signal by turning off the start
        /// of test signal (using . </summary>
        [Description( "End Test Acknowledged" )]
        EndTestAcknowledged
    }
}
