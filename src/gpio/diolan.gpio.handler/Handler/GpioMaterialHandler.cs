using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Diolan.ExceptionExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Gpio.Handler
{

    /// <summary> A material handler consisting of a Gpio handler and its emulator. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-16, 5645. </para>
    /// </remarks>
    public class GpioMaterialHandler : SubsystemTypeConnectorBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private GpioMaterialHandler() : base()
        {
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static GpioMaterialHandler Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static GpioMaterialHandler Get()
        {
            if ( Instance is not object || Instance.IsDisposed )
            {
                lock ( SyncLocker )
                    Instance = new GpioMaterialHandler();
            }

            return Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( SyncLocker )
                    return Instance is object && !Instance.IsDisposed;
            }
        }

        /// <summary> Dispose instance. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public static void DisposeInstance()
        {
            lock ( SyncLocker )
            {
                if ( Instance is object && !Instance.IsDisposed )
                {
                    Instance.Dispose();
                    Instance = null;
                }
            }
        }

        #region " I DISPOSABLE SUPPORT "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing">    <c>True</c> to release both managed and unmanaged resources;
        ///                             <c>False</c> to release only unmanaged resources when called from the
        ///                             runtime finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.GpioHandlerInternal?.Dispose();
                    this.GpioHandlerInternal = null;

                    this.GpioHandlerEmulatorInternal?.Dispose();
                    this.GpioHandlerEmulatorInternal = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SUBSYSTEM TYPE "

        private readonly SubsystemTypes _SubsystemType = SubsystemTypes.Gpio;

        /// <summary> the subsystem type. </summary>
        /// <value> the subsystem type. </value>
        public override SubsystemTypes SubsystemType => this._SubsystemType;

        /// <summary> Executes the subsystem type closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected override void OnSubsystemTypeClosed()
        {
            if ( this.GpioHandlerEmulatorInternal is object )
            {
                this.GpioHandlerEmulatorInternal.Dispose();
                this.GpioHandlerEmulatorInternal = null;
            }

            if ( this.GpioHandlerInternal is object )
            {
                this.GpioHandlerInternal.Dispose();
                this.GpioHandlerInternal = null;
            }

            base.OnSubsystemTypeClosed();
        }

        /// <summary> Executes the subsystem type closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnSubsystemTypeClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsSubsystemTypeOpen() )
                {
                    // un-register event handlers for all pins
                    foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                        pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                }
            }
        }

        /// <summary> Executes the subsystem type opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public override (bool Success, string Details) TryOpenSubsystemtype()
        {
            (bool Success, string Details) result = (true, string.Empty);
            this.GpioHandlerInternal = null;
            this.GpioHandlerEmulatorInternal = null;

            // Get port count
            if ( this.Device.Gpio.Pins.Count == 0 )
            {
                // this is already done when opening the device.
                result = ( false, $"Adapter '{this.Device.Caption()}' doesn't support GPIO interface." );
            }
            else
            {

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handler for all pins
                foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                    pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                this.GpioHandlerInternal = new GpioHandler();
                this.GpioHandlerEmulatorInternal = new GpioHandlerEmulator();
            }

            return result;
        }

        /// <summary> Queries if a Subsystem Type is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
        public override bool IsSubsystemTypeOpen()
        {
            return this.GpioHandlerInternal is object;
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                this.EventCaption = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}";
            }
        }

        #endregion

        #region " HANDLER + HANDLER EMULATOR "

        /// <summary> Attempts to configure from the given data. </summary>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryConfigure()
        {
            (bool Success, string Details) result = this.TryConfigureGpioHandler();
            if ( result.Success )
                result = this.TryConfigureGpioHandlerEmulator();
            if ( result.Success )
                result = this.TryInitializeKnownState();
            return result;
        }

        /// <summary> Attempts to initialize know state. </summary>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryInitializeKnownState()
        {
            (bool Success, string Details) result = (true, string.Empty);
            try
            {
                if ( this.GpioHandler is not object )
                {
                    result = ( false, "GPIO Handler not set" );
                }
                else if ( this.GpioHandlerEmulator is not object )
                {
                    result = ( false, "GPIO Handler emulator not set");
                }
                else if ( !this.GpioHandler.IsConfigured )
                {
                    result = ( false, "GPIO Handler not configured");
                }
                else if ( !this.GpioHandlerEmulator.IsConfigured )
                {
                    result = ( false, "GPIO Handler emulator Not configured");
                }
                else
                {
                    this.GpioHandlerEmulator.InitializeKnownState();
                    this.GpioHandler.InitializeKnownState();
                    this.GpioHandlerEmulator.InitializeKnownState();
                }
            }
            catch ( Exception ex )
            {
                _ = ex.AddExceptionData();
                result = ( false, ex.ToString() );
            }
            finally
            {
            }

            return result;
        }

        #endregion

        #region " HANDLER "

        /// <summary> Gets or sets the name of the GPIO handler source, which actually controls the GPIO. </summary>
        /// <value> The handler source. </value>
        public string GpioHandlerSource { get; private set; } = $"{nameof( GpioHandler )}.";

        private GpioHandler _GpioHandlerInternal;

        /// <summary>   Gets or sets the GPIO Handler internal. </summary>
        /// <value> The Handler internal. </value>
        private GpioHandler GpioHandlerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._GpioHandlerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._GpioHandlerInternal != null )
                {
                    this._GpioHandlerInternal.PropertyChanged -= this.GpioHandleHandlerPropertyChanged;
                }

                this._GpioHandlerInternal = value;
                if ( this._GpioHandlerInternal != null )
                {
                    this._GpioHandlerInternal.PropertyChanged += this.GpioHandleHandlerPropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the GPIO handler hardware implementation, which accesses the hardware GPIO. </summary>
        /// <value> The GPIO Handler hardware implementation. </value>
        [CLSCompliant( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandler GpioHandler
        {
            get => this.GpioHandlerInternal;

            set => this.GpioHandlerInternal = value;
        }

        /// <summary> Attempts to configure the GPIO handler from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryConfigureGpioHandler()
        {
            (bool Success, string Details) result;
            try
            {
                if ( !this.IsDeviceSubsystemTypeOpen )
                {
                    result = ( false, "Device not open" );
                }
                else if ( this.Device.Gpio is not object )
                {
                    result = ( false, $"Gpio is not supported on {this.Device.Caption()}" );
                }
                else if ( this.Device.Gpio.Pins is not object )
                {
                    result = ( false, $"Gpio 'pins' are not supported on {this.Device.Caption()}" );
                }
                else if ( this.GpioHandler is not object )
                {
                    result = ( false, $"The GPIO Handler was not constructed on {this.Device.Caption()}" );
                }
                else 
                {
                    result = this.GpioHandler.TryValidate( this.Device.Gpio.Pins );
                    if ( result.Success )
                        this.GpioHandler.Configure( this.Device.Gpio.Pins );
                }
            }
            catch ( Exception ex )
            {
                _ = ex.AddExceptionData();
                result = (false, ex.ToString());
            }
            finally
            {
            }

            return result;
        }

        /// <summary> Handles the GPIO handler property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void GpioHandleHandlerPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object )
            {
                this.NotifyPropertyChanged( $"{nameof( this.GpioHandler )}.{e.PropertyName}" );
            }
        }

        #endregion

        #region " EMULATOR "

        /// <summary> Gets or sets the name of the GPIO Handler emulator source. </summary>
        /// <value> The emulator source. </value>
        public string GpioHandlerEmulatorSource { get; private set; } = $"{nameof( GpioHandlerEmulator )}.";

        private GpioHandlerEmulator _GpioHandlerEmulatorInternal;

        /// <summary>   Gets or sets the GPIO handler emulator internal. </summary>
        /// <value> The handler emulator internal. </value>
        private GpioHandlerEmulator GpioHandlerEmulatorInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._GpioHandlerEmulatorInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._GpioHandlerEmulatorInternal != null )
                {

                    this._GpioHandlerEmulatorInternal.PropertyChanged -= this.HandleGpioHandlerEmulatorPropertyChanged;
                }

                this._GpioHandlerEmulatorInternal = value;
                if ( this._GpioHandlerEmulatorInternal != null )
                {
                    this._GpioHandlerEmulatorInternal.PropertyChanged += this.HandleGpioHandlerEmulatorPropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the GPIO handler emulator. </summary>
        /// <value> The handler emulator. </value>
        [CLSCompliant( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandlerEmulator GpioHandlerEmulator
        {
            get => this.GpioHandlerEmulatorInternal;

            set => this.GpioHandlerEmulatorInternal = value;
        }

        /// <summary> Attempts to configure the GPIO handler emulator from the given data. </summary>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryConfigureGpioHandlerEmulator()
        {
            (bool Success, string Details) result;
            try
            {
                if ( !this.IsDeviceSubsystemTypeOpen )
                {
                    result = ( false, "Device not open" );
                }
                else if ( this.Device.Gpio is not object )
                {
                    result = ( false, $"Gpio not supported on {this.Device.Caption()}" );
                }
                else if ( this.Device.Gpio.Pins is not object )
                {
                    result = ( false, $"Gpio not supported on {this.Device.Caption()}" );
                }
                else if ( this.GpioHandlerEmulator is not object )
                {
                    result = ( false, "The GPIO Handler Emulator was not constructed" );
                }
                else 
                {
                    result = this.GpioHandlerEmulator.TryValidate( this.Device.Gpio.Pins );
                    if ( result.Success )
                        this.GpioHandlerEmulator.Configure( this.Device.Gpio.Pins );
                }
            }
            catch ( Exception ex )
            {
                _ = ex.AddExceptionData();
                result = ( false, ex.ToString() );
            }
            finally
            {
            }

            return result;
        }

        /// <summary> Handles the GPIO Handler Emulator property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandleGpioHandlerEmulatorPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object )
            {
                this.NotifyPropertyChanged( $"{this.GpioHandlerEmulatorSource}.{e.PropertyName}" );
            }
        }

        #endregion

        #region " PLAY "

        /// <summary> Attempts to clear start from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryClearStart()
        {
            (bool Success, string Details) result = (true, string.Empty);
            try
            {
                if ( this.GpioHandler is not object )
                {
                    result = ( false, "GPIO Handler was not set" );
                }
                else if ( !this.GpioHandler.IsConfigured )
                {
                    result = ( false, "GPIO Handler was not configured" );
                }
                else if ( this.GpioHandler.State == HandlerState.Idle || this.GpioHandler.State == HandlerState.EndTestAcknowledged )
                {
                    this.GpioHandler.EnableStartTest();
                }
                else
                {
                    result = ( false, $"Invalid GPIO Handler state @'{this.GpioHandlerInternal.State}'; state should be '{HandlerState.Idle}' or '{HandlerState.EndTestAcknowledged}'" );
                }
            }
            catch ( Exception ex )
            {
                _ = ex.AddExceptionData();
                result = ( false, ex.ToString() );
            }

            return result;
        }

        /// <summary>   Attempts to start test from the given data. </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryStartTest()
        {
            (bool Success, string Details) result = (true, string.Empty);
            try
            {
                if ( this.GpioHandler is not object )
                {
                    result = ( false, "GPIO Handler not set" );
                }
                else if ( !this.GpioHandler.IsConfigured )
                {
                    result = ( false, "GPIO Handler not configured" );
                }
                else if ( this.GpioHandlerEmulator is not object )
                {
                    result = ( false, "GPIO Handler emulator not set" );
                }
                else if ( !this.GpioHandlerEmulator.IsConfigured )
                {
                    result = ( false, "GPIO Handler emulator not configured" );
                }
                else if ( this.GpioHandler.State != HandlerState.StartTestEnabled )
                {
                    result = ( false, $"Invalid handler state @'{this.GpioHandler.State}'; state should be @'{HandlerState.StartTestEnabled}'" );
                }
                else
                {
                    this.GpioHandlerEmulator.OutputStartTest();
                }
            }
            catch ( Exception ex )
            {
                _ = ex.AddExceptionData();
                result = ( false, ex.ToString() );
            }
            finally
            {
            }

            return result;
        }

        /// <summary> Sends the end test command. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="binValue"> The bin value. </param>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryOutputEndTest( int binValue )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.GpioHandler is not object )
            {
                result = ( false, "GPIO Handler not set" );
            }
            else if ( !this.GpioHandler.IsConfigured )
            {
                result = ( false, "GPIO Handler not configured" );
            }
            else if ( this.GpioHandler.State == HandlerState.StartTestReceived )
            {
                this.GpioHandler.OutputEndTest( binValue );
            }
            else 
            {
                result = this.TryInitializeKnownState();
                if ( !result.Success )
                    // ? 20170504 was if me.try....
                    result = ( false, $"Invalid handler state @'{this.GpioHandler.State}'; state should be @'{HandlerState.Idle}'; {result.Details}" );
            }

            return result;
        }

        #endregion

    }
}
