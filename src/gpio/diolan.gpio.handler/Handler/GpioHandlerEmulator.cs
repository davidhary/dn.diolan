using System;
using System.Diagnostics;

using isr.Diolan.ExceptionExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Gpio.Handler
{

    /// <summary> Gpio handler emulator. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    [CLSCompliant( false )]
    public class GpioHandlerEmulator : GpioHandlerBase
    {

        #region " CONSTRUCTOR "

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <param name="disposing">    true to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                }
                this.ReleasePins();
            }
            catch ( Exception ex )
            {
                _ = ex.AddExceptionData();
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLISHER "

        /// <summary> Publishes all values by raising the property changed events. </summary>
        /// <remarks>
        /// This is handled at the top level class in case the inheriting class added property over the
        /// base class.
        /// </remarks>
        public override void PublishAll()
        {
            foreach ( System.Reflection.PropertyInfo p in System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.GetProperties() )
                this.NotifyPropertyChanged( p.Name );
        }

        #endregion

        #region " CONFIGURE "

        /// <summary> Configures start test signal. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected override void ConfigureStartTest( Dln.Gpio.Pins pins )
        {
            if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
            if ( pins.Count == 0 ) throw new InvalidOperationException( "GPIO Port has no pins to configure for the handler" );
            this.StartTestPin = new GpioOutputPin( pins, this.StartTestPinNumber, this.ActiveLogic );
        }

        /// <summary> Configures end test signal. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected override void ConfigureEndTest( Dln.Gpio.Pins pins )
        {
            if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
            if ( pins.Count == 0 ) throw new InvalidOperationException( "GPIO Port has no pins to configure for the handler" );
            this.EndTestPin = new GpioInputPin( pins, this.EndTestPinNumber, this.ActiveLogic );
        }

        /// <summary> Registers the End test pin events. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void RegisterEndTestPinEvents()
        {
            if ( !this.RegisteredEndTestPinEvents )
            {
                var pin = this.Pins[this.EndTestPin.PinNumber];
                pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                pin.SetEventConfiguration( Dln.Gpio.EventType.Change, 0 );
                base.OnRegisteringEndTestPinEvents();
            }
        }

        /// <summary> Configures bin port. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected override void ConfigureBinPort( Dln.Gpio.Pins pins )
        {
            if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
            if ( pins.Count == 0 ) throw new InvalidOperationException( "GPIO Port has no pins to configure for the handler" );
            this.BinPort = new GpioInputPort( pins, this.BinMask, this.ActiveLogic );
        }

        /// <summary> Registers the bin port pins events. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void RegisterBinPortPinEvents()
        {
            if ( !this.RegisteredBinPortPinEvents )
            {
                Dln.Gpio.Pin pin;
                foreach ( int i in this.Pins.SelectMulti( ( ulong ) this.BinPort.Mask ) )
                {
                    pin = this.Pins[i];
                    pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                    pin.SetEventConfiguration( Dln.Gpio.EventType.Change, 0 );
                }

                base.OnRegisteringBinPortPinEvents();
            }
        }

        /// <summary> Executes the initialize known state action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public override void InitializeKnownState()
        {
            this.RegisterEndTestPinEvents();
            this.RegisterBinPortPinEvents();
            this.StartTestPin.LogicalValue = 0;
            base.InitializeKnownState();
        }

        #endregion

        #region " EVENTS "

        /// <summary> Filters the condition met event for end test events. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnEndTestEventOccurred( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                base.OnEndTestEventOccurred( e );
                switch ( this.EndTestMode )
                {
                    case EndTestMode.ActiveBin:
                        {
                            // with active bin, end of test is received only after binning.
                            if ( this.IsNotActiveState( e.Value ) )
                            {
                                // if logical 0, start of test was enabled
                                this.OnStartTestEnabled();
                            }
                            else
                            {
                                // end of test enables binning now.
                                this.OnEndTestSent();
                                // now read the bin value
                                this.BinValue = this.ReadEndTest();
                                this.AcknowledgeEndTest();
                            }

                            break;
                        }

                    case EndTestMode.ActiveTest:
                        {
                            // with active test, end of test is turned on by the handler after receiving
                            // start of test. 
                            if ( this.IsActiveState( e.Value ) )
                            {
                                // if the state is active, it means the start test was acknowledged.
                                this.OnStartTestReceived();
                            }
                            else
                            {
                                this.OnEndTestSent();
                                // now read the bin value
                                this.BinValue = this.ReadEndTest();
                                this.AcknowledgeEndTest();
                            }

                            break;
                        }

                    default:
                        {
                            Debug.Assert( !Debugger.IsAttached, "Unhandled end test mode", "End Test Mode {0} could not be handled at Emulator On End Test Event", this.EndTestMode );
                            break;
                        }
                }
            }
        }

        #endregion

        #region " EMULATOR ACTIONS "

        /// <summary> Tests enable start. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected override void OnStartTestEnabled()
        {
            // Turn off the start test signal
            this.StartTestPin.LogicalValue = 0;
            base.OnStartTestEnabled();
            this.StartTestLogicalValue = this.StartTestPin.LogicalValue == 1;
        }

        /// <summary> Output start test signal. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OutputStartTest()
        {

            // TO_DO?! Do we need this?
            // check state and input value in case the sequence was not started or went awry.
            // If Me.EndTestPin.LogicalValue = 0 OrElse Me.State <> HandlerState.StartTestEnabled Then
            // Me.OnStartTestEnabled()
            // My.MyLibrary.DoEventsDelay(10)
            // End If

            // turn on the start test pin
            this.StartTestPin.LogicalValue = 1;
            if ( this.EndTestMode == EndTestMode.ActiveBin )
            {
                this.OnStartTestReceived();
            }
            else if ( this.EndTestMode == EndTestMode.ActiveTest )
            {
            }
            // with active test, the end of test signal will move to the 
            // acknowledge start of test.
            else
            {
                Debug.Assert( !Debugger.IsAttached, "Unhandled end test mode", "End Test Mode {0} could not be handled at Emulator Output Start Test", this.EndTestMode );
            }

            this.StartTestLogicalValue = this.StartTestPin.LogicalValue == 1;
        }

        /// <summary> Tests read end. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> The end test. </returns>
        public long ReadEndTest()
        {
            long value = this.BinPort.MaskedValue;
            // turn off the start test signal, to acknowledge reading the end test signal.
            this.StartTestPin.LogicalValue = 0;
            this.StartTestLogicalValue = this.StartTestPin.LogicalValue == 1;
            return value;
        }

        /// <summary> Executes the start test received action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected override void OnStartTestReceived()
        {
            base.OnStartTestReceived();
        }

        /// <summary> Acknowledges end test reception. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void AcknowledgeEndTest()
        {
            // turn off the start test signal, to acknowledge reading the end test signal.
            this.StartTestPin.LogicalValue = 0;
            this.OnEndTestAcknowledged();
            this.StartTestLogicalValue = this.StartTestPin.LogicalValue == 1;
        }

        #endregion

    }
}
