﻿
namespace isr.Diolan.Gpio
{

    /// <summary> Port base. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-08, 1.0.. </para>
    /// </remarks>
    public abstract class PortBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="mask">        The mask. </param>
        /// <param name="activeLogic"> The active logic. </param>
        protected PortBase( long mask, ActiveLogic activeLogic ) : base()
        {
            this.Mask = mask;
            this.ActiveLogic = activeLogic;
        }

        /// <summary> Gets or sets the active logic. </summary>
        /// <value> The active logic. </value>
        public ActiveLogic ActiveLogic { get; private set; }

        /// <summary> Gets or sets the port value. </summary>
        /// <value> The port value. </value>
        public abstract long PortValue { get; set; }

        /// <summary> Gets or sets the mask. </summary>
        /// <value> The mask. </value>
        public long Mask { get; set; }

        /// <summary> Gets or sets the value mask. </summary>
        /// <value> The value mask. </value>
        public long ValueMask { get; set; }

        /// <summary> Gets or sets the number of bits. </summary>
        /// <value> The number of bits. </value>
        public int BitCount { get; set; }

        /// <summary> Gets or sets the masked value. </summary>
        /// <value> The masked value. </value>
        public long MaskedValue
        {
            get => this.ActiveLogic == ActiveLogic.ActiveLow ? this.ValueMask & ~this.PortValue : this.ValueMask & this.PortValue;

            set => this.PortValue = this.ActiveLogic == ActiveLogic.ActiveLow ? this.ValueMask & ~value : this.ValueMask & value;
        }
    }
}