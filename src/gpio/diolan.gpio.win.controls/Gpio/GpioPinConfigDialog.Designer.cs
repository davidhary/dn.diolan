using System.Diagnostics;

namespace isr.Diolan.Gpio.WinControls
{
    public partial class GpioPinConfigDialog
    {
        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GpioPinConfigDialog));
            this._GpioPinConfigControl = new isr.Diolan.Gpio.WinControls.GpioPinConfigControl();
            this.SuspendLayout();
            // 
            // _GpioPinConfigControl
            // 
            this._GpioPinConfigControl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioPinConfigControl.Location = new System.Drawing.Point(8, 8);
            this._GpioPinConfigControl.Name = "_GpioPinConfigControl";
            this._GpioPinConfigControl.Size = new System.Drawing.Size(287, 200);
            this._GpioPinConfigControl.TabIndex = 0;
            // 
            // GpioPinConfigDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 212);
            this.Controls.Add(this._GpioPinConfigControl);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "GpioPinConfigDialog";
            this.Text = "Gpio Pin Configuration";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        private GpioPinConfigControl _GpioPinConfigControl;
    }
}
