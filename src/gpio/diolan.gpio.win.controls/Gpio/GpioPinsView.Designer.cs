using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Diolan.Gpio.WinControls
{

    public partial class GpioPinsView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._BottomToolStrip = new System.Windows.Forms.ToolStrip();
            this._TabComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._SelectServerButton = new System.Windows.Forms.ToolStripSplitButton();
            this._ServerNameTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._DefaultServerMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._ConnectServerButton = new System.Windows.Forms.ToolStripButton();
            this._OpenDeviceSubsystemTypeButton = new System.Windows.Forms.ToolStripButton();
            this._SelectDeviceSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this._DevicesComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._DeviceInfoTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._Tabs = new isr.Diolan.WinControls.ExtendedTabControl();
            this._PrimaryTabPage = new System.Windows.Forms.TabPage();
            this._PrimaryTabLayout = new System.Windows.Forms.TableLayoutPanel();
            this._SubsystemGroupBox = new System.Windows.Forms.GroupBox();
            this._DebounceNumeric = new System.Windows.Forms.NumericUpDown();
            this._GetDebounceButton = new System.Windows.Forms.Button();
            this._SetDebounceButton = new System.Windows.Forms.Button();
            this._DebounceNumericLabel = new System.Windows.Forms.Label();
            this._InputPinsTabPage = new System.Windows.Forms.TabPage();
            this._InputPinsLayout = new System.Windows.Forms.TableLayoutPanel();
            this._OutputPinsTabPage = new System.Windows.Forms.TabPage();
            this._OutputPinsLayout = new System.Windows.Forms.TableLayoutPanel();
            this._EventLogTabPage = new System.Windows.Forms.TabPage();
            this._EventLogTextBox = new System.Windows.Forms.TextBox();
            this._ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this._TopToolStrip = new System.Windows.Forms.ToolStrip();
            this._EventPinNumberLabel = new System.Windows.Forms.ToolStripLabel();
            this._EvetnPinNameLabel = new System.Windows.Forms.ToolStripLabel();
            this._EventPinValue = new System.Windows.Forms.ToolStripTextBox();
            this._AddPinsSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this._NewPinNumberTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._AddInputPinMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._AddOutputPinMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._BottomToolStrip.SuspendLayout();
            this._Tabs.SuspendLayout();
            this._PrimaryTabPage.SuspendLayout();
            this._PrimaryTabLayout.SuspendLayout();
            this._SubsystemGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._DebounceNumeric)).BeginInit();
            this._InputPinsTabPage.SuspendLayout();
            this._OutputPinsTabPage.SuspendLayout();
            this._EventLogTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).BeginInit();
            this._ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            this._ToolStripContainer.ContentPanel.SuspendLayout();
            this._ToolStripContainer.TopToolStripPanel.SuspendLayout();
            this._ToolStripContainer.SuspendLayout();
            this._TopToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _BottomToolStrip
            // 
            this._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._BottomToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._TabComboBox,
            this._SelectServerButton,
            this._ConnectServerButton,
            this._OpenDeviceSubsystemTypeButton,
            this._SelectDeviceSplitButton,
            this._DeviceInfoTextBox});
            this._BottomToolStrip.Location = new System.Drawing.Point(0, 0);
            this._BottomToolStrip.Name = "_BottomToolStrip";
            this._BottomToolStrip.Size = new System.Drawing.Size(417, 29);
            this._BottomToolStrip.Stretch = true;
            this._BottomToolStrip.TabIndex = 0;
            // 
            // _TabComboBox
            // 
            this._TabComboBox.Name = "_TabComboBox";
            this._TabComboBox.Size = new System.Drawing.Size(81, 29);
            this._TabComboBox.ToolTipText = "Select panel";
            this._TabComboBox.SelectedIndexChanged += new System.EventHandler(this.TabComboBox_SelectedIndexChanged);
            // 
            // _SelectServerButton
            // 
            this._SelectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectServerButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ServerNameTextBox,
            this._DefaultServerMenuItem});
            this._SelectServerButton.Image = global::isr.Diolan.Gpio.WinControls.Properties.Resources.network_server;
            this._SelectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectServerButton.Name = "_SelectServerButton";
            this._SelectServerButton.Size = new System.Drawing.Size(38, 26);
            this._SelectServerButton.Text = "Select Server";
            // 
            // _ServerNameTextBox
            // 
            this._ServerNameTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._ServerNameTextBox.Name = "_ServerNameTextBox";
            this._ServerNameTextBox.Size = new System.Drawing.Size(100, 23);
            this._ServerNameTextBox.Text = "localhost:9656";
            // 
            // _DefaultServerMenuItem
            // 
            this._DefaultServerMenuItem.Checked = true;
            this._DefaultServerMenuItem.CheckOnClick = true;
            this._DefaultServerMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._DefaultServerMenuItem.Name = "_DefaultServerMenuItem";
            this._DefaultServerMenuItem.Size = new System.Drawing.Size(198, 22);
            this._DefaultServerMenuItem.Text = "User Default Server:Port";
            // 
            // _ConnectServerButton
            // 
            this._ConnectServerButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
            this._ConnectServerButton.Image = global::isr.Diolan.Gpio.WinControls.Properties.Resources.WIFI_open_22_Right_Up;
            this._ConnectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ConnectServerButton.Name = "_ConnectServerButton";
            this._ConnectServerButton.Size = new System.Drawing.Size(41, 26);
            this._ConnectServerButton.Text = "X";
            this._ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this._ConnectServerButton.ToolTipText = "Connect or disconnect serve and show attached devices.";
            this._ConnectServerButton.Click += new System.EventHandler(this.ConnectServerButton_Click);
            // 
            // _OpenDeviceSubsystemTypeButton
            // 
            this._OpenDeviceSubsystemTypeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._OpenDeviceSubsystemTypeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._OpenDeviceSubsystemTypeButton.Enabled = false;
            this._OpenDeviceSubsystemTypeButton.Image = global::isr.Diolan.Gpio.WinControls.Properties.Resources.user_offline_2;
            this._OpenDeviceSubsystemTypeButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._OpenDeviceSubsystemTypeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._OpenDeviceSubsystemTypeButton.Name = "_OpenDeviceSubsystemTypeButton";
            this._OpenDeviceSubsystemTypeButton.Size = new System.Drawing.Size(26, 26);
            this._OpenDeviceSubsystemTypeButton.Text = "Open";
            this._OpenDeviceSubsystemTypeButton.ToolTipText = "Open or close the device.";
            this._OpenDeviceSubsystemTypeButton.Click += new System.EventHandler(this.OpenDeviceSubsystemTypeButton_Click);
            // 
            // _SelectDeviceSplitButton
            // 
            this._SelectDeviceSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._SelectDeviceSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectDeviceSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._DevicesComboBox});
            this._SelectDeviceSplitButton.Image = global::isr.Diolan.Gpio.WinControls.Properties.Resources.network_server_database;
            this._SelectDeviceSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton";
            this._SelectDeviceSplitButton.Size = new System.Drawing.Size(38, 26);
            this._SelectDeviceSplitButton.Text = "Device";
            this._SelectDeviceSplitButton.ToolTipText = "Select Device";
            // 
            // _DevicesComboBox
            // 
            this._DevicesComboBox.Name = "_DevicesComboBox";
            this._DevicesComboBox.Size = new System.Drawing.Size(121, 23);
            this._DevicesComboBox.Text = "DLN-4M.1.1";
            // 
            // _DeviceInfoTextBox
            // 
            this._DeviceInfoTextBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._DeviceInfoTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._DeviceInfoTextBox.Name = "_DeviceInfoTextBox";
            this._DeviceInfoTextBox.ReadOnly = true;
            this._DeviceInfoTextBox.Size = new System.Drawing.Size(100, 29);
            this._DeviceInfoTextBox.Text = "closed";
            this._DeviceInfoTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _Tabs
            // 
            this._Tabs.Controls.Add(this._PrimaryTabPage);
            this._Tabs.Controls.Add(this._InputPinsTabPage);
            this._Tabs.Controls.Add(this._OutputPinsTabPage);
            this._Tabs.Controls.Add(this._EventLogTabPage);
            this._Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Tabs.HideTabHeaders = true;
            this._Tabs.Location = new System.Drawing.Point(0, 0);
            this._Tabs.Name = "_Tabs";
            this._Tabs.SelectedIndex = 0;
            this._Tabs.Size = new System.Drawing.Size(417, 452);
            this._Tabs.TabIndex = 0;
            // 
            // _PrimaryTabPage
            // 
            this._PrimaryTabPage.Controls.Add(this._PrimaryTabLayout);
            this._PrimaryTabPage.Location = new System.Drawing.Point(4, 24);
            this._PrimaryTabPage.Name = "_PrimaryTabPage";
            this._PrimaryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._PrimaryTabPage.Size = new System.Drawing.Size(409, 424);
            this._PrimaryTabPage.TabIndex = 0;
            this._PrimaryTabPage.Text = "GPIO";
            this._PrimaryTabPage.UseVisualStyleBackColor = true;
            // 
            // _PrimaryTabLayout
            // 
            this._PrimaryTabLayout.ColumnCount = 3;
            this._PrimaryTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PrimaryTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._PrimaryTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PrimaryTabLayout.Controls.Add(this._SubsystemGroupBox, 1, 1);
            this._PrimaryTabLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PrimaryTabLayout.Location = new System.Drawing.Point(3, 3);
            this._PrimaryTabLayout.Name = "_PrimaryTabLayout";
            this._PrimaryTabLayout.RowCount = 3;
            this._PrimaryTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PrimaryTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._PrimaryTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PrimaryTabLayout.Size = new System.Drawing.Size(403, 418);
            this._PrimaryTabLayout.TabIndex = 8;
            // 
            // _SubsystemGroupBox
            // 
            this._SubsystemGroupBox.Controls.Add(this._DebounceNumeric);
            this._SubsystemGroupBox.Controls.Add(this._GetDebounceButton);
            this._SubsystemGroupBox.Controls.Add(this._SetDebounceButton);
            this._SubsystemGroupBox.Controls.Add(this._DebounceNumericLabel);
            this._SubsystemGroupBox.Location = new System.Drawing.Point(31, 177);
            this._SubsystemGroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SubsystemGroupBox.Name = "_SubsystemGroupBox";
            this._SubsystemGroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SubsystemGroupBox.Size = new System.Drawing.Size(340, 64);
            this._SubsystemGroupBox.TabIndex = 6;
            this._SubsystemGroupBox.TabStop = false;
            this._SubsystemGroupBox.Text = "GPIO Subsystem Settings";
            // 
            // _DebounceNumeric
            // 
            this._DebounceNumeric.Location = new System.Drawing.Point(146, 23);
            this._DebounceNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._DebounceNumeric.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this._DebounceNumeric.Name = "_DebounceNumeric";
            this._DebounceNumeric.Size = new System.Drawing.Size(63, 23);
            this._DebounceNumeric.TabIndex = 1;
            // 
            // _GetDebounceButton
            // 
            this._GetDebounceButton.Location = new System.Drawing.Point(273, 19);
            this._GetDebounceButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._GetDebounceButton.Name = "_GetDebounceButton";
            this._GetDebounceButton.Size = new System.Drawing.Size(53, 30);
            this._GetDebounceButton.TabIndex = 3;
            this._GetDebounceButton.Text = "Get";
            this._ToolTip.SetToolTip(this._GetDebounceButton, "Gets the debounce interval");
            this._GetDebounceButton.UseVisualStyleBackColor = true;
            this._GetDebounceButton.Click += new System.EventHandler(this.GetDebounceButton_Click);
            // 
            // _SetDebounceButton
            // 
            this._SetDebounceButton.Location = new System.Drawing.Point(215, 19);
            this._SetDebounceButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SetDebounceButton.Name = "_SetDebounceButton";
            this._SetDebounceButton.Size = new System.Drawing.Size(53, 30);
            this._SetDebounceButton.TabIndex = 2;
            this._SetDebounceButton.Text = "Set";
            this._ToolTip.SetToolTip(this._SetDebounceButton, "Sets the debounce interval");
            this._SetDebounceButton.UseVisualStyleBackColor = true;
            this._SetDebounceButton.Click += new System.EventHandler(this.SetDebounceButton_Click);
            // 
            // _DebounceNumericLabel
            // 
            this._DebounceNumericLabel.AutoSize = true;
            this._DebounceNumericLabel.Location = new System.Drawing.Point(14, 27);
            this._DebounceNumericLabel.Name = "_DebounceNumericLabel";
            this._DebounceNumericLabel.Size = new System.Drawing.Size(129, 15);
            this._DebounceNumericLabel.TabIndex = 0;
            this._DebounceNumericLabel.Text = "Debounce Interval [us]:";
            this._DebounceNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _InputPinsTabPage
            // 
            this._InputPinsTabPage.Controls.Add(this._InputPinsLayout);
            this._InputPinsTabPage.Location = new System.Drawing.Point(4, 24);
            this._InputPinsTabPage.Name = "_InputPinsTabPage";
            this._InputPinsTabPage.Size = new System.Drawing.Size(409, 424);
            this._InputPinsTabPage.TabIndex = 2;
            this._InputPinsTabPage.Text = "Inputs";
            this._InputPinsTabPage.UseVisualStyleBackColor = true;
            // 
            // _InputPinsLayout
            // 
            this._InputPinsLayout.ColumnCount = 2;
            this._InputPinsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._InputPinsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._InputPinsLayout.Location = new System.Drawing.Point(73, 36);
            this._InputPinsLayout.Name = "_InputPinsLayout";
            this._InputPinsLayout.RowCount = 2;
            this._InputPinsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._InputPinsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._InputPinsLayout.Size = new System.Drawing.Size(200, 100);
            this._InputPinsLayout.TabIndex = 0;
            // 
            // _OutputPinsTabPage
            // 
            this._OutputPinsTabPage.Controls.Add(this._OutputPinsLayout);
            this._OutputPinsTabPage.Location = new System.Drawing.Point(4, 24);
            this._OutputPinsTabPage.Name = "_OutputPinsTabPage";
            this._OutputPinsTabPage.Size = new System.Drawing.Size(409, 424);
            this._OutputPinsTabPage.TabIndex = 2;
            this._OutputPinsTabPage.Text = "Outputs";
            this._OutputPinsTabPage.UseVisualStyleBackColor = true;
            // 
            // _OutputPinsLayout
            // 
            this._OutputPinsLayout.ColumnCount = 2;
            this._OutputPinsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._OutputPinsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._OutputPinsLayout.Location = new System.Drawing.Point(119, 55);
            this._OutputPinsLayout.Name = "_OutputPinsLayout";
            this._OutputPinsLayout.RowCount = 2;
            this._OutputPinsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._OutputPinsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._OutputPinsLayout.Size = new System.Drawing.Size(200, 100);
            this._OutputPinsLayout.TabIndex = 0;
            // 
            // _EventLogTabPage
            // 
            this._EventLogTabPage.Controls.Add(this._EventLogTextBox);
            this._EventLogTabPage.Location = new System.Drawing.Point(4, 24);
            this._EventLogTabPage.Name = "_EventLogTabPage";
            this._EventLogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._EventLogTabPage.Size = new System.Drawing.Size(409, 424);
            this._EventLogTabPage.TabIndex = 1;
            this._EventLogTabPage.Text = "Events";
            this._EventLogTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTextBox
            // 
            this._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._EventLogTextBox.Location = new System.Drawing.Point(3, 3);
            this._EventLogTextBox.Multiline = true;
            this._EventLogTextBox.Name = "_EventLogTextBox";
            this._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._EventLogTextBox.Size = new System.Drawing.Size(403, 418);
            this._EventLogTextBox.TabIndex = 0;
            this._EventLogTextBox.DoubleClick += new System.EventHandler(this.EventLogTextBox_DoubleClick);
            // 
            // _ErrorProvider
            // 
            this._ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            this._ToolStripContainer.BottomToolStripPanel.Controls.Add(this._BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            this._ToolStripContainer.ContentPanel.Controls.Add(this._Tabs);
            this._ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(417, 452);
            this._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            this._ToolStripContainer.Name = "_ToolStripContainer";
            this._ToolStripContainer.Size = new System.Drawing.Size(417, 506);
            this._ToolStripContainer.TabIndex = 0;
            this._ToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _ToolStripContainer.TopToolStripPanel
            // 
            this._ToolStripContainer.TopToolStripPanel.Controls.Add(this._TopToolStrip);
            // 
            // _TopToolStrip
            // 
            this._TopToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._TopToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._EventPinNumberLabel,
            this._EvetnPinNameLabel,
            this._EventPinValue,
            this._AddPinsSplitButton});
            this._TopToolStrip.Location = new System.Drawing.Point(3, 0);
            this._TopToolStrip.Name = "_TopToolStrip";
            this._TopToolStrip.Size = new System.Drawing.Size(165, 25);
            this._TopToolStrip.TabIndex = 0;
            // 
            // _EventPinNumberLabel
            // 
            this._EventPinNumberLabel.Name = "_EventPinNumberLabel";
            this._EventPinNumberLabel.Size = new System.Drawing.Size(31, 22);
            this._EventPinNumberLabel.Text = "Pin#";
            // 
            // _EvetnPinNameLabel
            // 
            this._EvetnPinNameLabel.Name = "_EvetnPinNameLabel";
            this._EvetnPinNameLabel.Size = new System.Drawing.Size(53, 22);
            this._EvetnPinNameLabel.Text = "<name>";
            // 
            // _EventPinValue
            // 
            this._EventPinValue.BackColor = System.Drawing.Color.Black;
            this._EventPinValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._EventPinValue.ForeColor = System.Drawing.Color.Aqua;
            this._EventPinValue.Name = "_EventPinValue";
            this._EventPinValue.Size = new System.Drawing.Size(13, 25);
            this._EventPinValue.Text = "0";
            // 
            // _AddPinsSplitButton
            // 
            this._AddPinsSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._AddPinsSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._NewPinNumberTextBox,
            this._AddInputPinMenuItem,
            this._AddOutputPinMenuItem});
            this._AddPinsSplitButton.Name = "_AddPinsSplitButton";
            this._AddPinsSplitButton.Size = new System.Drawing.Size(54, 22);
            this._AddPinsSplitButton.Text = "Add...";
            // 
            // _NewPinNumberTextBox
            // 
            this._NewPinNumberTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._NewPinNumberTextBox.Name = "_NewPinNumberTextBox";
            this._NewPinNumberTextBox.Size = new System.Drawing.Size(100, 23);
            this._NewPinNumberTextBox.Text = "0";
            this._NewPinNumberTextBox.ToolTipText = "Enter a new pin number to add";
            this._NewPinNumberTextBox.TextChanged += new System.EventHandler(this.NewPinNumberTextBox_TextChanged);
            // 
            // _AddInputPinMenuItem
            // 
            this._AddInputPinMenuItem.Name = "_AddInputPinMenuItem";
            this._AddInputPinMenuItem.Size = new System.Drawing.Size(160, 22);
            this._AddInputPinMenuItem.Text = "Add Input Pin";
            this._AddInputPinMenuItem.Click += new System.EventHandler(this.AddInputPinMenuItem_Click);
            // 
            // _AddOutputPinMenuItem
            // 
            this._AddOutputPinMenuItem.Name = "_AddOutputPinMenuItem";
            this._AddOutputPinMenuItem.Size = new System.Drawing.Size(160, 22);
            this._AddOutputPinMenuItem.Text = "Add Output Pin";
            this._AddOutputPinMenuItem.Click += new System.EventHandler(this.AddOutputPinMenuItem_Click);
            // 
            // GpioPinsView
            // 
            this.Controls.Add(this._ToolStripContainer);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GpioPinsView";
            this.Size = new System.Drawing.Size(417, 506);
            this._BottomToolStrip.ResumeLayout(false);
            this._BottomToolStrip.PerformLayout();
            this._Tabs.ResumeLayout(false);
            this._PrimaryTabPage.ResumeLayout(false);
            this._PrimaryTabLayout.ResumeLayout(false);
            this._SubsystemGroupBox.ResumeLayout(false);
            this._SubsystemGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._DebounceNumeric)).EndInit();
            this._InputPinsTabPage.ResumeLayout(false);
            this._OutputPinsTabPage.ResumeLayout(false);
            this._EventLogTabPage.ResumeLayout(false);
            this._EventLogTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).EndInit();
            this._ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            this._ToolStripContainer.BottomToolStripPanel.PerformLayout();
            this._ToolStripContainer.ContentPanel.ResumeLayout(false);
            this._ToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this._ToolStripContainer.TopToolStripPanel.PerformLayout();
            this._ToolStripContainer.ResumeLayout(false);
            this._ToolStripContainer.PerformLayout();
            this._TopToolStrip.ResumeLayout(false);
            this._TopToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        private ToolStrip _BottomToolStrip;
        private ToolStripTextBox _DeviceInfoTextBox;
        private ErrorProvider _ErrorProvider;
        private ToolTip _ToolTip;
        private ToolStripButton _OpenDeviceSubsystemTypeButton;
        private TabPage _PrimaryTabPage;
        private TabPage _EventLogTabPage;
        private TextBox _EventLogTextBox;
        private ToolStripContainer _ToolStripContainer;
        private isr.Diolan.WinControls.ExtendedTabControl _Tabs;
        private ToolStripComboBox _DevicesComboBox;
        private ToolStripSplitButton _SelectServerButton;
        private ToolStripTextBox _ServerNameTextBox;
        private ToolStripMenuItem _DefaultServerMenuItem;
        private ToolStripSplitButton _SelectDeviceSplitButton;
        private GroupBox _SubsystemGroupBox;
        private NumericUpDown _DebounceNumeric;
        private Button _GetDebounceButton;
        private Button _SetDebounceButton;
        private Label _DebounceNumericLabel;
        private TableLayoutPanel _PrimaryTabLayout;
        private TabPage _InputPinsTabPage;
        private TabPage _OutputPinsTabPage;
        private ToolStripComboBox _TabComboBox;
        private ToolStripButton _ConnectServerButton;
        private ToolStrip _TopToolStrip;
        private ToolStripLabel _EventPinNumberLabel;
        private ToolStripLabel _EvetnPinNameLabel;
        private ToolStripTextBox _EventPinValue;
        private ToolStripSplitButton _AddPinsSplitButton;
        private ToolStripTextBox _NewPinNumberTextBox;
        private ToolStripMenuItem _AddInputPinMenuItem;
        private ToolStripMenuItem _AddOutputPinMenuItem;
        private TableLayoutPanel _InputPinsLayout;
        private TableLayoutPanel _OutputPinsLayout;
    }
}
