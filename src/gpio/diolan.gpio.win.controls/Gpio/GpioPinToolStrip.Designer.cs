using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Diolan.Gpio.WinControls
{

    public partial class GpioPinToolStrip
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ToolStrip = new ToolStrip();
            _ConfigureButton = new ToolStripButton();
            _PinNumberLabel = new ToolStripLabel();
            _Separator1 = new ToolStripSeparator();
            _PinNameLabel = new ToolStripLabel();
            _LogicalStateButton = new ToolStripButton();
            _ActiveLogicToggleButton = new ToolStripButton();
            _ErrorProvider = new ErrorProvider(components);
            _ToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            SuspendLayout();
            // 
            // _ToolStrip
            // 
            _ToolStrip.BackColor = System.Drawing.SystemColors.Control;
            _ToolStrip.Dock = DockStyle.Fill;
            _ToolStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { _ConfigureButton, _PinNumberLabel, _Separator1, _PinNameLabel, _LogicalStateButton, _ActiveLogicToggleButton });
            _ToolStrip.Location = new System.Drawing.Point(0, 0);
            _ToolStrip.Margin = new Padding(3);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Padding = new Padding(0);
            _ToolStrip.Size = new System.Drawing.Size(230, 31);
            _ToolStrip.Stretch = true;
            _ToolStrip.TabIndex = 0;
            _ToolStrip.Text = "Tool Strip";
            // 
            // _ConfigureButton
            // 
            _ConfigureButton.AutoSize = false;
            _ConfigureButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _ConfigureButton.Image = isr.Diolan.Gpio.WinControls.Properties.Resources.configure_3;
            _ConfigureButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ConfigureButton.Margin = new Padding(0);
            _ConfigureButton.Name = "_ConfigureButton";
            _ConfigureButton.Overflow = ToolStripItemOverflow.Never;
            _ConfigureButton.Size = new System.Drawing.Size(24, 24);
            _ConfigureButton.Text = "Configure";
            _ConfigureButton.Click += new EventHandler( ConfigureButton_Click );
            // 
            // _PinNumberLabel
            // 
            _PinNumberLabel.Margin = new Padding(1, 1, 1, 2);
            _PinNumberLabel.Name = "_PinNumberLabel";
            _PinNumberLabel.Size = new System.Drawing.Size(22, 28);
            _PinNumberLabel.Text = "00";
            _PinNumberLabel.ToolTipText = "Pin number";
            // 
            // _Separator1
            // 
            _Separator1.Name = "_Separator1";
            _Separator1.Size = new System.Drawing.Size(6, 27);
            // 
            // _PinNameLabel
            // 
            _PinNameLabel.Margin = new Padding(1);
            _PinNameLabel.Name = "_PinNameLabel";
            _PinNameLabel.Overflow = ToolStripItemOverflow.Never;
            _PinNameLabel.Size = new System.Drawing.Size(58, 29);
            _PinNameLabel.Text = "<name>";
            _PinNameLabel.ToolTipText = "Pin name";
            // 
            // _LogicalStateButton
            // 
            _LogicalStateButton.Alignment = ToolStripItemAlignment.Right;
            _LogicalStateButton.CheckOnClick = true;
            _LogicalStateButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _LogicalStateButton.Image = isr.Diolan.Gpio.WinControls.Properties.Resources.circle_grey;
            _LogicalStateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _LogicalStateButton.Margin = new Padding(1);
            _LogicalStateButton.Name = "_LogicalStateButton";
            _LogicalStateButton.Size = new System.Drawing.Size(23, 29);
            _LogicalStateButton.Text = "Logical State";
            _LogicalStateButton.CheckStateChanged += new EventHandler( LogicalStateButton_CheckStateChanged );
            _LogicalStateButton.Click += new EventHandler( LogicalStateButton_Click );
            // 
            // _ActiveLogicToggleButton
            // 
            _ActiveLogicToggleButton.Alignment = ToolStripItemAlignment.Right;
            _ActiveLogicToggleButton.CheckOnClick = true;
            _ActiveLogicToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _ActiveLogicToggleButton.Image = isr.Diolan.Gpio.WinControls.Properties.Resources.Minus_Grey;
            _ActiveLogicToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ActiveLogicToggleButton.Margin = new Padding(1);
            _ActiveLogicToggleButton.Name = "_ActiveLogicToggleButton";
            _ActiveLogicToggleButton.Size = new System.Drawing.Size(23, 29);
            _ActiveLogicToggleButton.Text = "Active Logic";
            _ActiveLogicToggleButton.CheckStateChanged += new EventHandler( ActiveLogicToggleButton_CheckStateChanged );
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // GpioPinToolStrip
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.Fixed3D;
            Controls.Add(_ToolStrip);
            Margin = new Padding(0);
            Name = "GpioPinToolStrip";
            Size = new System.Drawing.Size(230, 31);
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _ToolStrip;
        private ToolStripButton _ConfigureButton;
        private ToolStripLabel _PinNumberLabel;
        private ToolStripLabel _PinNameLabel;
        private ToolStripButton _LogicalStateButton;
        private ToolStripButton _ActiveLogicToggleButton;
        private ErrorProvider _ErrorProvider;
        private ToolStripSeparator _Separator1;
    }
}
