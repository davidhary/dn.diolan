using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.Diolan.SubsystemExtensions;
using isr.Diolan.WinControls.ComboBoxEnumExtensions;
using isr.Diolan.WinControls.ErrorProviderExtensions;
using isr.Diolan.WinControls.SubsystemExtensions;

namespace isr.Diolan.Gpio.WinControls
{

    /// <summary> Gpio pin control. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-05 </para>
    /// </remarks>
    public partial class GpioPinControl : System.Windows.Forms.UserControl, INotifyPropertyChanged
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-11-23. </remarks>
        public GpioPinControl()
        {
            this.InitializeComponent();
        }

        #region " CONSTRUCTION "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                if ( !this.IsDisposed && disposing )
                {
                    this.components?.Dispose();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        private void OnDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.CloseSubsystemType();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " TOOL STRIP "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        private Dln.Device Device { get; set; }

        /// <summary>   Gets or sets the device. </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <param name="device">   The device. </param>
        [CLSCompliant( false )]
        public void OpenSubsystemType( Dln.Device device )
        {
            this.Device = device;
            this._PinComboBox.ComboBox.DataSource = null;
            this._ErrorProvider.Clear();

            // Get port count
            if ( this.Device.Gpio.Pins.Count == 0 )
            {
                // this is already done when opening the device.
                _ = this._ErrorProvider.Annunciate( this._PinComboBox, $"Adapter '{this.Device.Caption()}' doesn't support GPIO interface." );
                this.Device = null;
            }
            else
            {
                this.Device.Gpio.Pins.ListNumbers( this._PinComboBox.ComboBox );
                // Me._PinComboBox.ComboBox.ListNumbers(Me.Device.Gpio.Pins)

                this._OutputValueComboBox.ComboBox.DataSource = null;
                _ = this._OutputValueComboBox.ComboBox.ListEnumNames<PinValue>();
                this._PinComboBox.SelectedIndex = 0;
            }
        }

        /// <summary> Closes the subsystem type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void CloseSubsystemType()
        {
            if ( this._GpioSubsystem is object )
            {
                this._GpioSubsystem.Dispose();
                this._GpioSubsystem = null;
            }

            this._PinComboBox.ComboBox.DataSource = null;
            this._PinComboBox.ComboBox.Items.Clear();
            this._OutputValueComboBox.ComboBox.DataSource = null;
            this._OutputValueComboBox.ComboBox.Items.Clear();
            this.Device = null;
            this._ErrorProvider.Clear();
        }

        /// <summary> the subsystem type. </summary>
        /// <value> the subsystem type. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SubsystemTypes SubsystemType { get; set; } = SubsystemTypes.Gpio;

        /// <summary> Queries if a Subsystem Type is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
        private bool IsDeviceSubsystemTypeOpen()
        {
            return this.Device is object && this._PinComboBox.ComboBox.DataSource is object && this._PinComboBox.Items.Count > 0;
        }

        /// <summary> Pin combo box selected index changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PinComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen() )
                {
                    int pinNumber = this._PinComboBox.SelectedIndex;
                    if ( pinNumber >= 0 && pinNumber < this.Device.Gpio.Pins.Count )
                    {
                        var pin = this.Device.Gpio.Pins[pinNumber];
                        this._ValueTextBox.ReadOnly = true;
                        this._SetOutputButton.Visible = pin.Direction == ( int ) PinDirection.Output;
                        this._OutputValueComboBoxLabel.Visible = pin.Direction == ( int ) PinDirection.Output;
                        this._OutputValueComboBox.Visible = pin.Direction == ( int ) PinDirection.Output;
                        this._OutputValueComboBox.Enabled = pin.Enabled;
                        this._PulseDurationTextBoxLabel.Visible = pin.Direction == ( int ) PinDirection.Output;
                        this._PulseDurationTextBox.Visible = pin.Direction == ( int ) PinDirection.Output;
                        this._PulseDurationTextBox.Enabled = pin.Enabled;
                        this._StartButton.Visible = pin.Direction == ( int ) PinDirection.Output;
                        this._StartButton.Enabled = pin.Enabled && pin.Direction == ( int ) PinDirection.Output;
                    }
                    else if ( this._ErrorProvider is object )
                    {
                        _ = this._ErrorProvider.Annunciate( sender, "Pin index {0} is out of range of [0,{1}]", ( object ) (this.Device.Gpio.Pins.Count - 1) );
                    }
                }
                else if ( this._PinComboBox.SelectedIndex >= 0 && this._ErrorProvider is object )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Sets output button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SetOutputButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen() )
                {
                    int pinNumber = this._PinComboBox.SelectedIndex;
                    if ( pinNumber >= 0 && pinNumber < this.Device.Gpio.Pins.Count )
                    {
                        var pin = this.Device.Gpio.Pins[pinNumber];
                        pin.OutputValue = this._OutputValueComboBox.SelectedIndex;
                        this._OutputValueComboBox.SelectedIndex = pin.OutputValue;
                        this._ValueTextBox.Text = pin.Value.ToString();
                    }
                    else
                    {
                        _ = this._ErrorProvider.Annunciate( sender, "Pin index {0} is out of range of [0,{1}]", ( object ) (this.Device.Gpio.Pins.Count - 1) );
                    }
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Gets value button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void GetValueButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen() )
                {
                    int pinNumber = this._PinComboBox.SelectedIndex;
                    if ( pinNumber >= 0 && pinNumber < this.Device.Gpio.Pins.Count )
                    {
                        var pin = this.Device.Gpio.Pins[pinNumber];
                        this._OutputValueComboBox.SelectedIndex = pin.OutputValue;
                        this._ValueTextBox.Text = pin.Value.ToString();
                    }
                    else
                    {
                        _ = this._ErrorProvider.Annunciate( sender, "Pin index {0} is out of range of [0,{1}]", ( object ) (this.Device.Gpio.Pins.Count - 1) );
                    }
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> The gpio subsystem. </summary>
        private GpioSubsystem _GpioSubsystem;

        /// <summary> Starts button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void StartButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen() )
                {
                    int pinNumber = this._PinComboBox.SelectedIndex;
                    if ( pinNumber >= 0 && pinNumber < this.Device.Gpio.Pins.Count )
                    {
                        if ( this._GpioSubsystem is null )
                            this._GpioSubsystem = new GpioSubsystem();
                        if ( double.TryParse( this._PulseDurationTextBox.Text, out double duration ) )
                        {
                            this._GpioSubsystem.PulsePin( this.Device.Gpio.Pins[pinNumber], TimeSpan.FromMilliseconds( duration ) );
                        }
                        else
                        {
                            _ = this._ErrorProvider.Annunciate( sender, "Invalid duration: {0}", this._PulseDurationTextBox.Text );
                        }
                    }
                    else
                    {
                        _ = this._ErrorProvider.Annunciate( sender, "Pin index {0} is out of range of [0,{1}]", ( object ) (this.Device.Gpio.Pins.Count - 1) );
                    }
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        #endregion

    }
}
