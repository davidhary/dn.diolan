using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Diolan.Gpio.WinControls
{

    public partial class GpioPinControl
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._ToolStrip = new System.Windows.Forms.ToolStrip();
            this._PinComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            this._PinComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._OutputValueComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            this._OutputValueComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._SetOutputButton = new System.Windows.Forms.ToolStripButton();
            this._PulseDurationTextBoxLabel = new System.Windows.Forms.ToolStripLabel();
            this._PulseDurationTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._StartButton = new System.Windows.Forms.ToolStripButton();
            this._ValueTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._GetValueButton = new System.Windows.Forms.ToolStripButton();
            this._ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this._ToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // _ToolStrip
            // 
            this._ToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._PinComboBoxLabel,
            this._PinComboBox,
            this._OutputValueComboBoxLabel,
            this._OutputValueComboBox,
            this._SetOutputButton,
            this._PulseDurationTextBoxLabel,
            this._PulseDurationTextBox,
            this._StartButton,
            this._ValueTextBox,
            this._GetValueButton});
            this._ToolStrip.Location = new System.Drawing.Point(0, 0);
            this._ToolStrip.Name = "_ToolStrip";
            this._ToolStrip.Size = new System.Drawing.Size(401, 25);
            this._ToolStrip.TabIndex = 10;
            this._ToolStrip.Text = "ToolStrip1";
            // 
            // _PinComboBoxLabel
            // 
            this._PinComboBoxLabel.Name = "_PinComboBoxLabel";
            this._PinComboBoxLabel.Size = new System.Drawing.Size(27, 22);
            this._PinComboBoxLabel.Text = "Pin:";
            // 
            // _PinComboBox
            // 
            this._PinComboBox.AutoSize = false;
            this._PinComboBox.DropDownWidth = 25;
            this._PinComboBox.Margin = new System.Windows.Forms.Padding(1, 0, 3, 0);
            this._PinComboBox.Name = "_PinComboBox";
            this._PinComboBox.Size = new System.Drawing.Size(35, 23);
            this._PinComboBox.ToolTipText = "Pin number";
            this._PinComboBox.SelectedIndexChanged += new System.EventHandler(this.PinComboBox_SelectedIndexChanged);
            // 
            // _OutputValueComboBoxLabel
            // 
            this._OutputValueComboBoxLabel.Name = "_OutputValueComboBoxLabel";
            this._OutputValueComboBoxLabel.Size = new System.Drawing.Size(30, 22);
            this._OutputValueComboBoxLabel.Text = "Out:";
            // 
            // _OutputValueComboBox
            // 
            this._OutputValueComboBox.AutoSize = false;
            this._OutputValueComboBox.DropDownWidth = 55;
            this._OutputValueComboBox.Name = "_OutputValueComboBox";
            this._OutputValueComboBox.Size = new System.Drawing.Size(65, 23);
            this._OutputValueComboBox.Text = "Logic 0";
            this._OutputValueComboBox.ToolTipText = "Output value";
            // 
            // _SetOutputButton
            // 
            this._SetOutputButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._SetOutputButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SetOutputButton.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this._SetOutputButton.Name = "_SetOutputButton";
            this._SetOutputButton.Size = new System.Drawing.Size(27, 22);
            this._SetOutputButton.Text = "Set";
            this._SetOutputButton.ToolTipText = "Set the output";
            this._SetOutputButton.Click += new System.EventHandler(this.SetOutputButton_Click);
            // 
            // _PulseDurationTextBoxLabel
            // 
            this._PulseDurationTextBoxLabel.Name = "_PulseDurationTextBoxLabel";
            this._PulseDurationTextBoxLabel.Size = new System.Drawing.Size(60, 22);
            this._PulseDurationTextBoxLabel.Text = "Pulse, ms:";
            // 
            // _PulseDurationTextBox
            // 
            this._PulseDurationTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._PulseDurationTextBox.Name = "_PulseDurationTextBox";
            this._PulseDurationTextBox.Size = new System.Drawing.Size(49, 25);
            this._PulseDurationTextBox.Text = "10";
            this._PulseDurationTextBox.ToolTipText = "Pulse duration in ms.";
            // 
            // _StartButton
            // 
            this._StartButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._StartButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._StartButton.Name = "_StartButton";
            this._StartButton.Size = new System.Drawing.Size(35, 22);
            this._StartButton.Text = "Start";
            this._StartButton.ToolTipText = "Start a single pulse.";
            this._StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // _ValueTextBox
            // 
            this._ValueTextBox.BackColor = System.Drawing.Color.Black;
            this._ValueTextBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ValueTextBox.ForeColor = System.Drawing.Color.Aqua;
            this._ValueTextBox.Name = "_ValueTextBox";
            this._ValueTextBox.Size = new System.Drawing.Size(25, 25);
            this._ValueTextBox.Text = "0";
            this._ValueTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._ValueTextBox.ToolTipText = "Value";
            // 
            // _GetValueButton
            // 
            this._GetValueButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._GetValueButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._GetValueButton.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this._GetValueButton.Name = "_GetValueButton";
            this._GetValueButton.Size = new System.Drawing.Size(29, 22);
            this._GetValueButton.Text = "Get";
            this._GetValueButton.ToolTipText = "Get input or output value";
            this._GetValueButton.Click += new System.EventHandler(this.GetValueButton_Click);
            // 
            // _ErrorProvider
            // 
            this._ErrorProvider.ContainerControl = this;
            // 
            // GpioPinControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._ToolStrip);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "GpioPinControl";
            this.Size = new System.Drawing.Size(444, 24);
            this._ToolStrip.ResumeLayout(false);
            this._ToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private ToolStrip _ToolStrip;
        private ToolStripLabel _PinComboBoxLabel;
        private ToolStripComboBox _PinComboBox;
        private ToolStripLabel _OutputValueComboBoxLabel;
        private ToolStripComboBox _OutputValueComboBox;
        private ToolStripButton _SetOutputButton;
        private ToolStripLabel _PulseDurationTextBoxLabel;
        private ToolStripTextBox _PulseDurationTextBox;
        private ToolStripButton _StartButton;
        private ToolStripTextBox _ValueTextBox;
        private ToolStripButton _GetValueButton;
        private ErrorProvider _ErrorProvider;
    }
}
