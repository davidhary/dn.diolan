using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Diolan.Gpio.WinControls
{

    /// <summary> Dialog for setting the gpio pin configuration. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-04-07 </para>
    /// </remarks>
    public partial class GpioPinConfigDialog : Form
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-11-23. </remarks>
        public GpioPinConfigDialog()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                    this.components?.Dispose();
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Shows the dialog. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="owner"> The owner. </param>
        /// <param name="pin">   The pin. </param>
        /// <returns> A DialogResult. </returns>
        public DialogResult ShowDialog( IWin32Window owner, isr.Diolan.Gpio.GpioPin pin )
        {
            this._GpioPinConfigControl.Configure( pin );
            return this.ShowDialog( owner );
        }
    }
}
