using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.Diolan.SubsystemExtensions;
using isr.Diolan.WinControls.ComboBoxEnumExtensions;
using isr.Diolan.WinControls.ErrorProviderExtensions;

using isr.Diolan.WinControls.SubsystemExtensions;

namespace isr.Diolan.Gpio.WinControls
{

    /// <summary> User interface for managing a GPIO Handler. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    public partial class GpioView
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.OnSubsystemTypeClosed();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " SUBSYSTEM TYPE "

        /// <summary> the subsystem type. </summary>
        /// <value> the subsystem type. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SubsystemTypes SubsystemType { get; set; } = SubsystemTypes.Gpio;

        /// <summary> Executes the subsystem type closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeClosed()
        {
            this._GpioPinControl1.CloseSubsystemType();
            this._GpioPinControl2.CloseSubsystemType();
            this._PinComboBox.DataSource = null;
            this._PinComboBox.Items.Clear();
            this._PinDirectionComboBox.DataSource = null;
            this._PinDirectionComboBox.Items.Clear();
            this._EventTypeComboBox.DataSource = null;
            this._EventTypeComboBox.Items.Clear();
            this.Device = null;
            this.OnSubsystemTypeConnectionChanged();
        }

        /// <summary> Executes the subsystem type closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnSubsystemTypeClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsSubsystemTypeOpen() )
                {
                    // un-register event handlers for all pins
                    foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                        pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                }
            }
        }

        /// <summary> Executes the subsystem type opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeOpening()
        {

            // Fill controls
            this._PinDirectionComboBox.DataSource = null;
            this._PinDirectionComboBox.Items.Clear();
            _ = this._PinDirectionComboBox.ListEnumNames<PinDirection>();
            this.Device = this.DeviceConnectorInternal.Device;
            this._PinComboBox.DataSource = null;
            this._PinComboBox.Items.Clear();
            this._ErrorProvider.Clear();

            // Get port count
            if ( this.Device.Gpio.Pins.Count == 0 )
            {
                // this is already done when opening the device.
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, "Adapter '{0}' doesn't support GPIO interface.", this.Device.Caption() );
                this.Device = null;
                this._DeviceInfoTextBox.Text = "not supported";
            }
            else
            {
                this._DeviceInfoTextBox.Text = this.Device.Caption();
                this.Device.Gpio.Pins.ListNumbers( this._PinComboBox );
                this._GpioPinControl1.OpenSubsystemType( this.Device );
                this._GpioPinControl2.OpenSubsystemType( this.Device );

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handler for all pins
                foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                    pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;

                // Get subsystem parameters: Debounce
                this.ReadDebounceProperties();
                this._PinComboBox.SelectedIndex = 0;
            }
        }

        /// <summary> Queries if a Subsystem Type is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
        private bool IsSubsystemTypeOpen()
        {
            return this._PinComboBox.DataSource is object && this._PinComboBox.Items.Count > 0;
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Gpio.ConditionMetEventArgs e )
        {
            this.AppendEvent( e );
        }

        /// <summary> Appends an event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Condition met event information. </param>
        private void AppendEvent( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                string data = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}";

                // This event is handled in main thread,
                // so it is not needed to invoke when modifying form's controls.
                this._EventLogTextBox.AppendText( data );
            }
        }

        #endregion

        #region " SUBSYSTEM "

        /// <summary> Reads the debounce properties from the device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void ReadDebounceProperties()
        {
            // Get subsystem parameters: Debounce
            if ( this.Device.Gpio.Restrictions.Debounce == Dln.Restriction.NotSupported )
            {
                this._DebounceNumeric.Enabled = false;
                this._SetDebounceButton.Enabled = false;
                this._GetDebounceButton.Enabled = false;
            }
            else
            {
                this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
            }
        }

        /// <summary> Gets debounce button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void GetDebounceButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, $"Device not open for {this.SubsystemType}" );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Sets debounce button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SetDebounceButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    this.Device.Gpio.Debounce = ( int ) this._DebounceNumeric.Value;
                    this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, $"Device not open for {this.SubsystemType}" );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        #endregion

        #region " PIN "

        /// <summary> Gets pin configuration button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void GetPinConfigButton_Click( object sender, EventArgs e )
        {
            // Get pin parameters
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    int pinNumber = this._PinComboBox.SelectedIndex;
                    if ( pinNumber >= 0 && pinNumber < this.Device.Gpio.Pins.Count )
                    {
                        this.ReadPinProperties( this.Device.Gpio.Pins[pinNumber] );
                    }
                    else
                    {
                        _ = this._ErrorProvider.Annunciate( sender, "Pin index {0} is out of range of [0,{1}]", ( object ) (this.Device.Gpio.Pins.Count - 1) );
                    }
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Reads pin number properties. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pin"> The pin. </param>
        private void ReadPinProperties( Dln.Gpio.Pin pin )
        {
            // Main settings are present in all devices,
            // check availability only for extra settings like pull downs, debounce and events.

            this._PinEnabledCheckBox.Checked = pin.Enabled;
            this._PinDirectionComboBox.SelectedIndex = pin.Direction;

            // Pull up
            if ( pin.Restrictions.PullupEnabled == Dln.Restriction.NotSupported )
            {
                this._PinPullupCheckBox.Enabled = false;
            }
            else
            {
                this._PinPullupCheckBox.Enabled = true;
                this._PinPullupCheckBox.Checked = pin.PullupEnabled;
            }

            // Pull down
            if ( pin.Restrictions.PulldownEnabled == Dln.Restriction.NotSupported )
            {
                this._PulldownCheckBox.Enabled = false;
            }
            else
            {
                this._PulldownCheckBox.Enabled = true;
                this._PulldownCheckBox.Checked = pin.PulldownEnabled;
            }

            // Open drain
            if ( pin.Restrictions.OpendrainEnabled == Dln.Restriction.NotSupported )
            {
                this._OpenDrainCheckBox.Enabled = false;
            }
            else
            {
                this._OpenDrainCheckBox.Enabled = true;
                this._OpenDrainCheckBox.Checked = pin.OpendrainEnabled;
            }

            // Debounce
            if ( pin.Restrictions.DebounceEnabled == Dln.Restriction.NotSupported )
            {
                this._ToolTip.SetToolTip( this._DebounceEnabledCheckBox, "Not supported." );
            }
            else if ( pin.Restrictions.DebounceEnabled == Dln.Restriction.NoRestriction )
            {
                this._ToolTip.SetToolTip( this._DebounceEnabledCheckBox, "Enabled if checked." );
            }

            if ( pin.Restrictions.DebounceEnabled == Dln.Restriction.NotSupported )
            {
                this._DebounceEnabledCheckBox.Enabled = false;
            }
            else if ( pin.Restrictions.DebounceEnabled == Dln.Restriction.MustBeDisabled )
            {
                this._DebounceEnabledCheckBox.Enabled = false;
            }
            else
            {
                this._DebounceEnabledCheckBox.Enabled = true;
                this._DebounceEnabledCheckBox.Checked = pin.DebounceEnabled;
            }

            // Event type
            if ( pin.Restrictions.EventType == Dln.Restriction.NotSupported )
            {
                this._EventTypeComboBox.Enabled = false;
            }
            else
            {
                this._EventTypeComboBox.Enabled = true;
                this._EventTypeComboBox.DataSource = pin.SupportedEventTypes;
                this._EventTypeComboBox.SelectedItem = pin.EventType;
            }

            // Event period
            if ( pin.Restrictions.EventPeriod == Dln.Restriction.NotSupported )
            {
                this._EventPeriodNumeric.Enabled = false;
            }
            else
            {
                this._EventPeriodNumeric.Enabled = true;
                this._EventPeriodNumeric.Value = pin.EventPeriod;
            }
        }

        /// <summary> Applies the pin configuration described by pin. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pin"> The pin. </param>
        private void ApplyPinConfiguration( Dln.Gpio.Pin pin )
        {
            if ( pin is object )
            {
                // Set common controls
                pin.Enabled = this._PinEnabledCheckBox.Checked;
                pin.Direction = this._PinDirectionComboBox.SelectedIndex;

                // Extra functions are not present if we disable controls when getting settings
                if ( this._PinPullupCheckBox.Enabled )
                {
                    pin.PullupEnabled = this._PinPullupCheckBox.Checked;
                }

                if ( this._PulldownCheckBox.Enabled )
                {
                    pin.PulldownEnabled = this._PulldownCheckBox.Checked;
                }

                if ( this._OpenDrainCheckBox.Enabled )
                {
                    pin.OpendrainEnabled = this._OpenDrainCheckBox.Checked;
                }

                if ( this._DebounceEnabledCheckBox.Enabled )
                {
                    pin.DebounceEnabled = this._DebounceEnabledCheckBox.Checked;
                }

                if ( this._EventTypeComboBox.Enabled )
                {
                    pin.SetEventConfiguration( ( Dln.Gpio.EventType ) Convert.ToByte( this._EventTypeComboBox.SelectedItem ), Convert.ToUInt16( this._EventPeriodNumeric.Value ) );
                }
            }
        }

        /// <summary> Sets pin configuration button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SetPinConfigButton_Click( object sender, EventArgs e )
        {
            // Set pin configuration
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    int pinNumber = this._PinComboBox.SelectedIndex;
                    if ( pinNumber >= 0 && pinNumber < this.Device.Gpio.Pins.Count )
                    {
                        this.ApplyPinConfiguration( this.Device.Gpio.Pins[pinNumber] );
                    }
                    else
                    {
                        _ = this._ErrorProvider.Annunciate( sender, "Pin index {0} is out of range of [0,{1}]", ( object ) (this.Device.Gpio.Pins.Count - 1) );
                    }
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Pin combo box selected index changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void PinComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    int pinNumber = this._PinComboBox.SelectedIndex;
                    if ( pinNumber >= 0 && pinNumber < this.Device.Gpio.Pins.Count )
                    {
                        this.ReadPinProperties( this.Device.Gpio.Pins[pinNumber] );
                    }
                    else
                    {
                        _ = this._ErrorProvider.Annunciate( sender, "Pin index {0} is out of range of [0,{1}]", ( object ) (this.Device.Gpio.Pins.Count - 1) );
                    }
                }
                else if ( this.IsSubsystemTypeOpen() && this._ErrorProvider is object )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        #endregion

    }
}
