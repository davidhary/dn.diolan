using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Diolan.Gpio.WinControls
{
    public partial class GpioView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._BottomToolStrip = new System.Windows.Forms.ToolStrip();
            this._TabComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._SelectServerButton = new System.Windows.Forms.ToolStripSplitButton();
            this._ServerNameTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._DefaultServerMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._ConnectServerButton = new System.Windows.Forms.ToolStripButton();
            this._OpenDeviceSubsystemTypeButton = new System.Windows.Forms.ToolStripButton();
            this._SelectDeviceSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this._DevicesComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._DeviceInfoTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._Tabs = new isr.Diolan.WinControls.ExtendedTabControl();
            this._PrimaryTabPage = new System.Windows.Forms.TabPage();
            this._PrimaryTabLayout = new System.Windows.Forms.TableLayoutPanel();
            this._SubsystemGroupBox = new System.Windows.Forms.GroupBox();
            this._DebounceNumeric = new System.Windows.Forms.NumericUpDown();
            this._GetDebounceButton = new System.Windows.Forms.Button();
            this._SetDebounceButton = new System.Windows.Forms.Button();
            this._DebounceNumericLabel = new System.Windows.Forms.Label();
            this._PinTabPage = new System.Windows.Forms.TabPage();
            this._PinTabLayout = new System.Windows.Forms.TableLayoutPanel();
            this._PinConfigGroupBox = new System.Windows.Forms.GroupBox();
            this._OpenDrainCheckBox = new System.Windows.Forms.CheckBox();
            this._PinPullupCheckBox = new System.Windows.Forms.CheckBox();
            this._EventPeriodNumeric = new System.Windows.Forms.NumericUpDown();
            this._PulldownCheckBox = new System.Windows.Forms.CheckBox();
            this._DebounceEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this._EventPeriodNumericLabel = new System.Windows.Forms.Label();
            this._EventTypeComboBoxLabel = new System.Windows.Forms.Label();
            this._EventTypeComboBox = new System.Windows.Forms.ComboBox();
            this._GetPinConfigButton = new System.Windows.Forms.Button();
            this._SetPinConfigButton = new System.Windows.Forms.Button();
            this._PinDirectionComboBox = new System.Windows.Forms.ComboBox();
            this._PinDirectionComboBoxLabel = new System.Windows.Forms.Label();
            this._PinEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this._PinComboBox = new System.Windows.Forms.ComboBox();
            this._PinComboBoxLabel = new System.Windows.Forms.Label();
            this._EventLogTabPage = new System.Windows.Forms.TabPage();
            this._EventLogTextBox = new System.Windows.Forms.TextBox();
            this._ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this._GpioPinControl2 = new isr.Diolan.Gpio.WinControls.GpioPinControl();
            this._GpioPinControl1 = new isr.Diolan.Gpio.WinControls.GpioPinControl();
            this._BottomToolStrip.SuspendLayout();
            this._Tabs.SuspendLayout();
            this._PrimaryTabPage.SuspendLayout();
            this._PrimaryTabLayout.SuspendLayout();
            this._SubsystemGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._DebounceNumeric)).BeginInit();
            this._PinTabPage.SuspendLayout();
            this._PinTabLayout.SuspendLayout();
            this._PinConfigGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._EventPeriodNumeric)).BeginInit();
            this._EventLogTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).BeginInit();
            this._ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            this._ToolStripContainer.ContentPanel.SuspendLayout();
            this._ToolStripContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // _BottomToolStrip
            // 
            this._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._BottomToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._TabComboBox,
            this._SelectServerButton,
            this._ConnectServerButton,
            this._OpenDeviceSubsystemTypeButton,
            this._SelectDeviceSplitButton,
            this._DeviceInfoTextBox});
            this._BottomToolStrip.Location = new System.Drawing.Point(0, 0);
            this._BottomToolStrip.Name = "_BottomToolStrip";
            this._BottomToolStrip.Size = new System.Drawing.Size(417, 29);
            this._BottomToolStrip.Stretch = true;
            this._BottomToolStrip.TabIndex = 0;
            // 
            // _TabComboBox
            // 
            this._TabComboBox.Name = "_TabComboBox";
            this._TabComboBox.Size = new System.Drawing.Size(81, 29);
            this._TabComboBox.ToolTipText = "Select panel";
            this._TabComboBox.SelectedIndexChanged += new System.EventHandler(this.TabComboBox_SelectedIndexChanged);
            // 
            // _SelectServerButton
            // 
            this._SelectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectServerButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ServerNameTextBox,
            this._DefaultServerMenuItem});
            this._SelectServerButton.Image = global::isr.Diolan.Gpio.WinControls.Properties.Resources.network_server;
            this._SelectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectServerButton.Name = "_SelectServerButton";
            this._SelectServerButton.Size = new System.Drawing.Size(38, 26);
            this._SelectServerButton.Text = "Select Server";
            // 
            // _ServerNameTextBox
            // 
            this._ServerNameTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._ServerNameTextBox.Name = "_ServerNameTextBox";
            this._ServerNameTextBox.Size = new System.Drawing.Size(100, 23);
            this._ServerNameTextBox.Text = "localhost:9656";
            // 
            // _DefaultServerMenuItem
            // 
            this._DefaultServerMenuItem.Checked = true;
            this._DefaultServerMenuItem.CheckOnClick = true;
            this._DefaultServerMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._DefaultServerMenuItem.Name = "_DefaultServerMenuItem";
            this._DefaultServerMenuItem.Size = new System.Drawing.Size(198, 22);
            this._DefaultServerMenuItem.Text = "User Default Server:Port";
            // 
            // _ConnectServerButton
            // 
            this._ConnectServerButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
            this._ConnectServerButton.Image = global::isr.Diolan.Gpio.WinControls.Properties.Resources.WIFI_open_22_Right_Up;
            this._ConnectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ConnectServerButton.Name = "_ConnectServerButton";
            this._ConnectServerButton.Size = new System.Drawing.Size(41, 26);
            this._ConnectServerButton.Text = "X";
            this._ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this._ConnectServerButton.ToolTipText = "Connect or disconnect serve and show attached devices.";
            this._ConnectServerButton.Click += new System.EventHandler(this.ConnectServerButton_Click);
            // 
            // _OpenDeviceSubsystemTypeButton
            // 
            this._OpenDeviceSubsystemTypeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._OpenDeviceSubsystemTypeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._OpenDeviceSubsystemTypeButton.Enabled = false;
            this._OpenDeviceSubsystemTypeButton.Image = global::isr.Diolan.Gpio.WinControls.Properties.Resources.user_offline_2;
            this._OpenDeviceSubsystemTypeButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._OpenDeviceSubsystemTypeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._OpenDeviceSubsystemTypeButton.Name = "_OpenDeviceSubsystemTypeButton";
            this._OpenDeviceSubsystemTypeButton.Size = new System.Drawing.Size(26, 26);
            this._OpenDeviceSubsystemTypeButton.Text = "Open";
            this._OpenDeviceSubsystemTypeButton.ToolTipText = "Open or close the device.";
            this._OpenDeviceSubsystemTypeButton.Click += new System.EventHandler(this.OpenDeviceSubsystemTypeButton_Click);
            // 
            // _SelectDeviceSplitButton
            // 
            this._SelectDeviceSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._SelectDeviceSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectDeviceSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._DevicesComboBox});
            this._SelectDeviceSplitButton.Image = global::isr.Diolan.Gpio.WinControls.Properties.Resources.network_server_database;
            this._SelectDeviceSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton";
            this._SelectDeviceSplitButton.Size = new System.Drawing.Size(38, 26);
            this._SelectDeviceSplitButton.Text = "Device";
            this._SelectDeviceSplitButton.ToolTipText = "Select Device";
            // 
            // _DevicesComboBox
            // 
            this._DevicesComboBox.Name = "_DevicesComboBox";
            this._DevicesComboBox.Size = new System.Drawing.Size(121, 23);
            this._DevicesComboBox.Text = "DLN-4M.1.1";
            // 
            // _DeviceInfoTextBox
            // 
            this._DeviceInfoTextBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._DeviceInfoTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._DeviceInfoTextBox.Name = "_DeviceInfoTextBox";
            this._DeviceInfoTextBox.ReadOnly = true;
            this._DeviceInfoTextBox.Size = new System.Drawing.Size(100, 29);
            this._DeviceInfoTextBox.Text = "closed";
            this._DeviceInfoTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _Tabs
            // 
            this._Tabs.Controls.Add(this._PrimaryTabPage);
            this._Tabs.Controls.Add(this._PinTabPage);
            this._Tabs.Controls.Add(this._EventLogTabPage);
            this._Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Tabs.HideTabHeaders = true;
            this._Tabs.Location = new System.Drawing.Point(0, 57);
            this._Tabs.Name = "_Tabs";
            this._Tabs.SelectedIndex = 0;
            this._Tabs.Size = new System.Drawing.Size(417, 395);
            this._Tabs.TabIndex = 0;
            // 
            // _PrimaryTabPage
            // 
            this._PrimaryTabPage.Controls.Add(this._PrimaryTabLayout);
            this._PrimaryTabPage.Location = new System.Drawing.Point(4, 22);
            this._PrimaryTabPage.Name = "_PrimaryTabPage";
            this._PrimaryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._PrimaryTabPage.Size = new System.Drawing.Size(409, 369);
            this._PrimaryTabPage.TabIndex = 0;
            this._PrimaryTabPage.Text = "GPIO";
            this._PrimaryTabPage.UseVisualStyleBackColor = true;
            // 
            // _PrimaryTabLayout
            // 
            this._PrimaryTabLayout.ColumnCount = 3;
            this._PrimaryTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PrimaryTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._PrimaryTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PrimaryTabLayout.Controls.Add(this._SubsystemGroupBox, 1, 1);
            this._PrimaryTabLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PrimaryTabLayout.Location = new System.Drawing.Point(3, 3);
            this._PrimaryTabLayout.Name = "_PrimaryTabLayout";
            this._PrimaryTabLayout.RowCount = 3;
            this._PrimaryTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PrimaryTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._PrimaryTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PrimaryTabLayout.Size = new System.Drawing.Size(403, 363);
            this._PrimaryTabLayout.TabIndex = 8;
            // 
            // _SubsystemGroupBox
            // 
            this._SubsystemGroupBox.Controls.Add(this._DebounceNumeric);
            this._SubsystemGroupBox.Controls.Add(this._GetDebounceButton);
            this._SubsystemGroupBox.Controls.Add(this._SetDebounceButton);
            this._SubsystemGroupBox.Controls.Add(this._DebounceNumericLabel);
            this._SubsystemGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SubsystemGroupBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._SubsystemGroupBox.Location = new System.Drawing.Point(24, 149);
            this._SubsystemGroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SubsystemGroupBox.Name = "_SubsystemGroupBox";
            this._SubsystemGroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SubsystemGroupBox.Size = new System.Drawing.Size(355, 64);
            this._SubsystemGroupBox.TabIndex = 6;
            this._SubsystemGroupBox.TabStop = false;
            this._SubsystemGroupBox.Text = "GPIO Subsystem Settings";
            // 
            // _DebounceNumeric
            // 
            this._DebounceNumeric.Location = new System.Drawing.Point(146, 23);
            this._DebounceNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._DebounceNumeric.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this._DebounceNumeric.Name = "_DebounceNumeric";
            this._DebounceNumeric.Size = new System.Drawing.Size(63, 23);
            this._DebounceNumeric.TabIndex = 1;
            // 
            // _GetDebounceButton
            // 
            this._GetDebounceButton.Location = new System.Drawing.Point(280, 19);
            this._GetDebounceButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._GetDebounceButton.Name = "_GetDebounceButton";
            this._GetDebounceButton.Size = new System.Drawing.Size(61, 30);
            this._GetDebounceButton.TabIndex = 3;
            this._GetDebounceButton.Text = "Get";
            this._ToolTip.SetToolTip(this._GetDebounceButton, "Gets the debounce interval");
            this._GetDebounceButton.UseVisualStyleBackColor = true;
            this._GetDebounceButton.Click += new System.EventHandler(this.GetDebounceButton_Click);
            // 
            // _SetDebounceButton
            // 
            this._SetDebounceButton.Location = new System.Drawing.Point(214, 19);
            this._SetDebounceButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SetDebounceButton.Name = "_SetDebounceButton";
            this._SetDebounceButton.Size = new System.Drawing.Size(61, 30);
            this._SetDebounceButton.TabIndex = 2;
            this._SetDebounceButton.Text = "Set";
            this._ToolTip.SetToolTip(this._SetDebounceButton, "Sets the debounce interval");
            this._SetDebounceButton.UseVisualStyleBackColor = true;
            this._SetDebounceButton.Click += new System.EventHandler(this.SetDebounceButton_Click);
            // 
            // _DebounceNumericLabel
            // 
            this._DebounceNumericLabel.AutoSize = true;
            this._DebounceNumericLabel.Location = new System.Drawing.Point(14, 27);
            this._DebounceNumericLabel.Name = "_DebounceNumericLabel";
            this._DebounceNumericLabel.Size = new System.Drawing.Size(129, 15);
            this._DebounceNumericLabel.TabIndex = 0;
            this._DebounceNumericLabel.Text = "Debounce Interval [µs]:";
            this._DebounceNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PinTabPage
            // 
            this._PinTabPage.Controls.Add(this._PinTabLayout);
            this._PinTabPage.Location = new System.Drawing.Point(4, 22);
            this._PinTabPage.Name = "_PinTabPage";
            this._PinTabPage.Size = new System.Drawing.Size(409, 369);
            this._PinTabPage.TabIndex = 2;
            this._PinTabPage.Text = "Pin";
            this._PinTabPage.UseVisualStyleBackColor = true;
            // 
            // _PinTabLayout
            // 
            this._PinTabLayout.ColumnCount = 3;
            this._PinTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PinTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._PinTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PinTabLayout.Controls.Add(this._PinConfigGroupBox, 1, 1);
            this._PinTabLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PinTabLayout.Location = new System.Drawing.Point(0, 0);
            this._PinTabLayout.Name = "_PinTabLayout";
            this._PinTabLayout.RowCount = 3;
            this._PinTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PinTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._PinTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._PinTabLayout.Size = new System.Drawing.Size(409, 369);
            this._PinTabLayout.TabIndex = 9;
            // 
            // _PinConfigGroupBox
            // 
            this._PinConfigGroupBox.Controls.Add(this._OpenDrainCheckBox);
            this._PinConfigGroupBox.Controls.Add(this._PinPullupCheckBox);
            this._PinConfigGroupBox.Controls.Add(this._EventPeriodNumeric);
            this._PinConfigGroupBox.Controls.Add(this._PulldownCheckBox);
            this._PinConfigGroupBox.Controls.Add(this._DebounceEnabledCheckBox);
            this._PinConfigGroupBox.Controls.Add(this._EventPeriodNumericLabel);
            this._PinConfigGroupBox.Controls.Add(this._EventTypeComboBoxLabel);
            this._PinConfigGroupBox.Controls.Add(this._EventTypeComboBox);
            this._PinConfigGroupBox.Controls.Add(this._GetPinConfigButton);
            this._PinConfigGroupBox.Controls.Add(this._SetPinConfigButton);
            this._PinConfigGroupBox.Controls.Add(this._PinDirectionComboBox);
            this._PinConfigGroupBox.Controls.Add(this._PinDirectionComboBoxLabel);
            this._PinConfigGroupBox.Controls.Add(this._PinEnabledCheckBox);
            this._PinConfigGroupBox.Controls.Add(this._PinComboBox);
            this._PinConfigGroupBox.Controls.Add(this._PinComboBoxLabel);
            this._PinConfigGroupBox.Location = new System.Drawing.Point(48, 60);
            this._PinConfigGroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PinConfigGroupBox.Name = "_PinConfigGroupBox";
            this._PinConfigGroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PinConfigGroupBox.Size = new System.Drawing.Size(313, 249);
            this._PinConfigGroupBox.TabIndex = 8;
            this._PinConfigGroupBox.TabStop = false;
            this._PinConfigGroupBox.Text = "Pin Configuration";
            // 
            // _OpenDrainCheckBox
            // 
            this._OpenDrainCheckBox.AutoSize = true;
            this._OpenDrainCheckBox.Location = new System.Drawing.Point(187, 139);
            this._OpenDrainCheckBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._OpenDrainCheckBox.Name = "_OpenDrainCheckBox";
            this._OpenDrainCheckBox.Size = new System.Drawing.Size(80, 17);
            this._OpenDrainCheckBox.TabIndex = 10;
            this._OpenDrainCheckBox.Text = "Open Drain";
            this._ToolTip.SetToolTip(this._OpenDrainCheckBox, "Pin is open drain is checked.");
            this._OpenDrainCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PinPullupCheckBox
            // 
            this._PinPullupCheckBox.AutoSize = true;
            this._PinPullupCheckBox.Location = new System.Drawing.Point(19, 139);
            this._PinPullupCheckBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PinPullupCheckBox.Name = "_PinPullupCheckBox";
            this._PinPullupCheckBox.Size = new System.Drawing.Size(60, 17);
            this._PinPullupCheckBox.TabIndex = 8;
            this._PinPullupCheckBox.Text = "Pull Up";
            this._ToolTip.SetToolTip(this._PinPullupCheckBox, "pin is pulled up if checked.");
            this._PinPullupCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EventPeriodNumeric
            // 
            this._EventPeriodNumeric.Location = new System.Drawing.Point(133, 211);
            this._EventPeriodNumeric.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this._EventPeriodNumeric.Name = "_EventPeriodNumeric";
            this._EventPeriodNumeric.Size = new System.Drawing.Size(60, 20);
            this._EventPeriodNumeric.TabIndex = 14;
            this._ToolTip.SetToolTip(this._EventPeriodNumeric, "Selects the event period in ms");
            // 
            // _PulldownCheckBox
            // 
            this._PulldownCheckBox.AutoSize = true;
            this._PulldownCheckBox.Location = new System.Drawing.Point(95, 139);
            this._PulldownCheckBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PulldownCheckBox.Name = "_PulldownCheckBox";
            this._PulldownCheckBox.Size = new System.Drawing.Size(74, 17);
            this._PulldownCheckBox.TabIndex = 9;
            this._PulldownCheckBox.Text = "Pull Down";
            this._ToolTip.SetToolTip(this._PulldownCheckBox, "Pin is pulled down if checked");
            this._PulldownCheckBox.UseVisualStyleBackColor = true;
            // 
            // _DebounceEnabledCheckBox
            // 
            this._DebounceEnabledCheckBox.AutoSize = true;
            this._DebounceEnabledCheckBox.Location = new System.Drawing.Point(126, 66);
            this._DebounceEnabledCheckBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._DebounceEnabledCheckBox.Name = "_DebounceEnabledCheckBox";
            this._DebounceEnabledCheckBox.Size = new System.Drawing.Size(118, 17);
            this._DebounceEnabledCheckBox.TabIndex = 5;
            this._DebounceEnabledCheckBox.Text = "Debounce Enabled";
            this._ToolTip.SetToolTip(this._DebounceEnabledCheckBox, "Debounce is enabled if checked.");
            this._DebounceEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EventPeriodNumericLabel
            // 
            this._EventPeriodNumericLabel.AutoSize = true;
            this._EventPeriodNumericLabel.Location = new System.Drawing.Point(18, 215);
            this._EventPeriodNumericLabel.Name = "_EventPeriodNumericLabel";
            this._EventPeriodNumericLabel.Size = new System.Drawing.Size(93, 13);
            this._EventPeriodNumericLabel.TabIndex = 13;
            this._EventPeriodNumericLabel.Text = "Event Period [ms]:";
            this._EventPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EventTypeComboBoxLabel
            // 
            this._EventTypeComboBoxLabel.AutoSize = true;
            this._EventTypeComboBoxLabel.Location = new System.Drawing.Point(18, 180);
            this._EventTypeComboBoxLabel.Name = "_EventTypeComboBoxLabel";
            this._EventTypeComboBoxLabel.Size = new System.Drawing.Size(65, 13);
            this._EventTypeComboBoxLabel.TabIndex = 11;
            this._EventTypeComboBoxLabel.Text = "Event Type:";
            this._EventTypeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EventTypeComboBox
            // 
            this._EventTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._EventTypeComboBox.FormattingEnabled = true;
            this._EventTypeComboBox.Location = new System.Drawing.Point(94, 176);
            this._EventTypeComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._EventTypeComboBox.Name = "_EventTypeComboBox";
            this._EventTypeComboBox.Size = new System.Drawing.Size(101, 21);
            this._EventTypeComboBox.TabIndex = 12;
            this._ToolTip.SetToolTip(this._EventTypeComboBox, "Selects the event type.");
            // 
            // _GetPinConfigButton
            // 
            this._GetPinConfigButton.Location = new System.Drawing.Point(158, 22);
            this._GetPinConfigButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._GetPinConfigButton.Name = "_GetPinConfigButton";
            this._GetPinConfigButton.Size = new System.Drawing.Size(61, 30);
            this._GetPinConfigButton.TabIndex = 2;
            this._GetPinConfigButton.Text = "Get";
            this._GetPinConfigButton.UseVisualStyleBackColor = true;
            this._GetPinConfigButton.Click += new System.EventHandler(this.GetPinConfigButton_Click);
            // 
            // _SetPinConfigButton
            // 
            this._SetPinConfigButton.Location = new System.Drawing.Point(236, 22);
            this._SetPinConfigButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SetPinConfigButton.Name = "_SetPinConfigButton";
            this._SetPinConfigButton.Size = new System.Drawing.Size(61, 30);
            this._SetPinConfigButton.TabIndex = 3;
            this._SetPinConfigButton.Text = "Set";
            this._SetPinConfigButton.UseVisualStyleBackColor = true;
            this._SetPinConfigButton.Click += new System.EventHandler(this.SetPinConfigButton_Click);
            // 
            // _PinDirectionComboBox
            // 
            this._PinDirectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._PinDirectionComboBox.FormattingEnabled = true;
            this._PinDirectionComboBox.Location = new System.Drawing.Point(81, 102);
            this._PinDirectionComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PinDirectionComboBox.Name = "_PinDirectionComboBox";
            this._PinDirectionComboBox.Size = new System.Drawing.Size(79, 21);
            this._PinDirectionComboBox.TabIndex = 7;
            this._ToolTip.SetToolTip(this._PinDirectionComboBox, "Selects pin direction.");
            // 
            // _PinDirectionComboBoxLabel
            // 
            this._PinDirectionComboBoxLabel.AutoSize = true;
            this._PinDirectionComboBoxLabel.Location = new System.Drawing.Point(16, 106);
            this._PinDirectionComboBoxLabel.Name = "_PinDirectionComboBoxLabel";
            this._PinDirectionComboBoxLabel.Size = new System.Drawing.Size(52, 13);
            this._PinDirectionComboBoxLabel.TabIndex = 6;
            this._PinDirectionComboBoxLabel.Text = "Direction:";
            this._PinDirectionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PinEnabledCheckBox
            // 
            this._PinEnabledCheckBox.AutoSize = true;
            this._PinEnabledCheckBox.Location = new System.Drawing.Point(44, 66);
            this._PinEnabledCheckBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PinEnabledCheckBox.Name = "_PinEnabledCheckBox";
            this._PinEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this._PinEnabledCheckBox.TabIndex = 4;
            this._PinEnabledCheckBox.Text = "Enabled";
            this._PinEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PinComboBox
            // 
            this._PinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._PinComboBox.FormattingEnabled = true;
            this._PinComboBox.Location = new System.Drawing.Point(40, 25);
            this._PinComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PinComboBox.Name = "_PinComboBox";
            this._PinComboBox.Size = new System.Drawing.Size(100, 21);
            this._PinComboBox.TabIndex = 1;
            this._ToolTip.SetToolTip(this._PinComboBox, "Selects the pin number.");
            this._PinComboBox.SelectedIndexChanged += new System.EventHandler(this.PinComboBox_SelectedIndexChanged);
            // 
            // _PinComboBoxLabel
            // 
            this._PinComboBoxLabel.AutoSize = true;
            this._PinComboBoxLabel.Location = new System.Drawing.Point(10, 29);
            this._PinComboBoxLabel.Name = "_PinComboBoxLabel";
            this._PinComboBoxLabel.Size = new System.Drawing.Size(25, 13);
            this._PinComboBoxLabel.TabIndex = 0;
            this._PinComboBoxLabel.Text = "Pin:";
            this._PinComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EventLogTabPage
            // 
            this._EventLogTabPage.Controls.Add(this._EventLogTextBox);
            this._EventLogTabPage.Location = new System.Drawing.Point(4, 22);
            this._EventLogTabPage.Name = "_EventLogTabPage";
            this._EventLogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._EventLogTabPage.Size = new System.Drawing.Size(409, 369);
            this._EventLogTabPage.TabIndex = 1;
            this._EventLogTabPage.Text = "Events";
            this._EventLogTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTextBox
            // 
            this._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._EventLogTextBox.Location = new System.Drawing.Point(3, 3);
            this._EventLogTextBox.Multiline = true;
            this._EventLogTextBox.Name = "_EventLogTextBox";
            this._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._EventLogTextBox.Size = new System.Drawing.Size(403, 363);
            this._EventLogTextBox.TabIndex = 0;
            this._EventLogTextBox.DoubleClick += new System.EventHandler(this.EventLogTextBox_DoubleClick);
            // 
            // _ErrorProvider
            // 
            this._ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            this._ToolStripContainer.BottomToolStripPanel.Controls.Add(this._BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            this._ToolStripContainer.ContentPanel.Controls.Add(this._Tabs);
            this._ToolStripContainer.ContentPanel.Controls.Add(this._GpioPinControl2);
            this._ToolStripContainer.ContentPanel.Controls.Add(this._GpioPinControl1);
            this._ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(417, 452);
            this._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            this._ToolStripContainer.Name = "_ToolStripContainer";
            this._ToolStripContainer.Size = new System.Drawing.Size(417, 506);
            this._ToolStripContainer.TabIndex = 0;
            this._ToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _ToolStripContainer.TopToolStripPanel
            // 
            this._ToolStripContainer.TopToolStripPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // _GpioPinControl2
            // 
            this._GpioPinControl2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._GpioPinControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this._GpioPinControl2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioPinControl2.Location = new System.Drawing.Point(0, 29);
            this._GpioPinControl2.Name = "_GpioPinControl2";
            this._GpioPinControl2.Size = new System.Drawing.Size(417, 28);
            this._GpioPinControl2.TabIndex = 22;
            // 
            // _GpioPinControl1
            // 
            this._GpioPinControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._GpioPinControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this._GpioPinControl1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioPinControl1.Location = new System.Drawing.Point(0, 0);
            this._GpioPinControl1.Name = "_GpioPinControl1";
            this._GpioPinControl1.Size = new System.Drawing.Size(417, 29);
            this._GpioPinControl1.TabIndex = 21;
            // 
            // GpioView
            // 
            this.Controls.Add(this._ToolStripContainer);
            this.Name = "GpioView";
            this.Size = new System.Drawing.Size(417, 506);
            this._BottomToolStrip.ResumeLayout(false);
            this._BottomToolStrip.PerformLayout();
            this._Tabs.ResumeLayout(false);
            this._PrimaryTabPage.ResumeLayout(false);
            this._PrimaryTabLayout.ResumeLayout(false);
            this._SubsystemGroupBox.ResumeLayout(false);
            this._SubsystemGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._DebounceNumeric)).EndInit();
            this._PinTabPage.ResumeLayout(false);
            this._PinTabLayout.ResumeLayout(false);
            this._PinConfigGroupBox.ResumeLayout(false);
            this._PinConfigGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._EventPeriodNumeric)).EndInit();
            this._EventLogTabPage.ResumeLayout(false);
            this._EventLogTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).EndInit();
            this._ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            this._ToolStripContainer.BottomToolStripPanel.PerformLayout();
            this._ToolStripContainer.ContentPanel.ResumeLayout(false);
            this._ToolStripContainer.ResumeLayout(false);
            this._ToolStripContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        private ToolStrip _BottomToolStrip;
        private ToolStripTextBox _DeviceInfoTextBox;
        private ErrorProvider _ErrorProvider;
        private ToolTip _ToolTip;
        private ToolStripButton _OpenDeviceSubsystemTypeButton;
        private TabPage _PrimaryTabPage;
        private TabPage _EventLogTabPage;
        private TextBox _EventLogTextBox;
        private ToolStripContainer _ToolStripContainer;
        private isr.Diolan.WinControls.ExtendedTabControl _Tabs;
        private ToolStripComboBox _DevicesComboBox;
        private ToolStripSplitButton _SelectServerButton;
        private ToolStripTextBox _ServerNameTextBox;
        private ToolStripMenuItem _DefaultServerMenuItem;
        private ToolStripSplitButton _SelectDeviceSplitButton;
        private GroupBox _SubsystemGroupBox;
        private NumericUpDown _DebounceNumeric;
        private Button _GetDebounceButton;
        private Button _SetDebounceButton;
        private Label _DebounceNumericLabel;
        private TableLayoutPanel _PrimaryTabLayout;
        private TabPage _PinTabPage;
        private GroupBox _PinConfigGroupBox;
        private CheckBox _OpenDrainCheckBox;
        private CheckBox _PinPullupCheckBox;
        private NumericUpDown _EventPeriodNumeric;
        private CheckBox _PulldownCheckBox;
        private CheckBox _DebounceEnabledCheckBox;
        private Label _EventPeriodNumericLabel;
        private Label _EventTypeComboBoxLabel;
        private ComboBox _EventTypeComboBox;
        private Button _GetPinConfigButton;
        private Button _SetPinConfigButton;
        private ComboBox _PinDirectionComboBox;
        private Label _PinDirectionComboBoxLabel;
        private CheckBox _PinEnabledCheckBox;
        private ComboBox _PinComboBox;
        private Label _PinComboBoxLabel;
        private GpioPinControl _GpioPinControl2;
        private GpioPinControl _GpioPinControl1;
        private TableLayoutPanel _PinTabLayout;
        private ToolStripComboBox _TabComboBox;
        private ToolStripButton _ConnectServerButton;
    }
}
