using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Diolan.Gpio;
using isr.Diolan.SubsystemExtensions;
using isr.Diolan.WinControls.ErrorProviderExtensions;
using isr.Diolan.WinControls.SubsystemExtensions;

namespace isr.Diolan.Gpio.WinControls
{

    /// <summary> User interface for gpio pins. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-03 </para>
    /// </remarks>
    public partial class GpioPinsView : System.Windows.Forms.UserControl, INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        private bool InitializingComponents { get; set; }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioPinsView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();
            this.InitializingComponents = false;

            // Add any initialization after the InitializeComponent() call.
            this._TabComboBox.ComboBox.DataSource = this._Tabs.TabPages;
            this._TabComboBox.ComboBox.DisplayMember = "Text";
            this.InputPins = new GpioPinCollection();
            this.OutputPins = new GpioPinCollection();
        }

        /// <summary> Updates the un-rendered values. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void UpdateUnrenderedValues()
        {
            this._EventPinValue.Text = "0";
            this._EventPinValue.Invalidate();
            this._SubsystemGroupBox.Text = "GPIO Subsystem Settings";
            this._SubsystemGroupBox.Invalidate();
            this._DebounceNumericLabel.Text = $"Debounce Interval [{Convert.ToChar( 0x3BC )}s]:";
            this._DebounceNumericLabel.Invalidate();
            this._DebounceNumeric.Value = this.IsDeviceSubsystemTypeOpen ? this.Device.Gpio.Debounce : 0m;
            this._DebounceNumeric.Value += 1m;
            this._DebounceNumeric.Value -= 1m;
            this._DebounceNumeric.Invalidate();
        }

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );
            this.UpdateUnrenderedValues();
            this.Invalidate();
            Application.DoEvents();
        }

        /// <summary> Gpio pins panel load. </summary>
        /// <remarks> This is required when not calling the owner form within a Using statement. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void GpioPinsPanel_Load( object sender, EventArgs e )
        {
            this._EventPinValue.Invalidate();
            Application.DoEvents();
            this._DebounceNumericLabel.Invalidate();
            Application.DoEvents();
            this._SubsystemGroupBox.Invalidate();
            Application.DoEvents();
            this._DebounceNumeric.Invalidate();
            Application.DoEvents();
            this.Invalidate();
            Application.DoEvents();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                if ( !this.IsDisposed && disposing )
                {
                    this.components?.Dispose();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        private void OnDispose( bool disposing )
        {
            this.OnCustomDispose( disposing );
            if ( !this.IsDisposed && disposing )
            {
                if ( this.Device is object )
                    this.Device = null;
                if ( this.DeviceConnectorInternal is object )
                {
                    this.DeviceConnectorInternal.Dispose();
                    this.DeviceConnectorInternal = null;
                }

                if ( this.ServerConnectorInternal is object )
                    this.ServerConnectorInternal = null;
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " KNOWN STATE "

        /// <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public void InitializeKnownState()
        {
            this.OnServerConnectionChanged();
        }

        #endregion

        #region " SERVER "

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnPropertyChanged( LocalhostConnector sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( e is object )
                {
                    switch ( e.PropertyName ?? "" )
                    {
                        case nameof( LocalhostConnector.IsConnected ):
                            {
                                this.OnServerConnectionChanged();
                                break;
                            }

                        case nameof( LocalhostConnector.AttachedDevicesCount ):
                            {
                                this.OnServerAttachmentChanged();
                                break;
                            }
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, details );
            }
        }

        /// <summary> Server connector property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void ServerConnector_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ServerConnector_PropertyChanged ), new object[] { sender, e } );
            }

            this.OnPropertyChanged( sender as LocalhostConnector, e );
        }


        private LocalhostConnector _ServerConnectorInternal;

        /// <summary>   Gets or sets the server connector internal. </summary>
        /// <value> The server connector internal. </value>
        private LocalhostConnector ServerConnectorInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ServerConnectorInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ServerConnectorInternal != null )
                {
                    this._ServerConnectorInternal.PropertyChanged -= this.ServerConnector_PropertyChanged;
                }

                this._ServerConnectorInternal = value;
                if ( this._ServerConnectorInternal != null )
                {
                    this._ServerConnectorInternal.PropertyChanged += this.ServerConnector_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LocalhostConnector ServerConnector
        {
            get => this.ServerConnectorInternal;

            set => this.ServerConnectorInternal = value;
        }

        /// <summary> Connects a server button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConnectServerButton_Click( object sender, EventArgs e )
        {
            if ( this.ServerConnector.IsConnected )
            {
                if ( !this.ServerConnector.HasAttachedDevices() )
                {
                    this.ServerConnector.Disconnect();
                }
            }
            else
            {
                this.ServerConnector.Connect();
            }
        }

        /// <summary> Updates the server connection state. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void UpdateServerConnectionState()
        {
            this._OpenDeviceSubsystemTypeButton.Enabled = this.ServerConnector.IsConnected;
            this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
        }

        /// <summary> Executes the server connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnServerConnectionChanged()
        {
            if ( this.ServerConnector.IsConnected )
            {
                _ = this.ServerConnector.Connection.ListDevicesById( this._DevicesComboBox );
                this._DevicesComboBox.SelectedIndex = 0;
            }

            this.UpdateServerConnectionState();
            this.OnServerAttachmentChanged();
        }

        /// <summary> Executes the server attachment changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OnServerAttachmentChanged()
        {
            this._ConnectServerButton.Image = isr.Diolan.Gpio.WinControls.Properties.Resources.WIFI_open_22;
            if ( this.ServerConnector.IsConnected )
            {
                this._ConnectServerButton.Text = this.ServerConnector.AttachedDevicesCount().ToString();
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
                this._ConnectServerButton.Text = "X";
            }
        }

        #endregion

        #region " DEVICE CONNECTOR "


        private DeviceConnector _DeviceConnectorInternal;
        /// <summary>   Gets or sets the device connector internal. </summary>
        /// <value> The device connector internal. </value>
        private DeviceConnector DeviceConnectorInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceConnectorInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceConnectorInternal != null )
                {

                    this._DeviceConnectorInternal.DeviceClosed -= this.DeviceConnector_DeviceClosed;

                    this._DeviceConnectorInternal.DeviceClosing -= this.DeviceConnector_DeviceClosing;

                    this._DeviceConnectorInternal.DeviceOpened -= this.DeviceConnector_DeviceOpened;
                }

                this._DeviceConnectorInternal = value;
                if ( this._DeviceConnectorInternal != null )
                {
                    this._DeviceConnectorInternal.DeviceClosed += this.DeviceConnector_DeviceClosed;
                    this._DeviceConnectorInternal.DeviceClosing += this.DeviceConnector_DeviceClosing;
                    this._DeviceConnectorInternal.DeviceOpened += this.DeviceConnector_DeviceOpened;
                }
            }
        }

        /// <summary> Gets or sets the device connector. </summary>
        /// <value> The device connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DeviceConnector DeviceConnector
        {
            get => this.DeviceConnectorInternal;

            set => this.DeviceConnectorInternal = value;
        }

        /// <summary> Device connector device closed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceClosed( object sender, EventArgs e )
        {
            this.OnServerAttachmentChanged();
            this._DeviceInfoTextBox.Text = "closed";
            this.OnSubsystemTypeClosed();
        }

        /// <summary> Device connector device closing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void DeviceConnector_DeviceClosing( object sender, CancelEventArgs e )
        {
            this.OnSubsystemTypeClosing( e );
        }

        /// <summary> Device connector device opened. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceOpened( object sender, EventArgs e )
        {
            this.OnServerAttachmentChanged();
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        [CLSCompliant( false )]
        public Dln.Device Device { get; private set; }

        /// <summary> Selected device information. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A DeviceInfo. </returns>
        private DeviceInfo SelectedDeviceInfo()
        {
            return string.IsNullOrWhiteSpace( this._DevicesComboBox.Text )
                ? throw new InvalidOperationException( "No devices selected" )
                : new DeviceInfo( this._DevicesComboBox.Text );
        }

        /// <summary> Attempts to open local device from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="id"> The identifier. </param>
        /// <returns> A Tuple: ( success, details ); <c>true</c> if it succeeds; otherwise <c>false</c> + details </returns>
        public (bool Success, string Details) TryOpenLocalDevice( long id )
        {
            (bool success, string details) = (true, string.Empty);
            try
            {
                this.ServerConnectorInternal = LocalhostConnector.SingleInstance();
                if ( this.ServerConnector.IsConnected )
                {
                    this._OpenDeviceSubsystemTypeButton.Enabled = this.ServerConnector.IsConnected;
                    this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
                }
                else
                {
                    this.ServerConnector.Connect();
                }

                this.DeviceConnectorInternal = new DeviceConnector( this.ServerConnectorInternal );
                (success, details) = this.DeviceConnectorInternal.TryOpenDevice( id, this.SubsystemType );
                if ( success )
                {
                    this.OnSubsystemTypeOpening();
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( this._TopToolStrip, details );
                    this._DeviceInfoTextBox.Text = $"device #{id} failed opening";
                    this._DeviceInfoTextBox.ToolTipText = details;
                }
            }
            catch
            {
                try
                {
                    this.CloseDeviceSubsystemType();
                }
                catch
                {
                }

                throw;
            }
            finally
            {
                this.OnSubsystemTypeConnectionChanged();
            }

            return ( success, details );
        }

        /// <summary> Opens the device for the subsystem type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <param name="sender">   The sender. </param>
        private void OpenDeviceSubsystemType( long deviceId, Control sender )
        {
            if ( this.ServerConnector.IsConnected )
            {
                this._OpenDeviceSubsystemTypeButton.Enabled = this.ServerConnector.IsConnected;
                this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            }
            else
            {
                this.ServerConnector.Connect();
            }

            this.DeviceConnectorInternal = new DeviceConnector( this.ServerConnectorInternal );

            // Open device
            var (success, details) = this.DeviceConnectorInternal.TryOpenDevice( deviceId, this.SubsystemType );
            if ( success )
            {
                this.OnSubsystemTypeOpening();
            }
            else
            {
                if ( sender is object )
                    _ = this._ErrorProvider.Annunciate( sender, details );
                this._DeviceInfoTextBox.Text = "No devices";
            }
        }

        /// <summary> Closes device Subsystem Type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void CloseDeviceSubsystemType()
        {
            this.DeviceConnectorInternal.CloseDevice( this.SubsystemType );
            var e = new CancelEventArgs();
            this.OnSubsystemTypeClosing( e );
            if ( !e.Cancel )
            {
                this.OnSubsystemTypeClosed();
            }
        }

        /// <summary> Opens device button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OpenDeviceSubsystemTypeButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    this.CloseDeviceSubsystemType();
                }
                else if ( string.IsNullOrWhiteSpace( this._DevicesComboBox.Text ) )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Select a device first" );
                }
                else
                {
                    this.OpenDeviceSubsystemType( this.SelectedDeviceInfo().Id, sender as Control );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
                this.OnSubsystemTypeConnectionChanged();
            }
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        private bool IsDeviceSubsystemTypeOpen => this.Device is object && this.IsSubsystemTypeOpen();

        /// <summary> Executes the subsystem type connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeConnectionChanged()
        {
            if ( this.IsDeviceSubsystemTypeOpen )
            {
                this._OpenDeviceSubsystemTypeButton.Image = isr.Diolan.Gpio.WinControls.Properties.Resources.user_online_2;
                this._OpenDeviceSubsystemTypeButton.Text = "Close";
            }
            else
            {
                this._OpenDeviceSubsystemTypeButton.Image = isr.Diolan.Gpio.WinControls.Properties.Resources.user_offline_2;
                this._OpenDeviceSubsystemTypeButton.Text = "Open";
            }
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Event log text box double click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EventLogTextBox_DoubleClick( object sender, EventArgs e )
        {
            this._EventLogTextBox.Clear();
        }

        #endregion

        #region " TABS "

        /// <summary> Selects the tab. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void TabComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._TabComboBox.SelectedIndex >= 0 && this._TabComboBox.SelectedIndex < this._Tabs.TabCount )
            {
                this._Tabs.SelectTab( this._Tabs.TabPages[this._TabComboBox.SelectedIndex] );
            }
        }

        #endregion

        #region " CONSTRUCTION "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.OnSubsystemTypeClosed();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " SUBSYSTEM TYPE "

        /// <summary> the subsystem type. </summary>
        /// <value> the subsystem type. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SubsystemTypes SubsystemType { get; set; } = SubsystemTypes.Gpio;

        /// <summary> Executes the subsystem type closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeClosed()
        {
            this.Device = null;
            this.OnSubsystemTypeConnectionChanged();
        }

        /// <summary> Executes the subsystem type closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnSubsystemTypeClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsSubsystemTypeOpen() )
                {
                    // un-register event handlers for all pins
                    foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                        pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                }
            }
        }

        /// <summary> Executes the subsystem type opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeOpening()
        {

            // Assign the reference to the device
            this.Device = this.DeviceConnectorInternal.Device;
            this._ErrorProvider.Clear();

            // Get port count
            if ( this.Device.Gpio.Pins.Count == 0 )
            {
                // this is already done when opening the device.
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, "Adapter '{0}' doesn't support GPIO interface.", this.Device.Caption() );
                this.Device = null;
                this._DeviceInfoTextBox.Text = "not supported";
            }
            else
            {
                this._DeviceInfoTextBox.Text = this.Device.Caption();

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handler for all pins
                foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                    pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;

                // Get subsystem parameters: Debounce
                this.ReadDebounceProperties();
            }
        }

        /// <summary> Queries if a Subsystem Type is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
        private bool IsSubsystemTypeOpen()
        {
            return this.Device is object;
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Gpio.ConditionMetEventArgs e )
        {
            this.AppendEvent( e );
        }

        /// <summary> Appends an event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Condition met event information. </param>
        private void AppendEvent( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                string data = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}";

                // This event is handled in main thread,
                // so it is not needed to invoke when modifying form's controls.
                this._EventLogTextBox.AppendText( data );
            }
        }

        #endregion

        #region " SUBSYSTEM "

        /// <summary> Reads the debounce properties from the device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void ReadDebounceProperties()
        {
            // Get subsystem parameters: Debounce
            if ( this.Device.Gpio.Restrictions.Debounce == Dln.Restriction.NotSupported )
            {
                this._DebounceNumeric.Enabled = false;
                this._SetDebounceButton.Enabled = false;
                this._GetDebounceButton.Enabled = false;
            }
            else
            {
                this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
            }
        }

        /// <summary> Gets debounce button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void GetDebounceButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Sets debounce button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SetDebounceButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    this.Device.Gpio.Debounce = ( int ) this._DebounceNumeric.Value;
                    this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.SubsystemType );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        #endregion

        #region " TOP TOOL BOX "

        /// <summary> Shows the top menu. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="value"> true to value. </param>
        public void ShowTopMenu( bool value )
        {
            this._TopToolStrip.Visible = value;
        }

        #endregion

        #region " PINS "

        /// <summary> Gets or sets the input pins. </summary>
        /// <value> The input pins. </value>
        public GpioPinCollection InputPins { get; private set; }

        /// <summary> Gets or sets the output pins. </summary>
        /// <value> The output pins. </value>
        public GpioPinCollection OutputPins { get; private set; }

        /// <summary> Adds pin. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pin"> The pin to add. </param>
        public void Add( GpioPin pin )
        {
            if ( pin is null )
                throw new ArgumentNullException( nameof( pin ) );
            var pinToolStrip = new GpioPinToolStrip();
            pinToolStrip.Assign( pin );
            if ( pin.Direction == PinDirection.Input )
            {
                Add( pinToolStrip, this._InputPinsLayout, this.InputPins );
            }
            else
            {
                Add( pinToolStrip, this._OutputPinsLayout, this.OutputPins );
            }
        }

        /// <summary> Adds pin tool strip. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pinToolStrip"> The pin tool strip. </param>
        /// <param name="layout">       The layout. </param>
        /// <param name="pins">         Collection of pins. </param>
        private static void Add( GpioPinToolStrip pinToolStrip, TableLayoutPanel layout, GpioPinCollection pins )
        {
            if ( !pins.Any() )
            {
                layout.Controls.Clear();
                layout.ColumnStyles.Clear();
                layout.RowStyles.Clear();
                layout.ColumnCount = 4;
                _ = layout.ColumnStyles.Add( new ColumnStyle( SizeType.Absolute, 3f ) );
                _ = layout.ColumnStyles.Add( new ColumnStyle( SizeType.Percent, 50f ) );
                _ = layout.ColumnStyles.Add( new ColumnStyle( SizeType.Percent, 50f ) );
                _ = layout.ColumnStyles.Add( new ColumnStyle( SizeType.Absolute, 3f ) );
                layout.RowCount = 3;
                _ = layout.RowStyles.Add( new RowStyle( SizeType.Percent, 50f ) );
                _ = layout.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
                _ = layout.RowStyles.Add( new RowStyle( SizeType.Percent, 50f ) );
                layout.Dock = DockStyle.Fill;
                layout.Invalidate();
            }

            pins.Add( pinToolStrip.GpioPin );
            int rowIndex = 1 + (pins.Count - 1) / 2;
            int columnIndex = 1 + (pins.Count - 1) % 2;
            if ( rowIndex >= layout.RowCount - 1 )
            {
                layout.RowCount += 1;
                // layout.RowStyles.Insert(layout.RowStyles.Count - 2, New RowStyle(SizeType.AutoSize))
                layout.RowStyles.Insert( rowIndex, new RowStyle( SizeType.AutoSize ) );
                layout.Invalidate();
            }

            layout.Controls.Add( pinToolStrip, columnIndex, rowIndex );
            pinToolStrip.Dock = DockStyle.Top;
            pinToolStrip.Invalidate();
        }

        /// <summary> Creates a new pin number text box text changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void NewPinNumberTextBox_TextChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( sender is ToolStripTextBox textBox )
            {
                this._AddInputPinMenuItem.Text = $"Add input pin {textBox.Text}";
                this._AddOutputPinMenuItem.Text = $"Add output pin {textBox.Text}";
            }
        }

        /// <summary> Adds a pin handler to 'direction'. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">    The sender. </param>
        /// <param name="pinNumber"> The pin number. </param>
        /// <param name="direction"> The direction. </param>
        private void AddPinHandler( object sender, string pinNumber, PinDirection direction )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( int.TryParse( pinNumber, out int pinNo ) )
                {
                    if ( this.InputPins.Contains( pinNo ) )
                    {
                        _ = this._ErrorProvider.Annunciate( sender, $"Pin {pinNo} already exists as input" );
                    }
                    else if ( this.OutputPins.Contains( pinNo ) )
                    {
                        _ = this._ErrorProvider.Annunciate( sender, $"Pin {pinNo} already exists as output" );
                    }
                    else
                    {
                        this.Add( new GpioPin( this.Device.Gpio.Pins, pinNo, ActiveLogic.ActiveLow, direction ) );
                    }
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, $"Unable to convert {pinNumber} to a pin number" );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Adds an input pin menu item click to 'e'. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void AddInputPinMenuItem_Click( object sender, EventArgs e )
        {
            this.AddPinHandler( this._TopToolStrip, this._NewPinNumberTextBox.Text, PinDirection.Input );
        }

        /// <summary> Adds an output pin menu item click to 'e'. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void AddOutputPinMenuItem_Click( object sender, EventArgs e )
        {
            this.AddPinHandler( this._TopToolStrip, this._NewPinNumberTextBox.Text, PinDirection.Output );
        }

        #endregion

    }
}
