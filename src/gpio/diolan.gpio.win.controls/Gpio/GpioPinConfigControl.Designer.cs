using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Diolan.Gpio.WinControls
{

    public partial class GpioPinConfigControl
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._OpenDrainCheckBox = new System.Windows.Forms.CheckBox();
            this._PinPullupCheckBox = new System.Windows.Forms.CheckBox();
            this._EventPeriodNumeric = new System.Windows.Forms.NumericUpDown();
            this._PulldownCheckBox = new System.Windows.Forms.CheckBox();
            this._DebounceEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this._EventTypeComboBox = new System.Windows.Forms.ComboBox();
            this._PinDirectionComboBox = new System.Windows.Forms.ComboBox();
            this._PinNumberNumeric = new System.Windows.Forms.NumericUpDown();
            this._PinNameTextBox = new System.Windows.Forms.TextBox();
            this._ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this._EventPeriodNumericLabel = new System.Windows.Forms.Label();
            this._EventTypeComboBoxLabel = new System.Windows.Forms.Label();
            this._GetPinConfigButton = new System.Windows.Forms.Button();
            this._SetPinConfigButton = new System.Windows.Forms.Button();
            this._PinDirectionComboBoxLabel = new System.Windows.Forms.Label();
            this._PinEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this._PinNumericLabel = new System.Windows.Forms.Label();
            this._PinNameTextBoxLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._EventPeriodNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._PinNumberNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // _OpenDrainCheckBox
            // 
            this._OpenDrainCheckBox.AutoSize = true;
            this._OpenDrainCheckBox.Location = new System.Drawing.Point(182, 100);
            this._OpenDrainCheckBox.Name = "_OpenDrainCheckBox";
            this._OpenDrainCheckBox.Size = new System.Drawing.Size(86, 19);
            this._OpenDrainCheckBox.TabIndex = 10;
            this._OpenDrainCheckBox.Text = "Open Drain";
            this._ToolTip.SetToolTip(this._OpenDrainCheckBox, "Pin is open drain is checked.");
            this._OpenDrainCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PinPullupCheckBox
            // 
            this._PinPullupCheckBox.AutoSize = true;
            this._PinPullupCheckBox.Location = new System.Drawing.Point(14, 100);
            this._PinPullupCheckBox.Name = "_PinPullupCheckBox";
            this._PinPullupCheckBox.Size = new System.Drawing.Size(64, 19);
            this._PinPullupCheckBox.TabIndex = 8;
            this._PinPullupCheckBox.Text = "Pull Up";
            this._ToolTip.SetToolTip(this._PinPullupCheckBox, "pin is pulled up if checked.");
            this._PinPullupCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EventPeriodNumeric
            // 
            this._EventPeriodNumeric.Location = new System.Drawing.Point(128, 164);
            this._EventPeriodNumeric.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._EventPeriodNumeric.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this._EventPeriodNumeric.Name = "_EventPeriodNumeric";
            this._EventPeriodNumeric.Size = new System.Drawing.Size(59, 23);
            this._EventPeriodNumeric.TabIndex = 14;
            this._ToolTip.SetToolTip(this._EventPeriodNumeric, "Selects the event period in ms");
            // 
            // _PulldownCheckBox
            // 
            this._PulldownCheckBox.AutoSize = true;
            this._PulldownCheckBox.Location = new System.Drawing.Point(90, 100);
            this._PulldownCheckBox.Name = "_PulldownCheckBox";
            this._PulldownCheckBox.Size = new System.Drawing.Size(80, 19);
            this._PulldownCheckBox.TabIndex = 9;
            this._PulldownCheckBox.Text = "Pull Down";
            this._ToolTip.SetToolTip(this._PulldownCheckBox, "Pin is pulled down if checked");
            this._PulldownCheckBox.UseVisualStyleBackColor = true;
            // 
            // _DebounceEnabledCheckBox
            // 
            this._DebounceEnabledCheckBox.AutoSize = true;
            this._DebounceEnabledCheckBox.Location = new System.Drawing.Point(121, 36);
            this._DebounceEnabledCheckBox.Name = "_DebounceEnabledCheckBox";
            this._DebounceEnabledCheckBox.Size = new System.Drawing.Size(125, 19);
            this._DebounceEnabledCheckBox.TabIndex = 5;
            this._DebounceEnabledCheckBox.Text = "Debounce Enabled";
            this._ToolTip.SetToolTip(this._DebounceEnabledCheckBox, "Debounce is enabled if checked.");
            this._DebounceEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EventTypeComboBox
            // 
            this._EventTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._EventTypeComboBox.FormattingEnabled = true;
            this._EventTypeComboBox.Location = new System.Drawing.Point(89, 133);
            this._EventTypeComboBox.Name = "_EventTypeComboBox";
            this._EventTypeComboBox.Size = new System.Drawing.Size(101, 23);
            this._EventTypeComboBox.TabIndex = 12;
            this._ToolTip.SetToolTip(this._EventTypeComboBox, "Selects the event type.");
            // 
            // _PinDirectionComboBox
            // 
            this._PinDirectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._PinDirectionComboBox.FormattingEnabled = true;
            this._PinDirectionComboBox.Location = new System.Drawing.Point(76, 68);
            this._PinDirectionComboBox.Name = "_PinDirectionComboBox";
            this._PinDirectionComboBox.Size = new System.Drawing.Size(79, 23);
            this._PinDirectionComboBox.TabIndex = 7;
            this._ToolTip.SetToolTip(this._PinDirectionComboBox, "Selects pin direction.");
            // 
            // _PinNumberNumeric
            // 
            this._PinNumberNumeric.Location = new System.Drawing.Point(48, 2);
            this._PinNumberNumeric.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._PinNumberNumeric.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this._PinNumberNumeric.Name = "_PinNumberNumeric";
            this._PinNumberNumeric.Size = new System.Drawing.Size(35, 23);
            this._PinNumberNumeric.TabIndex = 1;
            this._ToolTip.SetToolTip(this._PinNumberNumeric, "Pin number");
            // 
            // _PinNameTextBox
            // 
            this._PinNameTextBox.Location = new System.Drawing.Point(145, 2);
            this._PinNameTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._PinNameTextBox.Name = "_PinNameTextBox";
            this._PinNameTextBox.Size = new System.Drawing.Size(131, 23);
            this._PinNameTextBox.TabIndex = 3;
            this._ToolTip.SetToolTip(this._PinNameTextBox, "Pin name");
            // 
            // _ErrorProvider
            // 
            this._ErrorProvider.ContainerControl = this;
            // 
            // _EventPeriodNumericLabel
            // 
            this._EventPeriodNumericLabel.AutoSize = true;
            this._EventPeriodNumericLabel.Location = new System.Drawing.Point(22, 168);
            this._EventPeriodNumericLabel.Name = "_EventPeriodNumericLabel";
            this._EventPeriodNumericLabel.Size = new System.Drawing.Size(103, 15);
            this._EventPeriodNumericLabel.TabIndex = 13;
            this._EventPeriodNumericLabel.Text = "Event Period [ms]:";
            this._EventPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EventTypeComboBoxLabel
            // 
            this._EventTypeComboBoxLabel.AutoSize = true;
            this._EventTypeComboBoxLabel.Location = new System.Drawing.Point(20, 137);
            this._EventTypeComboBoxLabel.Name = "_EventTypeComboBoxLabel";
            this._EventTypeComboBoxLabel.Size = new System.Drawing.Size(66, 15);
            this._EventTypeComboBoxLabel.TabIndex = 11;
            this._EventTypeComboBoxLabel.Text = "Event Type:";
            this._EventTypeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _GetPinConfigButton
            // 
            this._GetPinConfigButton.Location = new System.Drawing.Point(215, 130);
            this._GetPinConfigButton.Name = "_GetPinConfigButton";
            this._GetPinConfigButton.Size = new System.Drawing.Size(61, 27);
            this._GetPinConfigButton.TabIndex = 15;
            this._GetPinConfigButton.Text = "Get";
            this._GetPinConfigButton.UseVisualStyleBackColor = true;
            this._GetPinConfigButton.Click += new System.EventHandler(this.GetPinConfigButton_Click);
            // 
            // _SetPinConfigButton
            // 
            this._SetPinConfigButton.Location = new System.Drawing.Point(215, 164);
            this._SetPinConfigButton.Name = "_SetPinConfigButton";
            this._SetPinConfigButton.Size = new System.Drawing.Size(61, 27);
            this._SetPinConfigButton.TabIndex = 16;
            this._SetPinConfigButton.Text = "Set";
            this._SetPinConfigButton.UseVisualStyleBackColor = true;
            this._SetPinConfigButton.Click += new System.EventHandler(this.SetPinConfigButton_Click);
            // 
            // _PinDirectionComboBoxLabel
            // 
            this._PinDirectionComboBoxLabel.AutoSize = true;
            this._PinDirectionComboBoxLabel.Location = new System.Drawing.Point(15, 72);
            this._PinDirectionComboBoxLabel.Name = "_PinDirectionComboBoxLabel";
            this._PinDirectionComboBoxLabel.Size = new System.Drawing.Size(58, 15);
            this._PinDirectionComboBoxLabel.TabIndex = 6;
            this._PinDirectionComboBoxLabel.Text = "Direction:";
            this._PinDirectionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PinEnabledCheckBox
            // 
            this._PinEnabledCheckBox.AutoSize = true;
            this._PinEnabledCheckBox.Location = new System.Drawing.Point(38, 36);
            this._PinEnabledCheckBox.Name = "_PinEnabledCheckBox";
            this._PinEnabledCheckBox.Size = new System.Drawing.Size(68, 19);
            this._PinEnabledCheckBox.TabIndex = 4;
            this._PinEnabledCheckBox.Text = "Enabled";
            this._PinEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PinNumericLabel
            // 
            this._PinNumericLabel.AutoSize = true;
            this._PinNumericLabel.Location = new System.Drawing.Point(8, 6);
            this._PinNumericLabel.Name = "_PinNumericLabel";
            this._PinNumericLabel.Size = new System.Drawing.Size(37, 15);
            this._PinNumericLabel.TabIndex = 0;
            this._PinNumericLabel.Text = "Pin #:";
            this._PinNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PinNameTextBoxLabel
            // 
            this._PinNameTextBoxLabel.AutoSize = true;
            this._PinNameTextBoxLabel.Location = new System.Drawing.Point(100, 6);
            this._PinNameTextBoxLabel.Name = "_PinNameTextBoxLabel";
            this._PinNameTextBoxLabel.Size = new System.Drawing.Size(42, 15);
            this._PinNameTextBoxLabel.TabIndex = 2;
            this._PinNameTextBoxLabel.Text = "Name:";
            // 
            // GpioPinConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._PinNameTextBox);
            this.Controls.Add(this._PinNameTextBoxLabel);
            this.Controls.Add(this._PinNumberNumeric);
            this.Controls.Add(this._OpenDrainCheckBox);
            this.Controls.Add(this._PinPullupCheckBox);
            this.Controls.Add(this._EventPeriodNumeric);
            this.Controls.Add(this._PulldownCheckBox);
            this.Controls.Add(this._DebounceEnabledCheckBox);
            this.Controls.Add(this._EventPeriodNumericLabel);
            this.Controls.Add(this._EventTypeComboBoxLabel);
            this.Controls.Add(this._EventTypeComboBox);
            this.Controls.Add(this._GetPinConfigButton);
            this.Controls.Add(this._SetPinConfigButton);
            this.Controls.Add(this._PinDirectionComboBox);
            this.Controls.Add(this._PinDirectionComboBoxLabel);
            this.Controls.Add(this._PinEnabledCheckBox);
            this.Controls.Add(this._PinNumericLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GpioPinConfigControl";
            this.Size = new System.Drawing.Size(283, 196);
            ((System.ComponentModel.ISupportInitialize)(this._EventPeriodNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._PinNumberNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private ToolTip _ToolTip;
        private ErrorProvider _ErrorProvider;
        private TextBox _PinNameTextBox;
        private Label _PinNameTextBoxLabel;
        private NumericUpDown _PinNumberNumeric;
        private Label _PinNumericLabel;
        private CheckBox _PinEnabledCheckBox;
        private Label _PinDirectionComboBoxLabel;
        private ComboBox _PinDirectionComboBox;
        private Button _SetPinConfigButton;
        private Button _GetPinConfigButton;
        private ComboBox _EventTypeComboBox;
        private Label _EventTypeComboBoxLabel;
        private Label _EventPeriodNumericLabel;
        private CheckBox _DebounceEnabledCheckBox;
        private CheckBox _PulldownCheckBox;
        private NumericUpDown _EventPeriodNumeric;
        private CheckBox _PinPullupCheckBox;
        private CheckBox _OpenDrainCheckBox;
    }
}
