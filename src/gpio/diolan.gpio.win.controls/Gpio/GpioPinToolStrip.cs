using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Diolan.WinControls.ErrorProviderExtensions;

namespace isr.Diolan.Gpio.WinControls
{

    /// <summary> A gpio pin tool strip. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-04-07 </para>
    /// </remarks>
    public partial class GpioPinToolStrip : System.Windows.Forms.UserControl, INotifyPropertyChanged
    {

        #region " CONSTRUCTOR "

        private bool InitializingComponents { get; set; }
        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioPinToolStrip()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();
            this.InitializingComponents = false;

            // Add any initialization after the InitializeComponent() call.
            this.Enabled = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.Gpio_Pin = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " PIN "

        private GpioPin _Gpio_Pin;

        private GpioPin Gpio_Pin
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._Gpio_Pin;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._Gpio_Pin != null )
                {
                    this._Gpio_Pin.PropertyChanged -= this.GpioPin_PropertyChanged;
                }

                this._Gpio_Pin = value;
                if ( this._Gpio_Pin != null )
                {
                    this._Gpio_Pin.PropertyChanged += this.GpioPin_PropertyChanged;
                }
            }
        }

        /// <summary> Gets the gpio pin. </summary>
        /// <value> The gpio pin. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public GpioPin GpioPin => this.Gpio_Pin;

        /// <summary> Executes the property changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioPin sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( isr.Diolan.Gpio.GpioPin.Name ):
                    {
                        this._PinNameLabel.Text = sender.Name;
                        break;
                    }

                case nameof( isr.Diolan.Gpio.GpioPin.PinNumber ):
                    {
                        this._PinNumberLabel.Text = sender.PinNumber.ToString();
                        break;
                    }

                case nameof( isr.Diolan.Gpio.GpioPin.BitValue ):
                    {
                        this._LogicalStateButton.Checked = sender.LogicalState == LogicalState.Active;
                        break;
                    }

                case nameof( isr.Diolan.Gpio.GpioPin.ActiveLogic ):
                    {
                        this._ActiveLogicToggleButton.Checked = sender.ActiveLogic == ActiveLogic.ActiveHigh;
                        break;
                    }

                case nameof( isr.Diolan.Gpio.GpioPin.Direction ):
                    {
                        this._LogicalStateButton.CheckOnClick = sender.Direction == PinDirection.Output;
                        break;
                    }

                case nameof( isr.Diolan.Gpio.GpioPin.Enabled ):
                    {
                        this._ActiveLogicToggleButton.Enabled = sender.Enabled;
                        this._LogicalStateButton.Enabled = sender.Enabled;
                        break;
                    }
            }
        }

        /// <summary> Gpio pin property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        private void GpioPin_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.GpioPin_PropertyChanged ), new object[] { sender, e } );
                }

                this.OnPropertyChanged( sender as GpioPin, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Assigns the given pin. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pin"> The pin. </param>
        public void Assign( GpioPin pin )
        {
            this.Gpio_Pin = pin;
            this.Enabled = pin is object;
        }

        /// <summary> Configure button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                using var configForm = new GpioPinConfigDialog();
                _ = configForm.ShowDialog( this, this.GpioPin );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Logical state button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LogicalStateButton_Click( object sender, EventArgs e )
        {
            if ( sender is not ToolStripButton button )
                return;
            try
            {
                this._ErrorProvider.Clear();
                button.Image = this.GpioPin.LogicalState == LogicalState.Active
                    ? isr.Diolan.Gpio.WinControls.Properties.Resources.circle_green
                    : isr.Diolan.Gpio.WinControls.Properties.Resources.circle_grey;
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Logical state button check state changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LogicalStateButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( sender is not ToolStripButton button )
                return;
            try
            {
                this._ErrorProvider.Clear();
                if ( this.GpioPin.Pin.Direction == ( int ) PinDirection.Output )
                {
                    this.GpioPin.LogicalState = button.Checked ? LogicalState.Active : LogicalState.Inactive;
                }
                else
                {
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
                button.Image = this.GpioPin.LogicalState == LogicalState.Active ? isr.Diolan.Gpio.WinControls.Properties.Resources.circle_green : isr.Diolan.Gpio.WinControls.Properties.Resources.circle_grey;
            }
        }

        /// <summary> Active logic toggle button check state changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ActiveLogicToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( sender is not ToolStripButton button )
                return;
            try
            {
                this._ErrorProvider.Clear();
                this.GpioPin.ActiveLogic = button.Checked ? ActiveLogic.ActiveHigh : ActiveLogic.ActiveLow;
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
                button.Image = this.GpioPin.ActiveLogic == ActiveLogic.ActiveHigh ? isr.Diolan.Gpio.WinControls.Properties.Resources.Plus_Grey : isr.Diolan.Gpio.WinControls.Properties.Resources.Minus_Grey;
            }
        }

        #endregion

    }
}
