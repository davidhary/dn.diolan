using System;
using System.Diagnostics;
using System.Drawing;
using System.Resources;

namespace isr.Diolan.Handler
{

    /// <summary> The material handler test console. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-06 </para>
    /// </remarks>
    public partial class HandlerConsole : System.Windows.Forms.Form
    {

        #region " CONSTRUCTION and  "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public HandlerConsole()
        {

            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._SeverConnector = LocalhostConnector.SingleInstance();
            this._GpioPanel.ServerConnector = this._SeverConnector;
            this._GpioHandlerPanel.GpioMaterialHandler.ServerConnector = this._SeverConnector;

            //var resources = new System.ComponentModel.ComponentResourceManager( typeof( HandlerConsole ) );
            //this.Icon = ( Icon ) resources.GetObject( "$this.Icon" );
            this.Icon = isr.Diolan.Handler.Properties.Resources.Material_Handler;

        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources when called from the
        /// runtime finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._SeverConnector is object )
                        this._SeverConnector = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " SERVER CONNECTOR "

        /// <summary> The sever connector. </summary>
        private LocalhostConnector _SeverConnector;

        #endregion

    }
}
