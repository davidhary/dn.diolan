using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Diolan.Handler
{

    public partial class HandlerConsole : System.Windows.Forms.Form
    {

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this._Tabs = new System.Windows.Forms.TabControl();
            this._HandlerTabPage = new System.Windows.Forms.TabPage();
            this._GpioHandlerPanel = new isr.Diolan.Gpio.Handler.WinControls.GpioMaterialHandlerView();
            this._GpioTabPage = new System.Windows.Forms.TabPage();
            this._GpioPanel = new isr.Diolan.Gpio.WinControls.GpioView();
            this._Tabs.SuspendLayout();
            this._HandlerTabPage.SuspendLayout();
            this._GpioTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // _Tabs
            // 
            this._Tabs.Controls.Add(this._HandlerTabPage);
            this._Tabs.Controls.Add(this._GpioTabPage);
            this._Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Tabs.Location = new System.Drawing.Point(0, 0);
            this._Tabs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._Tabs.Name = "_Tabs";
            this._Tabs.SelectedIndex = 0;
            this._Tabs.Size = new System.Drawing.Size(525, 537);
            this._Tabs.TabIndex = 0;
            // 
            // _HandlerTabPage
            // 
            this._HandlerTabPage.Controls.Add(this._GpioHandlerPanel);
            this._HandlerTabPage.Location = new System.Drawing.Point(4, 24);
            this._HandlerTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._HandlerTabPage.Name = "_HandlerTabPage";
            this._HandlerTabPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._HandlerTabPage.Size = new System.Drawing.Size(517, 509);
            this._HandlerTabPage.TabIndex = 0;
            this._HandlerTabPage.Text = "Handler";
            this._HandlerTabPage.UseVisualStyleBackColor = true;
            // 
            // _GpioHandlerPanel
            // 
            this._GpioHandlerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._GpioHandlerPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioHandlerPanel.Location = new System.Drawing.Point(3, 2);
            this._GpioHandlerPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._GpioHandlerPanel.Name = "_GpioHandlerPanel";
            this._GpioHandlerPanel.Size = new System.Drawing.Size(511, 505);
            this._GpioHandlerPanel.TabIndex = 0;
            // 
            // _GpioTabPage
            // 
            this._GpioTabPage.Controls.Add(this._GpioPanel);
            this._GpioTabPage.Location = new System.Drawing.Point(4, 24);
            this._GpioTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._GpioTabPage.Name = "_GpioTabPage";
            this._GpioTabPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._GpioTabPage.Size = new System.Drawing.Size(517, 509);
            this._GpioTabPage.TabIndex = 1;
            this._GpioTabPage.Text = "GPIO";
            this._GpioTabPage.UseVisualStyleBackColor = true;
            // 
            // _GpioPanel
            // 
            this._GpioPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._GpioPanel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GpioPanel.Location = new System.Drawing.Point(3, 2);
            this._GpioPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._GpioPanel.Name = "_GpioPanel";
            this._GpioPanel.Size = new System.Drawing.Size(511, 505);
            this._GpioPanel.TabIndex = 0;
            // 
            // HandlerConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 537);
            this.Controls.Add(this._Tabs);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "HandlerConsole";
            this.Text = "Material Handler Console";
            this._Tabs.ResumeLayout(false);
            this._HandlerTabPage.ResumeLayout(false);
            this._GpioTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private TabControl _Tabs;
        private TabPage _HandlerTabPage;
        private TabPage _GpioTabPage;
        private isr.Diolan.Gpio.Handler.WinControls.GpioMaterialHandlerView _GpioHandlerPanel;
        private isr.Diolan.Gpio.WinControls.GpioView _GpioPanel;
    }
}
