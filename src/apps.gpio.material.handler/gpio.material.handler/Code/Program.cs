using System;
using System.Windows.Forms;

namespace isr.Diolan.Handler
{

    /// <summary>   A program. </summary>
    /// <remarks>   David, 2021-07-22. </remarks>
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
#if NET5_0_OR_GREATER
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
#endif
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Application.Run( new HandlerConsole() );
        }
    }
}
