Imports isr.Diolan.WinControls.ErrorProviderExtensions

''' <summary> Test console. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-06-06 </para>
''' </remarks>
Public Class HandlerConsole
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._SeverConnector = LocalhostConnector.SingleInstance
        Me._GpioPanel.ServerConnector = Me._SeverConnector
        Me._GpioHandlerPanel.GpioMaterialHandler.ServerConnector = Me._SeverConnector

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._SeverConnector IsNot Nothing Then Me._SeverConnector = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " SERVER CONNECTOR "

    ''' <summary> The sever connector. </summary>
    Private _SeverConnector As LocalhostConnector

#End Region

End Class
