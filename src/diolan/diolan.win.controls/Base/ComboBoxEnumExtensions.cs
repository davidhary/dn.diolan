using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using FastEnums;

namespace isr.Diolan.WinControls.ComboBoxEnumExtensions
{

    /// <summary> Encapsulates the Form related functionality of the <see cref="FastEnums"/></summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-25 </para><para>
    /// David, 2008-05-04" by="Stewart and StageSoft" revision="1.2.3411.x"> <para>
    /// From Strong-Type and Efficient .NET Enums By Hanna GIAT</para><para>
    /// http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx. </para> </para></remarks>
    public static class ComboBoxEnumExtensionMethods
    {

        #region " COMBO BOX ENUM VALUE SELECTION "

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="comboBox">     The combo box control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The selected Enum. </returns>
        public static Enum SelectedEnumValue( this ComboBox comboBox, Enum defaultValue )
        {
            if ( comboBox is object && comboBox.SelectedItem is object && defaultValue is object )
            {
                KeyValuePair<Enum, string> kvp = ( KeyValuePair<Enum, string> ) comboBox.SelectedItem;
                if ( Enum.IsDefined( defaultValue.GetType(), kvp.Key ) )
                {
                    defaultValue = kvp.Key;
                }
            }

            return defaultValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl"> The list control. </param>
        /// <returns> The selected Enum. </returns>
        public static TEnum SelectedEnumValue<TEnum>( this ListControl listControl ) where TEnum : struct, Enum
        {
            return listControl is null ? throw new ArgumentNullException( nameof( listControl ) ) : ( TEnum ) listControl.SelectedValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">  The list control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The selected Enum. </returns>
        public static TEnum SelectedEnumValue<TEnum>( this ListControl listControl, TEnum defaultValue ) where TEnum : struct, Enum
        {
            return listControl is null
                ? throw new ArgumentNullException( nameof( listControl ) )
                : Enum.IsDefined( typeof( TEnum ), listControl.SelectedValue ) ? ( TEnum ) listControl.SelectedValue : defaultValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="comboBox">     The tool strip combo box control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The selected Enum. </returns>
        public static Enum SelectedEnumValue( this ToolStripComboBox comboBox, Enum defaultValue )
        {
            if ( comboBox is object && comboBox.SelectedItem is object && defaultValue is object )
            {
                KeyValuePair<Enum, string> kvp = ( KeyValuePair<Enum, string> ) comboBox.SelectedItem;
                if ( Enum.IsDefined( defaultValue.GetType(), kvp.Key ) )
                {
                    defaultValue = kvp.Key;
                }
            }

            return defaultValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The tool strip combo box control. </param>
        /// <returns> The selected Enum. </returns>
        public static TEnum SelectedEnumValue<TEnum>( this ToolStripComboBox comboBox ) where TEnum : struct, Enum
        {
            return comboBox is null ? throw new ArgumentNullException( nameof( comboBox ) ) : ( TEnum ) comboBox.ComboBox.SelectedValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox">     The tool strip combo box control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The selected Enum. </returns>
        public static TEnum SelectedEnumValue<TEnum>( this ToolStripComboBox comboBox, TEnum defaultValue ) where TEnum : struct, Enum
        {
            return comboBox is null
                ? throw new ArgumentNullException( nameof( comboBox ) )
                : Enum.IsDefined( typeof( TEnum ), comboBox.ComboBox.SelectedValue ) ? ( TEnum ) comboBox.ComboBox.SelectedValue : defaultValue;
        }

        /// <summary> Returns the selected enum item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<TEnum, string> SelectedEnumItem<TEnum>( this ComboBox comboBox ) where TEnum : struct, Enum
        {
            return comboBox is null ? throw new ArgumentNullException( nameof( comboBox ) ) : ( KeyValuePair<TEnum, string> ) comboBox.SelectedItem;
        }

        /// <summary> Returns the selected enum item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="toolStripComboBox"> The tool strip combo box control. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<TEnum, string> SelectedEnumItem<TEnum>( this ToolStripComboBox toolStripComboBox ) where TEnum : struct, Enum
        {
            return toolStripComboBox is null
                ? throw new ArgumentNullException( nameof( toolStripComboBox ) )
                : ( KeyValuePair<TEnum, string> ) toolStripComboBox.SelectedItem;
        }

        /// <summary> Selects an item from the control and return the selected value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="item">     The item. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<TEnum, string> SelectItem<TEnum>( this ComboBox comboBox, KeyValuePair<TEnum, string> item ) where TEnum : struct, Enum
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.SelectedItem = item;
            return ( KeyValuePair<TEnum, string> ) comboBox.SelectedItem;
        }

        /// <summary> Selects a value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<TEnum, string> SelectValue<TEnum>( this ComboBox comboBox, TEnum value ) where TEnum : struct, Enum
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.SelectedValue = value;
            return ( KeyValuePair<TEnum, string> ) comboBox.SelectedItem;
        }

        /// <summary> Selects a combo box value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<Enum, string> SelectValue( this ComboBox comboBox, Enum value )
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.SelectedValue = value;
            return ( KeyValuePair<Enum, string> ) comboBox.SelectedItem;
        }

        /// <summary> Selects a combo box value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<TEnum, string> SelectValue<TEnum>( this ToolStripComboBox comboBox, TEnum value ) where TEnum : struct, Enum
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.ComboBox.SelectedValue = value;
            return ( KeyValuePair<TEnum, string> ) comboBox.SelectedItem;
        }

        /// <summary> Selects a combo box value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<Enum, string> SelectValue( this ToolStripComboBox comboBox, Enum value )
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.ComboBox.SelectedValue = value;
            return ( KeyValuePair<Enum, string> ) comboBox.SelectedItem;
        }

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control. </param>
        /// <param name="value">   Specifies the bit values which to add. </param>
        public static void SelectValue<TEnum>( this ListControl control, TEnum value ) where TEnum : struct, Enum
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            control.SelectedValue = value.ToInteger();
        }

        /// <summary> Selects a value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control. </param>
        /// <param name="value">   Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static Enum SelectValue( this ListControl control, Enum value )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            control.SelectedValue = value;
            return ( Enum ) control.SelectedValue;
        }

        #endregion

        #region " GENERTIC LIST CONTROL ENUM FUNCTIONS: LIST "

        /// <summary> List enum names. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <returns> An Integer. </returns>
        public static int ListEnumNames<T>( this ListControl listControl )
        {
            return listControl.ListEnums( typeof( T ).ValueNamePairs() );
        }

        /// <summary> Lists the enum names filtered by the provided masks. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <param name="includeMask"> The include mask. </param>
        /// <param name="excludeMask"> The exclude mask. </param>
        public static void ListEnumNames( this ListControl listControl, Enum includeMask, Enum excludeMask )
        {
            _ = listControl.ListEnums( includeMask.EnumValues().Filter( includeMask, excludeMask ).ValueNamePairs() );
        }

        /// <summary> Lists the enum names filtered by the provided masks. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <param name="includeMask"> The include mask. </param>
        /// <param name="excludeMask"> The exclude mask. </param>
        public static void ListEnumNames<TEnum>( this ListControl listControl, TEnum includeMask, TEnum excludeMask ) where TEnum : struct, Enum
        {
            _ = listControl.ListEnums( typeof( TEnum ).EnumValues<TEnum>().Filter( includeMask, excludeMask ).ValueNamePairs() );
        }

        /// <summary> List enum descriptions. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <returns> An Integer. </returns>
        public static int ListEnumDescriptions<TEnum>( this ListControl listControl ) where TEnum : struct, Enum
        {
            return listControl.ListEnums( typeof( TEnum ).ValueDescriptionPairs() );
        }

        /// <summary> Lists the enum descriptions. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <param name="includeMask"> The include mask. </param>
        /// <param name="excludeMask"> The exclude mask. </param>
        public static void ListEnumDescriptions( this ListControl listControl, Enum includeMask, Enum excludeMask )
        {
            _ = listControl.ListEnums( includeMask.EnumValues().Filter( includeMask, excludeMask ).ValueDescriptionPairs() );
        }

        /// <summary> Lists the enum descriptions. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <param name="includeMask"> The include mask. </param>
        /// <param name="excludeMask"> The exclude mask. </param>
        public static void ListEnumDescriptions<TEnum>( this ListControl listControl, TEnum includeMask, TEnum excludeMask ) where TEnum : struct, Enum
        {
            _ = listControl.ListEnums( typeof( TEnum ).EnumValues<TEnum>().Filter( includeMask, excludeMask ).ValueDescriptionPairs() );
        }

        /// <summary> List enum value and string pairs. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">   The list control. </param>
        /// <param name="enumTextPairs"> The enum text pairs. </param>
        /// <returns> An Integer. </returns>
        public static int ListEnums( this ListControl listControl, IEnumerable<KeyValuePair<Enum, string>> enumTextPairs )
        {
            if ( listControl is null )
            {
                throw new ArgumentNullException( nameof( listControl ) );
            }

            if ( enumTextPairs is null )
            {
                throw new ArgumentNullException( nameof( enumTextPairs ) );
            }

            bool comboEnabled = listControl.Enabled;
            listControl.Enabled = false;
            listControl.DataSource = null;
            listControl.ValueMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Key );
            listControl.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Value );

            // converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList();
            listControl.SelectedIndex = enumTextPairs.Any() ? 0 : -1;
            listControl.Enabled = comboEnabled;
            listControl.Invalidate();
            return enumTextPairs.Count();
        }

        /// <summary> List enum value and string pairs. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">   The list control. </param>
        /// <param name="enumTextPairs"> The enum text pairs. </param>
        /// <returns> An Integer. </returns>
        public static int ListEnums<TEnum>( this ListControl listControl, IEnumerable<KeyValuePair<TEnum, string>> enumTextPairs ) where TEnum : struct, Enum
        {
            if ( listControl is null )
            {
                throw new ArgumentNullException( nameof( listControl ) );
            }

            if ( enumTextPairs is null )
            {
                throw new ArgumentNullException( nameof( enumTextPairs ) );
            }

            bool comboEnabled = listControl.Enabled;
            listControl.Enabled = false;
            listControl.ValueMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Key );
            listControl.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Value );

            // converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList();
            listControl.SelectedIndex = enumTextPairs.Any() ? 0 : -1;
            listControl.Enabled = comboEnabled;
            listControl.Invalidate();
            return enumTextPairs.Count();
        }

        #endregion

        #region " GENERTIC LIST CONTROL DICTIONARY FUNCTIONS "

        /// <summary> List values. </summary>
        /// <remarks>
        /// The first step is to add a description attribute to your Enum.
        /// <code>
        /// Public Enum SimpleEnum2
        /// [System.ComponentModel.Description("Today")] Today
        /// [System.ComponentModel.Description("Last 7 days")] Last7
        /// [System.ComponentModel.Description("Last 14 days")] Last14
        /// [System.ComponentModel.Description("Last 30 days")] Last30
        /// [System.ComponentModel.Description("All")] All
        /// End Enum
        /// Public SimpleEfficientEnum As New EnumExtender(of SimpleEnum2)
        /// Dim combo As ComboBox = New ComboBox()
        /// combo.DataSource = SimpleEfficientEnum.GetValueDescriptionPairs(GetType(SimpleEnum2)).ToList
        /// combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        /// combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        /// dim selectedEnum as SimpleEnum2 = SimpleEfficientEnum.ToObjectFromPair(combo.SelectedItem)
        /// selectedEnum = combo.selectedValue(of SimpleEnum2)(SimpleEfficientEnum.ValueEnumPairs)
        /// </code>
        /// This works with any control that supports data binding, including the
        /// <see cref="System.Windows.Forms.ToolStripComboBox">tool strip combo box</see>,
        /// although you will need to cast the
        /// <see cref="System.Windows.Forms.ToolStripComboBox">control property</see>
        /// to a <see cref="System.Windows.Forms.ComboBox">Combo Box</see> to get to the DataSource
        /// property. In that case, you will also want to perform the same cast when you are referencing
        /// the selected value to work with it as your Enum type.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">   The list control. </param>
        /// <param name="enumTextPairs"> The enum text pairs. </param>
        /// <returns> The item count. </returns>
        public static int ListValues<TEnum>( this ListControl listControl, IDictionary<TEnum, string> enumTextPairs ) where TEnum : struct, Enum
        {
            return ComboBoxEnumExtensionMethods.ListValues<TEnum>( listControl, enumTextPairs.ToList() );
        }

        /// <summary>   List values. </summary>
        /// <remarks>
        /// The first step is to add a description attribute to your Enum.
        /// <code>
        /// Public Enum SimpleEnum2
        /// [System.ComponentModel.Description("Today")] Today
        /// [System.ComponentModel.Description("Last 7 days")] Last7
        /// [System.ComponentModel.Description("Last 14 days")] Last14
        /// [System.ComponentModel.Description("Last 30 days")] Last30
        /// [System.ComponentModel.Description("All")] All
        /// End Enum
        /// Public SimpleEfficientEnum As New EnumExtender(of SimpleEnum2)
        /// Dim combo As ComboBox = New ComboBox()
        /// combo.DataSource = SimpleEfficientEnum.GetValueDescriptionPairs(GetType(SimpleEnum2)).ToList
        /// combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        /// combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        /// dim selectedEnum as SimpleEnum2 = SimpleEfficientEnum.ToObjectFromPair(combo.SelectedItem)
        /// selectedEnum = combo.selectedValue(of SimpleEnum2)(SimpleEfficientEnum.ValueEnumPairs)
        /// </code>
        /// This works with any control that supports data binding, including the
        /// <see cref="System.Windows.Forms.ToolStripComboBox">tool strip combo box</see>,
        /// although you will need to cast the
        /// <see cref="System.Windows.Forms.ToolStripComboBox">control property</see>
        /// to a <see cref="System.Windows.Forms.ComboBox">Combo Box</see> to get to the DataSource
        /// property. In that case, you will also want to perform the same cast when you are referencing
        /// the selected value to work with it as your Enum type.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <typeparam name="TEnum">    Type of the enum. </typeparam>
        /// <param name="listControl">      The list control. </param>
        /// <param name="enumTextPairs">    The enum text pairs. </param>
        /// <returns>   The item count. </returns>
        public static int ListValues<TEnum>( this ListControl listControl, IEnumerable<KeyValuePair<TEnum, string>> enumTextPairs ) where TEnum : struct, Enum
        {
            if ( listControl is null )
            {
                throw new ArgumentNullException( nameof( listControl ) );
            }

            if ( enumTextPairs is null )
            {
                throw new ArgumentNullException( nameof( enumTextPairs ) );
            }

            bool comboEnabled = listControl.Enabled;
            listControl.Enabled = false;
            listControl.DataSource = null;
            listControl.ValueMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Key );
            listControl.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Value );

            // converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList();
            listControl.SelectedIndex = enumTextPairs.Any() ? 0 : -1;
            listControl.Enabled = comboEnabled;
            listControl.Invalidate();
            return ( int ) enumTextPairs.LongCount();
        }


        /// <summary> Selected value. </summary>
        /// <remarks> David, 2020-09-04. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox">       The combo box control. </param>
        /// <param name="valueEnumPairs"> The value enum pairs. </param>
        /// <returns> The selected Enum. </returns>
        public static TEnum SelectedValue<TEnum>( this ComboBox comboBox, IDictionary<int, TEnum> valueEnumPairs ) where TEnum : struct, Enum
        {
            return comboBox is null ? throw new ArgumentNullException( nameof( comboBox ) ) : valueEnumPairs.ToObject( comboBox.SelectedItem );
        }

        /// <summary> Returns the value converted to Enum from the given value name pair. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        /// <returns> converted to Enum from the given value name pair. </returns>
        public static TEnum ToObject<TEnum>( this IDictionary<int, TEnum> values, object value ) where TEnum : struct, Enum
        {
            return value is null ? default : values.ToObject( (( KeyValuePair<TEnum, string> ) value).Key );
        }

        /// <summary>   Selected value. </summary>
        /// <remarks>   David, 2021-03-01. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <typeparam name="TEnum">    Type of the enum. </typeparam>
        /// <param name="comboBox">         The combo box control. </param>
        /// <param name="valueEnumPairs">   The value enum pairs. </param>
        /// <returns>   The selected Enum. </returns>
        public static TEnum SelectedValue<TEnum>( this ComboBox comboBox, IEnumerable<KeyValuePair<int, TEnum>> valueEnumPairs ) where TEnum : struct, Enum
        {
            return comboBox is null ? throw new ArgumentNullException( nameof( comboBox ) ) : valueEnumPairs.ToObject( comboBox.SelectedItem );
        }

        /// <summary> Returns the value converted to Enum from the given value name pair. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        /// <returns> converted to Enum from the given value name pair. </returns>
        public static TEnum ToObject<TEnum>( this IEnumerable<KeyValuePair<int, TEnum>> values, object value ) where TEnum : struct, Enum
        {
            return value is null ? default : values.ToObject( (( KeyValuePair<TEnum, string> ) value).Key );
        }


        /// <summary> Converts a value to an integer. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> value as an Integer. </returns>
        public static int ToInteger( this object value )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            try
            {
                return Convert.ToInt32( value );
            }
            catch ( InvalidCastException ex )
            {
                throw new ArgumentException( $"Cannot convert {value} to integer", nameof( value ), ex );
            }
        }

        #endregion

        #region " GENERIC LIST VALUES "

        /// <summary>   List values. </summary>
        /// <remarks>   David, 2020-09-16. </remarks>
        /// <typeparam name="TEnum">    Type of the enum. </typeparam>
        /// <param name="listControl">  The list control. </param>
        /// <returns>   The item count. </returns>
        public static int ListValues<TEnum>( this ListControl listControl ) where TEnum : struct, Enum
        {
            return listControl.ListValues( FastEnum.GetValues<TEnum>().ValueDescriptionPairs() );
        }

        /// <summary> Selected value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox">     The combo box control. </param>
        /// <returns> The selected Enum. </returns>
        public static TEnum SelectedValue<TEnum>( this ComboBox comboBox ) where TEnum : struct, Enum
        {
            return comboBox.SelectedValue( FastEnum.GetValues<TEnum>().Int32EnumValuePairs() );
        }

        #endregion

    }
}
