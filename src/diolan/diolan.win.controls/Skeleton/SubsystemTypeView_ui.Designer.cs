using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Diolan.WinControls
{

    public partial class SubsystemTypeView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._BottomToolStrip = new System.Windows.Forms.ToolStrip();
            this._SelectTabButton = new System.Windows.Forms.ToolStripSplitButton();
            this._SelectPrimaryTabMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._SelectEventsTabMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._SelectServerButton = new System.Windows.Forms.ToolStripSplitButton();
            this._ServerNameTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.DefaultServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._ConnectServerButton = new System.Windows.Forms.ToolStripButton();
            this._DeviceInfoTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._SelectDeviceSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this._DevicesComboBox = new System.Windows.Forms.ToolStripComboBox();
            this._OpenDeviceSubsystemTypeButton = new System.Windows.Forms.ToolStripButton();
            this._Tabs = new isr.Diolan.WinControls.ExtendedTabControl();
            this._PrimaryTabPage = new System.Windows.Forms.TabPage();
            this._EventLogTabPage = new System.Windows.Forms.TabPage();
            this._EventLogTextBox = new System.Windows.Forms.TextBox();
            this._TopToolStrip = new System.Windows.Forms.ToolStrip();
            this._ValueTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._GetValueButton = new System.Windows.Forms.ToolStripButton();
            this._ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this._BottomToolStrip.SuspendLayout();
            this._Tabs.SuspendLayout();
            this._EventLogTabPage.SuspendLayout();
            this._TopToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).BeginInit();
            this._ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            this._ToolStripContainer.ContentPanel.SuspendLayout();
            this._ToolStripContainer.TopToolStripPanel.SuspendLayout();
            this._ToolStripContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // _BottomToolStrip
            // 
            this._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._BottomToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._SelectTabButton,
            this._SelectServerButton,
            this._ConnectServerButton,
            this._DeviceInfoTextBox,
            this._SelectDeviceSplitButton,
            this._OpenDeviceSubsystemTypeButton});
            this._BottomToolStrip.Location = new System.Drawing.Point(3, 0);
            this._BottomToolStrip.Name = "_BottomToolStrip";
            this._BottomToolStrip.Size = new System.Drawing.Size(280, 29);
            this._BottomToolStrip.TabIndex = 0;
            // 
            // _SelectTabButton
            // 
            this._SelectTabButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectTabButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._SelectPrimaryTabMenuItem,
            this._SelectEventsTabMenuItem});
            this._SelectTabButton.Image = global::isr.Diolan.WinControls.Properties.Resources.run_build_configure;
            this._SelectTabButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectTabButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectTabButton.Name = "_SelectTabButton";
            this._SelectTabButton.Size = new System.Drawing.Size(38, 26);
            this._SelectTabButton.Text = "Tab";
            // 
            // _SelectPrimaryTabMenuItem
            // 
            this._SelectPrimaryTabMenuItem.Image = global::isr.Diolan.WinControls.Properties.Resources.run_build_configure;
            this._SelectPrimaryTabMenuItem.Name = "_SelectPrimaryTabMenuItem";
            this._SelectPrimaryTabMenuItem.Size = new System.Drawing.Size(115, 22);
            this._SelectPrimaryTabMenuItem.Text = "Primary";
            this._SelectPrimaryTabMenuItem.Click += new System.EventHandler(this.SelectTabMenuItem_Click);
            // 
            // _SelectEventsTabMenuItem
            // 
            this._SelectEventsTabMenuItem.Image = global::isr.Diolan.WinControls.Properties.Resources.view_calendar_upcoming_events;
            this._SelectEventsTabMenuItem.Name = "_SelectEventsTabMenuItem";
            this._SelectEventsTabMenuItem.Size = new System.Drawing.Size(115, 22);
            this._SelectEventsTabMenuItem.Text = "Events";
            // 
            // _SelectServerButton
            // 
            this._SelectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectServerButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ServerNameTextBox,
            this.DefaultServerToolStripMenuItem});
            this._SelectServerButton.Image = global::isr.Diolan.WinControls.Properties.Resources.network_server;
            this._SelectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectServerButton.Name = "_SelectServerButton";
            this._SelectServerButton.Size = new System.Drawing.Size(38, 26);
            this._SelectServerButton.Text = "Select Server";
            // 
            // _ServerNameTextBox
            // 
            this._ServerNameTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._ServerNameTextBox.Name = "_ServerNameTextBox";
            this._ServerNameTextBox.Size = new System.Drawing.Size(100, 23);
            this._ServerNameTextBox.Text = "localhost:9656";
            // 
            // DefaultServerToolStripMenuItem
            // 
            this.DefaultServerToolStripMenuItem.Checked = true;
            this.DefaultServerToolStripMenuItem.CheckOnClick = true;
            this.DefaultServerToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DefaultServerToolStripMenuItem.Name = "DefaultServerToolStripMenuItem";
            this.DefaultServerToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.DefaultServerToolStripMenuItem.Text = "User Default Server:Port";
            // 
            // _ConnectServerButton
            // 
            this._ConnectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._ConnectServerButton.Image = global::isr.Diolan.WinControls.Properties.Resources.network_disconnect_2;
            this._ConnectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ConnectServerButton.Name = "_ConnectServerButton";
            this._ConnectServerButton.Size = new System.Drawing.Size(26, 26);
            this._ConnectServerButton.Text = "Connect Server";
            this._ConnectServerButton.Click += new System.EventHandler(this.ConnectServerButton_Click);
            // 
            // _DeviceInfoTextBox
            // 
            this._DeviceInfoTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._DeviceInfoTextBox.Name = "_DeviceInfoTextBox";
            this._DeviceInfoTextBox.ReadOnly = true;
            this._DeviceInfoTextBox.Size = new System.Drawing.Size(100, 29);
            this._DeviceInfoTextBox.Text = "closed";
            this._DeviceInfoTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _SelectDeviceSplitButton
            // 
            this._SelectDeviceSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._SelectDeviceSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._DevicesComboBox});
            this._SelectDeviceSplitButton.Enabled = false;
            this._SelectDeviceSplitButton.Image = global::isr.Diolan.WinControls.Properties.Resources.network_server_database;
            this._SelectDeviceSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton";
            this._SelectDeviceSplitButton.Size = new System.Drawing.Size(38, 26);
            this._SelectDeviceSplitButton.Text = "Device";
            this._SelectDeviceSplitButton.ToolTipText = "Select Device";
            // 
            // _DevicesComboBox
            // 
            this._DevicesComboBox.Name = "_DevicesComboBox";
            this._DevicesComboBox.Size = new System.Drawing.Size(121, 23);
            this._DevicesComboBox.Text = "DLN-4M.1.1";
            // 
            // _OpenDeviceSubsystemTypeButton
            // 
            this._OpenDeviceSubsystemTypeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._OpenDeviceSubsystemTypeButton.Enabled = false;
            this._OpenDeviceSubsystemTypeButton.Image = global::isr.Diolan.WinControls.Properties.Resources.user_offline_2;
            this._OpenDeviceSubsystemTypeButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._OpenDeviceSubsystemTypeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._OpenDeviceSubsystemTypeButton.Name = "_OpenDeviceSubsystemTypeButton";
            this._OpenDeviceSubsystemTypeButton.Size = new System.Drawing.Size(26, 26);
            this._OpenDeviceSubsystemTypeButton.Text = "Open";
            this._OpenDeviceSubsystemTypeButton.ToolTipText = "Open or close the device.";
            this._OpenDeviceSubsystemTypeButton.Click += new System.EventHandler(this.OpenDeviceSubsystemTypeButton_Click);
            // 
            // _Tabs
            // 
            this._Tabs.Controls.Add(this._PrimaryTabPage);
            this._Tabs.Controls.Add(this._EventLogTabPage);
            this._Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Tabs.HideTabHeaders = true;
            this._Tabs.Location = new System.Drawing.Point(0, 0);
            this._Tabs.Name = "_Tabs";
            this._Tabs.SelectedIndex = 0;
            this._Tabs.Size = new System.Drawing.Size(364, 427);
            this._Tabs.TabIndex = 0;
            // 
            // _PrimaryTabPage
            // 
            this._PrimaryTabPage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._PrimaryTabPage.Location = new System.Drawing.Point(4, 24);
            this._PrimaryTabPage.Name = "_PrimaryTabPage";
            this._PrimaryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._PrimaryTabPage.Size = new System.Drawing.Size(356, 399);
            this._PrimaryTabPage.TabIndex = 0;
            this._PrimaryTabPage.Text = "Primary";
            this._PrimaryTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTabPage
            // 
            this._EventLogTabPage.Controls.Add(this._EventLogTextBox);
            this._EventLogTabPage.Location = new System.Drawing.Point(4, 22);
            this._EventLogTabPage.Name = "_EventLogTabPage";
            this._EventLogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._EventLogTabPage.Size = new System.Drawing.Size(356, 401);
            this._EventLogTabPage.TabIndex = 1;
            this._EventLogTabPage.Text = "Events";
            this._EventLogTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTextBox
            // 
            this._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._EventLogTextBox.Location = new System.Drawing.Point(3, 3);
            this._EventLogTextBox.Multiline = true;
            this._EventLogTextBox.Name = "_EventLogTextBox";
            this._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._EventLogTextBox.Size = new System.Drawing.Size(350, 395);
            this._EventLogTextBox.TabIndex = 0;
            this._EventLogTextBox.DoubleClick += new System.EventHandler(this.EventLogTextBox_DoubleClick);
            // 
            // _TopToolStrip
            // 
            this._TopToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._TopToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this._TopToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._TopToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ValueTextBox,
            this._GetValueButton});
            this._TopToolStrip.Location = new System.Drawing.Point(3, 0);
            this._TopToolStrip.Name = "_TopToolStrip";
            this._TopToolStrip.Size = new System.Drawing.Size(162, 33);
            this._TopToolStrip.TabIndex = 0;
            // 
            // _ValueTextBox
            // 
            this._ValueTextBox.BackColor = System.Drawing.Color.Black;
            this._ValueTextBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ValueTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            this._ValueTextBox.Name = "_ValueTextBox";
            this._ValueTextBox.Size = new System.Drawing.Size(100, 33);
            this._ValueTextBox.Text = "0.0 V";
            this._ValueTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._ValueTextBox.ToolTipText = "Value";
            // 
            // _GetValueButton
            // 
            this._GetValueButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._GetValueButton.Image = global::isr.Diolan.WinControls.Properties.Resources.media_playback_start_4;
            this._GetValueButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._GetValueButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._GetValueButton.Name = "_GetValueButton";
            this._GetValueButton.Size = new System.Drawing.Size(26, 30);
            this._GetValueButton.Text = "Read Value";
            // 
            // _ErrorProvider
            // 
            this._ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            this._ToolStripContainer.BottomToolStripPanel.Controls.Add(this._BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            this._ToolStripContainer.ContentPanel.Controls.Add(this._Tabs);
            this._ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(364, 427);
            this._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            this._ToolStripContainer.Name = "_ToolStripContainer";
            this._ToolStripContainer.Size = new System.Drawing.Size(364, 489);
            this._ToolStripContainer.TabIndex = 0;
            this._ToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _ToolStripContainer.TopToolStripPanel
            // 
            this._ToolStripContainer.TopToolStripPanel.Controls.Add(this._TopToolStrip);
            this._ToolStripContainer.TopToolStripPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // DeviceSubsystemTypeView
            // 
            this.Controls.Add(this._ToolStripContainer);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DeviceSubsystemTypeView";
            this.Size = new System.Drawing.Size(364, 489);
            this._BottomToolStrip.ResumeLayout(false);
            this._BottomToolStrip.PerformLayout();
            this._Tabs.ResumeLayout(false);
            this._EventLogTabPage.ResumeLayout(false);
            this._EventLogTabPage.PerformLayout();
            this._TopToolStrip.ResumeLayout(false);
            this._TopToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).EndInit();
            this._ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            this._ToolStripContainer.BottomToolStripPanel.PerformLayout();
            this._ToolStripContainer.ContentPanel.ResumeLayout(false);
            this._ToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this._ToolStripContainer.TopToolStripPanel.PerformLayout();
            this._ToolStripContainer.ResumeLayout(false);
            this._ToolStripContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        private ToolStrip _BottomToolStrip;
        private ToolStripTextBox _DeviceInfoTextBox;
        private ErrorProvider _ErrorProvider;
        private ToolTip _ToolTip;
        private ToolStripButton _OpenDeviceSubsystemTypeButton;
        private ToolStrip _TopToolStrip;
        private ToolStripTextBox _ValueTextBox;
        private ToolStripButton _GetValueButton;
        private TabPage _PrimaryTabPage;
        private TabPage _EventLogTabPage;
        private TextBox _EventLogTextBox;
        private ToolStripContainer _ToolStripContainer;
        private ExtendedTabControl _Tabs;
        private ToolStripSplitButton _SelectTabButton;
        private ToolStripMenuItem _SelectEventsTabMenuItem;
        private ToolStripComboBox _DevicesComboBox;
        private ToolStripSplitButton _SelectServerButton;
        private ToolStripTextBox _ServerNameTextBox;
        private ToolStripMenuItem DefaultServerToolStripMenuItem;
        private ToolStripMenuItem _SelectPrimaryTabMenuItem;
        private ToolStripButton _ConnectServerButton;
        private ToolStripSplitButton _SelectDeviceSplitButton;
    }
}
