using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Diolan.WinControls.ErrorProviderExtensions;
using isr.Diolan.WinControls.SubsystemExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.WinControls
{

    /// <summary> User interface skeleton for a Subsystem Type view. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    public partial class SubsystemTypeView
    {

        #region " CONSTRUCTION "

        /// <summary> Executes the custom dispose action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to disposing. </param>
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.OnSubsystemTypeClosed();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " SUBSYSEM TYPE "

        /// <summary> the subsystem type. </summary>
        /// <value> the subsystem type. </value>
        public SubsystemTypes SubsystemType { get; set; } = SubsystemTypes.Adc;

        /// <summary> Executes the subsystem type closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeClosed()
        {
            this._PortComboBox.DataSource = null;
            this._PortComboBox.Items.Clear();
            // Me._ChannelComboBox.DataSource = Nothing
            // Me._ChannelComboBox.Items.Clear()
            this._Device = null;
        }

        /// <summary> Executes the subsystem type closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnSubsystemTypeClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    // un-register event handlers for all channels
                    foreach ( Dln.Adc.Port port in this._Device.Adc.Ports )
                    {
                        foreach ( Dln.Adc.Channel channel in port.Channels )
                            channel.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                    }
                }
            }
        }

        /// <summary> Executes the subsystem type opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeOpening()
        {
            this._Device = this.DeviceConnectorInternal.Device;
            this._PortComboBox.DataSource = null;
            this._PortComboBox.Items.Clear();
            this.OpenSubsystemType( this._Device.Adc );
        }

        /// <summary>   Opens a Subsystem Type. </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <param name="subsystemType">    the subsystem type. </param>
        private void OpenSubsystemType( Dln.Adc.Module subsystemType )
        {

            // Get port count
            if ( subsystemType.Ports.Count == 0 )
            {
                // this is already done when opening the device.
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceSubsystemTypeButton, "Adapter '{0}' doesn't support ADC interface.", this._Device.Caption() );
                this._Device = null;
                this._DeviceInfoTextBox.Text = "not supported";
            }
            else
            {
                this._DeviceInfoTextBox.Text = this._Device.Caption();
                subsystemType.Ports.ListNumbers( this._PortComboBox, true );

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handlers for all channels
                foreach ( Dln.Adc.Port port in subsystemType.Ports )
                {
                    foreach ( Dln.Adc.Channel channel in port.Channels )
                        channel.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                }

                this._PortComboBox.SelectedIndex = 0;
            }    // this will cause "OnChange" event
        }

        /// <summary> Queries if a Subsystem Type is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
        private bool IsSubsystemTypeOpen()
        {
            return this._PortComboBox.DataSource is object && this._PortComboBox.Items.Count > 0;
        }

        #endregion

        #region " DEVICE "

        /// <summary> The port combo box. </summary>
        private readonly ComboBox _PortComboBox;

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Adc.ConditionMetEventArgs e )
        {
            this.AppendEvent( e );
        }

        /// <summary> Appends an event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Condition met event information. </param>
        private void AppendEvent( Dln.Adc.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                string data = $"{DateTimeOffset.Now:hh:mm:ss.fff} {e.Port:D2}!{e.Channel:D2}={e.Value:D5} {e.EventType}{Environment.NewLine}";
                // This event is handled in main thread--invoke is not needed when modifying form's controls.
                this._EventLogTextBox.AppendText( data );
            }
        }


        #endregion

    }
}
