using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Diolan.WinControls.ErrorProviderExtensions;
using isr.Diolan.WinControls.SubsystemExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.WinControls
{
    public partial class SubsystemTypeView : System.Windows.Forms.UserControl, INotifyPropertyChanged
    {
        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-11-23. </remarks>
        public SubsystemTypeView()
        {
            this.InitializeComponent();
            this._PortComboBox = new ComboBox();
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                    this.components?.Dispose();
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Executes the dispose action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to disposing. </param>
        private void OnDispose( bool disposing )
        {
            this.OnCustomDispose( disposing );
            if ( disposing )
            {
                if ( this._Device is object )
                {
                    this._Device = null;
                }

                if ( this.DeviceConnectorInternal is object )
                {
                    this.DeviceConnectorInternal.Dispose();
                    this.DeviceConnectorInternal = null;
                }
            }
        }

        #endregion

        #region " SERVER "


        /// <summary> Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LocalhostConnector ServerConnector { get; set; }

        /// <summary> Connects a server button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConnectServerButton_Click( object sender, EventArgs e )
        {
            if ( this.ServerConnector.IsConnected )
            {
                if ( !this.ServerConnector.HasAttachedDevices() )
                {
                    this.ServerConnector.Disconnect();
                }
            }
            else
            {
                this.ServerConnector.Connect();
            }
        }


        /// <summary> Executes the server connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected void OnServerConnectionChanged()
        {
            if ( this.ServerConnector.IsConnected )
            {
                _ = this.ServerConnector.Connection.ListDevicesById( this._DevicesComboBox );
                this._DevicesComboBox.SelectedIndex = 0;
            }

            this._OpenDeviceSubsystemTypeButton.Enabled = this.ServerConnector.IsConnected;
            this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            this._OpenDeviceSubsystemTypeButton.Enabled = this.ServerConnector.IsConnected;
            this.OnServerAttachmentChanged();
        }

        /// <summary> Executes the server attachment changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OnServerAttachmentChanged()
        {
            this._ConnectServerButton.Image = isr.Diolan.WinControls.Properties.Resources.WIFI_open_22;
            if ( this.ServerConnector.IsConnected )
            {
                this._ConnectServerButton.Text = this.ServerConnector.AttachedDevicesCount().ToString();
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
                this._ConnectServerButton.Text = "X";
            }
        }

        #endregion

        #region " DEVICE CONNECTOR "

        private DeviceConnector _DeviceConnectorInternal;

        /// <summary>   Gets or sets the device connector internal. </summary>
        /// <value> The device connector internal. </value>
        private DeviceConnector DeviceConnectorInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceConnectorInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceConnectorInternal != null )
                {

                    this._DeviceConnectorInternal.DeviceClosed -= this.DeviceConnector_DeviceClosed;

                    this._DeviceConnectorInternal.DeviceClosing -= this.DeviceConnector_DeviceClosing;

                    this._DeviceConnectorInternal.DeviceOpened -= this.DeviceConnector_DeviceOpened;
                }

                this._DeviceConnectorInternal = value;
                if ( this._DeviceConnectorInternal != null )
                {
                    this._DeviceConnectorInternal.DeviceClosed += this.DeviceConnector_DeviceClosed;
                    this._DeviceConnectorInternal.DeviceClosing += this.DeviceConnector_DeviceClosing;
                    this._DeviceConnectorInternal.DeviceOpened += this.DeviceConnector_DeviceOpened;
                }
            }
        }

        /// <summary> Gets or sets the device connector. </summary>
        /// <value> The device connector. </value>
        public DeviceConnector DeviceConnector
        {
            get => this.DeviceConnectorInternal;

            set => this.DeviceConnectorInternal = value;
        }

        /// <summary> Device connector device closed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceClosed( object sender, EventArgs e )
        {
            this._DeviceInfoTextBox.Text = "closed";
            this.OnSubsystemTypeClosed();
        }

        /// <summary> Device connector device closing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void DeviceConnector_DeviceClosing( object sender, CancelEventArgs e )
        {
            this.OnSubsystemTypeClosing( e );
        }

        /// <summary> Device connector device opened. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceOpened( object sender, EventArgs e )
        {
            DeviceConnector connector = sender as DeviceConnector;
            this._Device = connector.Device;
            this._DeviceInfoTextBox.Text = this._Device.Caption();
            this.OnSubsystemTypeOpening();
        }


        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>
        private Dln.Device _Device;

        /// <summary> Selected device information. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A DeviceInfo. </returns>
        private DeviceInfo SelectedDeviceInfo()
        {
            return string.IsNullOrWhiteSpace( this._DevicesComboBox.Text )
                ? throw new InvalidOperationException( "No devices selected" )
                : new DeviceInfo( this._DevicesComboBox.Text );
        }

        /// <summary> Opens the device for the subsystem type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <param name="sender">   The sender. </param>
        private void OpenDeviceSubsystemType( long deviceId, Control sender )
        {
            if ( this.ServerConnector.IsConnected )
            {
                this._OpenDeviceSubsystemTypeButton.Enabled = this.ServerConnector.IsConnected;
                this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            }
            else
            {
                this.ServerConnector.Connect();
            }

            this.DeviceConnectorInternal = new DeviceConnector( this.ServerConnector );
            var (success, details) = this.DeviceConnectorInternal.TryOpenDevice( deviceId, this.SubsystemType );
            if ( success )
            {
                this.OnSubsystemTypeOpening();
            }
            else
            {
                if ( sender is object )
                    _ = this._ErrorProvider.Annunciate( sender, details );
                this._DeviceInfoTextBox.Text = "No devices";
            }
        }

        /// <summary> Closes device Subsystem Type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void CloseDeviceSubsystemType()
        {
            this.DeviceConnectorInternal.CloseDevice( this.SubsystemType );
            var e = new CancelEventArgs();
            this.OnSubsystemTypeClosing( e );
            if ( !e.Cancel )
            {
                this.OnSubsystemTypeClosed();
            }
        }

        /// <summary> Opens device subsystem type button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OpenDeviceSubsystemTypeButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceSubsystemTypeOpen )
                {
                    this.CloseDeviceSubsystemType();
                }
                else
                {
                    long deviceId = DeviceInfo.DefaultDeviceId;
                    if ( !string.IsNullOrWhiteSpace( this._DevicesComboBox.Text ) )
                    {
                        deviceId = this.SelectedDeviceInfo().Id;
                    }

                    this.OpenDeviceSubsystemType( deviceId, sender as Control );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
                this.OnSubsystemTypeConnectionChanged();
            }
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        private bool IsDeviceSubsystemTypeOpen => this._Device is object && this.IsSubsystemTypeOpen();

        /// <summary> Executes the subsystem type connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnSubsystemTypeConnectionChanged()
        {
            if ( this.IsDeviceSubsystemTypeOpen )
            {
                this._OpenDeviceSubsystemTypeButton.Image = isr.Diolan.WinControls.Properties.Resources.user_online_2;
                this._OpenDeviceSubsystemTypeButton.Text = "Close";
            }
            else
            {
                this._OpenDeviceSubsystemTypeButton.Image = isr.Diolan.WinControls.Properties.Resources.user_offline_2;
                this._OpenDeviceSubsystemTypeButton.Text = "Open";
            }
        }

        #endregion

        #region " SUBSYSTEM TYPES "

        /// <summary> Gets or sets the Subsystem Types. </summary>
        /// <value> The Subsystem Type. </value>
        public SubsystemTypes SupportedSubsystemTypes { get; set; }

        #endregion

        #region " EVENT LOG "

        /// <summary> Event log text box double click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EventLogTextBox_DoubleClick( object sender, EventArgs e )
        {
            this._EventLogTextBox.Clear();
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Select tab menu item click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SelectTabMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if ( item is object )
                this._SelectTabButton.Image = item.Image;
            switch ( item.Name ?? "" )
            {
                case "_SelectPrimaryTabMenuItem":
                    {
                        this._Tabs.SelectTab( this._PrimaryTabPage );
                        break;
                    }

                case "_SelectEventsTabMenuItem":
                    {
                        this._Tabs.SelectTab( this._EventLogTabPage );
                        break;
                    }
            }
        }

        #endregion

    }
}
