using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Diolan
{

    /// <summary>   Led subsystem. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-28 </para>
    /// </remarks>
    public class LedSubsystem : IDisposable
    {

        #region " CONSTRUCTOR "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public LedSubsystem() : base()
        {
        }

        #region "I Disposable Support"

        /// <summary> Gets or sets the disposed sentinel. </summary>
        /// <value> The disposed sentinel. </value>
        private bool Disposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to
        /// release only
        /// unmanaged resources
        /// when called from
        /// the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed && disposing )
                {
                    if ( this.PulseTimerInternal is object )
                    {
                        this.PulseTimerInternal.Dispose();
                        this.PulseTimerInternal = null;
                    }
                }
            }
            finally
            {
                this.Disposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " Led "

        /// <summary> The pulse timer. </summary>
        private System.Timers.Timer _PulseTimerInternal;

        private System.Timers.Timer PulseTimerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._PulseTimerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._PulseTimerInternal != null )
                {
                    this._PulseTimerInternal.Elapsed -= this.PulseTimer_Elapsed;
                }

                this._PulseTimerInternal = value;
                if ( this._PulseTimerInternal != null )
                {
                    this._PulseTimerInternal.Elapsed += this.PulseTimer_Elapsed;
                }
            }
        }

        /// <summary> The pulsed Led. </summary>
        private Dln.Led.Led _PulsedLed;

        /// <summary> Pulse LED. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="led">      The LED. </param>
        /// <param name="duration"> The duration. </param>
        [CLSCompliant( false )]
        public void PulseLed( Dln.Led.Led led, TimeSpan duration )
        {
            if ( led is not object ) throw new ArgumentNullException( nameof( led ) );
            if ( this.PulseTimerInternal is not object ) this.PulseTimerInternal = new System.Timers.Timer();

            this.PulseTimerInternal.AutoReset = false;
            this.PulseTimerInternal.Interval = duration.TotalMilliseconds;
            // toggle the Led value
            this._PulsedLed = led;
            this._PulsedLed.State = Dln.Led.State.On;
            this.PulseTimerInternal.Start();
        }

        /// <summary> Pulse timer elapsed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Elapsed event information. </param>
        private void PulseTimer_Elapsed( object sender, System.Timers.ElapsedEventArgs e )
        {
            if ( this._PulsedLed is object )
            {
                this._PulsedLed.State = Dln.Led.State.Off;
            }
            // the timer will not start again.
        }

        #endregion

    }
}
