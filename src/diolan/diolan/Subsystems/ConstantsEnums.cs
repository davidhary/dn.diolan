using System.ComponentModel;

namespace isr.Diolan
{

    /// <summary> Values that represent active logics. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum ActiveLogic
    {
        /// <summary>   An enum constant representing the active low option. </summary>
        [Description( "Active Low" )]
        ActiveLow = 0,

        /// <summary>   An enum constant representing the active high option. </summary>
        [Description( "Active High" )]
        ActiveHigh = 1
    }

    /// <summary> Values that represent logical states. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum LogicalState
    {
        /// <summary>   An enum constant representing the inactive option. </summary>
        [Description( "Inactive (Off)" )]
        Inactive = 0,

        /// <summary>   An enum constant representing the active option. </summary>
        [Description( "Active (On)" )]
        Active = 1
    }

    /// <summary> Values that represent pin directions. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum PinDirection
    {
        /// <summary>   An enum constant representing the input option. </summary>
        [Description( "Input" )]
        Input = 0,

        /// <summary>   An enum constant representing the output option. </summary>
        [Description( "Output" )]
        Output = 1
    }

    /// <summary> Values that represent pin values. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum PinValue
    {
        /// <summary>   An enum constant representing the logic zero option. </summary>
        [Description( "Logic 0" )]
        LogicZero = 0,

        /// <summary>   An enum constant representing the logic one option. </summary>
        [Description( "Logic 1" )]
        LogicOne = 1
    }
}

