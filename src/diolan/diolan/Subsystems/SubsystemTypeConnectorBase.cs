using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Diolan.ExceptionExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan
{

    /// <summary> Connector for device Subsystem Type or Types. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-18, 5647. </para>
    /// </remarks>
    public abstract class SubsystemTypeConnectorBase : INotifyPropertyChanged, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected SubsystemTypeConnectorBase() : base()
        {
        }

        #region " I DISPOSABLE SUPPORT "

        /// <summary> Gets or sets the disposed sentinel. </summary>
        /// <value> The disposed sentinel. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to
        /// release only
        /// unmanaged resources
        /// when called from
        /// the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    try
                    {
                        this.OnSubsystemTypeClosed();
                    }
                    catch ( Exception ex )
                    {
                        _ = ex.AddExceptionData();
                        Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                    }
                    this.Device = null;
                    this.DeviceConnectorInternal?.Dispose();
                    this.DeviceConnectorInternal = null;
                    this.ServerConnectorThis = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " SERVER "

        /// <summary> Gets or sets the server connector source. </summary>
        /// <value> The server connector source. </value>
        public string ServerConnectorSource { get; private set; } = $"{nameof( ServerConnector )}.";

        /// <summary> Server connector property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void ServerConnector_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object )
                this.NotifyPropertyChanged( $"{this.ServerConnectorSource}{e.PropertyName}" );
        }

        private LocalhostConnector _ServerConnectorThis;

        /// <summary>   Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        private LocalhostConnector ServerConnectorThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ServerConnectorThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ServerConnectorThis != null )
                {
                    this._ServerConnectorThis.PropertyChanged -= this.ServerConnector_PropertyChanged;
                }

                this._ServerConnectorThis = value;
                if ( this._ServerConnectorThis != null )
                {
                    this._ServerConnectorThis.PropertyChanged += this.ServerConnector_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        public LocalhostConnector ServerConnector
        {
            get => this.ServerConnectorThis;

            set => this.ServerConnectorThis = value;
        }

        #endregion

        #region " DEVICE CONNECTOR "

        private DeviceConnector _DeviceConnectorInternal;

        /// <summary>   Gets or sets the device connector internal. </summary>
        /// <value> The device connector internal. </value>
        private DeviceConnector DeviceConnectorInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceConnectorInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceConnectorInternal != null )
                {
                    this._DeviceConnectorInternal.DeviceClosed -= this.DeviceConnector_DeviceClosed;
                    this._DeviceConnectorInternal.DeviceClosing -= this.DeviceConnector_DeviceClosing;
                    this._DeviceConnectorInternal.DeviceOpened -= this.DeviceConnector_DeviceOpened;
                }

                this._DeviceConnectorInternal = value;
                if ( this._DeviceConnectorInternal != null )
                {
                    this._DeviceConnectorInternal.DeviceClosed += this.DeviceConnector_DeviceClosed;
                    this._DeviceConnectorInternal.DeviceClosing += this.DeviceConnector_DeviceClosing;
                    this._DeviceConnectorInternal.DeviceOpened += this.DeviceConnector_DeviceOpened;
                }
            }
        }

        /// <summary> Gets or sets the device connector. </summary>
        /// <value> The device connector. </value>
        public DeviceConnector DeviceConnector
        {
            get => this.DeviceConnectorInternal;

            set => this.DeviceConnectorInternal = value;
        }

        /// <summary> Device connector device closed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceClosed( object sender, EventArgs e )
        {
            this.DeviceCaption = "closed";
            this.NotifyPropertyChanged( nameof( this.DeviceCaption ) );
            this.OnSubsystemTypeClosed();
        }

        /// <summary> Device connector device closing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void DeviceConnector_DeviceClosing( object sender, CancelEventArgs e )
        {
            this.OnSubsystemTypeClosing( e );
        }

        /// <summary> Device connector device opened. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceOpened( object sender, EventArgs e )
        {
            this.DeviceCaption = this.DeviceConnectorInternal.Device.Caption();
            this.NotifyPropertyChanged( nameof( this.DeviceCaption ) );
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets information describing the device. </summary>
        /// <value> Information describing the device. </value>
        public string DeviceCaption { get; private set; }

        /// <summary> Gets the device. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The device. </value>
        [CLSCompliant( false )]
        protected Dln.Device Device { get; private set; }

        /// <summary> Opens the device for the subsystem type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryOpenDeviceSubsystemType( long deviceId )
        {
            // connect if not connected
            this.ServerConnector.Connect();
            this.DeviceConnectorInternal = new DeviceConnector( this.ServerConnectorThis );

            // Open device
            (bool Success, string Details) result = this.DeviceConnector.TryOpenDevice( deviceId, this.SubsystemType );
            if ( result.Success )
            {
                this.Device = this.DeviceConnectorInternal.Device;
                result = this.TryOpenSubsystemtype();
                if ( !result.Success )
                    this.Device = null;
            }
            else
            {
                this.Device = null;
                this.DeviceCaption = "failed";
                this.NotifyPropertyChanged( nameof( this.DeviceCaption ) );
            }

            this.NotifyPropertyChanged( nameof( this.IsDeviceSubsystemTypeOpen ) );
            return result;
        }

        /// <summary> Closes device Subsystem Type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void CloseDeviceSubsystemType()
        {
            this.DeviceConnector.CloseDevice( this.SubsystemType );
            var e = new CancelEventArgs();
            this.OnSubsystemTypeClosing( e );
            if ( !e.Cancel )
            {
                this.OnSubsystemTypeClosed();
                this.NotifyPropertyChanged( nameof( this.IsDeviceSubsystemTypeOpen ) );
            }
        }

        /// <summary> Gets a value indicating whether a device and its subsystem types are open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        public bool IsDeviceSubsystemTypeOpen => this.Device is object && this.IsSubsystemTypeOpen();

        #endregion

        #region " SUBSYSTEM TYPES "

        /// <summary> the subsystem type. </summary>
        /// <value> the subsystem type. </value>
        public abstract SubsystemTypes SubsystemType { get; }

        /// <summary> Executes the subsystem type closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected virtual void OnSubsystemTypeClosed()
        {
            this.Device = null;
            if ( this.ServerConnector?.IsConnected == true )
            {
                if ( !this.ServerConnector.HasAttachedDevices() )
                {
                    this.ServerConnector.Disconnect();
                }
            }
        }

        /// <summary> Executes the subsystem type closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected abstract void OnSubsystemTypeClosing( CancelEventArgs e );

        /// <summary> Executes the subsystem type opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract (bool Success, string Details) TryOpenSubsystemtype();

        /// <summary> Queries if a Subsystem Type is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
        public abstract bool IsSubsystemTypeOpen();

        #endregion

        #region " EVENT LOG "

        /// <summary> The event caption. </summary>
        private string _EventCaption;

        /// <summary> Gets or sets the event caption. </summary>
        /// <value> The event caption. </value>
        public string EventCaption
        {
            get => this._EventCaption;

            protected set {
                if ( !string.Equals( value, this.EventCaption ) )
                {
                    this._EventCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
