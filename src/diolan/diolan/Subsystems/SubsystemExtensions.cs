using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace isr.Diolan
{
    namespace SubsystemExtensions
    {

        /// <summary> Includes extensions for <see cref="Dln.Device">DLN Devices</see>. </summary>
        /// <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2015-05-26, 1.0.5624.x. </para></remarks>
        public static class SubsystemExtensionMethods
        {

            #region " CONNECTION EXTENSIONS "

            /// <summary> Return the host and port of the given connection. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="connection"> The connection. </param>
            /// <returns> A String. </returns>
            [CLSCompliant( false )]
            public static string Address( this Dln.Connection connection )
            {
                return connection is null
                    ? "Connection not created"
                    : connection.IsConnected() ? $"{connection.Host}:{connection.Port}" : "Connection not connected";
            }

            /// <summary> Connection information. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="connection"> The connection. </param>
            /// <returns>
            /// A Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
            /// </returns>
            [CLSCompliant( false )]
            public static System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>> ConnectionInfo( this Dln.Connection connection )
            {
                if ( connection is not object ) throw new ArgumentNullException( nameof( connection ) );
                var keyValueList = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>( "Host", connection.Host ), new KeyValuePair<string, string>( "Port", connection.Port.ToString() ) };
                return new System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>>( keyValueList );
            }

            #endregion

            #region " DEVICE EXTENSIONS "

            /// <summary> Returns a Captions the device. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device"> The device. </param>
            /// <returns> A String. </returns>
            [CLSCompliant( false )]
            public static string Caption( this Dln.Device device )
            {
                return device is not object ? throw new ArgumentNullException( nameof( device ) ) : $"{device.HardwareType}.{device.ID}.{device.SN}";
            }

            /// <summary> Converts a value to a version. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="value"> The value. </param>
            /// <returns> value as a Version. </returns>
            private static Version ToVersion( uint value )
            {
                return new Version( ( int ) (value >> 24 & 0xFFL), ( int ) (value >> 16 & 0xFFL), ( int ) (value >> 8 & 0xFFL), ( int ) (value & 0xFFL) );
            }

            /// <summary> Converts a value to a version string. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="value"> The value. </param>
            /// <returns> value as a String. </returns>
            private static string ToVersionString( uint value )
            {
                return $"{value >> 24 & 0xFFL}.{value >> 16 & 0xFFL}.{value >> 8 & 0xFFL}.{value & 0xFFL}";
            }

            /// <summary> Converts a value to a version string. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device">        The device. </param>
            /// <param name="deviceVersion"> The device version. </param>
            /// <returns> value as a String. </returns>
            [CLSCompliant( false )]
            public static string ToVersionString( this Dln.Device device, DeviceVersion deviceVersion )
            {
                if ( device is not object )  throw new ArgumentNullException( nameof( device ) );
                switch ( deviceVersion )
                {
                    case DeviceVersion.Firmware:
                        {
                            return ToVersionString( device.FirmwareVersion );
                        }

                    case DeviceVersion.Hardware:
                        {
                            return ToVersionString( device.HardwareVersion );
                        }

                    case DeviceVersion.Library:
                        {
                            return ToVersionString( device.LibraryVersion );
                        }

                    case DeviceVersion.Server:
                        {
                            return ToVersionString( device.ServerVersion );
                        }

                    default:
                        {
                            return ToVersionString( device.HardwareVersion );
                        }
                }
            }

            /// <summary> Version number. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device">        The device. </param>
            /// <param name="deviceVersion"> The device version. </param>
            /// <returns> A Version. </returns>
            [CLSCompliant( false )]
            public static Version ToVersion( this Dln.Device device, DeviceVersion deviceVersion )
            {
                if ( device is not object ) throw new ArgumentNullException( nameof( device ) );
                switch ( deviceVersion )
                {
                    case DeviceVersion.Firmware:
                        {
                            return ToVersion( device.FirmwareVersion );
                        }

                    case DeviceVersion.Hardware:
                        {
                            return ToVersion( device.HardwareVersion );
                        }

                    case DeviceVersion.Library:
                        {
                            return ToVersion( device.LibraryVersion );
                        }

                    case DeviceVersion.Server:
                        {
                            return ToVersion( device.ServerVersion );
                        }

                    default:
                        {
                            return ToVersion( device.HardwareVersion );
                        }
                }
            }

            /// <summary>   Supports. </summary>
            /// <remarks>   David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
            ///                                             null. </exception>
            /// <param name="device">           The device. </param>
            /// <param name="subsystemType">    the subsystem type. </param>
            /// <returns>
            /// <c>true</c> if the device supports the subsystem type; otherwise <c>false</c>
            /// </returns>
            [CLSCompliant( false )]
            public static bool Supports( this Dln.Device device, SubsystemTypes subsystemType )
            {
                if ( device is not object ) throw new ArgumentNullException( nameof( device ) );
                switch ( subsystemType )
                {
                    case SubsystemTypes.Adc:
                        {
                            return device.Adc.Ports.Any();
                        }

                    case SubsystemTypes.Gpio:
                        {
                            return device.Gpio.Pins.Any();
                        }

                    case SubsystemTypes.I2CMaster:
                        {
                            return device.I2cMaster.Ports.Any();
                        }

                    case SubsystemTypes.I2CSlave:
                        {
                            return device.I2cSlave.Ports.Any();
                        }

                    case SubsystemTypes.Leds:
                        {
                            return device.Led.Leds.Any();
                        }

                    case SubsystemTypes.PulseCounter:
                        {
                            return device.PulseCounter.Ports.Any();
                        }

                    case SubsystemTypes.Pwm:
                        {
                            return device.Pwm.Ports.Any();
                        }

                    case SubsystemTypes.SpiMaster:
                        {
                            return device.SpiMaster.Ports.Any();
                        }

                    case SubsystemTypes.SpiSlave:
                        {
                            return device.SpiSlave.Ports.Any();
                        }

                    case SubsystemTypes.Uart:
                        {
                            return device.Uart.Ports.Any();
                        }

                    default:
                        {
                            return false;
                        }
                }
            }

            /// <summary> Supports any. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device">     The device. </param>
            /// <param name="subsystemTypes"> The subsystem types. </param>
            /// <returns> <c>true</c> if the device supports any Subsystem Type; otherwise <c>false</c> </returns>
            [CLSCompliant( false )]
            public static bool SupportsAny( this Dln.Device device, SubsystemTypes subsystemTypes )
            {
                if ( device is not object ) throw new ArgumentNullException( nameof( device ) );
                bool affirmative = false;
                foreach ( SubsystemTypes subsystemType in Enum.GetValues( typeof( SubsystemTypes ) ) )
                {
                    if ( (subsystemTypes & subsystemType) != 0 && device.Supports( subsystemType ) )
                    {
                        affirmative = true;
                        break;
                    }
                }

                return affirmative;
            }

            /// <summary> Device information. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device"> The device. </param>
            /// <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
            [CLSCompliant( false )]
            public static System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>> DeviceInfo( this Dln.Device device )
            {
                if ( device is not object ) throw new ArgumentNullException( nameof( device ) );
                var keyValueList = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>( "Hardware Type", device.HardwareType.ToString() ), new KeyValuePair<string, string>( "Id", device.ID.ToString() ), new KeyValuePair<string, string>( "S/N", device.SN.ToString() ), new KeyValuePair<string, string>( "hardware Value", (( int ) device.HardwareType).ToString() ), new KeyValuePair<string, string>( "Firmware Version", device.ToVersionString( DeviceVersion.Firmware ) ), new KeyValuePair<string, string>( "Hardware Version", device.ToVersionString( DeviceVersion.Hardware ) ), new KeyValuePair<string, string>( "Library Version", device.ToVersionString( DeviceVersion.Library ) ), new KeyValuePair<string, string>( "Server Version", device.ToVersionString( DeviceVersion.Server ) ) };
                return new System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>>( keyValueList );
            }

            /// <summary> Builds a collection of subsystem type information for <see cref="Dln.Adc"/> <see cref="Dln.Adc.Ports"/>. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="ports"> The ports. </param>
            /// <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
            [CLSCompliant( false )]
            public static System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>> SubsystemTypeInfo( this Dln.Adc.Ports ports )
            {
                if ( ports is not object ) throw new ArgumentNullException( nameof( ports ) );
                var keyValueList = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>( "Adc Ports", ports.Count.ToString() ) };
                if ( ports.Any() )
                {
                    int i = 0;
                    foreach ( Dln.Adc.Port port in ports )
                    {
                        i += 1;
                        keyValueList.Add( new KeyValuePair<string, string>( $"Adc Port {i} Enabled", $"{port.Enabled.GetHashCode():true;0;false}" ) );
                        keyValueList.Add( new KeyValuePair<string, string>( $"Adc Port {i} Channels", port.Channels.Count.ToString() ) );
                        keyValueList.Add( new KeyValuePair<string, string>( $"Adc Port {i} Resolution", port.Resolution.ToString() ) );
                    }
                }

                return new System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>>( keyValueList );
            }

            /// <summary> Builds a collection of subsystem type information for <see cref="Dln.PulseCounter"/> <see cref="Dln.PulseCounter.Ports"/>. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="ports"> The ports. </param>
            /// <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
            [CLSCompliant( false )]
            public static System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>> SubsystemTypeInfo( this Dln.PulseCounter.Ports ports )
            {
                if ( ports is not object ) throw new ArgumentNullException( nameof( ports ) );
                var keyValueList = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>( "Pulse Counter Ports", ports.Count.ToString() ) };
                if ( ports.Any() )
                {
                    int i = 0;
                    foreach ( Dln.PulseCounter.Port port in ports )
                    {
                        i += 1;
                        keyValueList.Add( new KeyValuePair<string, string>( $"P/C Port {i} Enabled", $"{port.Enabled.GetHashCode():true;0;false}" ) );
                        keyValueList.Add( new KeyValuePair<string, string>( $"P/C Port {i} Resolution", port.Resolution.ToString() ) );
                        keyValueList.Add( new KeyValuePair<string, string>( $"P/C Port {i} Mode", port.Mode.ToString() ) );
                    }
                }

                return new System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>>( keyValueList );
            }

            /// <summary> Builds a collection of subsystem type information for <see cref="Dln.Pwm"/> <see cref="Dln.Pwm.Ports"/>. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="ports"> The ports. </param>
            /// <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
            [CLSCompliant( false )]
            public static System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>> SubsystemTypeInfo( this Dln.Pwm.Ports ports )
            {
                if ( ports is not object ) throw new ArgumentNullException( nameof( ports ) );
                var keyValueList = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>( "PWM Ports", ports.Count.ToString() ) };
                if ( ports.Any() )
                {
                    int i = 0;
                    foreach ( Dln.Pwm.Port port in ports )
                    {
                        i += 1;
                        keyValueList.Add( new KeyValuePair<string, string>( $"PWM Port {i} Enabled", $"{port.Enabled.GetHashCode():true;0;false}" ) );
                        keyValueList.Add( new KeyValuePair<string, string>( $"PWM Port {i} Channels", port.Channels.Count.ToString() ) );
                        keyValueList.Add( new KeyValuePair<string, string>( $"PWM Port {i} Max Frequency", port.MaxFrequency.ToString() ) );
                    }
                }

                return new System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>>( keyValueList );
            }

            /// <summary> Builds a collection of subsystem type information for this device. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device"> The device. </param>
            /// <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
            [CLSCompliant( false )]
            public static System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>> SubsystemTypeInfo( this Dln.Device device )
            {
                if ( device is not object ) throw new ArgumentNullException( nameof( device ) );
                var keyValueList = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>( "Gpio Pins", device.Gpio.Pins.Count.ToString() ), new KeyValuePair<string, string>( "I2c Master Ports", device.I2cMaster.Ports.Count.ToString() ), new KeyValuePair<string, string>( "I2c Slave Ports", device.I2cSlave.Ports.Count.ToString() ), new KeyValuePair<string, string>( "Leds", device.Led.Leds.Count.ToString() ), new KeyValuePair<string, string>( "SPI Flash Ports", device.SpiFlash.Ports.Count.ToString() ), new KeyValuePair<string, string>( "SPI Master Ports", device.SpiMaster.Ports.Count.ToString() ), new KeyValuePair<string, string>( "SPI Slave Ports", device.SpiSlave.Ports.Count.ToString() ) };
                keyValueList.AddRange( device.Adc.Ports.SubsystemTypeInfo() );
                keyValueList.AddRange( device.PulseCounter.Ports.SubsystemTypeInfo() );
                keyValueList.AddRange( device.Pwm.Ports.SubsystemTypeInfo() );
                return new System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>>( keyValueList );
            }

            /// <summary> Server information. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device"> The device. </param>
            /// <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
            [CLSCompliant( false )]
            [Obsolete( "Not supported rev 3.0.1" )]
            public static System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>> ServerInfo( this Dln.Device device )
            {
                if ( device is not object ) throw new ArgumentNullException( nameof( device ) );
                var keyValueList = new List<KeyValuePair<string, string>>();
                device.GetSrvParams( out byte[] uuid, out _, out _, out byte[] ipAddress, out _, out _, out _ );
                keyValueList.Add( new KeyValuePair<string, string>( "UUID", new System.Net.IPAddress( uuid ).ToString() ) );
                keyValueList.Add( new KeyValuePair<string, string>( "IP Address", new System.Net.IPAddress( ipAddress ).ToString() ) );
                return new System.Collections.ObjectModel.ReadOnlyCollection<KeyValuePair<string, string>>( keyValueList );
            }

            #endregion

            #region " GPIO PIN EXTENSIONS "

            /// <summary> Input value. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pin"> The pin. </param>
            /// <returns> A Unsigned long. </returns>
            [CLSCompliant( false )]
            public static PinValue InputValue( this Dln.Gpio.Pin pin )
            {
                return pin is not object ? throw new ArgumentNullException( nameof( pin ) ) : ( PinValue ) pin.Value;
            }

            /// <summary> Output value getter. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pin"> The pin. </param>
            /// <returns> A Unsigned long. </returns>
            [CLSCompliant( false )]
            public static PinValue OutputValueGetter( this Dln.Gpio.Pin pin )
            {
                return pin is not object ? throw new ArgumentNullException( nameof( pin ) ) : ( PinValue ) pin.OutputValue;
            }

            /// <summary> Output value setter. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pin">   The pin. </param>
            /// <param name="value"> The value. </param>
            /// <returns> A Unsigned long. </returns>
            [CLSCompliant( false )]
            public static PinValue OutputValueSetter( this Dln.Gpio.Pin pin, PinValue value )
            {
                if ( pin is not object ) throw new ArgumentNullException( nameof( pin ) );
                pin.OutputValue = ( int ) value;
                return pin.OutputValueGetter();
            }

            /// <summary> Configures the destination pin based on the source pin configuration. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="source">      Source for the. </param>
            /// <param name="destination"> Destination for the. </param>
            [CLSCompliant( false )]
            public static void Configure( this Dln.Gpio.Pin source, Dln.Gpio.Pin destination )
            {
                if ( source is not object ) throw new ArgumentNullException( nameof( source ) );
                if ( destination is not object ) throw new ArgumentNullException( nameof( destination ) );
                destination.Enabled = source.Enabled;
                if ( destination.Restrictions.DebounceEnabled != Dln.Restriction.NotSupported )
                {
                    destination.DebounceEnabled = source.DebounceEnabled;
                }

                if ( destination.Restrictions.Direction != Dln.Restriction.NotSupported )
                {
                    destination.Direction = source.Direction;
                }

                if ( destination.Restrictions.EventType != Dln.Restriction.NotSupported )
                {
                    destination.SetEventConfiguration( source.EventType, source.EventPeriod );
                }

                if ( destination.Restrictions.OpendrainEnabled != Dln.Restriction.NotSupported )
                {
                    destination.OpendrainEnabled = source.OpendrainEnabled;
                }

                if ( destination.Restrictions.PulldownEnabled != Dln.Restriction.NotSupported )
                {
                    destination.PulldownEnabled = source.PulldownEnabled;
                }

                if ( destination.Restrictions.PullupEnabled != Dln.Restriction.NotSupported )
                {
                    destination.PullupEnabled = source.PullupEnabled;
                }
            }

            #endregion

            #region " GPIO PINS EXTENSIONS "

            /// <summary> Input value. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pins"> The pins. </param>
            /// <param name="mask"> The mask. </param>
            /// <returns> A Unsigned long. </returns>
            [CLSCompliant( false )]
            public static ulong InputValue( this Dln.Gpio.Pins pins, ulong mask )
            {
                if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
                ulong value = 0UL;
                ulong pinValue = 1UL;
                int pinNumber = 0;
                while ( mask > 0m && pinNumber >= 0 && pinNumber < pins.Count )
                {
                    if ( (mask & 1) > 0m )
                    {
                        if ( pins[pinNumber].Value == 1 )
                        {
                            value += pinValue;
                        }

                        pinValue <<= 1;
                    }

                    pinNumber += 1;
                    mask >>= 1;
                }

                return value;
            }

            /// <summary> Output value getter. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pins"> The pins. </param>
            /// <param name="mask"> The mask. </param>
            /// <returns> A Unsigned long. </returns>
            [CLSCompliant( false )]
            public static ulong OutputValueGetter( this Dln.Gpio.Pins pins, ulong mask )
            {
                if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
                ulong value = 0UL;
                ulong pinValue = 1UL;
                int pinNumber = 0;
                while ( mask > 0m && pinNumber >= 0 && pinNumber < pins.Count )
                {
                    if ( (mask & 1) > 0m )
                    {
                        if ( pins[pinNumber].OutputValue == 1 )
                        {
                            value += pinValue;
                        }

                        pinValue <<= 1;
                    }

                    pinNumber += 1;
                    mask >>= 1;
                }

                return value;
            }

            /// <summary> First pin number. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="mask"> The mask. </param>
            /// <returns> An Integer; -1 if mask is 0. </returns>
            [CLSCompliant( false )]
            public static int FirstPinNumber( this ulong mask )
            {
                int pinNumber = -1;
                if ( mask > 0m )
                    pinNumber = 0;
                while ( mask > 0m )
                {
                    if ( (mask & 1) > 0m )
                    {
                        break;
                    }
                    else
                    {
                        pinNumber += 1;
                        mask >>= 1;
                    }
                }

                return pinNumber;
            }

            /// <summary> Output value setter. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pins">  The pins. </param>
            /// <param name="mask">  The mask. </param>
            /// <param name="value"> The value. </param>
            /// <returns> A Unsigned long. </returns>
            [CLSCompliant( false )]
            public static ulong OutputValueSetter( this Dln.Gpio.Pins pins, ulong mask, ulong value )
            {
                if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
                int pinNumber = 0;
                while ( mask > 0m && pinNumber >= 0 && pinNumber < pins.Count )
                {
                    if ( (mask & 1) > 0m )
                    {
                        pins[pinNumber].OutputValue = ( int ) (value & 1);
                        value >>= 1;
                    }

                    pinNumber += 1;
                    mask >>= 1;
                }

                return pins.OutputValueGetter( mask );
            }

            /// <summary> Configures the pins based on the specified pin configuration. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pins">   The pins. </param>
            /// <param name="mask">   The mask. </param>
            /// <param name="source"> Source for the. </param>
            [CLSCompliant( false )]
            public static void Configure( this Dln.Gpio.Pins pins, ulong mask, Dln.Gpio.Pin source )
            {
                if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
                if ( source is not object ) throw new ArgumentNullException( nameof( source ) );
                foreach ( int i in pins.SelectMulti( mask ) )
                    source.Configure( pins[i] );
            }

            /// <summary> Selects pins using the mask to index the pins. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pins"> The pins. </param>
            /// <param name="mask"> The mask. </param>
            /// <returns> A list of. </returns>
            [CLSCompliant( false )]
            public static ReadOnlyPinCollection SelectPins( this Dln.Gpio.Pins pins, ulong mask )
            {
                if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
                var l = new List<Dln.Gpio.Pin>();
                foreach ( int pinNumber in pins.SelectMulti( mask ) )
                    l.Add( pins[pinNumber] );
                return new ReadOnlyPinCollection( l );
            }

            /// <summary> Selects pin numbers using the mask to index the pins. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="pins"> The pins. </param>
            /// <param name="mask"> The mask. </param>
            /// <returns> A list of. </returns>
            [CLSCompliant( false )]
            public static System.Collections.ObjectModel.ReadOnlyCollection<int> SelectMulti( this Dln.Gpio.Pins pins, ulong mask )
            {
                if ( pins is not object ) throw new ArgumentNullException( nameof( pins ) );
                var l = new List<int>();
                int pinNumber = 0;
                while ( mask > 0m && pinNumber >= 0 && pinNumber < pins.Count )
                {
                    if ( (mask & 1) > 0m )
                    {
                        l.Add( pinNumber );
                    }

                    pinNumber += 1;
                    mask >>= 1;
                }

                return new System.Collections.ObjectModel.ReadOnlyCollection<int>( l );
            }

            /// <summary> Bit count. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="mask"> The mask. </param>
            /// <returns> An Integer. </returns>
            [CLSCompliant( false )]
            public static int BitCount( this ulong mask )
            {
                int result = 0;
                while ( mask > 0m )
                {
                    if ( (mask & 1) > 0m )
                    {
                        result += 1;
                    }

                    mask >>= 1;
                }

                return result;
            }

            #endregion

            #region " STOPWATCH EXTENSIONS "

            /// <summary>   Waits. </summary>
            /// <remarks>   David, 2021-09-02. </remarks>
            /// <param name="sw">               The software. </param>
            /// <param name="delay">            The delay. </param>
            /// <param name="doEventsAction">   (Optional) The do events action. </param>
            /// <returns>   A TimeSpan. </returns>
            public static TimeSpan Wait( this Stopwatch sw, TimeSpan delay, Action doEventsAction = null )
            {
                while ( sw.Elapsed < delay ) { doEventsAction?.Invoke(); }
                return sw.Elapsed;
            }

            #endregion

        }
    }

    /// <summary> Values that represent device versions. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum DeviceVersion
    {

        /// <summary> An enum constant representing the hardware option. </summary>
        [Description( "Hardware" )]
        Hardware,

        /// <summary> An enum constant representing the firmware option. </summary>
        [Description( "Firmware" )]
        Firmware,

        /// <summary> An enum constant representing the server option. </summary>
        [Description( "Server" )]
        Server,

        /// <summary> An enum constant representing the library option. </summary>
        [Description( "Library" )]
        Library
    }

    /// <summary> Values that represent LED color indexes for DLN-4ME. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum LedColorIndex
    {
        /// <summary>   An enum constant representing the red option. </summary>
        [Description( "Read" )]
        Red = 0,

        /// <summary>   An enum constant representing the green option. </summary>
        [Description( "Green" )]
        Green = 1
    }

    /// <summary> Collection of read only pins. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    [CLSCompliant( false )]
    public class ReadOnlyPinCollection : System.Collections.ObjectModel.ReadOnlyCollection<Dln.Gpio.Pin>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> class that is a read-
        /// only wrapper around the specified list.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins"> The list to wrap. </param>
        public ReadOnlyPinCollection( IList<Dln.Gpio.Pin> pins ) : base( pins )
        {
        }
    }

    /// <summary> Values that represent subsystem types. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    [Flags()]
    public enum SubsystemTypes
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not specified" )]
        None,

        /// <summary> An enum constant representing the ADC option. </summary>
        [Description( "A/D Conversion" )]
        Adc = 1,

        /// <summary> An enum constant representing the GPIO option. </summary>
        [Description( "General purpose I/O" )]
        Gpio = 2,

        /// <summary> An enum constant representing the Pulse Counter option. </summary>
        [Description( "Pulse Counter" )]
        PulseCounter = 4,

        /// <summary> An enum constant representing the Pulse Width Modulation option. </summary>
        [Description( "Pulse Width Modulation" )]
        Pwm = 8,

        /// <summary> An enum constant representing the I2C Master option. </summary>
        [Description( "I2c Master" )]
        I2CMaster = 16,

        /// <summary> An enum constant representing the I2C Slave option. </summary>
        [Description( "I2C Slave" )]
        I2CSlave = 32,

        /// <summary> An enum constant representing the Spi Master option. </summary>
        [Description( "SPI Master" )]
        SpiMaster = 64,

        /// <summary> An enum constant representing the Spi Slave option. </summary>
        [Description( "SPI Slave" )]
        SpiSlave = 128,

        /// <summary> An enum constant representing the serial option. </summary>
        [Description( "Serial" )]
        Uart = 256,

        /// <summary> An enum constant representing the Leds option. </summary>
        [Description( "Leds" )]
        Leds = 512
    }
}
