using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan
{

    /// <summary> Default Server connector. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-30 </para>
    /// </remarks>
    public sealed class LocalhostConnector : INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private LocalhostConnector() : base()
        {
            this._AttachedDevices = new AttachedDevices();
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static LocalhostConnector Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class. This class uses lazy
        /// instantiation, meaning the instance isn't created until the first time it's retrieved.
        /// </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static LocalhostConnector SingleInstance()
        {
            if ( Instance is not object )
            {
                lock ( SyncLocker )
                    Instance = new LocalhostConnector();
            }

            return Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( SyncLocker )
                    return Instance is object;
            }
        }

        /// <summary> Dispose instance. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public static void DisposeInstance()
        {
            lock ( SyncLocker )
            {
                if ( Instance is object )
                {
                    Instance = null;
                }
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        private void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        private void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " CONNECTION "

        /// <summary> The default host. </summary>
        public const string DefaultHost = "localhost";

        /// <summary> Gets the default Address. </summary>
        /// <value> The default Address. </value>
        public static string DefaultAddress => $"{DefaultHost}:{Dln.Connection.DefaultPort}";

        /// <summary> Gets URL of the document. </summary>
        /// <value> The URL. </value>
        public string Address => this.IsConnected ? this.Connection.Address() : "closed";

        private Dln.Connection _ConnectionInternal;

        /// <summary>   Gets or sets the connection internal. </summary>
        /// <value> The connection internal. </value>
        private Dln.Connection ConnectionInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ConnectionInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ConnectionInternal != null )
                {
                    this._ConnectionInternal.ConnectionLost -= this.Connection_ConnectionLost;
                }

                this._ConnectionInternal = value;
                if ( this._ConnectionInternal != null )
                {
                    this._ConnectionInternal.ConnectionLost += this.Connection_ConnectionLost;
                }
            }
        }

        /// <summary> Gets the connection. </summary>
        /// <value> The connection. </value>
        [CLSCompliant( false )]
        public Dln.Connection Connection => this.ConnectionInternal;

        /// <summary> Connects this object. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Connect()
        {
            // Connect to DLN server
            if ( !this.IsConnected )
            {
                this.ConnectionInternal = Dln.Library.Connect();
                this.NotifyPropertyChanged( nameof( this.IsConnected ) );
            }
        }


        /// <summary> Connection lost. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Connection_ConnectionLost( object sender, EventArgs e )
        {
            this.DisconnectWithoutEventsThis();
            this.NotifyPropertyChanged( nameof( this.IsConnected ) );
        }

        /// <summary> Disconnects this object without raising events. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void DisconnectWithoutEventsThis()
        {
            if ( this._AttachedDevices is object )
            {
                this._AttachedDevices.Clear();
            }

            if ( this.IsConnected )
                Dln.Library.Disconnect( this.Connection );
        }

        /// <summary> Disconnects this object. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Disconnect()
        {
            if ( this.IsConnected && !this.HasAttachedDevices() )
            {
                this.DisconnectWithoutEventsThis();
                this.NotifyPropertyChanged( nameof( this.IsConnected ) );
            }
        }

        /// <summary> Gets a value indicating whether this object is connected. </summary>
        /// <value> <c>true</c> if this object is connected; otherwise <c>false</c> </value>
        public bool IsConnected => this.Connection is object && this.Connection.IsConnected();

        #endregion

        #region " ATTACHED DEVICES "

        /// <summary> The attached devices. </summary>
        private readonly AttachedDevices _AttachedDevices;

        /// <summary> Attached device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="id"> The identifier. </param>
        /// <returns> A Dln.Device. </returns>
        [CLSCompliant( false )]
        public Dln.Device AttachedDevice( long id )
        {
            return this._AttachedDevices.AttachedDevice( id );
        }

        /// <summary> Query if 'device' is attached. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        /// <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
        [CLSCompliant( false )]
        public bool IsAttached( Dln.Device device )
        {
            return this._AttachedDevices is object && this._AttachedDevices.IsAttached( device );
        }

        /// <summary> Query if 'device' is attached. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
        public bool IsAttached( long deviceId )
        {
            return this._AttachedDevices is object && this._AttachedDevices.IsAttached( deviceId );
        }

        /// <summary> Query if this object has attached devices. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if attached devices; otherwise <c>false</c> </returns>
        public bool HasAttachedDevices()
        {
            return this._AttachedDevices is object && this._AttachedDevices.HasAttachedDevices();
        }

        /// <summary> Attached devices count. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> An Integer. </returns>
        public int AttachedDevicesCount()
        {
            return this._AttachedDevices is not object ? 0 : this._AttachedDevices.AttachedDevicesCount();
        }

        /// <summary>   Attaches the given device. </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <param name="device">           The device. </param>
        /// <param name="doEventsAction">   (Optional) The do events action. </param>
        [CLSCompliant( false )]
        public void Attach( Dln.Device device, Action doEventsAction = null )
        {
            _ = this._AttachedDevices.Attach( device );
            doEventsAction?.Invoke();
            this.NotifyPropertyChanged( nameof( LocalhostConnector.AttachedDevicesCount ) );
        }

        /// <summary>   Detaches the given device. </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <param name="device">           The device. </param>
        /// <param name="doEventsAction">   (Optional) The do events action. </param>
        [CLSCompliant( false )]
        public void Detach( Dln.Device device, Action doEventsAction = null )
        {
            _ = this._AttachedDevices.Detach( device );
            doEventsAction?.Invoke();
            this.NotifyPropertyChanged( nameof( LocalhostConnector.AttachedDevicesCount ) );
        }

        /// <summary> An attached devices. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private class AttachedDevices : Dictionary<long, int>
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2" />
            /// class that is empty, has the default initial capacity, and uses the default equality comparer
            /// for the key type.
            /// </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            public AttachedDevices() : base()
            {
                this._Devices = new Dictionary<long, Dln.Device>();
            }

            /// <summary>
            /// Removes all keys and values from the <see cref="T:System.Collections.Generic.Dictionary`2" />.
            /// </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            public new void Clear()
            {
                base.Clear();
                this._Devices = new Dictionary<long, Dln.Device>();
            }

            /// <summary> The devices. </summary>
            private Dictionary<long, Dln.Device> _Devices;

            /// <summary> Attached device. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="id"> The identifier. </param>
            /// <returns> A Dln.Device. </returns>
            public Dln.Device AttachedDevice( long id )
            {
                return this._Devices is object && this._Devices.ContainsKey( id ) ? this._Devices[id] : null;
            }

            /// <summary> Query if this object has attached devices. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="id"> The identifier. </param>
            /// <returns> <c>true</c> if attached devices; otherwise <c>false</c> </returns>
            public int AttachedDeviceCount( long id )
            {
                return this.IsAttached( id ) ? this[id] : 0;
            }

            /// <summary> Attaches the given device. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device"> The device. </param>
            /// <returns> An Integer. </returns>
            public int Attach( Dln.Device device )
            {
                if ( device is not object ) throw new ArgumentNullException( nameof( device ) );
                int id = ( int ) device.ID;
                if ( this.ContainsKey( id ) )
                {
                    this[id] += 1;
                }
                else
                {
                    this.Add( id, 1 );
                    this._Devices.Add( id, device );
                }

                return this[id];
            }

            /// <summary> Detaches the given device. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="device"> The device. </param>
            /// <returns> An Integer. </returns>
            public int Detach( Dln.Device device )
            {
                if ( device is not object ) throw new ArgumentNullException( nameof( device ) );
                int id = ( int ) device.ID;
                if ( this.ContainsKey( id ) )
                {
                    this[id] -= 1;
                }
                else
                {
                    this.Add( id, 0 );
                }

                return this[id];
            }

            /// <summary> Query if 'device' is attached. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="deviceId"> Identifier for the device. </param>
            /// <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
            public bool IsAttached( long deviceId )
            {
                return this.Any() && this.ContainsKey( deviceId ) && this[deviceId] > 0;
            }

            /// <summary> Query if 'device' is attached. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device"> The device. </param>
            /// <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
            public bool IsAttached( Dln.Device device )
            {
                return device is object && this.IsAttached( ( int ) device.ID );
            }

            /// <summary> Gets the number of attached devices. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <returns> <c>true</c> if attached devices; otherwise <c>false</c> </returns>
            public int AttachedDevicesCount()
            {
                int result = 0;
                if ( this.Any() )
                {
                    foreach ( int v in this.Values )
                        result += v;
                }

                return result;
            }

            /// <summary> Query if this object has attached devices. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <returns> <c>true</c> if attached devices; otherwise <c>false</c> </returns>
            public bool HasAttachedDevices()
            {
                bool result = false;
                if ( this.Any() )
                {
                    foreach ( int v in this.Values )
                    {
                        if ( v > 0 )
                        {
                            result = true;
                            break;
                        }
                    }
                }

                return result;
            }
        }

        #endregion

    }
}
