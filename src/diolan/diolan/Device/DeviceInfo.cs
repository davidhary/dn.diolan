using System;
using System.Collections.Generic;

using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan
{

    /// <summary> Information about the device. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    [CLSCompliant( false )]
    public class DeviceInfo
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        public DeviceInfo( Dln.Device device ) : base()
        {
            if ( device is object )
            {
                this.Id = device.ID;
                this.SerialNumber = device.SN;
                this.HardwareType = device.HardwareType;
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="caption"> The caption. </param>
        public DeviceInfo( string caption ) : base()
        {
            this.ParseThis( caption );
        }

        /// <summary> The default device identifier. </summary>
        public const int DefaultDeviceId = 0;

        /// <summary> Gets the identifier. </summary>
        /// <value> The identifier. </value>
        public long Id { get; set; }

        /// <summary> Gets the type of the hardware. </summary>
        /// <value> The type of the hardware. </value>
        public Dln.HardwareType HardwareType { get; set; }

        /// <summary> Gets the serial number. </summary>
        /// <value> The serial number. </value>
        public long SerialNumber { get; set; }

        /// <summary> Gets the caption. </summary>
        /// <value> The caption. </value>
        public string Caption => $"{this.HardwareType}.{this.Id}.{this.SerialNumber}";


        /// <summary> Parses. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="caption"> The caption. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>" )]
        private void ParseThis( string caption )
        {
            if ( string.IsNullOrWhiteSpace( caption ) )
                throw new ArgumentNullException( nameof( caption ) );
            var elements = new Stack<string>( caption.Split( '.' ) );
            if ( elements.Count < 3 )
            {
                throw new InvalidOperationException( $"Unable to parse {caption} because it has fewer than 3 elements" );
            }
            else
            {
                string value = elements.Pop();
                bool localTryParse() { long argresult = this.SerialNumber; var ret = long.TryParse( value, out argresult ); this.SerialNumber = argresult; return ret; }

                if ( !localTryParse() )
                {
                    throw new InvalidOperationException( $"Unable to parse {value} into a serial number" );
                }

                value = elements.Pop();
                bool localTryParse1() { long argresult = this.Id; var ret = long.TryParse( value, out argresult ); this.Id = argresult; return ret; }

                if ( !localTryParse1() )
                {
                    throw new InvalidOperationException( $"Unable to parse {value} into a device id" );
                }

                value = elements.Pop();
                bool localTryParse2() { var argresult = this.HardwareType; var ret = Enum.TryParse( value, out argresult ); this.HardwareType = argresult; return ret; }

                if ( !localTryParse2() )
                {
                    throw new InvalidOperationException( $"Unable to parse {value} into hardware type" );
                }
            }
        }
    }

    /// <summary> Collection of device information. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    [CLSCompliant( false )]
    public class DeviceInfoCollection : System.Collections.ObjectModel.Collection<DeviceInfo>
    {

        /// <summary>   Attempts to enumerate devices from the given data. </summary>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryEnumerateDevices( Dln.Connection connection )
        {
            (bool Success, string Details) result = (true, string.Empty);
            this.Clear();
            if ( connection is not object )
            {
                result = ( false, "Not created" );
            }
            else if ( !connection.IsConnected() )
            {
                result = (false, "Not connected" );
            }
            else if ( Dln.Device.Count() == 0L )
            {
                result = (false, $"No DLN-series adapters found @{connection.Address()}." );
            }
            else
            {
                for ( long i = 0L, loopTo = Dln.Device.Count() - 1L; i <= loopTo; i++ )
                {
                    var d = Dln.Device.Open( ( int ) i );
                    if ( d is object )
                        this.Add( new DeviceInfo( d ) );
                }
            }

            return result;
        }
    }
}
