using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan
{

    /// <summary> Device connector. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-30 </para>
    /// </remarks>
    public class DeviceConnector : IDisposable
    {

        #region " CONSTRUCTOR "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="serverConnector"> The server connector. </param>
        [CLSCompliant( false )]
        public DeviceConnector( LocalhostConnector serverConnector ) : base()
        {
            this._ServerConnector = serverConnector;
        }

        #region " DISPOSABLE SUPPORT "

        /// <summary> Gets the disposed sentinel. </summary>
        /// <value> The disposed sentinel. </value>
        private bool Disposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to
        /// release only
        /// unmanaged resources
        /// when called from
        /// the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed && disposing )
                {
                    // dispose managed state (managed objects).
                    this.RemoveDeviceOpenedEventHandlers();
                    this.RemoveDeviceClosedEventHandlers();
                    this.RemoveDeviceClosingEventHandlers();
                    if ( this.Device is object )
                        this.Device = null;
                    if ( this._ServerConnector is object )
                        this._ServerConnector = null;
                }
            }
            finally
            {
                this.Disposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " CONNECTION "

        /// <summary> The server connector. </summary>
        private LocalhostConnector _ServerConnector;

        /// <summary> Gets the connection. </summary>
        /// <value> The connector. </value>
        [CLSCompliant( false )]
        public Dln.Connection Connection => this._ServerConnector?.Connection;

        /// <summary> Gets a value indicating whether this object is local connection. </summary>
        /// <value> <c>true</c> if this object is local connection; otherwise <c>false</c> </value>
        public bool IsLocalConnection => this.Connection is object && this.Connection.IsConnected() && this.Connection.Host.Equals( LocalhostConnector.DefaultHost, StringComparison.OrdinalIgnoreCase );

        /// <summary> Attaches. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="serverConnector"> The server connector. </param>
        private void Attach( LocalhostConnector serverConnector )
        {
            if ( serverConnector is object )
                serverConnector.Attach( this.DeviceInternal );
        }

        /// <summary> Detaches the given device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="serverConnector"> The server connector. </param>
        private void Detach( LocalhostConnector serverConnector )
        {
            if ( serverConnector is object )
                serverConnector.Detach( this.DeviceInternal );
        }

        #endregion

        #region " DEVICE "

        private Dln.Device _DeviceInternal;

        /// <summary>   Gets or sets the device internal. </summary>
        /// <value> The device internal. </value>
        private Dln.Device DeviceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceInternal != null )
                {
                    this._DeviceInternal.DeviceRemovedThreadSafe -= this.Device_DeviceRemovedThreadSafe;
                }

                this._DeviceInternal = value;
                if ( this._DeviceInternal != null )
                {
                    this._DeviceInternal.DeviceRemovedThreadSafe += this.Device_DeviceRemovedThreadSafe;
                }
            }
        }

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [CLSCompliant( false )]
        public Dln.Device Device
        {
            get => this.DeviceInternal;

            set => this.DeviceInternal = value;
        }

        /// <summary>   Attempts to open device requiring the existence of specific subsystems. </summary>
        /// <param name="deviceId">         Identifier for the device. </param>
        /// <param name="requiredSubsystemType"> The required subsystem type. </param>
        /// <returns>
        /// A Tuple (bool Success, string Details ): Success is <c>true</c> if the test passes,
        /// <c>false</c> if the test fails; Details is empty if Success is <c>True</c>.
        /// </returns>
        public (bool Success, string Details) TryOpenDevice( long deviceId, SubsystemTypes requiredSubsystemType )
        {

            (bool Success, string Details) result = ( true, string.Empty );
            if ( this.Connection is not object )
            {
                result = ( false, "Device connection not set" );
            }
            else if ( this.Connection.IsConnected() )
            {
                // Open device
                if ( Dln.Device.Count() == 0L )
                {
                    result = ( false, "No DLN-series adapters have been detected." );
                }
                else if ( !this.IsDeviceOpen )
                {
                    this.DeviceInternal = this._ServerConnector.IsAttached( deviceId ) ? this._ServerConnector.AttachedDevice( deviceId ) : Dln.Device.OpenById( ( uint ) deviceId );
                    if ( requiredSubsystemType == SubsystemTypes.None || this.Device.SupportsAny( requiredSubsystemType ) )
                    {
                        this.OnDeviceOpened( requiredSubsystemType );
                    }
                    else
                    {
                        result = ( false, $"{this.Device.HardwareType} does not support {requiredSubsystemType}." );
                        this.DeviceInternal = null;
                    }
                }
            }
            else
            {
                result = ( false, "Device not connected" );
            }

            return result;
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        public bool IsDeviceOpen => this.DeviceInternal is object;

        /// <summary> Closes the device with references to the specified subsystem. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="requiredSubsystemType"> The required subsystem type. </param>
        public void CloseDevice( SubsystemTypes requiredSubsystemType )
        {
            _ = Detach( this.DeviceInternal, requiredSubsystemType );
            if ( this.IsDeviceOpen && !HasAttachedSubsystemTypes( this.DeviceInternal ) )
            {
                var e = new System.ComponentModel.CancelEventArgs();
                this.OnDeviceClosing( e );
                if ( !e.Cancel )
                {
                    this.OnDeviceClosed();
                }
            }
        }

        #endregion

        #region " DEVICE OPENED "

        /// <summary>   Event queue for all listeners interested in DeviceOpened events. </summary>
        public event EventHandler<EventArgs> DeviceOpened;

        /// <summary>   Executes the 'device opened' action. </summary>
        /// <remarks>   David, 2021-09-02. </remarks>
        /// <param name="requiredSubsystemType"> The required subsystem type. </param>
        private void OnDeviceOpened( SubsystemTypes requiredSubsystemType )
        {
            _ = Attach( this.DeviceInternal, requiredSubsystemType );
            if ( !this._ServerConnector.IsAttached( this.DeviceInternal ) )
            {
                this.Attach( this._ServerConnector );
                this.NotifyDeviceOpened( EventArgs.Empty );
            }
        }

        /// <summary>   Executes the 'device opened' action. </summary>
        /// <remarks>   David, 2021-09-02. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDeviceOpened( object sender, EventArgs e )
        {
            this.DeviceOpened?.Invoke( sender, e );
        }

        /// <summary>   Notifies a device opened. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyDeviceOpened( EventArgs e )
        {
            this.OnDeviceOpened( this, e );
        }

        /// <summary>   Removes the device opened event handlers. </summary>
        protected void RemoveDeviceOpenedEventHandlers()
        {
            var handler = this.DeviceOpened;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( EventHandler<EventArgs> ) item;
                }
            }
        }


        #endregion

        #region " DEVICE CLOSING "

        /// <summary>   Event queue for all listeners interested in DeviceClosing events. </summary>
        public event EventHandler<System.ComponentModel.CancelEventArgs> DeviceClosing;

        /// <summary>
        /// Raises the system. component model. cancel event. Allows taking actions before DeviceClosing.
        /// </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnDeviceClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is not object ) throw new ArgumentNullException( nameof( e ) );
            if ( !e.Cancel )
            {
                this.NotifyDeviceClosing( e );
            }
        }

        /// <summary>
        /// Raises the system. component model. cancel event. Allows taking actions before DeviceClosing.
        /// </summary>
        /// <remarks>   David, 2021-09-02. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDeviceClosing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this.DeviceClosing?.Invoke( sender, e );
        }

        /// <summary>   Notifies a device Closing. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyDeviceClosing( System.ComponentModel.CancelEventArgs e )
        {
            this.OnDeviceClosing( this, e );
        }

        /// <summary>   Removes the device Closing event handlers. </summary>
        protected void RemoveDeviceClosingEventHandlers()
        {
            var handler = this.DeviceClosing;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( EventHandler<System.ComponentModel.CancelEventArgs> ) item;
                }
            }
        }

        #endregion

        #region " DEVICE CLOSED "

        /// <summary>   Event queue for all listeners interested in DeviceClosed events. </summary>
        public event EventHandler<EventArgs> DeviceClosed;


        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnDeviceClosed()
        {
            if ( this.IsDeviceOpen && !HasAttachedSubsystemTypes( this.DeviceInternal ) )
            {
                this.Detach( this._ServerConnector );
                this.DeviceInternal = null;
                this.NotifyDeviceClosed( EventArgs.Empty );
            }
        }

        /// <summary> Device removed event hander. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        [CLSCompliant( false )]
        protected virtual void OnDeviceRemoved( Dln.Device device )
        {
            if ( device is object && this.DeviceInternal is object && this.Device.ID == device.ID )
            {
                Detach( device );
                this.Detach( this._ServerConnector );
                this.DeviceInternal = null;
                this.NotifyDeviceClosed( EventArgs.Empty );
            }
        }

        /// <summary> Device removed thread safe. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Device_DeviceRemovedThreadSafe( object sender, EventArgs e )
        {
            this.OnDeviceRemoved( sender as Dln.Device );
        }

        /// <summary>   Executes the 'device Closed' action. </summary>
        /// <remarks>   David, 2021-09-02. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDeviceClosed( object sender, EventArgs e )
        {
            this.DeviceClosed?.Invoke( sender, e );
        }

        /// <summary>   Notifies a device Closed. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyDeviceClosed( EventArgs e )
        {
            this.OnDeviceClosed( this, e );
        }

        /// <summary>   Removes the device Closed event handlers. </summary>
        protected void RemoveDeviceClosedEventHandlers()
        {
            var handler = this.DeviceClosed;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( EventHandler<EventArgs> ) item;
                }
            }
        }

        #endregion

        #region " attached Subsystem Types "

        /// <summary>   Query if the 'device' has an attached subsystem type. </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <param name="device">           The device. </param>
        /// <param name="subsystemType">    Type of the subsystem. </param>
        /// <returns>   <c>true</c> if attached; otherwise <c>false</c> </returns>
        [CLSCompliant( false )]
        public static bool IsAttached( Dln.Device device, SubsystemTypes subsystemType )
        {
            return _AttachedSubsystemTypes is object && _AttachedSubsystemTypes.IsAttached( device, subsystemType );
        }

        /// <summary> The attached Subsystem Types. </summary>
        private static AttachedDeviceSubsystemTypes _AttachedSubsystemTypes;

        /// <summary> Query if 'device' has attached Subsystem Types. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        /// <returns> True if attached Subsystem Types, false if not. </returns>
        [CLSCompliant( false )]
        public static bool HasAttachedSubsystemTypes( Dln.Device device )
        {
            return _AttachedSubsystemTypes is object && _AttachedSubsystemTypes.HasAttachedSubsystemTypes( device );
        }

        /// <summary> Attaches the specified subsystem type </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="subsystemType"> the subsystem type. </param>
        /// <returns> An Integer. </returns>
        [CLSCompliant( false )]
        public static int Attach( Dln.Device device, SubsystemTypes subsystemType )
        {
            if ( _AttachedSubsystemTypes is not object )
            {
                _AttachedSubsystemTypes = new AttachedDeviceSubsystemTypes();
            }

            return _AttachedSubsystemTypes.Attach( device, subsystemType );
        }

        /// <summary> Detaches the given subsystem type. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="subsystemType"> the subsystem type. </param>
        /// <returns> An Integer. </returns>
        [CLSCompliant( false )]
        public static int Detach( Dln.Device device, SubsystemTypes subsystemType )
        {
            if ( _AttachedSubsystemTypes is not object )
            {
                _AttachedSubsystemTypes = new AttachedDeviceSubsystemTypes();
            }

            return _AttachedSubsystemTypes.Detach( device, subsystemType );
        }

        /// <summary> Detaches the given device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        [CLSCompliant( false )]
        public static void Detach( Dln.Device device )
        {
            if ( _AttachedSubsystemTypes is object )
            {
                _AttachedSubsystemTypes.Detach( device );
            }
        }

        /// <summary> Attached subsystem types. </summary>
        /// <remarks>
        /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2015-06-02 </para>
        /// </remarks>
        private class AttachedDeviceSubsystemTypes : Dictionary<uint, AttachedSubsystemTypes>
        {

            /// <summary>   Attaches. </summary>
            /// <remarks>   David, 2020-10-24. </remarks>
            /// <param name="device">           The device. </param>
            /// <param name="subsystemType">    the subsystem type. </param>
            /// <returns>   An Integer. </returns>
            public int Attach( Dln.Device device, SubsystemTypes subsystemType )
            {
                if ( !this.ContainsKey( device.ID ) )
                {
                    this.Add( device.ID, new AttachedSubsystemTypes() );
                }

                return this[device.ID].Attach( subsystemType );
            }

            /// <summary> Detaches. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device"> The device. </param>
            public void Detach( Dln.Device device )
            {
                if ( this.ContainsKey( device.ID ) )
                {
                    _ = this.Remove( device.ID );
                }
            }

            /// <summary>   Detaches. </summary>
            /// <remarks>   David, 2020-10-24. </remarks>
            /// <param name="device">           The device. </param>
            /// <param name="subsystemType">    the subsystem type. </param>
            /// <returns>   An Integer. </returns>
            public int Detach( Dln.Device device, SubsystemTypes subsystemType )
            {
                if ( !this.ContainsKey( device.ID ) )
                {
                    this.Add( device.ID, new AttachedSubsystemTypes() );
                    return 0;
                }
                else
                {
                    return this[device.ID].Detach( subsystemType );
                }
            }

            /// <summary> Query if the device has the subsystem type attached. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device">   The device. </param>
            /// <param name="subsystemType"> the subsystem type. </param>
            /// <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
            public bool IsAttached( Dln.Device device, SubsystemTypes subsystemType )
            {
                return this.Any() && this.ContainsKey( device.ID ) && this[device.ID][subsystemType] > 0;
            }

            /// <summary> Query if this device has attached Subsystem Types. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device"> The device. </param>
            /// <returns> <c>true</c> if attached Subsystem Types; otherwise <c>false</c> </returns>
            public bool HasAttachedSubsystemTypes( Dln.Device device )
            {
                bool result = false;
                if ( this.Any() )
                {
                    foreach ( int v in this[device.ID].Values )
                    {
                        if ( v > 0 )
                        {
                            result = true;
                            break;
                        }
                    }
                }

                return result;
            }
        }

        /// <summary> A dictionary of attached subsystem types keyed by subsystem type. </summary>
        /// <remarks>
        /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2015-06-02 </para>
        /// </remarks>
        private class AttachedSubsystemTypes : Dictionary<SubsystemTypes, int>
        {

            /// <summary> Attaches the given subsystem type. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="subsystemType"> the subsystem type. </param>
            /// <returns> An Integer. </returns>
            public int Attach( SubsystemTypes subsystemType )
            {
                if ( this.ContainsKey( subsystemType ) )
                {
                    this[subsystemType] += 1;
                }
                else
                {
                    this.Add( subsystemType, 1 );
                }

                return this[subsystemType];
            }

            /// <summary> Detaches the given subsystem type. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="subsystemType"> the subsystem type. </param>
            /// <returns> An Integer. </returns>
            public int Detach( SubsystemTypes subsystemType )
            {
                if ( this.ContainsKey( subsystemType ) )
                {
                    this[subsystemType] -= 1;
                }
                else
                {
                    this.Add( subsystemType, 0 );
                }

                return this[subsystemType];
            }

            /// <summary>   Query if a subsystem type is attached. </summary>
            /// <remarks>   David, 2020-10-24. </remarks>
            /// <param name="subsystemType">    the subsystem type. </param>
            /// <returns>   <c>true</c> if attached; otherwise <c>false</c> </returns>
            public bool IsAttached( SubsystemTypes subsystemType )
            {
                return this.Any() && this.ContainsKey( subsystemType ) && this[subsystemType] > 0;
            }

            /// <summary> Query if this object has attached Subsystem Types. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <returns> <c>true</c> if attached Subsystem Types; otherwise <c>false</c> </returns>
            public bool HasAttachedSubsystemTypes()
            {
                bool result = false;
                if ( this.Any() )
                {
                    foreach ( int v in this.Values )
                    {
                        if ( v > 0 )
                        {
                            result = true;
                            break;
                        }
                    }
                }

                return result;
            }
        }


        #endregion

    }
}
