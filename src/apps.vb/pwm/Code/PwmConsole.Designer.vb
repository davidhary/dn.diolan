<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PwmConsole

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PwmConsole))
        Me._ChannelEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._ChannelComboBox = New System.Windows.Forms.ComboBox()
        Me._DutyCycleNumericLabel = New System.Windows.Forms.Label()
        Me._FrequencyNumericLabel = New System.Windows.Forms.Label()
        Me._ChannelComboBoxLabel = New System.Windows.Forms.Label()
        Me._PortEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._PortComboBoxLabel = New System.Windows.Forms.Label()
        Me._PortComboBox = New System.Windows.Forms.ComboBox()
        Me._GetDutyCycleButton = New System.Windows.Forms.Button()
        Me._GetFrequencyButton = New System.Windows.Forms.Button()
        Me._SetDutyCycleButton = New System.Windows.Forms.Button()
        Me._SetFrequencyButton = New System.Windows.Forms.Button()
        Me._FrequencyNumeric = New System.Windows.Forms.NumericUpDown()
        Me._DutyCycleNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._OpenDeviceSubsystemTypeButton = New System.Windows.Forms.Button()
        Me._DeviceIdNumeric = New System.Windows.Forms.NumericUpDown()
        Me._DeviceGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceIdNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceInfoTextBox = New System.Windows.Forms.TextBox()
        CType(Me._FrequencyNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DutyCycleNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._DeviceGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ChannelEnabledCheckBox
        '
        Me._ChannelEnabledCheckBox.AutoSize = True
        Me._ChannelEnabledCheckBox.Location = New System.Drawing.Point(148, 94)
        Me._ChannelEnabledCheckBox.Name = "_ChannelEnabledCheckBox"
        Me._ChannelEnabledCheckBox.Size = New System.Drawing.Size(65, 17)
        Me._ChannelEnabledCheckBox.TabIndex = 21
        Me._ChannelEnabledCheckBox.Text = "Enabled"
        Me._ChannelEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_ChannelComboBox
        '
        Me._ChannelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._ChannelComboBox.FormattingEnabled = True
        Me._ChannelComboBox.Location = New System.Drawing.Point(73, 93)
        Me._ChannelComboBox.Name = "_ChannelComboBox"
        Me._ChannelComboBox.Size = New System.Drawing.Size(68, 21)
        Me._ChannelComboBox.TabIndex = 20
        Me._ToolTip.SetToolTip(Me._ChannelComboBox, "Selects the pulse width modulation channel.")
        '
        '_DutyCycleNumericLabel
        '
        Me._DutyCycleNumericLabel.AutoSize = True
        Me._DutyCycleNumericLabel.Location = New System.Drawing.Point(9, 158)
        Me._DutyCycleNumericLabel.Name = "_DutyCycleNumericLabel"
        Me._DutyCycleNumericLabel.Size = New System.Drawing.Size(61, 13)
        Me._DutyCycleNumericLabel.TabIndex = 19
        Me._DutyCycleNumericLabel.Text = "Duty Cycle:"
        Me._DutyCycleNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_FrequencyNumericLabel
        '
        Me._FrequencyNumericLabel.AutoSize = True
        Me._FrequencyNumericLabel.Location = New System.Drawing.Point(10, 129)
        Me._FrequencyNumericLabel.Name = "_FrequencyNumericLabel"
        Me._FrequencyNumericLabel.Size = New System.Drawing.Size(60, 13)
        Me._FrequencyNumericLabel.TabIndex = 18
        Me._FrequencyNumericLabel.Text = "Frequency:"
        Me._FrequencyNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ChannelComboBoxLabel
        '
        Me._ChannelComboBoxLabel.AutoSize = True
        Me._ChannelComboBoxLabel.Location = New System.Drawing.Point(21, 96)
        Me._ChannelComboBoxLabel.Name = "_ChannelComboBoxLabel"
        Me._ChannelComboBoxLabel.Size = New System.Drawing.Size(49, 13)
        Me._ChannelComboBoxLabel.TabIndex = 17
        Me._ChannelComboBoxLabel.Text = "Channel:"
        Me._ChannelComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PortEnabledCheckBox
        '
        Me._PortEnabledCheckBox.AutoSize = True
        Me._PortEnabledCheckBox.Location = New System.Drawing.Point(148, 67)
        Me._PortEnabledCheckBox.Name = "_PortEnabledCheckBox"
        Me._PortEnabledCheckBox.Size = New System.Drawing.Size(65, 17)
        Me._PortEnabledCheckBox.TabIndex = 16
        Me._PortEnabledCheckBox.Text = "Enabled"
        Me._PortEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_PortComboBoxLabel
        '
        Me._PortComboBoxLabel.AutoSize = True
        Me._PortComboBoxLabel.Location = New System.Drawing.Point(40, 68)
        Me._PortComboBoxLabel.Name = "_PortComboBoxLabel"
        Me._PortComboBoxLabel.Size = New System.Drawing.Size(29, 13)
        Me._PortComboBoxLabel.TabIndex = 15
        Me._PortComboBoxLabel.Text = "Port:"
        Me._PortComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PortComboBox
        '
        Me._PortComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PortComboBox.FormattingEnabled = True
        Me._PortComboBox.Location = New System.Drawing.Point(73, 65)
        Me._PortComboBox.Name = "_PortComboBox"
        Me._PortComboBox.Size = New System.Drawing.Size(68, 21)
        Me._PortComboBox.TabIndex = 14
        Me._ToolTip.SetToolTip(Me._PortComboBox, "Selects the pulse width modulation power")
        '
        '_GetDutyCycleButton
        '
        Me._GetDutyCycleButton.Location = New System.Drawing.Point(220, 153)
        Me._GetDutyCycleButton.Name = "_GetDutyCycleButton"
        Me._GetDutyCycleButton.Size = New System.Drawing.Size(56, 23)
        Me._GetDutyCycleButton.TabIndex = 31
        Me._GetDutyCycleButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetDutyCycleButton, "Gets the current duty cycle.")
        Me._GetDutyCycleButton.UseVisualStyleBackColor = True
        '
        '_GetFrequencyButton
        '
        Me._GetFrequencyButton.Location = New System.Drawing.Point(220, 124)
        Me._GetFrequencyButton.Name = "_GetFrequencyButton"
        Me._GetFrequencyButton.Size = New System.Drawing.Size(56, 23)
        Me._GetFrequencyButton.TabIndex = 30
        Me._GetFrequencyButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetFrequencyButton, "Get the current frequency")
        Me._GetFrequencyButton.UseVisualStyleBackColor = True
        '
        '_SetDutyCycleButton
        '
        Me._SetDutyCycleButton.Location = New System.Drawing.Point(159, 153)
        Me._SetDutyCycleButton.Name = "_SetDutyCycleButton"
        Me._SetDutyCycleButton.Size = New System.Drawing.Size(56, 23)
        Me._SetDutyCycleButton.TabIndex = 29
        Me._SetDutyCycleButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetDutyCycleButton, "Sets the duty cycle.")
        Me._SetDutyCycleButton.UseVisualStyleBackColor = True
        '
        '_SetFrequencyButton
        '
        Me._SetFrequencyButton.Location = New System.Drawing.Point(159, 124)
        Me._SetFrequencyButton.Name = "_SetFrequencyButton"
        Me._SetFrequencyButton.Size = New System.Drawing.Size(56, 23)
        Me._SetFrequencyButton.TabIndex = 28
        Me._SetFrequencyButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetFrequencyButton, "Set the frequency")
        Me._SetFrequencyButton.UseVisualStyleBackColor = True
        '
        '_FrequencyNumeric
        '
        Me._FrequencyNumeric.Location = New System.Drawing.Point(73, 126)
        Me._FrequencyNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._FrequencyNumeric.Name = "_FrequencyNumeric"
        Me._FrequencyNumeric.Size = New System.Drawing.Size(79, 20)
        Me._FrequencyNumeric.TabIndex = 33
        Me._ToolTip.SetToolTip(Me._FrequencyNumeric, "The pulse width modulation frequency.")
        '
        '_DutyCycleNumeric
        '
        Me._DutyCycleNumeric.DecimalPlaces = 3
        Me._DutyCycleNumeric.Location = New System.Drawing.Point(73, 155)
        Me._DutyCycleNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._DutyCycleNumeric.Name = "_DutyCycleNumeric"
        Me._DutyCycleNumeric.Size = New System.Drawing.Size(79, 20)
        Me._DutyCycleNumeric.TabIndex = 34
        Me._ToolTip.SetToolTip(Me._DutyCycleNumeric, "The pulse duty cycle.")
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_OpenDeviceSubsystemTypeButton
        '
        Me._OpenDeviceSubsystemTypeButton.Location = New System.Drawing.Point(78, 18)
        Me._OpenDeviceSubsystemTypeButton.Name = "_OpenDeviceSubsystemTypeButton"
        Me._OpenDeviceSubsystemTypeButton.Size = New System.Drawing.Size(60, 23)
        Me._OpenDeviceSubsystemTypeButton.TabIndex = 5
        Me._OpenDeviceSubsystemTypeButton.Text = "Open"
        Me._ToolTip.SetToolTip(Me._OpenDeviceSubsystemTypeButton, "Opens the device")
        Me._OpenDeviceSubsystemTypeButton.UseVisualStyleBackColor = True
        '
        '_DeviceIdNumeric
        '
        Me._DeviceIdNumeric.Location = New System.Drawing.Point(28, 21)
        Me._DeviceIdNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._DeviceIdNumeric.Name = "_DeviceIdNumeric"
        Me._DeviceIdNumeric.Size = New System.Drawing.Size(42, 20)
        Me._DeviceIdNumeric.TabIndex = 11
        Me._ToolTip.SetToolTip(Me._DeviceIdNumeric, "Select device id")
        '
        '_DeviceGroupBox
        '
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumeric)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumericLabel)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceInfoTextBox)
        Me._DeviceGroupBox.Controls.Add(Me._OpenDeviceSubsystemTypeButton)
        Me._DeviceGroupBox.Location = New System.Drawing.Point(11, 5)
        Me._DeviceGroupBox.Name = "_DeviceGroupBox"
        Me._DeviceGroupBox.Size = New System.Drawing.Size(266, 50)
        Me._DeviceGroupBox.TabIndex = 35
        Me._DeviceGroupBox.TabStop = False
        Me._DeviceGroupBox.Text = "Device"
        '
        '_DeviceIdNumericLabel
        '
        Me._DeviceIdNumericLabel.AutoSize = True
        Me._DeviceIdNumericLabel.Location = New System.Drawing.Point(5, 24)
        Me._DeviceIdNumericLabel.Name = "_DeviceIdNumericLabel"
        Me._DeviceIdNumericLabel.Size = New System.Drawing.Size(21, 13)
        Me._DeviceIdNumericLabel.TabIndex = 10
        Me._DeviceIdNumericLabel.Text = "ID:"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Location = New System.Drawing.Point(140, 19)
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(117, 20)
        Me._DeviceInfoTextBox.TabIndex = 7
        '
        'PwmConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(287, 182)
        Me.Controls.Add(Me._DeviceGroupBox)
        Me.Controls.Add(Me._DutyCycleNumeric)
        Me.Controls.Add(Me._FrequencyNumeric)
        Me.Controls.Add(Me._GetDutyCycleButton)
        Me.Controls.Add(Me._GetFrequencyButton)
        Me.Controls.Add(Me._SetDutyCycleButton)
        Me.Controls.Add(Me._SetFrequencyButton)
        Me.Controls.Add(Me._ChannelEnabledCheckBox)
        Me.Controls.Add(Me._ChannelComboBox)
        Me.Controls.Add(Me._DutyCycleNumericLabel)
        Me.Controls.Add(Me._FrequencyNumericLabel)
        Me.Controls.Add(Me._ChannelComboBoxLabel)
        Me.Controls.Add(Me._PortEnabledCheckBox)
        Me.Controls.Add(Me._PortComboBoxLabel)
        Me.Controls.Add(Me._PortComboBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "PwmConsole"
        Me.Text = "Pulse Width Modulation Console"
        CType(Me._FrequencyNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DutyCycleNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._DeviceGroupBox.ResumeLayout(False)
        Me._DeviceGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ChannelEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ChannelComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _DutyCycleNumericLabel As System.Windows.Forms.Label
    Private WithEvents _FrequencyNumericLabel As System.Windows.Forms.Label
    Private WithEvents _ChannelComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PortEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PortComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PortComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _GetDutyCycleButton As System.Windows.Forms.Button
    Private WithEvents _GetFrequencyButton As System.Windows.Forms.Button
    Private WithEvents _SetDutyCycleButton As System.Windows.Forms.Button
    Private WithEvents _SetFrequencyButton As System.Windows.Forms.Button
    Private WithEvents _FrequencyNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _DutyCycleNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _DeviceGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DeviceInfoTextBox As System.Windows.Forms.TextBox
    Private WithEvents _OpenDeviceSubsystemTypeButton As System.Windows.Forms.Button
    Private WithEvents _DeviceIdNumeric As NumericUpDown
    Private WithEvents _DeviceIdNumericLabel As Label
End Class
