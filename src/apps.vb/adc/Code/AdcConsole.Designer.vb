<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdcConsole

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AdcConsole))
        Me._PortComboBoxLabel = New System.Windows.Forms.Label()
        Me._EventLogTextBox = New System.Windows.Forms.TextBox()
        Me._PortComboBox = New System.Windows.Forms.ComboBox()
        Me._ChannelGroupBox = New System.Windows.Forms.GroupBox()
        Me._ThresholdHighNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ThresholdLowNumeric = New System.Windows.Forms.NumericUpDown()
        Me._EventPeriodNumeric = New System.Windows.Forms.NumericUpDown()
        Me._EventEventTypeComboBox = New System.Windows.Forms.ComboBox()
        Me._GetEventConfigButton = New System.Windows.Forms.Button()
        Me._EventConfigLabel = New System.Windows.Forms.Label()
        Me._GetChannelValueButton = New System.Windows.Forms.Button()
        Me._EventEventTypeComboBoxLabel = New System.Windows.Forms.Label()
        Me._ChannelValueTextBox = New System.Windows.Forms.TextBox()
        Me._ChannelValueTextBoxLabel = New System.Windows.Forms.Label()
        Me._SetEventConfigButton = New System.Windows.Forms.Button()
        Me._ChannelEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._ChannelComboBox = New System.Windows.Forms.ComboBox()
        Me._EventPeriodNumericLabel = New System.Windows.Forms.Label()
        Me._ChannelComboBoxLabel = New System.Windows.Forms.Label()
        Me._ThresholdLowNumericLabel = New System.Windows.Forms.Label()
        Me._ThresholdHighNumericLabel = New System.Windows.Forms.Label()
        Me._PortEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._ResolutionComboBox = New System.Windows.Forms.ComboBox()
        Me._ResolutionComboBoxLabel = New System.Windows.Forms.Label()
        Me._PortGroupBox = New System.Windows.Forms.GroupBox()
        Me._GetResolutionButton = New System.Windows.Forms.Button()
        Me._GetPortValuesButton = New System.Windows.Forms.Button()
        Me._SetResolutionButton = New System.Windows.Forms.Button()
        Me._PortValuesTextBox = New System.Windows.Forms.TextBox()
        Me._PortValuesTextBoxLabel = New System.Windows.Forms.Label()
        Me._EventLogGroupBox = New System.Windows.Forms.GroupBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._DeviceIdNumeric = New System.Windows.Forms.NumericUpDown()
        Me._OpenDeviceSubsystemTypeButton = New System.Windows.Forms.Button()
        Me._DeviceGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceIdNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceInfoTextBox = New System.Windows.Forms.TextBox()
        Me._ChannelGroupBox.SuspendLayout()
        CType(Me._ThresholdHighNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ThresholdLowNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._EventPeriodNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._PortGroupBox.SuspendLayout()
        Me._EventLogGroupBox.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._DeviceGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_PortComboBoxLabel
        '
        Me._PortComboBoxLabel.AutoSize = True
        Me._PortComboBoxLabel.Location = New System.Drawing.Point(38, 22)
        Me._PortComboBoxLabel.Name = "_PortComboBoxLabel"
        Me._PortComboBoxLabel.Size = New System.Drawing.Size(29, 13)
        Me._PortComboBoxLabel.TabIndex = 2
        Me._PortComboBoxLabel.Text = "Port:"
        '
        '_EventLogTextBox
        '
        Me._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EventLogTextBox.Location = New System.Drawing.Point(3, 16)
        Me._EventLogTextBox.Multiline = True
        Me._EventLogTextBox.Name = "_EventLogTextBox"
        Me._EventLogTextBox.ReadOnly = True
        Me._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._EventLogTextBox.Size = New System.Drawing.Size(299, 360)
        Me._EventLogTextBox.TabIndex = 1
        '
        '_PortComboBox
        '
        Me._PortComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PortComboBox.FormattingEnabled = True
        Me._PortComboBox.Location = New System.Drawing.Point(69, 19)
        Me._PortComboBox.Name = "_PortComboBox"
        Me._PortComboBox.Size = New System.Drawing.Size(74, 21)
        Me._PortComboBox.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._PortComboBox, "Port numbers")
        '
        '_ChannelGroupBox
        '
        Me._ChannelGroupBox.Controls.Add(Me._ThresholdHighNumeric)
        Me._ChannelGroupBox.Controls.Add(Me._ThresholdLowNumeric)
        Me._ChannelGroupBox.Controls.Add(Me._EventPeriodNumeric)
        Me._ChannelGroupBox.Controls.Add(Me._EventEventTypeComboBox)
        Me._ChannelGroupBox.Controls.Add(Me._GetEventConfigButton)
        Me._ChannelGroupBox.Controls.Add(Me._EventConfigLabel)
        Me._ChannelGroupBox.Controls.Add(Me._GetChannelValueButton)
        Me._ChannelGroupBox.Controls.Add(Me._EventEventTypeComboBoxLabel)
        Me._ChannelGroupBox.Controls.Add(Me._ChannelValueTextBox)
        Me._ChannelGroupBox.Controls.Add(Me._ChannelValueTextBoxLabel)
        Me._ChannelGroupBox.Controls.Add(Me._SetEventConfigButton)
        Me._ChannelGroupBox.Controls.Add(Me._ChannelEnabledCheckBox)
        Me._ChannelGroupBox.Controls.Add(Me._ChannelComboBox)
        Me._ChannelGroupBox.Controls.Add(Me._EventPeriodNumericLabel)
        Me._ChannelGroupBox.Controls.Add(Me._ChannelComboBoxLabel)
        Me._ChannelGroupBox.Controls.Add(Me._ThresholdLowNumericLabel)
        Me._ChannelGroupBox.Controls.Add(Me._ThresholdHighNumericLabel)
        Me._ChannelGroupBox.Location = New System.Drawing.Point(6, 104)
        Me._ChannelGroupBox.Name = "_ChannelGroupBox"
        Me._ChannelGroupBox.Size = New System.Drawing.Size(287, 210)
        Me._ChannelGroupBox.TabIndex = 0
        Me._ChannelGroupBox.TabStop = False
        Me._ChannelGroupBox.Text = "Channels"
        '
        '_ThresholdHighNumeric
        '
        Me._ThresholdHighNumeric.Location = New System.Drawing.Point(121, 179)
        Me._ThresholdHighNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._ThresholdHighNumeric.Maximum = New Decimal(New Integer() {4096, 0, 0, 0})
        Me._ThresholdHighNumeric.Name = "_ThresholdHighNumeric"
        Me._ThresholdHighNumeric.Size = New System.Drawing.Size(50, 20)
        Me._ThresholdHighNumeric.TabIndex = 23
        Me._ToolTip.SetToolTip(Me._ThresholdHighNumeric, "High threshold in bit value")
        '
        '_ThresholdLowNumeric
        '
        Me._ThresholdLowNumeric.Location = New System.Drawing.Point(121, 153)
        Me._ThresholdLowNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._ThresholdLowNumeric.Maximum = New Decimal(New Integer() {4096, 0, 0, 0})
        Me._ThresholdLowNumeric.Name = "_ThresholdLowNumeric"
        Me._ThresholdLowNumeric.Size = New System.Drawing.Size(50, 20)
        Me._ThresholdLowNumeric.TabIndex = 22
        Me._ToolTip.SetToolTip(Me._ThresholdLowNumeric, "Low threshold in bits")
        '
        '_EventPeriodNumeric
        '
        Me._EventPeriodNumeric.Location = New System.Drawing.Point(121, 128)
        Me._EventPeriodNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._EventPeriodNumeric.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me._EventPeriodNumeric.Name = "_EventPeriodNumeric"
        Me._EventPeriodNumeric.Size = New System.Drawing.Size(50, 20)
        Me._EventPeriodNumeric.TabIndex = 21
        Me._ToolTip.SetToolTip(Me._EventPeriodNumeric, "Event period in ms")
        '
        '_EventEventTypeComboBox
        '
        Me._EventEventTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._EventEventTypeComboBox.FormattingEnabled = True
        Me._EventEventTypeComboBox.Location = New System.Drawing.Point(121, 101)
        Me._EventEventTypeComboBox.Name = "_EventEventTypeComboBox"
        Me._EventEventTypeComboBox.Size = New System.Drawing.Size(127, 21)
        Me._EventEventTypeComboBox.TabIndex = 16
        Me._ToolTip.SetToolTip(Me._EventEventTypeComboBox, "Selects the event type")
        '
        '_GetEventConfigButton
        '
        Me._GetEventConfigButton.Location = New System.Drawing.Point(205, 160)
        Me._GetEventConfigButton.Name = "_GetEventConfigButton"
        Me._GetEventConfigButton.Size = New System.Drawing.Size(43, 23)
        Me._GetEventConfigButton.TabIndex = 20
        Me._GetEventConfigButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetEventConfigButton, "Gets the current event configuration")
        Me._GetEventConfigButton.UseVisualStyleBackColor = True
        '
        '_EventConfigLabel
        '
        Me._EventConfigLabel.AutoSize = True
        Me._EventConfigLabel.Location = New System.Drawing.Point(9, 80)
        Me._EventConfigLabel.Name = "_EventConfigLabel"
        Me._EventConfigLabel.Size = New System.Drawing.Size(100, 13)
        Me._EventConfigLabel.TabIndex = 11
        Me._EventConfigLabel.Text = "Event Configuration"
        '
        '_GetChannelValueButton
        '
        Me._GetChannelValueButton.Location = New System.Drawing.Point(153, 47)
        Me._GetChannelValueButton.Name = "_GetChannelValueButton"
        Me._GetChannelValueButton.Size = New System.Drawing.Size(43, 23)
        Me._GetChannelValueButton.TabIndex = 10
        Me._GetChannelValueButton.Text = "Get"
        Me._GetChannelValueButton.UseVisualStyleBackColor = True
        '
        '_EventEventTypeComboBoxLabel
        '
        Me._EventEventTypeComboBoxLabel.AutoSize = True
        Me._EventEventTypeComboBoxLabel.Location = New System.Drawing.Point(56, 104)
        Me._EventEventTypeComboBoxLabel.Name = "_EventEventTypeComboBoxLabel"
        Me._EventEventTypeComboBoxLabel.Size = New System.Drawing.Size(65, 13)
        Me._EventEventTypeComboBoxLabel.TabIndex = 11
        Me._EventEventTypeComboBoxLabel.Text = "Event Type:"
        '
        '_ChannelValueTextBox
        '
        Me._ChannelValueTextBox.BackColor = System.Drawing.Color.Black
        Me._ChannelValueTextBox.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ChannelValueTextBox.ForeColor = System.Drawing.Color.Aqua
        Me._ChannelValueTextBox.Location = New System.Drawing.Point(63, 46)
        Me._ChannelValueTextBox.Name = "_ChannelValueTextBox"
        Me._ChannelValueTextBox.ReadOnly = True
        Me._ChannelValueTextBox.Size = New System.Drawing.Size(73, 33)
        Me._ChannelValueTextBox.TabIndex = 8
        Me._ChannelValueTextBox.Text = "0"
        Me._ChannelValueTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._ChannelValueTextBox, "Measured value")
        '
        '_ChannelValueTextBoxLabel
        '
        Me._ChannelValueTextBoxLabel.AutoSize = True
        Me._ChannelValueTextBoxLabel.Location = New System.Drawing.Point(25, 52)
        Me._ChannelValueTextBoxLabel.Name = "_ChannelValueTextBoxLabel"
        Me._ChannelValueTextBoxLabel.Size = New System.Drawing.Size(37, 13)
        Me._ChannelValueTextBoxLabel.TabIndex = 5
        Me._ChannelValueTextBoxLabel.Text = "Value:"
        '
        '_SetEventConfigButton
        '
        Me._SetEventConfigButton.Location = New System.Drawing.Point(206, 131)
        Me._SetEventConfigButton.Name = "_SetEventConfigButton"
        Me._SetEventConfigButton.Size = New System.Drawing.Size(43, 23)
        Me._SetEventConfigButton.TabIndex = 19
        Me._SetEventConfigButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetEventConfigButton, "Configures the event")
        Me._SetEventConfigButton.UseVisualStyleBackColor = True
        '
        '_ChannelEnabledCheckBox
        '
        Me._ChannelEnabledCheckBox.AutoSize = True
        Me._ChannelEnabledCheckBox.Location = New System.Drawing.Point(153, 21)
        Me._ChannelEnabledCheckBox.Name = "_ChannelEnabledCheckBox"
        Me._ChannelEnabledCheckBox.Size = New System.Drawing.Size(65, 17)
        Me._ChannelEnabledCheckBox.TabIndex = 4
        Me._ChannelEnabledCheckBox.Text = "Enabled"
        Me._ChannelEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_ChannelComboBox
        '
        Me._ChannelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._ChannelComboBox.FormattingEnabled = True
        Me._ChannelComboBox.Location = New System.Drawing.Point(63, 19)
        Me._ChannelComboBox.Name = "_ChannelComboBox"
        Me._ChannelComboBox.Size = New System.Drawing.Size(74, 21)
        Me._ChannelComboBox.TabIndex = 2
        Me._ToolTip.SetToolTip(Me._ChannelComboBox, "Select channel")
        '
        '_EventPeriodNumericLabel
        '
        Me._EventPeriodNumericLabel.AutoSize = True
        Me._EventPeriodNumericLabel.Location = New System.Drawing.Point(27, 130)
        Me._EventPeriodNumericLabel.Name = "_EventPeriodNumericLabel"
        Me._EventPeriodNumericLabel.Size = New System.Drawing.Size(90, 13)
        Me._EventPeriodNumericLabel.TabIndex = 12
        Me._EventPeriodNumericLabel.Text = "Event Period, ms:"
        Me._EventPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ChannelComboBoxLabel
        '
        Me._ChannelComboBoxLabel.AutoSize = True
        Me._ChannelComboBoxLabel.Location = New System.Drawing.Point(13, 22)
        Me._ChannelComboBoxLabel.Name = "_ChannelComboBoxLabel"
        Me._ChannelComboBoxLabel.Size = New System.Drawing.Size(49, 13)
        Me._ChannelComboBoxLabel.TabIndex = 0
        Me._ChannelComboBoxLabel.Text = "Channel:"
        Me._ChannelComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ThresholdLowNumericLabel
        '
        Me._ThresholdLowNumericLabel.AutoSize = True
        Me._ThresholdLowNumericLabel.Location = New System.Drawing.Point(13, 156)
        Me._ThresholdLowNumericLabel.Name = "_ThresholdLowNumericLabel"
        Me._ThresholdLowNumericLabel.Size = New System.Drawing.Size(102, 13)
        Me._ThresholdLowNumericLabel.TabIndex = 13
        Me._ThresholdLowNumericLabel.Text = "Threshold Low, bits:"
        '
        '_ThresholdHighNumericLabel
        '
        Me._ThresholdHighNumericLabel.AutoSize = True
        Me._ThresholdHighNumericLabel.Location = New System.Drawing.Point(9, 182)
        Me._ThresholdHighNumericLabel.Name = "_ThresholdHighNumericLabel"
        Me._ThresholdHighNumericLabel.Size = New System.Drawing.Size(104, 13)
        Me._ThresholdHighNumericLabel.TabIndex = 14
        Me._ThresholdHighNumericLabel.Text = "Threshold High, bits:"
        '
        '_PortEnabledCheckBox
        '
        Me._PortEnabledCheckBox.AutoSize = True
        Me._PortEnabledCheckBox.Location = New System.Drawing.Point(159, 21)
        Me._PortEnabledCheckBox.Name = "_PortEnabledCheckBox"
        Me._PortEnabledCheckBox.Size = New System.Drawing.Size(65, 17)
        Me._PortEnabledCheckBox.TabIndex = 3
        Me._PortEnabledCheckBox.Text = "Enabled"
        Me._PortEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_ResolutionComboBox
        '
        Me._ResolutionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._ResolutionComboBox.FormattingEnabled = True
        Me._ResolutionComboBox.Location = New System.Drawing.Point(69, 48)
        Me._ResolutionComboBox.Name = "_ResolutionComboBox"
        Me._ResolutionComboBox.Size = New System.Drawing.Size(65, 21)
        Me._ResolutionComboBox.TabIndex = 4
        Me._ToolTip.SetToolTip(Me._ResolutionComboBox, "A/D resolution in bits")
        '
        '_ResolutionComboBoxLabel
        '
        Me._ResolutionComboBoxLabel.AutoSize = True
        Me._ResolutionComboBoxLabel.Location = New System.Drawing.Point(6, 51)
        Me._ResolutionComboBoxLabel.Name = "_ResolutionComboBoxLabel"
        Me._ResolutionComboBoxLabel.Size = New System.Drawing.Size(60, 13)
        Me._ResolutionComboBoxLabel.TabIndex = 5
        Me._ResolutionComboBoxLabel.Text = "Resolution:"
        '
        '_PortGroupBox
        '
        Me._PortGroupBox.Controls.Add(Me._GetResolutionButton)
        Me._PortGroupBox.Controls.Add(Me._GetPortValuesButton)
        Me._PortGroupBox.Controls.Add(Me._SetResolutionButton)
        Me._PortGroupBox.Controls.Add(Me._PortValuesTextBox)
        Me._PortGroupBox.Controls.Add(Me._PortValuesTextBoxLabel)
        Me._PortGroupBox.Controls.Add(Me._ResolutionComboBoxLabel)
        Me._PortGroupBox.Controls.Add(Me._ResolutionComboBox)
        Me._PortGroupBox.Controls.Add(Me._PortEnabledCheckBox)
        Me._PortGroupBox.Controls.Add(Me._PortComboBoxLabel)
        Me._PortGroupBox.Controls.Add(Me._PortComboBox)
        Me._PortGroupBox.Controls.Add(Me._ChannelGroupBox)
        Me._PortGroupBox.Location = New System.Drawing.Point(12, 76)
        Me._PortGroupBox.Name = "_PortGroupBox"
        Me._PortGroupBox.Size = New System.Drawing.Size(299, 320)
        Me._PortGroupBox.TabIndex = 4
        Me._PortGroupBox.TabStop = False
        Me._PortGroupBox.Text = "ADC Port"
        '
        '_GetResolutionButton
        '
        Me._GetResolutionButton.Location = New System.Drawing.Point(189, 46)
        Me._GetResolutionButton.Name = "_GetResolutionButton"
        Me._GetResolutionButton.Size = New System.Drawing.Size(43, 23)
        Me._GetResolutionButton.TabIndex = 10
        Me._GetResolutionButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetResolutionButton, "Gets the current resolution")
        Me._GetResolutionButton.UseVisualStyleBackColor = True
        '
        '_GetPortValuesButton
        '
        Me._GetPortValuesButton.Location = New System.Drawing.Point(250, 75)
        Me._GetPortValuesButton.Name = "_GetPortValuesButton"
        Me._GetPortValuesButton.Size = New System.Drawing.Size(43, 23)
        Me._GetPortValuesButton.TabIndex = 9
        Me._GetPortValuesButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetPortValuesButton, "Gets port values")
        Me._GetPortValuesButton.UseVisualStyleBackColor = True
        '
        '_SetResolutionButton
        '
        Me._SetResolutionButton.Location = New System.Drawing.Point(141, 46)
        Me._SetResolutionButton.Name = "_SetResolutionButton"
        Me._SetResolutionButton.Size = New System.Drawing.Size(43, 23)
        Me._SetResolutionButton.TabIndex = 8
        Me._SetResolutionButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetResolutionButton, "Sets resolution")
        Me._SetResolutionButton.UseVisualStyleBackColor = True
        '
        '_PortValuesTextBox
        '
        Me._PortValuesTextBox.BackColor = System.Drawing.Color.Black
        Me._PortValuesTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PortValuesTextBox.ForeColor = System.Drawing.Color.Aqua
        Me._PortValuesTextBox.Location = New System.Drawing.Point(69, 77)
        Me._PortValuesTextBox.Name = "_PortValuesTextBox"
        Me._PortValuesTextBox.ReadOnly = True
        Me._PortValuesTextBox.Size = New System.Drawing.Size(175, 25)
        Me._PortValuesTextBox.TabIndex = 7
        Me._PortValuesTextBox.Text = "0,0"
        Me._PortValuesTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._PortValuesTextBox, "Port values")
        '
        '_PortValuesTextBoxLabel
        '
        Me._PortValuesTextBoxLabel.AutoSize = True
        Me._PortValuesTextBoxLabel.Location = New System.Drawing.Point(26, 80)
        Me._PortValuesTextBoxLabel.Name = "_PortValuesTextBoxLabel"
        Me._PortValuesTextBoxLabel.Size = New System.Drawing.Size(42, 13)
        Me._PortValuesTextBoxLabel.TabIndex = 6
        Me._PortValuesTextBoxLabel.Text = "Values:"
        '
        '_EventLogGroupBox
        '
        Me._EventLogGroupBox.Controls.Add(Me._EventLogTextBox)
        Me._EventLogGroupBox.Location = New System.Drawing.Point(317, 17)
        Me._EventLogGroupBox.Name = "_EventLogGroupBox"
        Me._EventLogGroupBox.Size = New System.Drawing.Size(305, 379)
        Me._EventLogGroupBox.TabIndex = 5
        Me._EventLogGroupBox.TabStop = False
        Me._EventLogGroupBox.Text = "Event Log"
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_DeviceIdNumeric
        '
        Me._DeviceIdNumeric.Location = New System.Drawing.Point(28, 21)
        Me._DeviceIdNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._DeviceIdNumeric.Name = "_DeviceIdNumeric"
        Me._DeviceIdNumeric.Size = New System.Drawing.Size(42, 20)
        Me._DeviceIdNumeric.TabIndex = 11
        Me._ToolTip.SetToolTip(Me._DeviceIdNumeric, "Select device id")
        '
        '_OpenDeviceSubsystemTypeButton
        '
        Me._OpenDeviceSubsystemTypeButton.Location = New System.Drawing.Point(78, 18)
        Me._OpenDeviceSubsystemTypeButton.Name = "_OpenDeviceSubsystemTypeButton"
        Me._OpenDeviceSubsystemTypeButton.Size = New System.Drawing.Size(60, 23)
        Me._OpenDeviceSubsystemTypeButton.TabIndex = 5
        Me._OpenDeviceSubsystemTypeButton.Text = "Open"
        Me._ToolTip.SetToolTip(Me._OpenDeviceSubsystemTypeButton, "Opens the device")
        Me._OpenDeviceSubsystemTypeButton.UseVisualStyleBackColor = True
        '
        '_DeviceGroupBox
        '
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumeric)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumericLabel)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceInfoTextBox)
        Me._DeviceGroupBox.Controls.Add(Me._OpenDeviceSubsystemTypeButton)
        Me._DeviceGroupBox.Location = New System.Drawing.Point(14, 10)
        Me._DeviceGroupBox.Name = "_DeviceGroupBox"
        Me._DeviceGroupBox.Size = New System.Drawing.Size(266, 50)
        Me._DeviceGroupBox.TabIndex = 36
        Me._DeviceGroupBox.TabStop = False
        Me._DeviceGroupBox.Text = "Device"
        '
        '_DeviceIdNumericLabel
        '
        Me._DeviceIdNumericLabel.AutoSize = True
        Me._DeviceIdNumericLabel.Location = New System.Drawing.Point(5, 24)
        Me._DeviceIdNumericLabel.Name = "_DeviceIdNumericLabel"
        Me._DeviceIdNumericLabel.Size = New System.Drawing.Size(21, 13)
        Me._DeviceIdNumericLabel.TabIndex = 10
        Me._DeviceIdNumericLabel.Text = "ID:"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Location = New System.Drawing.Point(142, 21)
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(117, 20)
        Me._DeviceInfoTextBox.TabIndex = 7
        '
        'AdcConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(628, 407)
        Me.Controls.Add(Me._DeviceGroupBox)
        Me.Controls.Add(Me._PortGroupBox)
        Me.Controls.Add(Me._EventLogGroupBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AdcConsole"
        Me.Text = "Analog to Digital Conversion Console"
        Me._ChannelGroupBox.ResumeLayout(False)
        Me._ChannelGroupBox.PerformLayout()
        CType(Me._ThresholdHighNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ThresholdLowNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._EventPeriodNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._PortGroupBox.ResumeLayout(False)
        Me._PortGroupBox.PerformLayout()
        Me._EventLogGroupBox.ResumeLayout(False)
        Me._EventLogGroupBox.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._DeviceGroupBox.ResumeLayout(False)
        Me._DeviceGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _PortComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _EventLogTextBox As System.Windows.Forms.TextBox
    Private WithEvents _PortComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _ChannelGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _EventEventTypeComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _GetEventConfigButton As System.Windows.Forms.Button
    Private WithEvents _EventConfigLabel As System.Windows.Forms.Label
    Private WithEvents _GetChannelValueButton As System.Windows.Forms.Button
    Private WithEvents _EventEventTypeComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ChannelValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ChannelValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SetEventConfigButton As System.Windows.Forms.Button
    Private WithEvents _ChannelEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ChannelComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _EventPeriodNumericLabel As System.Windows.Forms.Label
    Private WithEvents _ChannelComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ThresholdLowNumericLabel As System.Windows.Forms.Label
    Private WithEvents _ThresholdHighNumericLabel As System.Windows.Forms.Label
    Private WithEvents _PortEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ResolutionComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _ResolutionComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PortGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _GetResolutionButton As System.Windows.Forms.Button
    Private WithEvents _GetPortValuesButton As System.Windows.Forms.Button
    Private WithEvents _SetResolutionButton As System.Windows.Forms.Button
    Private WithEvents _PortValuesTextBox As System.Windows.Forms.TextBox
    Private WithEvents _PortValuesTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _EventLogGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ThresholdHighNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _ThresholdLowNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _EventPeriodNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _DeviceIdNumeric As NumericUpDown
    Private WithEvents _DeviceIdNumericLabel As Label
    Private WithEvents _OpenDeviceSubsystemTypeButton As Button
    Private WithEvents _DeviceInfoTextBox As TextBox
    Private WithEvents _DeviceGroupBox As GroupBox
End Class
