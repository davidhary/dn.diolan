Imports isr.Diolan.SubsystemExtensions
Imports isr.Diolan.WinControls.SubsystemExtensions
Imports isr.Diolan.WinControls.ErrorProviderExtensions

''' <summary> ADC Console. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-29 </para>
''' </remarks>
Partial Friend Class AdcConsole

#Region " CONSTRUCTION "

    ''' <summary> Executes the custom dispose action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> true to disposing. </param>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnSubsystemTypeClosed()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " SUBSYSTEM TYPE "

    ''' <summary> the subsystem type. </summary>
    ''' <value> the subsystem type. </value>
    Public Property SubsystemType As SubsystemTypes = SubsystemTypes.Adc

    ''' <summary> Executes the subsystem type closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnSubsystemTypeClosed()
        Me._PortComboBox.DataSource = Nothing
        Me._PortComboBox.Items.Clear()
        Me._ChannelComboBox.DataSource = Nothing
        Me._ChannelComboBox.Items.Clear()
        Me._Device = Nothing
    End Sub

    ''' <summary> Executes the subsystem type closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnSubsystemTypeClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            If Me.IsDeviceSubsystemTypeOpen Then
                ' un-register event handlers for all channels
                For Each port As Dln.Adc.Port In Me.Device.Adc.Ports
                    For Each channel As Dln.Adc.Channel In port.Channels
                        RemoveHandler channel.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                    Next
                Next
            End If
        End If
    End Sub

    ''' <summary> Executes the subsystem type opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnSubsystemTypeOpening()

        Me._Device = Me._DeviceConnector.Device
        Me._PortComboBox.DataSource = Nothing
        Me._PortComboBox.Items.Clear()
        Me._ChannelComboBox.DataSource = Nothing
        Me._ChannelComboBox.Items.Clear()

        ' Get port count
        If Me.Device.Adc.Ports.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._OpenDeviceSubsystemTypeButton,
                                         "Adapter '{0}' doesn't support ADC interface.",
                                         Me.Device.Caption)
            Me._Device = Nothing
            Me._DeviceInfoTextBox.Text = "not supported"
        Else
            Me._DeviceInfoTextBox.Text = Me.Device.Caption
            Me.Device.Adc.Ports.ListNumbers(Me._PortComboBox, True)

            ' Set current context to run thread safe events in main form thread
            Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current

            ' Register event handlers for all channels
            For Each port As Dln.Adc.Port In Me.Device.Adc.Ports
                For Each channel As Dln.Adc.Channel In port.Channels
                    AddHandler channel.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                Next
            Next

            Me._PortComboBox.SelectedIndex = 0    ' this will cause "OnChange" event
        End If

    End Sub

    ''' <summary> Queries if a Subsystem Type is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
    Private Function IsSubsystemTypeOpen() As Boolean
        Return Me._PortComboBox.DataSource IsNot Nothing AndAlso Me._PortComboBox.Items.Count > 0
    End Function

#End Region

#Region " EVENT LOG "

    ''' <summary> Handler, called when the condition met event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Condition met event information. </param>
    Private Sub ConditionMetEventHandler(ByVal sender As Object, ByVal e As Dln.Adc.ConditionMetEventArgs)
        Me.AppendEvent(e)
    End Sub

    ''' <summary> Appends an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Condition met event information. </param>
    Private Sub AppendEvent(ByVal e As Dln.Adc.ConditionMetEventArgs)
        If e IsNot Nothing Then
            Dim data As String = $"{DateTimeOffset.Now:hh:mm:ss.fff} A/D{e.Port:D2}!{e.Channel:D2}={e.Value:D5} {e.EventType}{Environment.NewLine}"
            ' This event is handled in main thread--invoke is not needed when modifying form's controls.
            Me._EventLogTextBox.AppendText(data)
        End If
    End Sub


#End Region

#Region " PORT "

    ''' <summary> Select ADC port. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="adcPort"> The ADC port. </param>
    Private Sub ReadAdcPortProperties(adcPort As Dln.Adc.Port)

        ' Get supported resolutions
        Me._ResolutionComboBox.DataSource = adcPort.SupportedResolutions

        ' Get ports parameters
        Me._PortEnabledCheckBox.Checked = adcPort.Enabled
        Me.GetResolution(adcPort)
        Me.GetValues(adcPort)

        ' list the channels.
        adcPort.Channels.ListNumbers(Me._ChannelComboBox)
        Me._ChannelComboBox.SelectedIndex = 0
    End Sub

    ''' <summary> Port combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PortComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PortComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    Me.ReadAdcPortProperties(adcPort)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If

            ElseIf Me.IsSubsystemTypeOpen AndAlso Me._ErrorProvider IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Port enabled check box checked changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PortEnabledCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PortEnabledCheckBox.CheckedChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    adcPort.Enabled = Me._PortEnabledCheckBox.Checked
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)

                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Sets resolution button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SetResolutionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetResolutionButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    adcPort.Resolution = CType(Me._ResolutionComboBox.SelectedItem, Integer)
                Else

                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets a resolution. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="adcPort"> The ADC port. </param>
    Private Sub GetResolution(ByVal adcPort As Dln.Adc.Port)
        Me._ResolutionComboBox.SelectedItem = adcPort.Resolution
        Me._ThresholdLowNumeric.Maximum = CDec(2 ^ adcPort.Resolution)
        Me._ThresholdHighNumeric.Maximum = CDec(2 ^ adcPort.Resolution)
    End Sub

    ''' <summary> Gets resolution button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub GetResolutionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetResolutionButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    Me.GetResolution(adcPort)

                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets the values. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="adcPort"> The ADC port. </param>
    Private Sub GetValues(ByVal adcPort As Dln.Adc.Port)
        Me._PortValuesTextBox.Text = "reading..."
        Dim values() As Integer = adcPort.Values
        Dim builder As New System.Text.StringBuilder
        For Each value As Integer In values
            builder.AppendFormat("{0} ", value)
        Next
        Me._PortValuesTextBox.Text = builder.ToString
    End Sub

    ''' <summary> Gets port values button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub GetPortValuesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetPortValuesButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)

                    Me.GetValues(adcPort)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region

#Region " CHANNEL "

    ''' <summary> Channel combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ChannelComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ChannelComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then

                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < adcPort.Channels.Count Then
                        Dim adcChannel As Dln.Adc.Channel = Me.Device.Adc.Ports(portNumber).Channels(channelNumber)
                        ' Get supported event types
                        Me._EventEventTypeComboBox.DataSource = adcChannel.SupportedEventTypes

                        ' Get channel configuration
                        Me._ChannelEnabledCheckBox.Checked = adcChannel.Enabled
                        Me.GetValue(adcChannel)
                        Me.GetEvent(adcPort, adcChannel)
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", adcPort.Channels.Count - 1)
                    End If
                ElseIf Me._PortComboBox.DataSource IsNot Nothing Then
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If
            ElseIf Me.IsSubsystemTypeOpen AndAlso Me._ErrorProvider IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Channel enabled check box checked changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ChannelEnabledCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ChannelEnabledCheckBox.CheckedChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex

                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < adcPort.Channels.Count Then
                        Dim adcChannel As Dln.Adc.Channel = Me._Device.Adc.Ports(portNumber).Channels(channelNumber)
                        adcChannel.Enabled = Me._ChannelEnabledCheckBox.Checked
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", adcPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets a value. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="adcChannel"> The ADC channel. </param>
    Private Sub GetValue(ByVal adcChannel As Dln.Adc.Channel)
        Dim value As String = If((adcChannel.Restrictions.Enabled = Dln.Restriction.MustBeEnabled AndAlso Not adcChannel.Enabled),
                                    "disabled",
                                    adcChannel.Value.ToString())
        ' Reading value is allowed only for enabled channels
        Me._ChannelValueTextBox.Text = value
    End Sub

    ''' <summary> Gets channel value button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub GetChannelValueButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetChannelValueButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then

                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < adcPort.Channels.Count Then
                        Dim adcChannel As Dln.Adc.Channel = Me._Device.Adc.Ports(portNumber).Channels(channelNumber)
                        Me.GetValue(adcChannel)
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", adcPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region

#Region " EVENT "

    ''' <summary> Sets channel event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="adcChannel"> The ADC channel. </param>
    Private Sub SetEvent(ByVal adcChannel As Dln.Adc.Channel)
        Dim eventType As Dln.Adc.EventType = CType(Me._EventEventTypeComboBox.SelectedItem, Dln.Adc.EventType)
        Dim eventPeriod As Integer = CInt(Me._EventPeriodNumeric.Value)
        Dim thrLow As Integer = CInt(Me._ThresholdLowNumeric.Value)
        Dim thrHigh As Integer = CInt(Me._ThresholdHighNumeric.Value)
        adcChannel.SetEventConfig(eventType, eventPeriod, thrLow, thrHigh)
    End Sub

    ''' <summary> Sets event configuration button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SetEventConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetEventConfigButton.Click
        Try
            Me._ErrorProvider.Clear()

            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < adcPort.Channels.Count Then
                        Dim adcChannel As Dln.Adc.Channel = Me.Device.Adc.Ports(portNumber).Channels(channelNumber)
                        Me.SetEvent(adcChannel)
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", adcPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="adcPort">    The ADC port. </param>
    ''' <param name="adcChannel"> The ADC channel. </param>
    Private Sub GetEvent(ByVal adcPort As Dln.Adc.Port, ByVal adcChannel As Dln.Adc.Channel)
        Me._ThresholdLowNumeric.Maximum = CDec(2 ^ adcPort.Resolution)
        Me._ThresholdHighNumeric.Maximum = CDec(2 ^ adcPort.Resolution)

        Me._EventEventTypeComboBox.SelectedItem = adcChannel.EventType
        Me._EventPeriodNumeric.Value = adcChannel.EventPeriod
        Me._ThresholdLowNumeric.Value = adcChannel.ThresholdLow
        Me._ThresholdHighNumeric.Value = adcChannel.ThresholdHigh
    End Sub

    ''' <summary> Gets event configuration button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub GetEventConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetEventConfigButton.Click
        Try

            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Adc.Ports.Count Then
                    Dim adcPort As Dln.Adc.Port = Me.Device.Adc.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < adcPort.Channels.Count Then
                        Dim adcChannel As Dln.Adc.Channel = Me.Device.Adc.Ports(portNumber).Channels(channelNumber)
                        Me.GetEvent(adcPort, adcChannel)
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", adcPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Adc.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region

End Class
