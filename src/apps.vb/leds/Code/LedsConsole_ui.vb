
Imports isr.Diolan.ExceptionExtensions
Imports isr.Diolan.WinControls.ErrorProviderExtensions

Public Class LedsConsole
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnDispose(disposing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Me.OnCustomDispose(disposing)
        If Not Me.IsDisposed AndAlso disposing Then
            If Me._Device IsNot Nothing Then Me._Device = Nothing
            If Me._DeviceConnector IsNot Nothing Then Me._DeviceConnector.Dispose() : Me._DeviceConnector = Nothing
        End If
    End Sub

#End Region

#Region " DEVICE "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DeviceConnector As DeviceConnector
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Device connector device closed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceClosed(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceClosed
        Me._DeviceInfoTextBox.Text = "closed"
        Me.OnSubsystemTypeClosed()
    End Sub

    ''' <summary> Device connector device closing. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub DeviceConnector_DeviceClosing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _DeviceConnector.DeviceClosing
        Me.OnSubsystemTypeClosing(e)
    End Sub

    ''' <summary> Device connector device opened. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceOpened(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceOpened
    End Sub

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    Private ReadOnly Property Device As Dln.Device

    ''' <summary> Opens the device for the subsystem type. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="deviceId"> Identifier for the device. </param>
    ''' <param name="sender">   The sender. </param>
    Private Sub OpenDeviceSubsystemType(ByVal deviceId As Long, ByVal sender As Control)

        LocalhostConnector.SingleInstance.Connect()
        Me._DeviceConnector = New DeviceConnector(LocalhostConnector.SingleInstance)

        ' Open device
        Dim outcome As (succces As Boolean, details As String) = Me._DeviceConnector.TryOpenDevice(deviceId, Me.SubsystemType)
        If outcome.succces Then
            Me.OnSubsystemTypeOpening()
        Else
            If sender IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, outcome.details)
            End If
            Me._DeviceInfoTextBox.Text = "No devices"
        End If

    End Sub

    ''' <summary> Closes device Subsystem Type. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub CloseDeviceSubsystemType()
        Me._DeviceConnector.CloseDevice(Me.SubsystemType)
        LocalhostConnector.SingleInstance.Disconnect()
        Dim e As New System.ComponentModel.CancelEventArgs
        Me.OnSubsystemTypeClosing(e)
        If Not e.Cancel Then
            Me.OnSubsystemTypeClosed()
        End If
    End Sub

    ''' <summary> Opens device button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenDeviceSubsystemTypeButton_Click(sender As Object, e As System.EventArgs) Handles _OpenDeviceSubsystemTypeButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Me.CloseDeviceSubsystemType()
            Else
                Me.OpenDeviceSubsystemType(CLng(Me._DeviceIdNumeric.Value), TryCast(sender, Control))
            End If
        Catch ex As Exception
            ex.AddExceptionData()
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        Finally
            Me._OpenDeviceSubsystemTypeButton.Text = If(Me.IsDeviceSubsystemTypeOpen, "Close", "Open")
        End Try
    End Sub

    ''' <summary> Gets a value indicating whether a device is open. </summary>
    ''' <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
    Private ReadOnly Property IsDeviceSubsystemTypeOpen As Boolean
        Get
            Return Me._DeviceConnector IsNot Nothing AndAlso Me._DeviceConnector.IsDeviceOpen AndAlso Me.IsSubsystemTypeOpen
        End Get
    End Property

#End Region

#Region " EVENT LOG "

#End Region

End Class
