<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LedsConsole

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LedsConsole))
        Me._SetLedStateButton = New System.Windows.Forms.Button()
        Me._LedStateComboBox = New System.Windows.Forms.ComboBox()
        Me._LedStateComboBoxLabel = New System.Windows.Forms.Label()
        Me._LedNumberComboBox = New System.Windows.Forms.ComboBox()
        Me._LedNumberComboBoxLabel = New System.Windows.Forms.Label()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._DurationNumeric = New System.Windows.Forms.NumericUpDown()
        Me._PulseButtonButton = New System.Windows.Forms.Button()
        Me._DeviceIdNumeric = New System.Windows.Forms.NumericUpDown()
        Me._OpenDeviceSubsystemTypeButton = New System.Windows.Forms.Button()
        Me._LedGroupBox = New System.Windows.Forms.GroupBox()
        Me._DurationNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceIdNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceInfoTextBox = New System.Windows.Forms.TextBox()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DurationNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._LedGroupBox.SuspendLayout()
        Me._DeviceGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SetLedStateButton
        '
        Me._SetLedStateButton.Location = New System.Drawing.Point(198, 32)
        Me._SetLedStateButton.Name = "_SetLedStateButton"
        Me._SetLedStateButton.Size = New System.Drawing.Size(63, 23)
        Me._SetLedStateButton.TabIndex = 9
        Me._SetLedStateButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetLedStateButton, "Sets the state of the selected LED.")
        Me._SetLedStateButton.UseVisualStyleBackColor = True
        '
        '_LedStateComboBox
        '
        Me._LedStateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._LedStateComboBox.FormattingEnabled = True
        Me._LedStateComboBox.Location = New System.Drawing.Point(102, 34)
        Me._LedStateComboBox.Name = "_LedStateComboBox"
        Me._LedStateComboBox.Size = New System.Drawing.Size(90, 21)
        Me._LedStateComboBox.TabIndex = 8
        Me._ToolTip.SetToolTip(Me._LedStateComboBox, "Selects the state of the selected LED.")
        '
        '_LedStateComboBoxLabel
        '
        Me._LedStateComboBoxLabel.AutoSize = True
        Me._LedStateComboBoxLabel.Location = New System.Drawing.Point(100, 18)
        Me._LedStateComboBoxLabel.Name = "_LedStateComboBoxLabel"
        Me._LedStateComboBoxLabel.Size = New System.Drawing.Size(56, 13)
        Me._LedStateComboBoxLabel.TabIndex = 7
        Me._LedStateComboBoxLabel.Text = "LED State"
        '
        '_LedNumberComboBox
        '
        Me._LedNumberComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._LedNumberComboBox.FormattingEnabled = True
        Me._LedNumberComboBox.Location = New System.Drawing.Point(15, 34)
        Me._LedNumberComboBox.Name = "_LedNumberComboBox"
        Me._LedNumberComboBox.Size = New System.Drawing.Size(80, 21)
        Me._LedNumberComboBox.TabIndex = 6
        Me._ToolTip.SetToolTip(Me._LedNumberComboBox, "Selected LED number.")
        '
        '_LedNumberComboBoxLabel
        '
        Me._LedNumberComboBoxLabel.AutoSize = True
        Me._LedNumberComboBoxLabel.Location = New System.Drawing.Point(15, 18)
        Me._LedNumberComboBoxLabel.Name = "_LedNumberComboBoxLabel"
        Me._LedNumberComboBoxLabel.Size = New System.Drawing.Size(68, 13)
        Me._LedNumberComboBoxLabel.TabIndex = 5
        Me._LedNumberComboBoxLabel.Text = "LED Number"
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_DurationNumeric
        '
        Me._DurationNumeric.Location = New System.Drawing.Point(103, 63)
        Me._DurationNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._DurationNumeric.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me._DurationNumeric.Name = "_DurationNumeric"
        Me._DurationNumeric.Size = New System.Drawing.Size(88, 20)
        Me._DurationNumeric.TabIndex = 10
        Me._ToolTip.SetToolTip(Me._DurationNumeric, "On duration in ms")
        Me._DurationNumeric.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        '_PulseButtonButton
        '
        Me._PulseButtonButton.Location = New System.Drawing.Point(196, 61)
        Me._PulseButtonButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._PulseButtonButton.Name = "_PulseButtonButton"
        Me._PulseButtonButton.Size = New System.Drawing.Size(63, 23)
        Me._PulseButtonButton.TabIndex = 11
        Me._PulseButtonButton.Text = "Pulse"
        Me._ToolTip.SetToolTip(Me._PulseButtonButton, "Turn on for the specified duration")
        Me._PulseButtonButton.UseVisualStyleBackColor = True
        '
        '_DeviceIdNumeric
        '
        Me._DeviceIdNumeric.Location = New System.Drawing.Point(28, 21)
        Me._DeviceIdNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._DeviceIdNumeric.Name = "_DeviceIdNumeric"
        Me._DeviceIdNumeric.Size = New System.Drawing.Size(42, 20)
        Me._DeviceIdNumeric.TabIndex = 11
        Me._ToolTip.SetToolTip(Me._DeviceIdNumeric, "Select device id")
        '
        '_OpenDeviceSubsystemTypeButton
        '
        Me._OpenDeviceSubsystemTypeButton.Location = New System.Drawing.Point(78, 18)
        Me._OpenDeviceSubsystemTypeButton.Name = "_OpenDeviceSubsystemTypeButton"
        Me._OpenDeviceSubsystemTypeButton.Size = New System.Drawing.Size(60, 23)
        Me._OpenDeviceSubsystemTypeButton.TabIndex = 5
        Me._OpenDeviceSubsystemTypeButton.Text = "Open"
        Me._ToolTip.SetToolTip(Me._OpenDeviceSubsystemTypeButton, "Opens the device")
        Me._OpenDeviceSubsystemTypeButton.UseVisualStyleBackColor = True
        '
        '_LedGroupBox
        '
        Me._LedGroupBox.Controls.Add(Me._DurationNumericLabel)
        Me._LedGroupBox.Controls.Add(Me._PulseButtonButton)
        Me._LedGroupBox.Controls.Add(Me._DurationNumeric)
        Me._LedGroupBox.Controls.Add(Me._LedNumberComboBox)
        Me._LedGroupBox.Controls.Add(Me._LedNumberComboBoxLabel)
        Me._LedGroupBox.Controls.Add(Me._SetLedStateButton)
        Me._LedGroupBox.Controls.Add(Me._LedStateComboBoxLabel)
        Me._LedGroupBox.Controls.Add(Me._LedStateComboBox)
        Me._LedGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._LedGroupBox.Location = New System.Drawing.Point(0, 56)
        Me._LedGroupBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._LedGroupBox.Name = "_LedGroupBox"
        Me._LedGroupBox.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._LedGroupBox.Size = New System.Drawing.Size(277, 89)
        Me._LedGroupBox.TabIndex = 18
        Me._LedGroupBox.TabStop = False
        Me._LedGroupBox.Text = "LED"
        '
        '_DurationNumericLabel
        '
        Me._DurationNumericLabel.AutoSize = True
        Me._DurationNumericLabel.Location = New System.Drawing.Point(28, 67)
        Me._DurationNumericLabel.Name = "_DurationNumericLabel"
        Me._DurationNumericLabel.Size = New System.Drawing.Size(72, 13)
        Me._DurationNumericLabel.TabIndex = 12
        Me._DurationNumericLabel.Text = "Duration [ms]:"
        Me._DurationNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_DeviceGroupBox
        '
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumeric)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumericLabel)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceInfoTextBox)
        Me._DeviceGroupBox.Controls.Add(Me._OpenDeviceSubsystemTypeButton)
        Me._DeviceGroupBox.Location = New System.Drawing.Point(5, 4)
        Me._DeviceGroupBox.Name = "_DeviceGroupBox"
        Me._DeviceGroupBox.Size = New System.Drawing.Size(266, 50)
        Me._DeviceGroupBox.TabIndex = 36
        Me._DeviceGroupBox.TabStop = False
        Me._DeviceGroupBox.Text = "Device"
        '
        '_DeviceIdNumericLabel
        '
        Me._DeviceIdNumericLabel.AutoSize = True
        Me._DeviceIdNumericLabel.Location = New System.Drawing.Point(5, 24)
        Me._DeviceIdNumericLabel.Name = "_DeviceIdNumericLabel"
        Me._DeviceIdNumericLabel.Size = New System.Drawing.Size(21, 13)
        Me._DeviceIdNumericLabel.TabIndex = 10
        Me._DeviceIdNumericLabel.Text = "ID:"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Location = New System.Drawing.Point(140, 19)
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(117, 20)
        Me._DeviceInfoTextBox.TabIndex = 7
        '
        'LedsConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(277, 145)
        Me.Controls.Add(Me._DeviceGroupBox)
        Me.Controls.Add(Me._LedGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "LedsConsole"
        Me.Text = "LED Console"
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DurationNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._LedGroupBox.ResumeLayout(False)
        Me._LedGroupBox.PerformLayout()
        Me._DeviceGroupBox.ResumeLayout(False)
        Me._DeviceGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _SetLedStateButton As System.Windows.Forms.Button
    Private WithEvents _LedStateComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _LedStateComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _LedNumberComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _LedNumberComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _LedGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DurationNumericLabel As System.Windows.Forms.Label
    Private WithEvents _PulseButtonButton As System.Windows.Forms.Button
    Private WithEvents _DurationNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _DeviceIdNumeric As NumericUpDown
    Private WithEvents _DeviceIdNumericLabel As Label
    Private WithEvents _OpenDeviceSubsystemTypeButton As Button
    Private WithEvents _DeviceInfoTextBox As TextBox
    Private WithEvents _DeviceGroupBox As GroupBox
End Class
