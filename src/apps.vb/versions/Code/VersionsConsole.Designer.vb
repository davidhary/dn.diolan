﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VersionsConsole

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then components.Dispose() : components = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VersionsConsole))
        Me._LibraryVersionTextBoxLabel = New System.Windows.Forms.Label()
        Me._ServerVersionTextBoxLabel = New System.Windows.Forms.Label()
        Me._FirmwareVersionTextBoxLabel = New System.Windows.Forms.Label()
        Me._HardwareVersionTextBoxLabel = New System.Windows.Forms.Label()
        Me._HardwareTypeTextBoxLabel = New System.Windows.Forms.Label()
        Me._LibraryVersionTextBox = New System.Windows.Forms.TextBox()
        Me._ServerVersionTextBox = New System.Windows.Forms.TextBox()
        Me._FirmwareVersionTextBox = New System.Windows.Forms.TextBox()
        Me._HardwareVersionTextBox = New System.Windows.Forms.TextBox()
        Me._HardwareTypeTextBox = New System.Windows.Forms.TextBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._DeviceGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceIdNumeric = New System.Windows.Forms.NumericUpDown()
        Me._DeviceIdNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceInfoTextBox = New System.Windows.Forms.TextBox()
        Me._ShowVersionsButton = New System.Windows.Forms.Button()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ListView = New System.Windows.Forms.ListView()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._DeviceGroupBox.SuspendLayout()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_LibraryVersionTextBoxLabel
        '
        Me._LibraryVersionTextBoxLabel.AutoSize = True
        Me._LibraryVersionTextBoxLabel.Location = New System.Drawing.Point(34, 165)
        Me._LibraryVersionTextBoxLabel.Name = "_LibraryVersionTextBoxLabel"
        Me._LibraryVersionTextBoxLabel.Size = New System.Drawing.Size(79, 13)
        Me._LibraryVersionTextBoxLabel.TabIndex = 21
        Me._LibraryVersionTextBoxLabel.Text = "Library Version:"
        '
        '_ServerVersionTextBoxLabel
        '
        Me._ServerVersionTextBoxLabel.AutoSize = True
        Me._ServerVersionTextBoxLabel.Location = New System.Drawing.Point(34, 139)
        Me._ServerVersionTextBoxLabel.Name = "_ServerVersionTextBoxLabel"
        Me._ServerVersionTextBoxLabel.Size = New System.Drawing.Size(79, 13)
        Me._ServerVersionTextBoxLabel.TabIndex = 20
        Me._ServerVersionTextBoxLabel.Text = "Server Version:"
        '
        '_FirmwareVersionTextBoxLabel
        '
        Me._FirmwareVersionTextBoxLabel.AutoSize = True
        Me._FirmwareVersionTextBoxLabel.Location = New System.Drawing.Point(23, 113)
        Me._FirmwareVersionTextBoxLabel.Name = "_FirmwareVersionTextBoxLabel"
        Me._FirmwareVersionTextBoxLabel.Size = New System.Drawing.Size(90, 13)
        Me._FirmwareVersionTextBoxLabel.TabIndex = 19
        Me._FirmwareVersionTextBoxLabel.Text = "Firmware Version:"
        '
        '_HardwareVersionTextBoxLabel
        '
        Me._HardwareVersionTextBoxLabel.AutoSize = True
        Me._HardwareVersionTextBoxLabel.Location = New System.Drawing.Point(19, 87)
        Me._HardwareVersionTextBoxLabel.Name = "_HardwareVersionTextBoxLabel"
        Me._HardwareVersionTextBoxLabel.Size = New System.Drawing.Size(94, 13)
        Me._HardwareVersionTextBoxLabel.TabIndex = 18
        Me._HardwareVersionTextBoxLabel.Text = "Hardware Version:"
        '
        '_HardwareTypeTextBoxLabel
        '
        Me._HardwareTypeTextBoxLabel.AutoSize = True
        Me._HardwareTypeTextBoxLabel.Location = New System.Drawing.Point(30, 61)
        Me._HardwareTypeTextBoxLabel.Name = "_HardwareTypeTextBoxLabel"
        Me._HardwareTypeTextBoxLabel.Size = New System.Drawing.Size(83, 13)
        Me._HardwareTypeTextBoxLabel.TabIndex = 17
        Me._HardwareTypeTextBoxLabel.Text = "Hardware Type:"
        '
        '_LibraryVersionTextBox
        '
        Me._LibraryVersionTextBox.Enabled = False
        Me._LibraryVersionTextBox.Location = New System.Drawing.Point(116, 162)
        Me._LibraryVersionTextBox.Name = "_LibraryVersionTextBox"
        Me._LibraryVersionTextBox.Size = New System.Drawing.Size(100, 20)
        Me._LibraryVersionTextBox.TabIndex = 16
        '
        '_ServerVersionTextBox
        '
        Me._ServerVersionTextBox.Enabled = False
        Me._ServerVersionTextBox.Location = New System.Drawing.Point(116, 136)
        Me._ServerVersionTextBox.Name = "_ServerVersionTextBox"
        Me._ServerVersionTextBox.Size = New System.Drawing.Size(100, 20)
        Me._ServerVersionTextBox.TabIndex = 15
        '
        '_FirmwareVersionTextBox
        '
        Me._FirmwareVersionTextBox.Enabled = False
        Me._FirmwareVersionTextBox.Location = New System.Drawing.Point(116, 110)
        Me._FirmwareVersionTextBox.Name = "_FirmwareVersionTextBox"
        Me._FirmwareVersionTextBox.Size = New System.Drawing.Size(100, 20)
        Me._FirmwareVersionTextBox.TabIndex = 14
        '
        '_HardwareVersionTextBox
        '
        Me._HardwareVersionTextBox.Enabled = False
        Me._HardwareVersionTextBox.Location = New System.Drawing.Point(116, 84)
        Me._HardwareVersionTextBox.Name = "_HardwareVersionTextBox"
        Me._HardwareVersionTextBox.Size = New System.Drawing.Size(100, 20)
        Me._HardwareVersionTextBox.TabIndex = 13
        '
        '_HardwareTypeTextBox
        '
        Me._HardwareTypeTextBox.Enabled = False
        Me._HardwareTypeTextBox.Location = New System.Drawing.Point(116, 58)
        Me._HardwareTypeTextBox.Name = "_HardwareTypeTextBox"
        Me._HardwareTypeTextBox.Size = New System.Drawing.Size(100, 20)
        Me._HardwareTypeTextBox.TabIndex = 12
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_DeviceGroupBox
        '
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumeric)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumericLabel)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceInfoTextBox)
        Me._DeviceGroupBox.Controls.Add(Me._ShowVersionsButton)
        Me._DeviceGroupBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._DeviceGroupBox.Location = New System.Drawing.Point(0, 0)
        Me._DeviceGroupBox.Name = "_DeviceGroupBox"
        Me._DeviceGroupBox.Size = New System.Drawing.Size(574, 46)
        Me._DeviceGroupBox.TabIndex = 22
        Me._DeviceGroupBox.TabStop = False
        Me._DeviceGroupBox.Text = "Device"
        '
        '_DeviceIdNumeric
        '
        Me._DeviceIdNumeric.Location = New System.Drawing.Point(65, 17)
        Me._DeviceIdNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._DeviceIdNumeric.Name = "_DeviceIdNumeric"
        Me._DeviceIdNumeric.Size = New System.Drawing.Size(42, 20)
        Me._DeviceIdNumeric.TabIndex = 9
        Me._ToolTip.SetToolTip(Me._DeviceIdNumeric, "Select device id")
        '
        '_DeviceIdNumericLabel
        '
        Me._DeviceIdNumericLabel.AutoSize = True
        Me._DeviceIdNumericLabel.Location = New System.Drawing.Point(42, 20)
        Me._DeviceIdNumericLabel.Name = "_DeviceIdNumericLabel"
        Me._DeviceIdNumericLabel.Size = New System.Drawing.Size(21, 13)
        Me._DeviceIdNumericLabel.TabIndex = 8
        Me._DeviceIdNumericLabel.Text = "ID:"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Location = New System.Drawing.Point(220, 16)
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(120, 20)
        Me._DeviceInfoTextBox.TabIndex = 7
        '
        '_ShowVersionsButton
        '
        Me._ShowVersionsButton.Location = New System.Drawing.Point(116, 15)
        Me._ShowVersionsButton.Name = "_ShowVersionsButton"
        Me._ShowVersionsButton.Size = New System.Drawing.Size(99, 22)
        Me._ShowVersionsButton.TabIndex = 5
        Me._ShowVersionsButton.Text = "Show"
        Me._ToolTip.SetToolTip(Me._ShowVersionsButton, "Opens the devices, displays the versions and closes the device.")
        Me._ShowVersionsButton.UseVisualStyleBackColor = True
        '
        '_ListView
        '
        Me._ListView.HideSelection = False
        Me._ListView.Location = New System.Drawing.Point(220, 58)
        Me._ListView.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._ListView.Name = "_ListView"
        Me._ListView.Size = New System.Drawing.Size(344, 124)
        Me._ListView.TabIndex = 23
        Me._ListView.UseCompatibleStateImageBehavior = False
        '
        'VersionsConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(574, 189)
        Me.Controls.Add(Me._ListView)
        Me.Controls.Add(Me._DeviceGroupBox)
        Me.Controls.Add(Me._LibraryVersionTextBoxLabel)
        Me.Controls.Add(Me._ServerVersionTextBoxLabel)
        Me.Controls.Add(Me._FirmwareVersionTextBoxLabel)
        Me.Controls.Add(Me._HardwareVersionTextBoxLabel)
        Me.Controls.Add(Me._HardwareTypeTextBoxLabel)
        Me.Controls.Add(Me._LibraryVersionTextBox)
        Me.Controls.Add(Me._ServerVersionTextBox)
        Me.Controls.Add(Me._FirmwareVersionTextBox)
        Me.Controls.Add(Me._HardwareVersionTextBox)
        Me.Controls.Add(Me._HardwareTypeTextBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "VersionsConsole"
        Me.Text = "Versions Console"
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._DeviceGroupBox.ResumeLayout(False)
        Me._DeviceGroupBox.PerformLayout()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _LibraryVersionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ServerVersionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _FirmwareVersionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _HardwareVersionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _HardwareTypeTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _LibraryVersionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ServerVersionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _FirmwareVersionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _HardwareVersionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _HardwareTypeTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _DeviceGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DeviceInfoTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ShowVersionsButton As System.Windows.Forms.Button
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _ListView As System.Windows.Forms.ListView
    Private WithEvents _DeviceIdNumeric As NumericUpDown
    Private WithEvents _DeviceIdNumericLabel As Label
End Class
