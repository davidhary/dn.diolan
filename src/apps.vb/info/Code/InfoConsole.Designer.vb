<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InfoConsole

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If components IsNot Nothing Then components.Dispose() : components = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(InfoConsole))
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._DeviceIdNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ShowDeviceInformationButton = New System.Windows.Forms.Button()
        Me._ListView = New System.Windows.Forms.ListView()
        Me._DeviceGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceIdNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceInfoTextBox = New System.Windows.Forms.TextBox()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._DeviceGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_DeviceIdNumeric
        '
        Me._DeviceIdNumeric.Location = New System.Drawing.Point(28, 21)
        Me._DeviceIdNumeric.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._DeviceIdNumeric.Name = "_DeviceIdNumeric"
        Me._DeviceIdNumeric.Size = New System.Drawing.Size(42, 20)
        Me._DeviceIdNumeric.TabIndex = 11
        Me._ToolTip.SetToolTip(Me._DeviceIdNumeric, "Select device id")
        '
        '_ShowDeviceInformationButton
        '
        Me._ShowDeviceInformationButton.Location = New System.Drawing.Point(78, 18)
        Me._ShowDeviceInformationButton.Name = "_ShowDeviceInformationButton"
        Me._ShowDeviceInformationButton.Size = New System.Drawing.Size(60, 23)
        Me._ShowDeviceInformationButton.TabIndex = 5
        Me._ShowDeviceInformationButton.Text = "Show"
        Me._ToolTip.SetToolTip(Me._ShowDeviceInformationButton, "Opens the device nad displays its information")
        Me._ShowDeviceInformationButton.UseVisualStyleBackColor = True
        '
        '_ListView
        '
        Me._ListView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ListView.HideSelection = False
        Me._ListView.Location = New System.Drawing.Point(0, 50)
        Me._ListView.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me._ListView.Name = "_ListView"
        Me._ListView.Size = New System.Drawing.Size(354, 139)
        Me._ListView.TabIndex = 23
        Me._ListView.UseCompatibleStateImageBehavior = False
        '
        '_DeviceGroupBox
        '
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumeric)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumericLabel)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceInfoTextBox)
        Me._DeviceGroupBox.Controls.Add(Me._ShowDeviceInformationButton)
        Me._DeviceGroupBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._DeviceGroupBox.Location = New System.Drawing.Point(0, 0)
        Me._DeviceGroupBox.Name = "_DeviceGroupBox"
        Me._DeviceGroupBox.Size = New System.Drawing.Size(354, 50)
        Me._DeviceGroupBox.TabIndex = 36
        Me._DeviceGroupBox.TabStop = False
        Me._DeviceGroupBox.Text = "Device"
        '
        '_DeviceIdNumericLabel
        '
        Me._DeviceIdNumericLabel.AutoSize = True
        Me._DeviceIdNumericLabel.Location = New System.Drawing.Point(5, 24)
        Me._DeviceIdNumericLabel.Name = "_DeviceIdNumericLabel"
        Me._DeviceIdNumericLabel.Size = New System.Drawing.Size(21, 13)
        Me._DeviceIdNumericLabel.TabIndex = 10
        Me._DeviceIdNumericLabel.Text = "ID:"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Location = New System.Drawing.Point(140, 19)
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(117, 20)
        Me._DeviceInfoTextBox.TabIndex = 7
        '
        'InfoConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(354, 189)
        Me.Controls.Add(Me._ListView)
        Me.Controls.Add(Me._DeviceGroupBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "InfoConsole"
        Me.Text = "Diolan Device Information"
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._DeviceGroupBox.ResumeLayout(False)
        Me._DeviceGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _ListView As System.Windows.Forms.ListView
    Private WithEvents _DeviceIdNumeric As NumericUpDown
    Private WithEvents _DeviceIdNumericLabel As Label
    Private WithEvents _ShowDeviceInformationButton As Button
    Private WithEvents _DeviceInfoTextBox As TextBox
    Private WithEvents _DeviceGroupBox As GroupBox
End Class
