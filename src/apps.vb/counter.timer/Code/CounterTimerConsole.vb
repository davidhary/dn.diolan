Imports isr.Diolan.SubsystemExtensions
Imports isr.Diolan.WinControls.SubsystemExtensions
Imports isr.Diolan.WinControls.ErrorProviderExtensions

''' <summary> Counter Timer Console. </summary>
''' <remarks>
''' Pulse counter interface allows to count with pulses and set timers.<para>
''' Start using Pulse counter module by enabling the Pulse counter port and set the pulse counter
''' module mode.</para><para>
''' There are 3 available modes: free run mode, time based mode and pulse based mode.
''' </para><para>
''' In "Free Run Mode" pulses are counted continuously. You can suspend, resume or reset the
''' counter and get the number of pulses at any time.</para><para>
''' In "Time Based Mode" pulses are counted during the user-defined time period. When the
''' predefined time period (limit) is exceeded, the counting starts again from 0. The pulse
''' counter can send a match event to PC if activated. The event contains the number of pulses
''' detected during this period. </para><para>
''' In "Pulse Based Mode" Pulses are counted until the number of pulses reaches the user-defined
''' value (limit). Then the counting starts again from 0. The counter can send an event to PC if
''' activated. The event contains time elapsed from the moment you started the counter.
''' </para><para>
''' At any moment pulse counter or timer value can be read.  </para><para>
''' You can also enable the events to get alert when condition is met. One of the few conditions
''' can be selected: pulse counter/timer overflow, when it matches selected value or repeat event
''' for selected period.</para> (c) 2015 Integrated Scientific Resources, Inc. All rights
''' reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-29 </para>
''' </remarks>
Partial Public Class CounterTimerConsole

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnSubsystemTypeClosed()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " SUBSYSTEM TYPE "

    ''' <summary> the subsystem type. </summary>
    ''' <value> the subsystem type. </value>
    Public Property SubsystemType As SubsystemTypes = SubsystemTypes.PulseCounter

    ''' <summary> Executes the subsystem type closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnSubsystemTypeClosed()
        Me._PortComboBox.DataSource = Nothing
        Me._PortComboBox.Items.Clear()
        Me._Device = Nothing
    End Sub

    ''' <summary> Executes the subsystem type closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnSubsystemTypeClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            If Me.IsDeviceSubsystemTypeOpen Then
                ' un-register event handlers for all ports
                For Each port As Dln.PulseCounter.Port In Me._Device.PulseCounter.Ports
                    RemoveHandler port.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                Next
            End If
        End If
    End Sub

    ''' <summary> Executes the subsystem type opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnSubsystemTypeOpening()

        Me._Device = Me._DeviceConnector.Device
        Me._PortComboBox.DataSource = Nothing
        Me._PortComboBox.Items.Clear()

        ' Get port count
        If Me.Device.PulseCounter.Ports.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._OpenDeviceSubsystemTypeButton,
                                         "Adapter '{0}' doesn't support Counter/Timer interface.",
                                         Me.Device.Caption)
            Me._Device = Nothing
            Me._DeviceInfoTextBox.Text = "not supported"
        Else
            Me._DeviceInfoTextBox.Text = Me.Device.Caption
            Me.Device.PulseCounter.Ports.ListNumbers(Me._PortComboBox, True)

            ' Set current context to run thread safe events in main form thread
            Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current

            ' Register event handlers for all ports
            For Each port As Dln.PulseCounter.Port In Me._Device.PulseCounter.Ports
                AddHandler port.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
            Next

            ' Activate port 0
            Me._PortComboBox.SelectedIndex = 0

        End If

    End Sub

    ''' <summary> Queries if a Subsystem Type is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a Subsystem Type is open; otherwise <c>false</c> </returns>
    Private Function IsSubsystemTypeOpen() As Boolean
        Return Me._PortComboBox.DataSource IsNot Nothing AndAlso Me._PortComboBox.Items.Count > 0
    End Function

#End Region

#Region " EVENT LOG "

    ''' <summary> Appends an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Condition met event information. </param>
    Private Sub AppendEvent(ByVal e As Dln.PulseCounter.ConditionMetEventArgs)
        If e IsNot Nothing Then
            Dim data As String = $"{DateTimeOffset.Now:hh:mm:ss.fff} Port{e.Port:D2} {e.EventType} Time={e.TimerValue} Count={e.CounterValue}{Environment.NewLine}"
            'This event is handled in main thread,
            'so it is not needed to invoke when modifying form's controls.
            Me._EventLogTextBox.AppendText(data)
        End If
    End Sub


#End Region

#Region " SUBSYSTEM "

    ''' <summary> Handler, called when the condition met event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Condition met event information. </param>
    Private Sub ConditionMetEventHandler(ByVal sender As Object, ByVal e As Dln.PulseCounter.ConditionMetEventArgs)
        Me.AppendEvent(e)
    End Sub

#End Region

#Region " PORT "

    ''' <summary> Select port. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pulseCounterPort"> The pulse counter port. </param>
    Private Sub SelectPort(ByVal pulseCounterPort As Dln.PulseCounter.Port)

        ' Get supported modes
        Me._CounterTimerModeComboBox.DataSource = pulseCounterPort.SupportedModes

        ' Get configuration
        Me._PortEnabledCheckBox.Checked = pulseCounterPort.Enabled
        Me._ResolutionTextBox.Text = pulseCounterPort.Resolution.ToString
        Me.GetValues(pulseCounterPort)
        Me.GetMode(pulseCounterPort)
        Me.GetEvent(pulseCounterPort)
    End Sub

    ''' <summary> Port combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PortComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PortComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.PulseCounter.Ports.Count Then
                    Dim pulseCounterPort As Dln.PulseCounter.Port = Me.Device.PulseCounter.Ports(portNumber)
                    Me.SelectPort(pulseCounterPort)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.PulseCounter.Ports.Count - 1)
                End If

            ElseIf Me.IsSubsystemTypeOpen AndAlso Me._ErrorProvider IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets the values. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pulseCounterPort"> The pulse counter port. </param>
    Private Sub GetValues(ByVal pulseCounterPort As Dln.PulseCounter.Port)

        Dim timerValue As UInteger
        Dim counterValue As UInteger
        pulseCounterPort.GetValue(timerValue, counterValue)

        Me._ElapsedTimeTextBox.Text = timerValue.ToString()
        Me._CountValueTextBox.Text = counterValue.ToString()
    End Sub

    ''' <summary> Gets counter timer values button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub GetCounterTimerValuesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetCounterTimerValuesButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.PulseCounter.Ports.Count Then
                    Dim pulseCounterPort As Dln.PulseCounter.Port = Me.Device.PulseCounter.Ports(portNumber)
                    Me.GetValues(pulseCounterPort)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.PulseCounter.Ports.Count - 1)

                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Resets the counter timer button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResetCounterTimerButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ResetCounterTimerButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.PulseCounter.Ports.Count Then
                    Dim pulseCounterPort As Dln.PulseCounter.Port = Me.Device.PulseCounter.Ports(portNumber)
                    Dim resetTimer As Boolean = Me._ResetTimerCheckBox.Checked
                    Dim resetCounter As Boolean = Me._ResetCounterCheckBox.Checked

                    pulseCounterPort.Reset(resetTimer, resetCounter)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.PulseCounter.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Sets a mode. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pulseCounterPort"> The pulse counter port. </param>
    Private Sub SetMode(ByVal pulseCounterPort As Dln.PulseCounter.Port)
        Dim mode As Dln.PulseCounter.Mode = CType(Me._CounterTimerModeComboBox.SelectedItem, Dln.PulseCounter.Mode)
        Dim limit As UInteger = CUInt(Me._ModeLimitNumeric.Value)
        pulseCounterPort.SetMode(mode, limit)
    End Sub

    ''' <summary> Sets mode button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SetModeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetModeButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.PulseCounter.Ports.Count Then
                    Dim pulseCounterPort As Dln.PulseCounter.Port = Me.Device.PulseCounter.Ports(portNumber)
                    Me.SetMode(pulseCounterPort)

                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.PulseCounter.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets a mode. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pulseCounterPort"> The pulse counter port. </param>
    Private Sub GetMode(ByVal pulseCounterPort As Dln.PulseCounter.Port)
        Me._CounterTimerModeComboBox.SelectedItem = pulseCounterPort.Mode
        Me._ModeLimitNumeric.Value = pulseCounterPort.Limit
    End Sub

    ''' <summary> Gets mode button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub GetModeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetModeButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.PulseCounter.Ports.Count Then
                    Dim pulseCounterPort As Dln.PulseCounter.Port = Me.Device.PulseCounter.Ports(portNumber)

                    Me.GetMode(pulseCounterPort)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.PulseCounter.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Sets an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pulseCounterPort"> The pulse counter port. </param>
    Private Sub SetEvent(ByVal pulseCounterPort As Dln.PulseCounter.Port)
        Dim eventType As Dln.PulseCounter.EventType = Dln.PulseCounter.EventType.None
        If Me._OverflowEventTypeCheckBox.Checked Then
            eventType = eventType Or Dln.PulseCounter.EventType.Overflow
        End If
        If Me._MatchEventTypeCheckBox.Checked Then
            eventType = eventType Or Dln.PulseCounter.EventType.Match
        End If
        If Me._RepeatEventTypeCheckBox.Checked Then
            eventType = eventType Or Dln.PulseCounter.EventType.Repeat
        End If
        Dim repeatInterval As UInteger = Convert.ToUInt32(Me._EventRepeatIntervalNumeric.Value)
        pulseCounterPort.SetEventConfig(eventType, repeatInterval)
    End Sub

    ''' <summary> Sets event configuration button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SetEventConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetEventConfigButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.PulseCounter.Ports.Count Then

                    Dim pulseCounterPort As Dln.PulseCounter.Port = Me.Device.PulseCounter.Ports(portNumber)
                    Me.SetEvent(pulseCounterPort)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.PulseCounter.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pulseCounterPort"> The pulse counter port. </param>
    Private Sub GetEvent(ByVal pulseCounterPort As Dln.PulseCounter.Port)
        Dim eventType As Dln.PulseCounter.EventType = pulseCounterPort.EventType
        Me._OverflowEventTypeCheckBox.Checked = eventType.HasFlag(Dln.PulseCounter.EventType.Overflow)
        Me._MatchEventTypeCheckBox.Checked = eventType.HasFlag(Dln.PulseCounter.EventType.Match)
        Me._RepeatEventTypeCheckBox.Checked = eventType.HasFlag(Dln.PulseCounter.EventType.Repeat)
        Me._EventRepeatIntervalNumeric.Value = pulseCounterPort.RepeatInterval
    End Sub

    ''' <summary> Gets event configuration button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub GetEventConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetEventConfigButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex

                If portNumber >= 0 AndAlso portNumber < Me.Device.PulseCounter.Ports.Count Then
                    Dim pulseCounterPort As Dln.PulseCounter.Port = Me.Device.PulseCounter.Ports(portNumber)
                    Me.GetEvent(pulseCounterPort)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.PulseCounter.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Port enabled check box checked changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PortEnabledCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PortEnabledCheckBox.CheckedChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceSubsystemTypeOpen Then

                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.PulseCounter.Ports.Count Then
                    Dim pulseCounterPort As Dln.PulseCounter.Port = Me.Device.PulseCounter.Ports(portNumber)
                    pulseCounterPort.Enabled = Me._PortEnabledCheckBox.Checked
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.PulseCounter.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.SubsystemType)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region

End Class
